<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInnerBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inner_banner', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->collation('utf8_general_ci');
            $table->string('fkIntImgId',255)->collation('utf8_general_ci')->nullable()->default(NULL);
            $table->unsignedInteger('fkIntPageId')->collation('utf8_general_ci')->nullable();
            $table->unsignedInteger('fkModuleId')->collation('utf8_general_ci')->nullable();
            $table->string('varTitle',255)->collation('utf8_general_ci')->nullable();
            $table->string('varSubTitle',255)->collation('utf8_general_ci')->nullable()->default(null);
            $table->integer('intDisplayOrder')->collation('utf8_general_ci')->default(0);
            $table->char('chrPublish',1)->default('Y')->collation('utf8_general_ci');
            $table->char('chrDelete',1)->default('N')->collation('utf8_general_ci');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(NULL)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inner_banner');
    }
}
