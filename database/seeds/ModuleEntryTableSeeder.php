<?php
use Illuminate\Database\Seeder;
use Carbon\Carbon;
class ModuleEntryTableSeeder extends Seeder
{
	public function run()
	{
		
		DB::table('module')->where([
			'varTitle' => 'Client Category',
			'varModuleName' =>  'client-category',
			'varTableName' => 'client_category',
			'varModelName' => 'ClientCategory',
			'varModuleClass' => 'ClientCategoryController'
		])->delete();

		DB::table('module')->insert([
			'varTitle' => 'Client Category',
			'varModuleName' =>  'client-category',
			'varTableName' => 'client_category',
			'varModelName' => 'ClientCategory',
			'varModuleClass' => 'ClientCategoryController',
			'intDisplayOrder' => 0,
			'chrIsFront' => 'Y',
			'chrIsPowerpanel' => 'Y',
			'decVersion' => 1.0,
			'chrPublish' => 'Y',
			'chrDelete' => 'N',
			'varPermissions'=> 'list, create, edit, delete, publish',
			'created_at'=> Carbon::now(),
			'updated_at'=> Carbon::now()
		]);	


	}
}