<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModuleTableSeeder extends Seeder
{
		public function run()
		{

				DB::table('module')->insert([
					'varTitle' => 'Front Home',
					'varModuleName' =>  'home',
					'varTableName' => '',
					'varModelName' => '',
					'varModuleClass' => 'HomeController',
					'intDisplayOrder' => 1,
					'chrIsFront' => 'N',
					'chrIsPowerpanel' => 'N',
					'chrIsGenerated' => 'N',
 					'decVersion' => 1.0,
					'chrPublish' => 'Y',
					'chrDelete' => 'N',
					'varPermissions'=>'',
					'created_at'=> Carbon::now(),
					'updated_at'=> Carbon::now()
				]);
				
									
					
					DB::table('module')->insert([
						'varTitle' => 'Menu',
						'varModuleName' =>  'menu',
						'varTableName' => 'menu',
						'varModelName' => 'Menu',
						'varModuleClass' => 'MenuController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 3.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Pages',
						'varModuleName' =>  'pages',
						'varTableName' => 'cms_page',
						'varModelName' => 'CmsPage',
						'varModuleClass' => 'CmsPagesController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'Y',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Staticblocks',
						'varModuleName' =>  'static-block',
						'varTableName' => 'static_block',
						'varModelName' => 'StaticBlocks',
						'varModuleClass' => 'StaticBlocksController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Contact Info',
						'varModuleName' =>  'contact-info',
						'varTableName' => 'contact_info',
						'varModelName' => 'ContactInfo',
						'varModuleClass' => 'ContactInfoController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Sitemap',
						'varModuleName' =>  'sitemap',
						'varTableName' => '',
						'varModelName' => '',
						'varModuleClass' => '',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'menu-type',
						'varModuleName' =>  'menu-type',
						'varTableName' => 'menu_type',
						'varModelName' => 'MenuType',
						'varModuleClass' => '',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Email Log',
						'varModuleName' =>  'email-log',
						'varTableName' => 'email_log',
						'varModelName' => 'EmailLog',
						'varModuleClass' => 'EmailLogController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list,delete',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'log',
						'varModuleName' =>  'log',
						'varTableName' => 'log',
						'varModelName' => 'Log',
						'varModuleClass' => 'LogController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, delete, advanced',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Inner Banner',
						'varModuleName' =>  'inner-banner',
						'varTableName' => 'inner_banner',
						'varModelName' => 'InnerBanner',
						'varModuleClass' => 'InnerBannerController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Privacy Removal Leads',
						'varModuleName' =>  'privacy-removal-leads',
						'varTableName' => 'privacy_removal_leads',
						'varModelName' => 'PrivacyRemovalLead',
						'varModuleClass' => 'PrivacyRemovalLeadsController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, delete',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Banners',
						'varModuleName' =>  'banners',
						'varTableName' => 'banner',
						'varModelName' => 'Banner',
						'varModuleClass' => 'BannerController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Contact Us',
						'varModuleName' =>  'contact-us',
						'varTableName' => 'contact_lead',
						'varModelName' => 'ContactLead',
						'varModuleClass' => 'ContactleadController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'Y',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 2.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, delete',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'FAQ',
						'varModuleName' =>  'faq',
						'varTableName' => 'faq',
						'varModelName' => 'Faq',
						'varModuleClass' => 'FaqController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'Y',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 2.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Services',
						'varModuleName' =>  'services',
						'varTableName' => 'services',
						'varModelName' => 'Services',
						'varModuleClass' => 'ServicesController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'Y',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Testimonials',
						'varModuleName' =>  'testimonial',
						'varTableName' => 'testimonials',
						'varModelName' => 'Testimonial',
						'varModuleClass' => 'TestimonialController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'Y',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Services Category',
						'varModuleName' =>  'service-category',
						'varTableName' => 'service_category',
						'varModelName' => 'ServiceCategory',
						'varModuleClass' => 'ServiceCategoryController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'Y',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Client',
						'varModuleName' =>  'client',
						'varTableName' => 'client',
						'varModelName' => 'Client',
						'varModuleClass' => 'ClientController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'Y',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
						
					
					DB::table('module')->insert([
						'varTitle' => 'Client Category',
						'varModuleName' =>  'client-category',
						'varTableName' => 'client_category',
						'varModelName' => 'ClientCategory',
						'varModuleClass' => 'ClientCategoryController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'Y',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=> 'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
					
								
					DB::table('module')->insert([
						'varTitle' => 'Users',
						'varModuleName' =>  'users',
						'varTableName' => 'users',
						'varModelName' => 'User',
						'varModuleClass' => 'UserController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=>'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);

					DB::table('module')->insert([
						'varTitle' => 'Role',
						'varModuleName' =>  'roles',
						'varTableName' => 'roles',
						'varModelName' => 'Role',
						'varModuleClass' => 'RoleController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=>'list, create, edit, delete, publish',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);

					DB::table('module')->insert([
						'varTitle' => 'Profile',
						'varModuleName' =>  'changeprofile',
						'varTableName' => 'users',
						'varModelName' => 'User',
						'varModuleClass' => 'ProfileController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=>'edit, change-password',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);

					DB::table('module')->insert([
						'varTitle' => 'General Setting',
						'varModuleName' =>  'settings',
						'varTableName' => 'general_setting',
						'varModelName' => 'GeneralSettings',
						'varModuleClass' => 'SettingsController',
						'intDisplayOrder' => 0,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=>'general-setting-management, smtp-mail-setting, seo-setting, social-setting, social-media-share-setting, other-setting, maintenance-setting, recent-activities, module-setting',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);

					DB::table('module')->insert([
						'varTitle' => 'Login history',
						'varModuleName' =>  'login-history',
						'varTableName' => 'login_history',
						'varModelName' => 'Login History',
						'varModuleClass' => 'LoginHistoryController',
						'intDisplayOrder' => 61,
						'chrIsFront' => 'N',
						'chrIsPowerpanel' => 'Y',
						'chrIsGenerated' => 'N',
						'decVersion' => 1.0,
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'varPermissions'=>'list, delete',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
				
				
		}
}
