<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;

class MenuTableSeeder extends Seeder
{
		public function run()
		{
			$moduleCode = DB::table('module')->select('id')->where('varModuleName','menu')->first();

							   					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Home')->first();		
					
											DB::table('menu')->insert([
							'id'=>1,
							'intParentMenuId' => 0,
							'intItemOrder' => 0,
							'intParentItemOrder' => 1,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Home'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'Home',
							'txtPageUrl' => '/', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','About Us')->first();		
					
											DB::table('menu')->insert([
							'id'=>1837,
							'intParentMenuId' => 0,
							'intItemOrder' => 2,
							'intParentItemOrder' => 0,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('About Us'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'About Us',
							'txtPageUrl' => 'about-us', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Services')->first();		
					
											DB::table('menu')->insert([
							'id'=>3,
							'intParentMenuId' => 0,
							'intItemOrder' => 0,
							'intParentItemOrder' => 3,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Services'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'Services',
							'txtPageUrl' => 'services', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Services Detail')->first();		
					
											DB::table('menu')->insert([
							'id'=>1452,
							'intParentMenuId' => 3,
							'intItemOrder' => 1,
							'intParentItemOrder' => 0,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Services Detail'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'Services Detail',
							'txtPageUrl' => 'services-detail', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Service Category')->first();		
					
											DB::table('menu')->insert([
							'id'=>1627,
							'intParentMenuId' => 3,
							'intItemOrder' => 2,
							'intParentItemOrder' => 0,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Service Category'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'Service Category',
							'txtPageUrl' => 'service-category', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Testimonial')->first();		
					
											DB::table('menu')->insert([
							'id'=>6,
							'intParentMenuId' => 0,
							'intItemOrder' => 0,
							'intParentItemOrder' => 5,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Testimonial'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'Testimonial',
							'txtPageUrl' => 'testimonial', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Contact Us')->first();		
					
											DB::table('menu')->insert([
							'id'=>7,
							'intParentMenuId' => 0,
							'intItemOrder' => 0,
							'intParentItemOrder' => 6,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Contact Us'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'Contact Us',
							'txtPageUrl' => 'contact-us', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Pages')->first();		
					
											DB::table('menu')->insert([
							'id'=>9,
							'intParentMenuId' => 0,
							'intItemOrder' => 0,
							'intParentItemOrder' => 7,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Pages'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'Pages',
							'txtPageUrl' => '#', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Faq')->first();		
					
											DB::table('menu')->insert([
							'id'=>1297,
							'intParentMenuId' => 9,
							'intItemOrder' => 1,
							'intParentItemOrder' => 0,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Faq'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'Faq',
							'txtPageUrl' => 'faq', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Privacy Policy')->first();		
					
					
											DB::table('menu')->insert([
						'id'=>2648,
						'intParentMenuId' => 9,
						'intItemOrder' => 2,
						'intParentItemOrder' => 0,						
						'intPosition' => 2,						
						'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Privacy Policy')[0],$moduleCode->id),
						'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
						'varTitle' => 'Privacy Policy',
						'txtPageUrl' => 'privacy-policy', 
						'chrActive' => 'Y',
						'chrMegaMenu' => 'N',
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Terms  &amp; Conditions')->first();		
					
					
											DB::table('menu')->insert([
						'id'=>2796,
						'intParentMenuId' => 9,
						'intItemOrder' => 3,
						'intParentItemOrder' => 0,						
						'intPosition' => 2,						
						'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Terms  &amp; Conditions')[0],$moduleCode->id),
						'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
						'varTitle' => 'Terms  &amp; Conditions',
						'txtPageUrl' => 'terms-conditions', 
						'chrActive' => 'Y',
						'chrMegaMenu' => 'N',
						'chrPublish' => 'Y',
						'chrDelete' => 'N',
						'created_at'=> Carbon::now(),
						'updated_at'=> Carbon::now()
					]);
						
					
				 					$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Thank You')->first();		
					
											DB::table('menu')->insert([
							'id'=>1614,
							'intParentMenuId' => 9,
							'intItemOrder' => 4,
							'intParentItemOrder' => 0,
							'intPosition' => 1,						
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Thank You'),$moduleCode->id),
							'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
							'varTitle' => 'Thank You',
							'txtPageUrl' => 'thank-you', 
							'chrActive' => 'Y',
							'chrMegaMenu' => 'N',
							'chrPublish' => 'Y',
							'chrDelete' => 'N',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now()
						]);
					
						
					
								

			$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Privacy Policy')->first();		
			DB::table('menu')->insert([
					'intParentMenuId' => 0,
					'intItemOrder' => 1,
					'intParentItemOrder' => 1,
					'intPosition' => 2,
					'intAliasId' => MyLibrary::insertAlias(slug::create_slug('Privacy Policy')[0],$moduleCode->id),
					'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
					'varTitle' => 'Privacy Policy',
					'txtPageUrl' => 'privacy-policy', 
					'chrActive' => 'Y',
					'chrMegaMenu' => 'N',
					'chrPublish' => 'Y',
					'chrDelete' => 'N',
					'created_at'=> Carbon::now(),
					'updated_at'=> Carbon::now()
				]);

				$pageObj = DB::table('cms_page')->select('id')->where('varTitle','Sitemap')->first();		
				DB::table('menu')->insert([
					'intParentMenuId' => 0,
					'intItemOrder' => 1,
					'intParentItemOrder' => 1,
					'intPosition' => 2,
					'intAliasId' => null,
					'intPageId' => (isset($pageObj->id)?$pageObj->id:null),
					'varTitle' => 'Sitemap',
					'txtPageUrl' => 'sitemap', 
					'chrActive' => 'Y',
					'chrMegaMenu' => 'N',
					'chrPublish' => 'Y',
					'chrDelete' => 'N',
					'created_at'=> Carbon::now(),
					'updated_at'=> Carbon::now()
				]);
		}
}
