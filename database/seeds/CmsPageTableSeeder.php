<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;

class CmsPageTableSeeder extends Seeder
{
		public function run()
		{
			$moduleCode = DB::table('module')->select('id')->where('varModuleName','pages')->first();		
							
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Home')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%home')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Home'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Home'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Home')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Home',
							'varMetaKeyword' => 'Home',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Blogs')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%blogs')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Blogs'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Blogs'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Blogs')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Blogs',
							'varMetaKeyword' => 'Blogs',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Contact Us')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%contact-us')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Contact Us'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Contact Us'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Contact Us')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Contact Us',
							'varMetaKeyword' => 'Contact Us',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','FAQ')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%faq')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('FAQ'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('FAQ'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('FAQ')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'FAQ',
							'varMetaKeyword' => 'FAQ',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Services')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%services')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Services'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Services'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Services')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Services',
							'varMetaKeyword' => 'Services',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Testimonials')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%testimonial')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Testimonials'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Testimonials'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Testimonials')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Testimonials',
							'varMetaKeyword' => 'Testimonials',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Team')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%team')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Team'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Team'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Team')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Team',
							'varMetaKeyword' => 'Team',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Pages')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%pages')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Pages'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Pages'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Pages')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Pages',
							'varMetaKeyword' => 'Pages',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','News')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%news')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('News'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('News'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('News')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'News',
							'varMetaKeyword' => 'News',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Photo Album')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%photo-album')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Photo Album'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Photo Album'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Photo Album')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Photo Album',
							'varMetaKeyword' => 'Photo Album',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','News Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%news-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('News Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('News Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('News Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'News Category',
							'varMetaKeyword' => 'News Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Video Album')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%video-album')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Video Album'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Video Album'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Video Album')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Video Album',
							'varMetaKeyword' => 'Video Album',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Services Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%service-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Services Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Services Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Services Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Services Category',
							'varMetaKeyword' => 'Services Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Sponsors')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%sponsor')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Sponsors'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Sponsors'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Sponsors')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Sponsors',
							'varMetaKeyword' => 'Sponsors',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Blog Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%blog-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Blog Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Blog Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Blog Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Blog Category',
							'varMetaKeyword' => 'Blog Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Events')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%events')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Events'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Events'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Events')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Events',
							'varMetaKeyword' => 'Events',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Shows Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%show-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Shows Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Shows Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Shows Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Shows Category',
							'varMetaKeyword' => 'Shows Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Product Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%product-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Product Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Product Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Product Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Product Category',
							'varMetaKeyword' => 'Product Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Products')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%products')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Products'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Products'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Products')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Products',
							'varMetaKeyword' => 'Products',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Shows')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%shows')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Shows'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Shows'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Shows')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Shows',
							'varMetaKeyword' => 'Shows',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Careers')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%careers')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Careers'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Careers'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Careers')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Careers',
							'varMetaKeyword' => 'Careers',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Careers Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%careers-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Careers Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Careers Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Careers Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Careers Category',
							'varMetaKeyword' => 'Careers Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Sponsor Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%sponsor-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Sponsor Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Sponsor Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Sponsor Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Sponsor Category',
							'varMetaKeyword' => 'Sponsor Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Event Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%event-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Event Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Event Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Event Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Event Category',
							'varMetaKeyword' => 'Event Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Client')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%client')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Client'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Client'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Client')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Client',
							'varMetaKeyword' => 'Client',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Restaurant Menu Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%restaurant-menu-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Restaurant Menu Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Restaurant Menu Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Restaurant Menu Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Restaurant Menu Category',
							'varMetaKeyword' => 'Restaurant Menu Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Restaurant Reservations')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%restaurant-reservations')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Restaurant Reservations'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Restaurant Reservations'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Restaurant Reservations')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Restaurant Reservations',
							'varMetaKeyword' => 'Restaurant Reservations',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Restaurant Menu')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%restaurant-menu')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Restaurant Menu'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Restaurant Menu'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Restaurant Menu')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Restaurant Menu',
							'varMetaKeyword' => 'Restaurant Menu',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Book Appointment')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%appointment-lead')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Book Appointment'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Book Appointment'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Book Appointment')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Book Appointment',
							'varMetaKeyword' => 'Book Appointment',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Gallery')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%gallery')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Gallery'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Gallery'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Gallery')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Gallery',
							'varMetaKeyword' => 'Gallery',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Projects')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%projects')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Projects'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Projects'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Projects')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Projects',
							'varMetaKeyword' => 'Projects',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Project Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%project-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Project Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Project Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Project Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Project Category',
							'varMetaKeyword' => 'Project Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Client Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%client-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Client Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Client Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Client Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Client Category',
							'varMetaKeyword' => 'Client Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Testimonial')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%testimonial')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Testimonial'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Testimonial'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Testimonial')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Testimonial',
							'varMetaKeyword' => 'Testimonial',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Services')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%services')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Services'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Services'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Services')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Services',
							'varMetaKeyword' => 'Services',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Faq')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%faq')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Faq'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Faq'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Faq')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Faq',
							'varMetaKeyword' => 'Faq',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Client')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%client')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Client'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Client'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Client')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Client',
							'varMetaKeyword' => 'Client',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Service Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%service-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Service Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Service Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Service Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Service Category',
							'varMetaKeyword' => 'Service Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Client Category')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%client-category')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Client Category'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Client Category'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Client Category')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Client Category',
							'varMetaKeyword' => 'Client Category',
							'varMetaDescription' => '',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','About Us')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%pages')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('About Us'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('About Us'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('About Us')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '&lt;section class=&quot;page_section&quot;&gt; &lt;div class=&quot;container&quot;&gt; &lt;div class=&quot;row&quot;&gt; &lt;div class=&quot;col-12&quot;&gt; &lt;div class=&quot;cms&quot;&gt; &lt;h2&gt;The standard Lorem Ipsum passage, used since the 1500s&lt;/h2&gt; &lt;p&gt;&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;&lt;/p&gt; &lt;figure class=&quot;image&quot;&gt; &lt;img src=&quot;https://storage.googleapis.com/gweb-uniblog-publish-prod/images/P1133656_yq72Ist.max-2000x2000.jpg&quot;&gt; &lt;figcaption&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit&lt;/figcaption&gt; &lt;/figure&gt; &lt;blockquote&gt; &lt;p&gt;If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation, any sum is appreciated - click here to donate using PayPal. Thank you for your support.&lt;/p&gt; &lt;/blockquote&gt; &lt;hr&gt; &lt;h2&gt;Section 1.10.32 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC&lt;/h2&gt; &lt;p&gt;&quot;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&quot;&lt;/p&gt; &lt;figure class=&quot;image image_resized image-style-align-left&quot; style=&quot;width:500px;&quot;&gt; &lt;img src=&quot;https://storage.googleapis.com/gweb-uniblog-publish-prod/images/P1133449_42IOn9Y.max-2000x2000.jpg&quot;&gt; &lt;figcaption&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit&lt;/figcaption&gt; &lt;/figure&gt; &lt;p&gt;The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.&lt;/p&gt; &lt;ul&gt; &lt;li&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.&lt;/li&gt; &lt;li&gt;It is a long established fact that a reader will be distracted by the readable.&lt;/li&gt; &lt;li&gt;There are many variations of passages of Lorem Ipsum available&lt;/li&gt; &lt;li&gt;The standard Lorem Ipsum passage, used since the 1500s&lt;/li&gt; &lt;li&gt;Section 1.10.32 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC&lt;/li&gt; &lt;/ul&gt; &lt;blockquote&gt; &lt;p&gt;If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation, any sum is appreciated - click here to donate using PayPal. Thank you for your support.&lt;/p&gt; &lt;/blockquote&gt; &lt;p&gt;&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.&lt;/p&gt; &lt;div class=&quot;page-break&quot; style=&quot;page-break-after:always;&quot;&gt;&lt;/div&gt; &lt;h2&gt;1914 translation by H. Rackham&lt;/h2&gt; &lt;p&gt;&quot;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.&quot;&lt;/p&gt; &lt;figure class=&quot;image image_resized image-style-align-right&quot; style=&quot;width:500px;&quot;&gt; &lt;img src=&quot;https://storage.googleapis.com/gweb-uniblog-publish-prod/images/P1133656_yq72Ist.max-2000x2000.jpg&quot;&gt; &lt;figcaption&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit&lt;/figcaption&gt; &lt;/figure&gt; &lt;p&gt;&quot;At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.&lt;/p&gt; &lt;ul&gt; &lt;li&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.&lt;/li&gt; &lt;li&gt;It is a long established fact that a reader will be distracted by the readable.&lt;/li&gt; &lt;li&gt;There are many variations of passages of Lorem Ipsum available&lt;/li&gt; &lt;li&gt;The standard Lorem Ipsum passage, used since the 1500s&lt;/li&gt; &lt;li&gt;Section 1.10.32 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC&lt;/li&gt; &lt;/ul&gt; &lt;p&gt;Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.&quot;&lt;/p&gt; &lt;div class=&quot;page-break&quot; style=&quot;page-break-after:always;&quot;&gt;&lt;/div&gt; &lt;h2&gt;1914 translation by H. Rackham&lt;/h2&gt; &lt;figure class=&quot;media&quot;&gt; &lt;oembed url=&quot;https://www.youtube.com/watch?v=mqe67axdgY8&quot;&gt;&lt;/oembed&gt; &lt;iframe src=&quot;//www.youtube.com/embed/mqe67axdgY8&quot; allowfullscreen&gt;&lt;/iframe&gt; &lt;/figure&gt; &lt;p&gt;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.&lt;/p&gt; &lt;ul&gt; &lt;li&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.&lt;/li&gt; &lt;li&gt;It is a long established fact that a reader will be distracted by the readable.&lt;/li&gt; &lt;li&gt;There are many variations of passages of Lorem Ipsum available&lt;/li&gt; &lt;/ul&gt; &lt;figure class=&quot;table&quot;&gt; &lt;table&gt; &lt;thead&gt; &lt;tr&gt; &lt;th&gt;Heading&lt;/th&gt; &lt;th&gt;Heading&lt;/th&gt; &lt;th&gt;Heading&lt;/th&gt; &lt;th&gt;Heading&lt;/th&gt; &lt;th&gt;Heading&lt;/th&gt; &lt;th&gt;Heading&lt;/th&gt; &lt;/tr&gt; &lt;/thead&gt; &lt;tbody&gt; &lt;tr&gt; &lt;th&gt;Cell&lt;/th&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;th&gt;Cell&lt;/th&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;th&gt;Cell&lt;/th&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;th&gt;Cell&lt;/th&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;/tr&gt; &lt;tr&gt; &lt;th&gt;Cell&lt;/th&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;td&gt;Cell&lt;/td&gt; &lt;/tr&gt; &lt;/tbody&gt; &lt;/table&gt; &lt;/figure&gt; &lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/section&gt;',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'About Us',
							'varMetaKeyword' => 'About Us',
							'varMetaDescription' => '&amp;lt;section class=&amp;quot;page_section&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;container&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;row&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;col-12&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;cms&amp;quot;&amp;gt; &amp;lt;h2&amp;gt;The standard Lorem Ipsum passage, used since the 1500s&amp;lt;/h2&amp;gt; &amp;lt;p&amp;gt;&amp;quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&amp;quot;&amp;lt;/p&amp;gt; &amp;lt;figure class=&amp;quot;image&amp;quot;&amp;gt; &amp;lt;img src=&amp;quot;https://storage.googleapis.com/gweb-uniblog-publish-prod/images/P1133656_yq72Ist.max-2000x2000.jpg&amp;quot;&amp;gt; &amp;lt;figcaption&amp;gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit&amp;lt;/figcaption&amp;gt; &amp;lt;/figure&amp;gt; &amp;lt;blockquote&amp;gt; &amp;lt;p&amp;gt;If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation, any sum is appreciated - click here to donate using PayPal. Thank you for your support.&amp;lt;/p&amp;gt; &amp;lt;/blockquote&amp;gt; &amp;lt;hr&amp;gt; &amp;lt;h2&amp;gt;Section 1.10.32 of &amp;quot;de Finibus Bonorum et Malorum&amp;quot;, written by Cicero in 45 BC&amp;lt;/h2&amp;gt; &amp;lt;p&amp;gt;&amp;quot;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&amp;quot;&amp;lt;/p&amp;gt; &amp;lt;figure class=&amp;quot;image image_resized image-style-align-left&amp;quot; style=&amp;quot;width:500px;&amp;quot;&amp;gt; &amp;lt;img src=&amp;quot;https://storage.googleapis.com/gweb-uniblog-publish-prod/images/P1133449_42IOn9Y.max-2000x2000.jpg&amp;quot;&amp;gt; &amp;lt;figcaption&amp;gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit&amp;lt;/figcaption&amp;gt; &amp;lt;/figure&amp;gt; &amp;lt;p&amp;gt;The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &amp;quot;de Finibus Bonorum et Malorum&amp;quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.&amp;lt;/p&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;It is a long established fact that a reader will be distracted by the readable.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;There are many variations of passages of Lorem Ipsum available&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;The standard Lorem Ipsum passage, used since the 1500s&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;Section 1.10.32 of &amp;quot;de Finibus Bonorum et Malorum&amp;quot;, written by Cicero in 45 BC&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;blockquote&amp;gt; &amp;lt;p&amp;gt;If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation, any sum is appreciated - click here to donate using PayPal. Thank you for your support.&amp;lt;/p&amp;gt; &amp;lt;/blockquote&amp;gt; &amp;lt;p&amp;gt;&amp;quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.&amp;lt;/p&amp;gt; &amp;lt;div class=&amp;quot;page-break&amp;quot; style=&amp;quot;page-break-after:always;&amp;quot;&amp;gt;&amp;lt;/div&amp;gt; &amp;lt;h2&amp;gt;1914 translation by H. Rackham&amp;lt;/h2&amp;gt; &amp;lt;p&amp;gt;&amp;quot;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.&amp;quot;&amp;lt;/p&amp;gt; &amp;lt;figure class=&amp;quot;image image_resized image-style-align-right&amp;quot; style=&amp;quot;width:500px;&amp;quot;&amp;gt; &amp;lt;img src=&amp;quot;https://storage.googleapis.com/gweb-uniblog-publish-prod/images/P1133656_yq72Ist.max-2000x2000.jpg&amp;quot;&amp;gt; &amp;lt;figcaption&amp;gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit&amp;lt;/figcaption&amp;gt; &amp;lt;/figure&amp;gt; &amp;lt;p&amp;gt;&amp;quot;At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.&amp;lt;/p&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;It is a long established fact that a reader will be distracted by the readable.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;There are many variations of passages of Lorem Ipsum available&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;The standard Lorem Ipsum passage, used since the 1500s&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;Section 1.10.32 of &amp;quot;de Finibus Bonorum et Malorum&amp;quot;, written by Cicero in 45 BC&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;p&amp;gt;Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.&amp;quot;&amp;lt;/p&amp;gt; &amp;lt;div class=&amp;quot;page-break&amp;quot; style=&amp;quot;page-break-after:always;&amp;quot;&amp;gt;&amp;lt;/div&amp;gt; &amp;lt;h2&amp;gt;1914 translation by H. Rackham&amp;lt;/h2&amp;gt; &amp;lt;figure class=&amp;quot;media&amp;quot;&amp;gt; &amp;lt;oembed url=&amp;quot;https://www.youtube.com/watch?v=mqe67axdgY8&amp;quot;&amp;gt;&amp;lt;/oembed&amp;gt; &amp;lt;iframe src=&amp;quot;//www.youtube.com/embed/mqe67axdgY8&amp;quot; allowfullscreen&amp;gt;&amp;lt;/iframe&amp;gt; &amp;lt;/figure&amp;gt; &amp;lt;p&amp;gt;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &amp;quot;de Finibus Bonorum et Malorum&amp;quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &amp;quot;Lorem ipsum dolor sit amet..&amp;quot;, comes from a line in section 1.10.32.&amp;lt;/p&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;It is a long established fact that a reader will be distracted by the readable.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;There are many variations of passages of Lorem Ipsum available&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;figure class=&amp;quot;table&amp;quot;&amp;gt; &amp;lt;table&amp;gt; &amp;lt;thead&amp;gt; &amp;lt;tr&amp;gt; &amp;lt;th&amp;gt;Heading&amp;lt;/th&amp;gt; &amp;lt;th&amp;gt;Heading&amp;lt;/th&amp;gt; &amp;lt;th&amp;gt;Heading&amp;lt;/th&amp;gt; &amp;lt;th&amp;gt;Heading&amp;lt;/th&amp;gt; &amp;lt;th&amp;gt;Heading&amp;lt;/th&amp;gt; &amp;lt;th&amp;gt;Heading&amp;lt;/th&amp;gt; &amp;lt;/tr&amp;gt; &amp;lt;/thead&amp;gt; &amp;lt;tbody&amp;gt; &amp;lt;tr&amp;gt; &amp;lt;th&amp;gt;Cell&amp;lt;/th&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;/tr&amp;gt; &amp;lt;tr&amp;gt; &amp;lt;th&amp;gt;Cell&amp;lt;/th&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;/tr&amp;gt; &amp;lt;tr&amp;gt; &amp;lt;th&amp;gt;Cell&amp;lt;/th&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;/tr&amp;gt; &amp;lt;tr&amp;gt; &amp;lt;th&amp;gt;Cell&amp;lt;/th&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;/tr&amp;gt; &amp;lt;tr&amp;gt; &amp;lt;th&amp;gt;Cell&amp;lt;/th&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;td&amp;gt;Cell&amp;lt;/td&amp;gt; &amp;lt;/tr&amp;gt; &amp;lt;/tbody&amp;gt; &amp;lt;/table&amp;gt; &amp;lt;/figure&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/section&amp;gt;',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Privacy Policy')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%pages')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Privacy Policy'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Privacy Policy'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Privacy Policy')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '&lt;section class=&quot;page_section privacy_policy_01&quot;&gt; &lt;div class=&quot;container&quot;&gt; &lt;div class=&quot;row&quot;&gt; &lt;div class=&quot;col-12&quot;&gt; &lt;div class=&quot;cms&quot;&gt; &lt;h3&gt;&lt;strong&gt;[Conpany Name]&lt;/strong&gt;&lt;/h3&gt; &lt;p&gt;This Privacy Policy governs the manner in which &lt;strong&gt;[Conpany Name]&lt;/strong&gt; collects, uses, maintains information collected from users (each, a &quot;User&quot;) of the &lt;a href=&quot;#&quot; title=&quot;Title Here&quot;&gt;[www.domainame.com]&lt;/a&gt;. This privacy policy applies to the Site and all products and services offered by &lt;strong&gt;[Conpany Name]&lt;/strong&gt;&lt;/p&gt; &lt;h3&gt;Personal identification information&lt;/h3&gt; &lt;p&gt;We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, place an order, fill out a form, respond to a survey, subscribe to the newsletter and in connection with other activities, services, features or resources we make available on our Site.&lt;/p&gt; &lt;h3&gt;Non-personal identification information&lt;/h3&gt; &lt;p&gt;We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the computer OS and technical information about Users means of connection to our Site, such as the operating system and the Internet Protocol address of your Internet service provider.&lt;/p&gt; &lt;h3&gt;Web browser cookies&lt;/h3&gt; &lt;p&gt;Our Site may use &quot;cookies&quot; to enhance User experience. User&#039;s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.&lt;/p&gt; &lt;h3&gt;How we protect your information&lt;/h3&gt; &lt;p&gt;&lt;strong&gt;[Conpany Name]&lt;/strong&gt; collects and uses Users personal information for the following purposes:&lt;/p&gt; &lt;ul&gt; &lt;li&gt;&lt;strong&gt;To improve customer service&lt;/strong&gt; &lt;br&gt; Your information helps us to more effectively respond to your customer service requests and support needs.&lt;/li&gt; &lt;li&gt;&lt;strong&gt;To personalize user experience&lt;/strong&gt; &lt;br&gt; We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.&lt;/li&gt; &lt;li&gt;&lt;strong&gt;To improve our Site&lt;/strong&gt; &lt;br&gt; We continually strive to improve our website offerings based on the information and feedback we receive from you.&lt;/li&gt; &lt;li&gt;&lt;strong&gt;To administer a content, promotion, survey or other Site feature&lt;/strong&gt; &lt;br&gt; To send Users information they agreed to receive about topics we think will be of interest to them.&lt;/li&gt; &lt;li&gt;&lt;strong&gt;To share your information with third parties&lt;/strong&gt; &lt;br&gt; We may share or sell information with third parties for marketing or other purposes.&lt;/li&gt; &lt;li&gt;&lt;strong&gt;To send periodic emails&lt;/strong&gt; &lt;br&gt; The email address Users provide for order processing, will only be used to send them information and updates pertaining to their order. It may also be used to respond to their inquiries, and/or other requests or questions. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.&lt;/li&gt; &lt;/ul&gt; &lt;h3&gt;How we protect your information&lt;/h3&gt; &lt;p&gt;We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information and data stored on our Site.&lt;/p&gt; &lt;h3&gt;Sharing your personal information&lt;/h3&gt; &lt;p&gt;We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above. We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.&lt;/p&gt; &lt;h3&gt;Third party websites&lt;/h3&gt; &lt;p&gt;Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website&#039;s own terms and policies.&lt;/p&gt; &lt;h3&gt;Changes to this privacy policy&lt;/h3&gt; &lt;p&gt;&lt;strong&gt;[Conpany Name]&lt;/strong&gt; has the discretion to update this privacy policy at any time. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.&lt;/p&gt; &lt;h3&gt;Your acceptance of these terms&lt;/h3&gt; &lt;p&gt;By using this Site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.&lt;/p&gt; &lt;h3&gt;Contacting us&lt;/h3&gt; &lt;p&gt;If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please &lt;a href=&quot;#&quot; title=&quot;Contact Us&quot;&gt;Contact Us&lt;/a&gt;.&lt;/p&gt; &lt;p&gt;You can also request data removal via the form on our &lt;a href=&quot;data-removal&quot;&gt;Data Removal Request page&lt;/a&gt;&lt;/p&gt; &lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/section&gt;',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Privacy Policy',
							'varMetaKeyword' => 'Privacy Policy',
							'varMetaDescription' => '&amp;lt;section class=&amp;quot;page_section privacy_policy_01&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;container&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;row&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;col-12&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;cms&amp;quot;&amp;gt; &amp;lt;h3&amp;gt;&amp;lt;strong&amp;gt;[Conpany Name]&amp;lt;/strong&amp;gt;&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;This Privacy Policy governs the manner in which &amp;lt;strong&amp;gt;[Conpany Name]&amp;lt;/strong&amp;gt; collects, uses, maintains information collected from users (each, a &amp;quot;User&amp;quot;) of the &amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Title Here&amp;quot;&amp;gt;[www.domainame.com]&amp;lt;/a&amp;gt;. This privacy policy applies to the Site and all products and services offered by &amp;lt;strong&amp;gt;[Conpany Name]&amp;lt;/strong&amp;gt;&amp;lt;/p&amp;gt; &amp;lt;h3&amp;gt;Personal identification information&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, place an order, fill out a form, respond to a survey, subscribe to the newsletter and in connection with other activities, services, features or resources we make available on our Site.&amp;lt;/p&amp;gt; &amp;lt;h3&amp;gt;Non-personal identification information&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the computer OS and technical information about Users means of connection to our Site, such as the operating system and the Internet Protocol address of your Internet service provider.&amp;lt;/p&amp;gt; &amp;lt;h3&amp;gt;Web browser cookies&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;Our Site may use &amp;quot;cookies&amp;quot; to enhance User experience. User&amp;#039;s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.&amp;lt;/p&amp;gt; &amp;lt;h3&amp;gt;How we protect your information&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;&amp;lt;strong&amp;gt;[Conpany Name]&amp;lt;/strong&amp;gt; collects and uses Users personal information for the following purposes:&amp;lt;/p&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;strong&amp;gt;To improve customer service&amp;lt;/strong&amp;gt; &amp;lt;br&amp;gt; Your information helps us to more effectively respond to your customer service requests and support needs.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;strong&amp;gt;To personalize user experience&amp;lt;/strong&amp;gt; &amp;lt;br&amp;gt; We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;strong&amp;gt;To improve our Site&amp;lt;/strong&amp;gt; &amp;lt;br&amp;gt; We continually strive to improve our website offerings based on the information and feedback we receive from you.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;strong&amp;gt;To administer a content, promotion, survey or other Site feature&amp;lt;/strong&amp;gt; &amp;lt;br&amp;gt; To send Users information they agreed to receive about topics we think will be of interest to them.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;strong&amp;gt;To share your information with third parties&amp;lt;/strong&amp;gt; &amp;lt;br&amp;gt; We may share or sell information with third parties for marketing or other purposes.&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;strong&amp;gt;To send periodic emails&amp;lt;/strong&amp;gt; &amp;lt;br&amp;gt; The email address Users provide for order processing, will only be used to send them information and updates pertaining to their order. It may also be used to respond to their inquiries, and/or other requests or questions. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;h3&amp;gt;How we protect your information&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information and data stored on our Site.&amp;lt;/p&amp;gt; &amp;lt;h3&amp;gt;Sharing your personal information&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above. We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.&amp;lt;/p&amp;gt; &amp;lt;h3&amp;gt;Third party websites&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website&amp;#039;s own terms and policies.&amp;lt;/p&amp;gt; &amp;lt;h3&amp;gt;Changes to this privacy policy&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;&amp;lt;strong&amp;gt;[Conpany Name]&amp;lt;/strong&amp;gt; has the discretion to update this privacy policy at any time. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.&amp;lt;/p&amp;gt; &amp;lt;h3&amp;gt;Your acceptance of these terms&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;By using this Site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.&amp;lt;/p&amp;gt; &amp;lt;h3&amp;gt;Contacting us&amp;lt;/h3&amp;gt; &amp;lt;p&amp;gt;If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please &amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Contact Us&amp;quot;&amp;gt;Contact Us&amp;lt;/a&amp;gt;.&amp;lt;/p&amp;gt; &amp;lt;p&amp;gt;You can also request data removal via the form on our &amp;lt;a href=&amp;quot;data-removal&amp;quot;&amp;gt;Data Removal Request page&amp;lt;/a&amp;gt;&amp;lt;/p&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/section&amp;gt;',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Terms &amp; Conditions')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%pages')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Terms &amp; Conditions'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Terms &amp; Conditions'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Terms &amp; Conditions')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '&lt;section class=&quot;page_section terms_conditions_01&quot;&gt; &lt;div class=&quot;container&quot;&gt; &lt;div class=&quot;row&quot;&gt; &lt;div class=&quot;col-12&quot;&gt; &lt;div class=&quot;cms&quot;&gt; &lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/section&gt;',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Terms &amp; Conditions',
							'varMetaKeyword' => 'Terms &amp; Conditions',
							'varMetaDescription' => '&amp;lt;section class=&amp;quot;page_section terms_conditions_01&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;container&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;row&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;col-12&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;cms&amp;quot;&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/section&amp;gt;',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
				
					$pageModuleCode = DB::table('module')->select('id')
					->where('varTitle','Site Map')
					->first();

					if(!isset($pageModuleCode->id) || empty($pageModuleCode->id)){
						$pageModuleCode = DB::table('module')->select('id')
						->where('varModuleName','like','%pages')						
						->first();
					}

					if(isset($pageModuleCode->id) && !empty($pageModuleCode->id))
					{
						$intFKModuleCode = $pageModuleCode->id;		
					}else{
						$intFKModuleCode = $moduleCode->id;
					}
					
					$exists = DB::table('cms_page')->select('id')->where('varTitle',htmlspecialchars_decode('Site Map'))->first();		
					if(!isset($exists->id)){					
						DB::table('cms_page')->insert([
							'varTitle' =>  htmlspecialchars_decode('Site Map'),
							'intAliasId' => MyLibrary::insertAlias(slug::create_slug(htmlspecialchars_decode('Site Map')),$moduleCode->id),
							'intFKModuleCode' => $intFKModuleCode,
							'txtDescription' => '&lt;section class=&quot;page_section sitemap_01&quot;&gt; &lt;div class=&quot;container&quot;&gt; &lt;div class=&quot;row&quot;&gt; &lt;div class=&quot;col-12&quot;&gt; &lt;div class=&quot;cms mb-xs-30&quot;&gt; &lt;/div&gt; &lt;ul class=&quot;list cols&quot;&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Home&quot;&gt;Home&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Main Menu Item&quot;&gt;Main Menu Item&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Main Menu Item&quot;&gt;Main Menu Item&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Main Menu Item&quot;&gt;Main Menu Item&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Main Menu Item&quot;&gt;Main Menu Item&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Main Menu Item&quot;&gt;Main Menu Item&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt; &lt;ul&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Sub Menu Itam&quot;&gt;Sub Menu Itam&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;XML Sitemap&quot; target=&quot;_blank&quot;&gt;XML Sitemap&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Privacy Data Removal&quot;&gt;Privacy Data Removal&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;hr&gt; &lt;h2 class=&quot;nqtitle mb-xs-30&quot;&gt;Socia Media&lt;/h2&gt; &lt;ul class=&quot;list cols&quot;&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Facebook&quot; target=&quot;_blank&quot;&gt;Facebook&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Twitter&quot; target=&quot;_blank&quot;&gt;Twitter&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;YouTube&quot; target=&quot;_blank&quot;&gt;YouTube&lt;/a&gt;&lt;/li&gt; &lt;li&gt;&lt;a href=&quot;#&quot; title=&quot;Instagram&quot; target=&quot;_blank&quot;&gt;Instagram&lt;/a&gt;&lt;/li&gt; &lt;/ul&gt; &lt;/div&gt; &lt;/div&gt; &lt;/div&gt; &lt;/section&gt;',
							'chrPublish' => 'Y',
							'chrDelete'=> 'N',
							'varMetaTitle' => 'Site Map',
							'varMetaKeyword' => 'Site Map',
							'varMetaDescription' => '&amp;lt;section class=&amp;quot;page_section sitemap_01&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;container&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;row&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;col-12&amp;quot;&amp;gt; &amp;lt;div class=&amp;quot;cms mb-xs-30&amp;quot;&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;ul class=&amp;quot;list cols&amp;quot;&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Home&amp;quot;&amp;gt;Home&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Main Menu Item&amp;quot;&amp;gt;Main Menu Item&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Main Menu Item&amp;quot;&amp;gt;Main Menu Item&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Main Menu Item&amp;quot;&amp;gt;Main Menu Item&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Main Menu Item&amp;quot;&amp;gt;Main Menu Item&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Main Menu Item&amp;quot;&amp;gt;Main Menu Item&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt; &amp;lt;ul&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Sub Menu Itam&amp;quot;&amp;gt;Sub Menu Itam&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;XML Sitemap&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;XML Sitemap&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Privacy Data Removal&amp;quot;&amp;gt;Privacy Data Removal&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;hr&amp;gt; &amp;lt;h2 class=&amp;quot;nqtitle mb-xs-30&amp;quot;&amp;gt;Socia Media&amp;lt;/h2&amp;gt; &amp;lt;ul class=&amp;quot;list cols&amp;quot;&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Facebook&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;Facebook&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Twitter&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;Twitter&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;YouTube&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;YouTube&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;li&amp;gt;&amp;lt;a href=&amp;quot;#&amp;quot; title=&amp;quot;Instagram&amp;quot; target=&amp;quot;_blank&amp;quot;&amp;gt;Instagram&amp;lt;/a&amp;gt;&amp;lt;/li&amp;gt; &amp;lt;/ul&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/div&amp;gt; &amp;lt;/section&amp;gt;',
							'created_at'=> Carbon::now(),
							'updated_at'=> Carbon::now(),
						]);					
					}					
							

		}
}
