<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ImageTableSeeder extends Seeder
{
		public function run()
		{
		
									
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'logo',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'logo',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'footer_logo',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'footer_logo',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
									

									
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_banner_01_02',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_banner_01_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
								

									
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_banner_01_02',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_banner_01_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_banner_01_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_banner_01_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_bg',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_07_bg',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_07_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_02',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_07_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_03',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_07_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_04',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_07_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_07_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_07_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_07_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_07_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_01',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_02',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_03',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_04',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_15_01',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_15_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_15_02',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_15_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_15_03',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_15_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_15_04',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_15_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_15_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_15_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_15_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_15_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_15_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_15_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_15_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_15_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_07_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_02',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_07_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_03',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_07_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_04',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_07_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_07_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_07_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_07_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_07_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_07_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'captcha-img',
						'varImageExtension' => 'gif',
						'txtImgOriginalName' => 'captcha-img',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'cms',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'cms',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'li',
						'varImageExtension' => 'svg',
						'txtImgOriginalName' => 'li',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_01',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_02',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_03',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_04',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_08_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_08_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_01',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_02',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_03',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_04',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'jpg',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_inner_banner_01_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_inner_banner_01_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_01',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_02',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_03',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_04',
						'varImageExtension' => 'png',
						'txtImgOriginalName' => 'theme_01_section_10_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_01',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_01',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_02',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_02',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_03',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_03',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
						
					DB::table('image')->insert([
						'fkIntUserId' =>  1,
						'txtImageName' => 'theme_01_section_10_img_04',
						'varImageExtension' => 'webp',
						'txtImgOriginalName' => 'theme_01_section_10_img_04',
						'chrIsUserUploaded' => 'N',
						'chrPublish'=> 'Y',
						'chrDelete'=> 'N',
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now()
					]);
							
			
			DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_07_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_07_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_08_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_08_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_10_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_10_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_10_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_10_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_10_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_10_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_10_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_10_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_10_img_01',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_10_img_01',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_10_img_02',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_10_img_02',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_10_img_03',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_10_img_03',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_10_img_04',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_10_img_04',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

DB::table('image')->insert([
'fkIntUserId' =>  1,
'txtImageName' => 'theme_01_section_10_img_05',
'varImageExtension' => 'jpg',
'txtImgOriginalName' => 'theme_01_section_10_img_05',
'chrIsUserUploaded' => 'N',
'chrPublish' => 'Y',
'chrDelete' => 'N',
'created_at' => Carbon::now(),
'updated_at' => Carbon::now()
]);

#=
#==

			
			
		}
}
