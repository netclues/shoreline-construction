<?php
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Helpers\MyLibrary;
class ContactinfoTableSeeder extends Seeder {
	/**
	* Run the database seeds.
	*
	* @return void
	*/
	public function run() 
	{
		
		DB::table('contact_info')->insert([
	'varTitle'=>'Contact Infomation',
	'varEmail'=>serialize(['netquickteam@gmail.com']),
	'varPhoneNo'=>serialize(['+1 (345)-XXX-XXXX']),
	'intDisplayOrder'=>'1',
	'fkIntImgId'=>null,
	'txtAddress'=>' 122-123, Cannon Place,
North Sound Road,
Industrial Park George town,
George Town KY1-1208, Cayman Islands ',
	'varOpeningHours'=> null,
	'chrIsPrimary'=> 'Y'
	]);
#=
#==


	}
	
}