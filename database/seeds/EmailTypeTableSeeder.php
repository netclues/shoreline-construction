<?php
use Illuminate\Database\Seeder;

class EmailTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
				$emailTypeData = 
				[
            ['varEmailType' => 'Forgot Password', 'chrPublish' => 'Y', 'chrDelete' => 'N'],
            ['varEmailType' => 'Contact Us Lead', 'chrPublish' => 'Y', 'chrDelete' => 'N'],
            ['varEmailType' => 'General', 'chrPublish' => 'Y', 'chrDelete' => 'N'],
            ['varEmailType' => 'Appointment Lead', 'chrPublish' => 'Y', 'chrDelete' => 'N'],
            ['varEmailType' => 'Reservation Lead', 'chrPublish' => 'Y', 'chrDelete' => 'N'],
            ['varEmailType' => 'Privacy Removal', 'chrPublish' => 'Y', 'chrDelete' => 'N']
				];
				
        DB::table('email_type')->insert($emailTypeData);
    }
}
