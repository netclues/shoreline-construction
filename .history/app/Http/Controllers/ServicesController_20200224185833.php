<?php
namespace App\Http\Controllers;
		use App\Services;
		use App\ServiceCategory;
		use App\Video;
	use Config;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


class ServicesController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads services list view
		 * @return  View
		 * @since   2020-02-07
		 * @author  NetQuick
		 */
		public function index() 
		{
            $ServiceCategory      		= ServiceCategory::getFrontList();
            // $ServiceCategory[' '] 	   	= '--Select Category--';
            // asort($ServiceCategory);
            $data                      	= array();
            $filterArr                 	= [];
            $filterArr['catFilter']    	= (null !== Request::get('catFilter')) ? Request::get('catFilter') : null;
            $filterArr['searchFilter'] 	= (null !== Request::get('searchFilter')) ? Request::get('searchFilter') : null;
            $services                  	= Services::getFrontList($filterArr, Request::get('page'));
            $data['services']          	= $services;
            $data['ServiceCategory']   	= $ServiceCategory;
            return view('services', $data);
        }



				/**
		* This method loads services detail view
		* @param   Alias of record
		* @return  View
		* @since   2020-02-07
		* @author  NetQuick
		*/ 
				public function detail($alias) 
		{
				$id      = slug::resolve_alias($alias);
$service = Services::getFrontDetail($id);
if (!empty($service)) {
    $serviceCategory = null;
    $categoryIds     = unserialize($service->txtCategories);
    if (!empty($categoryIds)) {
        $serviceCategory = ServiceCategory::getRecordByIds($categoryIds);
    }
    $videoIDAray             = explode(',', $service->fkIntVideoId);
    $videoObj                = Video::getVideoData($videoIDAray);
    $metaInfo                = array('varMetaTitle' => $service->varMetaTitle, 'varMetaKeyword' => $service->varMetaKeyword, 'varMetaDescription' => $service->varMetaDescription);
    $data                    = array();
    
    $breadcrumb              = [];
    $segmentArr  = Request::segments();
    $url = '';
    foreach ($segmentArr as $key => $value) 
    {
        $url .= $value.'/';
        $breadcrumb[$key]['title'] =  ucwords(str_replace('-', ' ', $value));
        $breadcrumb[$key]['url'] =  rtrim($url,'/');
    } 

    $similarServices =  Services::getSimilarRecordList($service->id);   
    $data['service']         = $service;
    $data['alias']           = $alias;
    $data['similarServices'] = $similarServices;
    $data['metaInfo']        = $metaInfo;
    $data['breadcrumb']      = $breadcrumb;
    $data['serviceCategory'] = $serviceCategory;
    $data['videoObj']        = $videoObj;

    if(isset($service->fkIntImgId) && !empty($service->fkIntImgId)) {
        $imageArr = explode(',', $service->fkIntImgId);
        view()->share('SHARE_IMG', $imageArr[0]); 
    }            

    if (isset($service->varMetaTitle) && !empty($service->varMetaTitle)) {
        view()->share('META_TITLE', $service->varMetaTitle);
    }
    
    if (isset($service->varMetaKeyword) && !empty($service->varMetaKeyword)) {
        view()->share('META_KEYWORD', $service->varMetaKeyword);
    }
    if (isset($service->varMetaDescription) && !empty($service->varMetaDescription)) {
        view()->share('META_DESCRIPTION', substr(trim($service->varMetaDescription), 0, 500));
    }				
								return view('services-detail', $data);				
			}else{
				abort(404);
			}
		}
		}