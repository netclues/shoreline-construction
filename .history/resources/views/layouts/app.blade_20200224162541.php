
<!DOCTYPE html>
<html lang="en-US">

<head>
    @if(!empty(Config::get('Constant.GOOGLE_ANALYTIC_CODE'))) {!! Config::get('Constant.GOOGLE_ANALYTIC_CODE') !!} @endif
    
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
    <meta name="theme-color" content="" />
    <!-- Meta Description S -->
        <title>{{ $META_TITLE }}</title>
        <meta name="title" content="{{ $META_TITLE }}">
        <meta name="description" content="{{ $META_DESCRIPTION }}">
    <!-- Meta Description E -->

    <!-- Monitoring S -->
	    @if(!empty($site_monitor))
	    <meta name="Monitoring" content="{{ $site_monitor->varTitle }}">
	    @endif
    <!-- Monitoring E -->
     <!-- CSRF-Token Canonical Link S -->
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <link rel="canonical" href="{{ url()->current() }}" />
    <!-- CSRF-Token Canonical Link E -->

    <!-- Style Sheet File S -->
	    <link rel="stylesheet" href="{!! url('assets/css/main.css') !!}" />
    <!-- Style Sheet File E -->

    <!-- Google Font Link S -->
        <link href="https://fonts.googleapis.com/css?family=Muli:400,500,600|Work+Sans:400,500,600,700&display=swap" rel="stylesheet">
    <!-- Google Font Link E -->

    <!-- Favicon Icon S -->
	    <link rel="shortcut icon" href="{!! url('assets/images/favicon.ico') !!}" type="image/x-icon" />
	    <link rel="apple-touch-icon" sizes="144x144" href="{!! url('assets/images/apple-touch-icon-144.png') !!}" />
	    <link rel="apple-touch-icon" sizes="114x114" href="{!! url('assets/images/apple-touch-icon-114.png') !!}" />
	    <link rel="apple-touch-icon" sizes="72x72" href="{!! url('assets/images/apple-touch-icon-72.png') !!}" />
	    <link rel="apple-touch-icon" sizes="57x57" href="{!! url('assets/images/apple-touch-icon-57.png') !!}" />
    <!-- Favicon Icon E -->

    <!-- Javascript File S -->
        <!-- <script type="text/javascript" src="{{ url('assets/libraries/jquery/jquery-3.3.1.min.js') }}"></script> -->
    	<script type="text/javascript" src="{{ url('assets/js/jquery-3.4.1.min.js') }}"></script>
    <!-- Javascript File E -->

    <!-- Device JS S -->
	    <script type="text/javascript">
		    var site_url = "{{ url('') }}";
		    var rootUrl = site_url;
		    var deviceType = "{{ Config::get('Constant.DEVICE') }}";
	    </script>
    <!-- Device JS E -->
    @php header('Cache-Control: no-cache, no-store, must-revalidate'); @endphp
</head>

<body class="index_body 
		">
    @if(!empty(Config::get('Constant.GOOGLE_TAG_MANAGER_FOR_BODY'))) {!! Config::get('Constant.GOOGLE_TAG_MANAGER_FOR_BODY') !!} @endif

    <!-- Organization S -->
    <script type='application/ld+json'>
    {
        "@context": "http://www.schema.org",
        "@type": "Organization",
        "name": "{{ Config::get('Constant.SITE_NAME') }}",
        "url": "{{ url('/') }}",
        "sameAs": [
            @if(!empty(Config::get('Constant.SOCIAL_FB_LINK')))
                "{{ Config::get('Constant.SOCIAL_FB_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_TWITTER_LINK')))
                "{{ Config::get('Constant.SOCIAL_TWITTER_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_YOUTUBE_LINK')))
                "{{ Config::get('Constant.SOCIAL_YOUTUBE_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_TRIPADVISOR_LINK')))
                "{{ Config::get('Constant.SOCIAL_TRIPADVISOR_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_LINKEDIN_LINK')))
                "{{ Config::get('Constant.SOCIAL_LINKEDIN_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_INSTAGRAM_LINK')))
                "{{ Config::get('Constant.SOCIAL_INSTAGRAM_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_TUMBLR_LINK')))
                "{{ Config::get('Constant.SOCIAL_TUMBLR_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_PINTEREST_LINK')))
                "{{ Config::get('Constant.SOCIAL_PINTEREST_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_FLICKR_LINK')))
                "{{ Config::get('Constant.SOCIAL_FLICKR_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_DRIBBBLE_LINK')))
                "{{ Config::get('Constant.SOCIAL_DRIBBBLE_LINK') }}",
            @endif
            @if(!empty(Config::get('Constant.SOCIAL_RSS_FEED_LINK')))
                "{{ Config::get('Constant.SOCIAL_RSS_FEED_LINK') }}"
            @endif
        ],
        "logo": "{!! App\Helpers\resize_image::resize(Config::get('Constant.FRONT_LOGO_ID')) !!}",
        "image": "{!! App\Helpers\resize_image::resize(Config::get('Constant.FRONT_LOGO_ID')) !!}",
        "description": "",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "",
            "postalCode": "",
            "addressCountry": ""
        }
    }
    </script>
    <!-- Organization E -->

    <!-- Browser Upgrade S -->
    <div id="buorg" class="buorg">
        <div class="buorg__text"><i class="fa fa-exclamation-triangle"></i> For a better view on {{ Config::get('Constant.SITE_NAME') }}, <a href="https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads" title="Update Your Browser" target="_blank" rel="nofollow">Update Your Browser.</a></div>
    </div>
    <!-- Browser Upgrade E -->
    <!-- Scroll To Top S -->
    <div id="back-top" title="Scroll To Top" style="display: none;">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll To Top E -->
    <!-- Wrapper S -->
    <div id="wrapper">
<!-- Header E -->
@if(!Request::ajax())
<header class="header_01">
    <div class="header-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="hs-left">
                        <div class="hs-logo">
                            <a href="{{ url('/') }}" title="{{ Config::get('Constant.SITE_NAME') }}" itemtype="http://schema.org/Organization" itemscope="">
                            	<!-- Email S -->
	                            	@php $email=''; @endphp
	                            	@if(isset($objContactInfo->varEmail) && !empty($objContactInfo->varEmail))
	                            		@php
	                            			$email = unserialize($objContactInfo->varEmail);
	                            			$email =count($email)>0?$email[0]:$email;
	                            		@endphp
	                            	@endif
                            	<!-- Email E -->
                            	<!-- Phone Number S -->
	                            	@php $phone=''; @endphp
	                            	@if(isset($objContactInfo->varPhoneNo) && !empty($objContactInfo->varEmail))
	                            		@php
	                            			$phone = unserialize($objContactInfo->varPhoneNo);
	                            			$phone =count($phone)>0?$phone[0]:$phone;
	                            		@endphp
	                            	@endif
                            	<!-- Phone Number E -->
                                <meta itemprop="name" content="{{ Config::get('Constant.SITE_NAME') }}">
                                <meta itemprop="email" content="{{ $email }}">
                                <meta itemprop="telephone" content="{{ $phone }}">
                                <meta itemprop="address" content="{{!empty($objContactInfo->txtAddress)?$objContactInfo->txtAddress:''}}">
                                <img src="{!! App\Helpers\resize_image::resize(Config::get('Constant.FRONT_LOGO_ID')) !!}" alt="{{ Config::get('Constant.SITE_NAME') }}" title="{{ Config::get('Constant.SITE_NAME') }}">
                            </a>
                        </div>
                    </div>
                    <div class="hs-right">
                    	<!-- Menu S -->
	                        <nav class="menu" id="menu">
	                            <div class="menu_mobile_visibility">
	                                <a href="{{ url('/') }}" title="{{ Config::get('Constant.SITE_NAME') }}">
	                                    <img src="{!! App\Helpers\resize_image::resize(Config::get('Constant.FRONT_LOGO_ID')) !!}" alt="{{ Config::get('Constant.SITE_NAME') }}" title="{{ Config::get('Constant.SITE_NAME') }}">
	                                </a>
	                            </div>
	                            {!! $headerMenu->asUl(array('class' => 'brand-nav brand-navbar'), array('class' => 'sub-menu')) !!}
	                        </nav>
                        <!-- Menu E -->
                        <div class="nav-overlay" onclick="closeNav()"></div>
                        <div class="menu_open_close text-right">
                            <a href="javascript:void(0)" class="menu__open" id="menu__open" onclick="openNav()" title="Menu Open"><span></span></a>
                            <a href="javascript:void(0)" class="menu__close" id="menu__close" onclick="closeNav()" title="Menu Close"><span></span></a>
                        </div>
                        <div class="hs-info"> 
                            <a href="javascript:void(0)" class="btn" title="Request A Quote"><span>Request A Quote</span><i class="icon-notebook"></i></a>  
                            <a href="tel:{{ $phone }}" class="hs-call" title="Call Us On: {{ $phone }}">
                            
							<?php echo '<?xml version="1.0" encoding="UTF-8">'; ?>
                                <!-- Generator: Adobe Illustrator 15.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="600.261px" height="600.281px" viewBox="0 0 600.261 600.281" enable-background="new 0 0 600.261 600.281"
                                     xml:space="preserve">
                                <g>
                                    <defs>
                                        <rect id="SVGID_1_" width="600.261" height="600.281"/>
                                    </defs>
                                    <clipPath id="SVGID_2_">
                                        <use xlink:href="#SVGID_1_"  overflow="visible"/>
                                    </clipPath>
                                    <path clip-path="url(#SVGID_2_)" fill="#F2A104" d="M425.09,2.5H175.171c-24.089,0-43.704,19.615-43.704,43.704v507.864
                                        c0,24.098,19.615,43.713,43.704,43.713h249.908c24.101,0,43.713-19.615,43.713-43.703V46.205C468.792,22.115,449.18,2.5,425.09,2.5
                                         M151.311,81.872h297.64v377.011h-297.64V81.872z M175.171,22.344h249.908c13.166,0,23.871,10.705,23.871,23.861v15.824h-297.64
                                        V46.205C151.311,33.048,162.016,22.344,175.171,22.344 M425.09,577.938H175.171c-13.156,0-23.861-10.705-23.861-23.859v-75.354
                                        h297.64v75.354C448.95,567.232,438.245,577.938,425.09,577.938"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#F2A104" stroke-width="5" stroke-miterlimit="10" d="M425.09,2.5H175.171
                                        c-24.089,0-43.704,19.615-43.704,43.704v507.864c0,24.098,19.615,43.713,43.704,43.713h249.908
                                        c24.101,0,43.713-19.615,43.713-43.703V46.205C468.792,22.115,449.18,2.5,425.09,2.5z M151.311,81.872h297.64v377.011h-297.64
                                        V81.872z M175.171,22.344h249.908c13.166,0,23.871,10.705,23.871,23.861v15.824h-297.64V46.205
                                        C151.311,33.048,162.016,22.344,175.171,22.344z M425.09,577.938H175.171c-13.156,0-23.861-10.705-23.861-23.859v-75.354h297.64
                                        v75.354C448.95,567.232,438.245,577.938,425.09,577.938z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M300.131,493.313c-19.313,0-35.02,15.706-35.02,35.019
                                        s15.707,35.019,35.02,35.019c19.312,0,35.019-15.706,35.019-35.019S319.442,493.313,300.131,493.313 M300.131,545.841
                                        c-9.656,0-17.509-7.853-17.509-17.509s7.853-17.51,17.509-17.51c9.655,0,17.509,7.854,17.509,17.51
                                        S309.786,545.841,300.131,545.841"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M300.131,493.313
                                        c-19.313,0-35.02,15.706-35.02,35.019s15.707,35.019,35.02,35.019c19.312,0,35.019-15.706,35.019-35.019
                                        S319.442,493.313,300.131,493.313z M300.131,545.841c-9.656,0-17.509-7.853-17.509-17.509s7.853-17.51,17.509-17.51
                                        c9.655,0,17.509,7.854,17.509,17.51S309.786,545.841,300.131,545.841z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M272.167,48.927h26.962c3.727,0,6.741-3.013,6.741-6.741
                                        c0-3.727-3.014-6.74-6.741-6.74h-26.962c-3.728,0-6.741,3.013-6.741,6.74C265.426,45.914,268.439,48.927,272.167,48.927"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M272.167,48.927h26.962
                                        c3.727,0,6.741-3.013,6.741-6.741c0-3.727-3.014-6.74-6.741-6.74h-26.962c-3.728,0-6.741,3.013-6.741,6.74
                                        C265.426,45.914,268.439,48.927,272.167,48.927z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M321.353,48.927h6.739c3.729,0,6.741-3.013,6.741-6.741
                                        c0-3.727-3.013-6.74-6.741-6.74h-6.739c-3.729,0-6.742,3.013-6.742,6.74C314.61,45.914,317.623,48.927,321.353,48.927"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M321.353,48.927h6.739
                                        c3.729,0,6.741-3.013,6.741-6.741c0-3.727-3.013-6.74-6.741-6.74h-6.739c-3.729,0-6.742,3.013-6.742,6.74
                                        C314.61,45.914,317.623,48.927,321.353,48.927z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M564.158,47.832c-3.882-3.879-10.15-3.879-14.029,0s-3.879,10.149,0,14.028
                                        c37.065,37.067,37.065,97.378,0,134.454c-3.879,3.88-3.879,10.15,0,14.029c1.933,1.935,4.474,2.907,7.013,2.907
                                        c2.541,0,5.08-0.972,7.017-2.907C608.963,165.547,608.963,92.637,564.158,47.832"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M564.158,47.832
                                        c-3.882-3.879-10.15-3.879-14.029,0s-3.879,10.149,0,14.028c37.065,37.067,37.065,97.378,0,134.454
                                        c-3.879,3.88-3.879,10.15,0,14.029c1.933,1.935,4.474,2.907,7.013,2.907c2.541,0,5.08-0.972,7.017-2.907
                                        C608.963,165.547,608.963,92.637,564.158,47.832z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M522.379,70.412c-3.88-3.879-10.15-3.879-14.029,0s-3.879,10.15,0,14.029
                                        c12.272,12.273,19.029,28.623,19.029,46.025c0,17.402-6.757,33.742-19.029,46.015c-3.879,3.88-3.879,10.15,0,14.029
                                        c1.935,1.935,4.474,2.907,7.015,2.907c2.539,0,5.078-0.972,7.015-2.907c16.022-16.013,24.844-37.344,24.844-60.044
                                        C547.223,107.766,538.411,86.425,522.379,70.412"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M522.379,70.412
                                        c-3.88-3.879-10.15-3.879-14.029,0s-3.879,10.15,0,14.029c12.272,12.273,19.029,28.623,19.029,46.025
                                        c0,17.402-6.757,33.742-19.029,46.015c-3.879,3.88-3.879,10.15,0,14.029c1.935,1.935,4.474,2.907,7.015,2.907
                                        c2.539,0,5.078-0.972,7.015-2.907c16.022-16.013,24.844-37.344,24.844-60.044C547.223,107.766,538.411,86.425,522.379,70.412z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M50.132,61.86c3.879-3.879,3.879-10.149,0-14.028
                                        c-3.879-3.88-10.149-3.88-14.029,0c-44.804,44.804-44.804,117.706,0,162.511c1.935,1.934,4.475,2.907,7.015,2.907
                                        s5.079-0.973,7.014-2.907c3.879-3.879,3.879-10.15,0-14.029C13.056,159.248,13.056,98.936,50.132,61.86"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M50.132,61.86
                                        c3.879-3.879,3.879-10.149,0-14.028c-3.879-3.88-10.149-3.88-14.029,0c-44.804,44.804-44.804,117.706,0,162.511
                                        c1.935,1.934,4.475,2.907,7.015,2.907s5.079-0.973,7.014-2.907c3.879-3.879,3.879-10.15,0-14.029
                                        C13.056,159.248,13.056,98.936,50.132,61.86z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M91.911,67.674c-3.879-3.879-10.149-3.879-14.029,0
                                        c-16.023,16.013-24.843,37.344-24.843,60.044c0,22.7,8.82,44.031,24.843,60.054c1.935,1.935,4.475,2.907,7.015,2.907
                                        c2.539,0,5.079-0.972,7.014-2.907c3.879-3.879,3.879-10.15,0-14.029c-12.272-12.273-19.029-28.623-19.029-46.025
                                        s6.746-33.742,19.029-46.015C95.781,77.834,95.781,71.553,91.911,67.674"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M91.911,67.674
                                        c-3.879-3.879-10.149-3.879-14.029,0c-16.023,16.013-24.843,37.344-24.843,60.044c0,22.7,8.82,44.031,24.843,60.054
                                        c1.935,1.935,4.475,2.907,7.015,2.907c2.539,0,5.079-0.972,7.014-2.907c3.879-3.879,3.879-10.15,0-14.029
                                        c-12.272-12.273-19.029-28.623-19.029-46.025s6.746-33.742,19.029-46.015C95.781,77.834,95.781,71.553,91.911,67.674z"/>
                                </g>
                                </svg>                                
                            <!-- <span>+1.345 922 Team(8326)</span></a> -->
                            <span>{{ $phone }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@endif
<!-- Header S -->
@yield('content')
@if(!Request::ajax())
<!-- Footer S -->
<footer class="footer_02">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="footer-logo">
                    <a href="{{ url('') }}" title="Shoreline Construction">
                        <img src="{{ url('assets/images/footer-logo.png') }}" alt="Shoreline-Construction-Footer" title="Shoreline Construction">
                    </a>
                </div>
            </div>
            <div class="col-md-9 col-sm-12">
                <div class="footer-contact">
                    <ul>
                        <li>
                            <a href="tel:{{ $phone }}" title="Call Us On:{{ $phone }}">
                            <span>                                
								<?php echo '<?xml version="1.0" encoding="UTF-8">'; ?>
                                <!-- Generator: Adobe Illustrator 15.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="600.261px" height="600.281px" viewBox="0 0 600.261 600.281" enable-background="new 0 0 600.261 600.281"
                                     xml:space="preserve">
                                <g>
                                    <defs>
                                        <rect id="SVGID_1_" width="600.261" height="600.281"/>
                                    </defs>
                                    <clipPath id="SVGID_2_">
                                        <use xlink:href="#SVGID_1_"  overflow="visible"/>
                                    </clipPath>
                                    <path clip-path="url(#SVGID_2_)" fill="#F2A104" d="M425.09,2.5H175.171c-24.089,0-43.704,19.615-43.704,43.704v507.864
                                        c0,24.098,19.615,43.713,43.704,43.713h249.908c24.101,0,43.713-19.615,43.713-43.703V46.205C468.792,22.115,449.18,2.5,425.09,2.5
                                         M151.311,81.872h297.64v377.011h-297.64V81.872z M175.171,22.344h249.908c13.166,0,23.871,10.705,23.871,23.861v15.824h-297.64
                                        V46.205C151.311,33.048,162.016,22.344,175.171,22.344 M425.09,577.938H175.171c-13.156,0-23.861-10.705-23.861-23.859v-75.354
                                        h297.64v75.354C448.95,567.232,438.245,577.938,425.09,577.938"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#F2A104" stroke-width="5" stroke-miterlimit="10" d="M425.09,2.5H175.171
                                        c-24.089,0-43.704,19.615-43.704,43.704v507.864c0,24.098,19.615,43.713,43.704,43.713h249.908
                                        c24.101,0,43.713-19.615,43.713-43.703V46.205C468.792,22.115,449.18,2.5,425.09,2.5z M151.311,81.872h297.64v377.011h-297.64
                                        V81.872z M175.171,22.344h249.908c13.166,0,23.871,10.705,23.871,23.861v15.824h-297.64V46.205
                                        C151.311,33.048,162.016,22.344,175.171,22.344z M425.09,577.938H175.171c-13.156,0-23.861-10.705-23.861-23.859v-75.354h297.64
                                        v75.354C448.95,567.232,438.245,577.938,425.09,577.938z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M300.131,493.313c-19.313,0-35.02,15.706-35.02,35.019
                                        s15.707,35.019,35.02,35.019c19.312,0,35.019-15.706,35.019-35.019S319.442,493.313,300.131,493.313 M300.131,545.841
                                        c-9.656,0-17.509-7.853-17.509-17.509s7.853-17.51,17.509-17.51c9.655,0,17.509,7.854,17.509,17.51
                                        S309.786,545.841,300.131,545.841"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M300.131,493.313
                                        c-19.313,0-35.02,15.706-35.02,35.019s15.707,35.019,35.02,35.019c19.312,0,35.019-15.706,35.019-35.019
                                        S319.442,493.313,300.131,493.313z M300.131,545.841c-9.656,0-17.509-7.853-17.509-17.509s7.853-17.51,17.509-17.51
                                        c9.655,0,17.509,7.854,17.509,17.51S309.786,545.841,300.131,545.841z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M272.167,48.927h26.962c3.727,0,6.741-3.013,6.741-6.741
                                        c0-3.727-3.014-6.74-6.741-6.74h-26.962c-3.728,0-6.741,3.013-6.741,6.74C265.426,45.914,268.439,48.927,272.167,48.927"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M272.167,48.927h26.962
                                        c3.727,0,6.741-3.013,6.741-6.741c0-3.727-3.014-6.74-6.741-6.74h-26.962c-3.728,0-6.741,3.013-6.741,6.74
                                        C265.426,45.914,268.439,48.927,272.167,48.927z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M321.353,48.927h6.739c3.729,0,6.741-3.013,6.741-6.741
                                        c0-3.727-3.013-6.74-6.741-6.74h-6.739c-3.729,0-6.742,3.013-6.742,6.74C314.61,45.914,317.623,48.927,321.353,48.927"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M321.353,48.927h6.739
                                        c3.729,0,6.741-3.013,6.741-6.741c0-3.727-3.013-6.74-6.741-6.74h-6.739c-3.729,0-6.742,3.013-6.742,6.74
                                        C314.61,45.914,317.623,48.927,321.353,48.927z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M564.158,47.832c-3.882-3.879-10.15-3.879-14.029,0s-3.879,10.149,0,14.028
                                        c37.065,37.067,37.065,97.378,0,134.454c-3.879,3.88-3.879,10.15,0,14.029c1.933,1.935,4.474,2.907,7.013,2.907
                                        c2.541,0,5.08-0.972,7.017-2.907C608.963,165.547,608.963,92.637,564.158,47.832"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M564.158,47.832
                                        c-3.882-3.879-10.15-3.879-14.029,0s-3.879,10.149,0,14.028c37.065,37.067,37.065,97.378,0,134.454
                                        c-3.879,3.88-3.879,10.15,0,14.029c1.933,1.935,4.474,2.907,7.013,2.907c2.541,0,5.08-0.972,7.017-2.907
                                        C608.963,165.547,608.963,92.637,564.158,47.832z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M522.379,70.412c-3.88-3.879-10.15-3.879-14.029,0s-3.879,10.15,0,14.029
                                        c12.272,12.273,19.029,28.623,19.029,46.025c0,17.402-6.757,33.742-19.029,46.015c-3.879,3.88-3.879,10.15,0,14.029
                                        c1.935,1.935,4.474,2.907,7.015,2.907c2.539,0,5.078-0.972,7.015-2.907c16.022-16.013,24.844-37.344,24.844-60.044
                                        C547.223,107.766,538.411,86.425,522.379,70.412"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M522.379,70.412
                                        c-3.88-3.879-10.15-3.879-14.029,0s-3.879,10.15,0,14.029c12.272,12.273,19.029,28.623,19.029,46.025
                                        c0,17.402-6.757,33.742-19.029,46.015c-3.879,3.88-3.879,10.15,0,14.029c1.935,1.935,4.474,2.907,7.015,2.907
                                        c2.539,0,5.078-0.972,7.015-2.907c16.022-16.013,24.844-37.344,24.844-60.044C547.223,107.766,538.411,86.425,522.379,70.412z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M50.132,61.86c3.879-3.879,3.879-10.149,0-14.028
                                        c-3.879-3.88-10.149-3.88-14.029,0c-44.804,44.804-44.804,117.706,0,162.511c1.935,1.934,4.475,2.907,7.015,2.907
                                        s5.079-0.973,7.014-2.907c3.879-3.879,3.879-10.15,0-14.029C13.056,159.248,13.056,98.936,50.132,61.86"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M50.132,61.86
                                        c3.879-3.879,3.879-10.149,0-14.028c-3.879-3.88-10.149-3.88-14.029,0c-44.804,44.804-44.804,117.706,0,162.511
                                        c1.935,1.934,4.475,2.907,7.015,2.907s5.079-0.973,7.014-2.907c3.879-3.879,3.879-10.15,0-14.029
                                        C13.056,159.248,13.056,98.936,50.132,61.86z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M91.911,67.674c-3.879-3.879-10.149-3.879-14.029,0
                                        c-16.023,16.013-24.843,37.344-24.843,60.044c0,22.7,8.82,44.031,24.843,60.054c1.935,1.935,4.475,2.907,7.015,2.907
                                        c2.539,0,5.079-0.972,7.014-2.907c3.879-3.879,3.879-10.15,0-14.029c-12.272-12.273-19.029-28.623-19.029-46.025
                                        s6.746-33.742,19.029-46.015C95.781,77.834,95.781,71.553,91.911,67.674"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M91.911,67.674
                                        c-3.879-3.879-10.149-3.879-14.029,0c-16.023,16.013-24.843,37.344-24.843,60.044c0,22.7,8.82,44.031,24.843,60.054
                                        c1.935,1.935,4.475,2.907,7.015,2.907c2.539,0,5.079-0.972,7.014-2.907c3.879-3.879,3.879-10.15,0-14.029
                                        c-12.272-12.273-19.029-28.623-19.029-46.025s6.746-33.742,19.029-46.015C95.781,77.834,95.781,71.553,91.911,67.674z"/>
                                </g>
                                </svg>
                            </span>
                                {{ $phone }}
                            </a>
                        </li>
                        <li>
                            <a href="mailto:{{ $email }}" title="Email Us On:  {{ $email }}">
                            <span>
                                <?php echo '<?xml version="1.0" encoding="UTF-8">'; ?>
                                <!-- Generator: Adobe Illustrator 15.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                                <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="600.28px" height="600.271px" viewBox="0 0 600.28 600.271" enable-background="new 0 0 600.28 600.271"
                                     xml:space="preserve">
                                <g>
                                    <defs>
                                        <rect id="SVGID_1_" width="600.28" height="600.271"/>
                                    </defs>
                                    <clipPath id="SVGID_2_">
                                        <use xlink:href="#SVGID_1_"  overflow="visible"/>
                                    </clipPath>
                                    <path clip-path="url(#SVGID_2_)" fill="#F2A104" d="M597.661,230.066c-0.063-0.683-0.198-1.359-0.407-2.013
                                        c-0.136-0.585-0.326-1.156-0.566-1.707c-0.276-0.538-0.608-1.048-0.989-1.519c-0.384-0.559-0.826-1.074-1.321-1.538
                                        c-0.16-0.138-0.229-0.328-0.396-0.466l-95.418-74.106V71.914c0-16.439-13.327-29.766-29.766-29.766H361.274L318.221,8.721
                                        c-10.635-8.295-25.549-8.295-36.184,0l-43.061,33.427H131.452c-16.438,0-29.765,13.326-29.765,29.766v76.805L6.299,222.825
                                        c-0.169,0.139-0.238,0.328-0.397,0.466c-0.494,0.463-0.937,0.979-1.32,1.539c-0.381,0.47-0.713,0.979-0.991,1.518
                                        c-0.241,0.55-0.429,1.122-0.565,1.707c-0.207,0.645-0.343,1.31-0.407,1.985c0,0.208-0.119,0.387-0.119,0.595v337.371
                                        c0.014,6.311,2.056,12.449,5.824,17.512c0.059,0.088,0.07,0.197,0.138,0.277c0.07,0.078,0.219,0.168,0.318,0.277
                                        c5.574,7.334,14.243,11.658,23.455,11.7h535.779c9.25-0.03,17.955-4.374,23.535-11.749c0.08-0.098,0.2-0.129,0.269-0.229
                                        c0.069-0.098,0.077-0.188,0.139-0.277c3.769-5.063,5.81-11.201,5.824-17.512V230.663
                                        C597.78,230.455,597.67,230.274,597.661,230.066 M294.171,24.376c3.465-2.753,8.372-2.753,11.837,0l22.889,17.77H271.35
                                        L294.171,24.376z M34.735,577.927l259.437-201.523c3.467-2.748,8.371-2.748,11.836,0l259.506,201.523H34.735z M577.936,562.458
                                        L318.221,360.748c-10.637-8.289-25.547-8.289-36.184,0L22.313,562.458V245.772l162.589,126.256
                                        c4.333,3.359,10.567,2.57,13.926-1.762c3.359-4.332,2.571-10.566-1.761-13.926L31.888,228.083l69.8-54.243v76.665
                                        c0,5.48,4.443,9.922,9.921,9.922c5.48,0,9.922-4.443,9.922-9.922V71.913c0-5.48,4.442-9.922,9.921-9.922h337.343
                                        c5.479,0,9.92,4.442,9.92,9.922v178.592c0,5.48,4.442,9.922,9.922,9.922c5.481,0,9.922-4.443,9.922-9.922v-76.665l69.802,54.243
                                        L402.893,356.57c-2.846,2.158-4.313,5.678-3.844,9.219c0.473,3.541,2.812,6.555,6.123,7.891c3.313,1.336,7.088,0.789,9.885-1.434
                                        l162.877-126.473v316.686H577.936z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#F2A104" stroke-width="5" stroke-miterlimit="10" d="M597.661,230.066
                                        c-0.063-0.683-0.198-1.359-0.407-2.013c-0.136-0.585-0.326-1.156-0.566-1.707c-0.276-0.538-0.608-1.048-0.989-1.519
                                        c-0.384-0.559-0.826-1.074-1.321-1.538c-0.16-0.138-0.229-0.328-0.396-0.466l-95.418-74.106V71.914
                                        c0-16.439-13.327-29.766-29.766-29.766H361.274L318.221,8.721c-10.635-8.295-25.549-8.295-36.184,0l-43.061,33.427H131.452
                                        c-16.438,0-29.765,13.326-29.765,29.766v76.805L6.299,222.825c-0.169,0.139-0.238,0.328-0.397,0.466
                                        c-0.494,0.463-0.937,0.979-1.32,1.539c-0.381,0.47-0.713,0.979-0.991,1.518c-0.241,0.55-0.429,1.122-0.565,1.707
                                        c-0.207,0.645-0.343,1.31-0.407,1.985c0,0.208-0.119,0.387-0.119,0.595v337.371c0.014,6.311,2.056,12.449,5.824,17.512
                                        c0.059,0.088,0.07,0.197,0.138,0.277c0.07,0.078,0.219,0.168,0.318,0.277c5.574,7.334,14.243,11.658,23.455,11.7h535.779
                                        c9.25-0.03,17.955-4.374,23.535-11.749c0.08-0.098,0.2-0.129,0.269-0.229c0.069-0.098,0.077-0.188,0.139-0.277
                                        c3.769-5.063,5.81-11.201,5.824-17.512V230.663C597.78,230.455,597.67,230.274,597.661,230.066z M294.171,24.376
                                        c3.465-2.753,8.372-2.753,11.837,0l22.889,17.77H271.35L294.171,24.376z M34.735,577.927l259.437-201.523
                                        c3.467-2.748,8.371-2.748,11.836,0l259.506,201.523H34.735z M577.936,562.458L318.221,360.748c-10.637-8.289-25.547-8.289-36.184,0
                                        L22.313,562.458V245.772l162.589,126.256c4.333,3.359,10.567,2.57,13.926-1.762c3.359-4.332,2.571-10.566-1.761-13.926
                                        L31.888,228.083l69.8-54.243v76.665c0,5.48,4.443,9.922,9.921,9.922c5.48,0,9.922-4.443,9.922-9.922V71.913
                                        c0-5.48,4.442-9.922,9.921-9.922h337.343c5.479,0,9.92,4.442,9.92,9.922v178.592c0,5.48,4.442,9.922,9.922,9.922
                                        c5.481,0,9.922-4.443,9.922-9.922v-76.665l69.802,54.243L402.893,356.57c-2.846,2.158-4.313,5.678-3.844,9.219
                                        c0.473,3.541,2.812,6.555,6.123,7.891c3.313,1.336,7.088,0.789,9.885-1.434l162.877-126.473v316.686H577.936z"/>
                                    <path clip-path="url(#SVGID_2_)" fill="#C08003" d="M401.188,244.584v-33.688c0-55.815-45.248-101.062-101.063-101.062
                                        c-55.816,0-101.063,45.247-101.063,101.062c0,55.816,45.247,101.063,101.063,101.063c4.652,0,8.421-3.769,8.421-8.421
                                        c0-4.65-3.769-8.421-8.421-8.421c-46.513,0-84.219-37.706-84.219-84.218c0-46.513,37.706-84.219,84.219-84.219
                                        c46.512,0,84.219,37.706,84.219,84.219v33.687c0,9.303-7.542,16.844-16.844,16.844c-9.303,0-16.843-7.541-16.843-16.844v-33.687
                                        c0-4.652-3.773-8.422-8.423-8.422c-4.651,0-8.423,3.77-8.423,8.422c0,18.604-15.083,33.687-33.687,33.687
                                        c-18.605,0-33.687-15.083-33.687-33.687c0-18.606,15.082-33.688,33.687-33.688c4.652,0,8.421-3.77,8.421-8.421
                                        c0-4.652-3.769-8.422-8.421-8.422c-22.616-0.05-42.512,14.933-48.709,36.683c-6.199,21.75,2.81,44.97,22.055,56.849
                                        c19.245,11.879,44.039,9.524,60.702-5.767c1.836,17.81,17.316,31.054,35.196,30.107
                                        C387.248,277.292,401.244,262.489,401.188,244.584"/>
                                    <path clip-path="url(#SVGID_2_)" fill="none" stroke="#C08003" stroke-width="5" stroke-miterlimit="10" d="M401.188,244.584
                                        v-33.688c0-55.815-45.248-101.062-101.063-101.062c-55.816,0-101.063,45.247-101.063,101.062
                                        c0,55.816,45.247,101.063,101.063,101.063c4.652,0,8.421-3.769,8.421-8.421c0-4.65-3.769-8.421-8.421-8.421
                                        c-46.513,0-84.219-37.706-84.219-84.218c0-46.513,37.706-84.219,84.219-84.219c46.512,0,84.219,37.706,84.219,84.219v33.687
                                        c0,9.303-7.542,16.844-16.844,16.844c-9.303,0-16.843-7.541-16.843-16.844v-33.687c0-4.652-3.773-8.422-8.423-8.422
                                        c-4.651,0-8.423,3.77-8.423,8.422c0,18.604-15.083,33.687-33.687,33.687c-18.605,0-33.687-15.083-33.687-33.687
                                        c0-18.606,15.082-33.688,33.687-33.688c4.652,0,8.421-3.77,8.421-8.421c0-4.652-3.769-8.422-8.421-8.422
                                        c-22.616-0.05-42.512,14.933-48.709,36.683c-6.199,21.75,2.81,44.97,22.055,56.849c19.245,11.879,44.039,9.524,60.702-5.767
                                        c1.836,17.81,17.316,31.054,35.196,30.107C387.248,277.292,401.244,262.489,401.188,244.584z"/>
                                </g>
                                </svg>
                            </span>
                                {{ $email }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="col-md-3 col-sm-12">
                <div class="footer-social">
                    @if(!empty(Config::get('Constant.SOCIAL_FB_LINK')) ||
                    !empty(Config::get('Constant.SOCIAL_TWITTER_LINK')) ||
                    !empty(Config::get('Constant.SOCIAL_INSTAGRAM_LINK')) ||
                    !empty(Config::get('Constant.SOCIAL_LINKEDIN_LINK')))
                    <ul>
                        @if(!empty(Config::get('Constant.SOCIAL_FB_LINK')))
                        <li>                           
                            <a href="{{ Config::get('Constant.SOCIAL_FB_LINK') }}" title="Follow Us On Facebook" target="_blank">
                                <i class="icon-facebook"></i>
                            </a>
                        </li>
                        @endif
                        @if(!empty(Config::get('Constant.SOCIAL_INSTAGRAM_LINK')))
                        <li>                            
                            <a href="{{ Config::get('Constant.SOCIAL_INSTAGRAM_LINK') }}" title="Follow Us On Instagram" target="_blank">
                                <i class="icon-instagram"></i>
                            </a>
                        </li>
                        @endif
                        @if(!empty(Config::get('Constant.SOCIAL_TWITTER_LINK')))
                        <li>                            
                            <a href="{{ Config::get('Constant.SOCIAL_TWITTER_LINK') }}" title="Follow us on Twitter" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        @endif
                        @if(!empty(Config::get('Constant.SOCIAL_LINKEDIN_LINK')))
                        <li>                            
                            <a href="{{ Config::get('Constant.SOCIAL_LINKEDIN_LINK') }}" title="Follow us on LinkedIn" target="_blank">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        @endif
                    </ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="footer-default">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-12 text-lg-left text-center">
                    <div class="copyright">{!! (!empty(Config::get('Constant.FOOTER_COPYRIGHTS'))?Config::get('Constant.FOOTER_COPYRIGHTS'):'') !!} {!! date('Y') !!} {{ Config::get('Constant.SITE_NAME') }} | All Rights Reserved.</div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-12 text-center text-xl-center text-lg-left">
                    <ul class="link">
                        <li><a title="Privacy Policy" href="{{ url('privacy-policy') }}" class="active">Privacy Policy</a></li>
                        <li><a title="Site Map" href="{{ url('sitemap') }}">Site Map</a></li>
                    </ul>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 text-lg-right text-center">
                    <div class="designed">Website Designed &amp; Developed By: <a href="javascript:void(0)" target="_blank" rel="nofollow" title="Netclues!" class="netclues_logo"></a></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer E -->

<!-- Cookies S -->
@if(Cookie::get('cookiesPopupStore') !="cookiesPopupStore")
<div class="nq-cookies">
    <div class="nq-c-info">
        This site uses cookies: <a class="nq-c-find" href="{{ url('privacy-policy') }}" title="Find out more">Find out more</a>
        <br><a class="nq-c-btn" href="javascript:void(0)" id="cookie_policy" title="Okay, Thanks">Okay, Thanks</a>
    </div>
</div>
<!-- Cookies E -->
@endif
@endif

<script> var site_url = "{{ url('') }}"; var rootUrl=site_url; </script> 
@if(!Request::ajax())
<script src="{{ url('https://use.fontawesome.com/3042c539bb.js') }}"></script>

<script src="{{ url('assets/libraries/popper/popper.min.js') }}"></script>

<script src="{{ url('assets/libraries/bootstrap/js/bootstrap.min.js') }}"></script>

<script src="{{ url('assets/libraries/animate/js/animate.js') }}"></script>

<script src="{{ url('assets/libraries/lazy/jquery.lazy.min.js') }}"></script>

<script src="{{ url('assets/libraries/materialize/js/materialize-form.js') }}"></script>

<script src="{{ url('assets/libraries/placeholder/jquery.placeholder.min.js') }}"></script>

<script src="{{ url('assets/js/html5.min.js') }}"></script>

<script src="{{ url('assets/libraries/custom/browser-upgrade/js/browser-upgrade.js') }}"></script>

<script src="{{ url('assets/libraries/custom/back-top/js/back-top.js') }}"></script>

<script src="{{ url('assets/libraries/fancybox/js/jquery.fancybox.min.js') }}"></script>

<script src="{{ url('assets/js/common.js') }}"></script>

<script src="{{ url('assets/libraries/custom/menu/js/menu.js') }}"></script>

<script src="{{ url('assets/libraries/owl.carousel/js/owl.carousel.min.js') }}"></script>

<script src="{{ url('assets/libraries/libraries-update/owl.carousel/js/owl.carousel-update.js') }}"></script>

<script src="{{ url('assets/libraries/jquery-validation/js/jquery.validate.min.js') }}"></script>

<script src="{{ url('assets/libraries/jquery-validation/js/additional-methods.min.js') }}"></script>

<script src="{{ url('assets/libraries/jquery-validation/js/jquery.validate-function.js') }}"></script>

@endif
</div>
<!-- Wrapper E -->

<!-- Google Capcha Key S -->
@if(!Request::ajax())
	<script>
	var onloadCallback = function() {
	    grecaptcha.render('html_element', {
	        'sitekey': '{{Config::get("Constant.GOOGLE_CAPCHA_KEY")}}'
	    });
	};
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	</script>
	<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
	
@endif
<!-- Google Capcha Key E -->

<!-- JS S -->
	@if(!Request::ajax())@yield('footer_scripts')@endif
<!-- JS E -->
</body>
</html>
