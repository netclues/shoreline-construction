@section('css')
<link href="{{ url('resources/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet"
  type="text/css" />
@endsection
@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection
@section('content')
@php $settings = json_decode(Config::get("Constant.MODULE.SETTINGS")); @endphp
@include('powerpanel.partials.breadcrumbs')
<div class="col-md-12 settings">
  @if(Session::has('message'))
  <div class="alert alert-success">
    <button class="close" data-close="alert"></button>
    {{ Session::get('message') }}
  </div>
  @endif
  <div class="row">
    <div class="portlet light bordered">
      <div class="portlet-body">
        <div class="tabbable tabbable-tabdrop">
          <div class="tab-content">
            <div class="row">
              <div class="col-md-12">
                <div class="portlet-body form_pattern">
                  {!! Form::open(['method' => 'post','enctype' => 'multipart/form-data','id'=>'frmBanner']) !!}
                  <div class="form-body">
                    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }} form-md-line-input">
                      {!! Form::text('title', isset($banners->varTitle)?$banners->varTitle:old('title'),
                      array('maxlength' => 150,'class' => 'form-control input-sm maxlength-handler', 'data-url' =>
                      'powerpanel/banners','id' => 'title','placeholder' =>
                      trans('template.common.title'),'autocomplete'=>'off')) !!}
                      <label class="form_title" for="title">{!! trans('template.common.title') !!} <span
                          aria-required="true" class="required"> * </span></label>
                      <span style="color:#e73d4a">
                        {{ $errors->first('title') }}
                      </span>
                    </div>
                    <div class="form-group {{ $errors->has('sub_title') ? ' has-error' : '' }} form-md-line-input">
                      {!! Form::text('sub_title', isset($banners->varSubTitle)?$banners->varSubTitle:old('sub_title'),
                      array('maxlength' => 150,'class' => 'form-control input-sm maxlength-handler', 'data-url' =>
                      'powerpanel/banners','id' => 'sub_title','placeholder' =>
                      trans('template.common.sub_title'),'autocomplete'=>'off')) !!}
                      <label class="form_title" for="sub_title">{!! trans('template.common.sub_title') !!}</label>
                      <span style="color:#e73d4a">
                        {{ $errors->first('sub_title') }}
                      </span>
                    </div>
										

                    @if ((isset($banners->varBannerVersion) && $banners->varBannerVersion == 'img_banner') ||
                    old('bannerversion')=='img_banner' || (!isset($banners->varBannerVersion) && old('bannerversion') ==
                    null))
                    	@php $checked_yes = 'checked' @endphp
                    @else
                    	@php $checked_yes = '' @endphp	
                    @endif
                    <!-- @if ((isset($banners->varBannerVersion) && $banners->varBannerVersion == 'vid_banner') ||
                    old('bannerversion')=='vid_banner')
                    	@php $ichecked_vid_yes = 'checked' @endphp	
                    @else
                    	@php $ichecked_vid_yes = '' @endphp
                    @endif -->
                    
                    <div class="form-group bannerversion {{ $errors->has('bannerversion') ? ' has-error' : '' }}">
                      <label class="form_title" for="bannerversion">{!! trans('template.bannerModule.version') !!} <span
                          aria-required="true" class="required"> * </span></label>
                      <div class="md-radio-inline">
                        <div class="md-radio">
                          <input type="radio" {{ $checked_yes }} value="img_banner" id="img_banner" name="bannerversion"
                            class="md-radiobtn versionradio">
                          <label for="img_banner">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> {!! trans('template.bannerModule.imageBanner') !!}
                          </label>
                        </div>
                        <!-- <div class="md-radio">
                          <input type="radio" {{ $ichecked_vid_yes }} value="vid_banner" id="vid_banner"
                            name="bannerversion" class="md-radiobtn versionradio">
                          <label for="vid_banner">
                            <span class="inc"></span>
                            <span class="check"></span>
                            <span class="box"></span> {!! trans('template.bannerModule.videoBanner') !!}
                          </label>
                        </div> -->
                      </div>
                      <span class="help-block">
                        <strong>{{ $errors->first('bannerversion') }}</strong>
                      </span>
                    </div>

                    @include('powerpanel.partials.imageControl',['type' => 'single','label' =>
                    trans('template.bannerModule.selectBanner').' <span aria-required="true" class="required"> * </span>' ,'data'=> isset($banners)?$banners:null , 'id' =>
                    'banner_image', 'name' => 'img_id_image_upload', 'settings' => $settings,'width' => '1920', 'height'
                    => '767'])

                    <!-- @include('powerpanel.partials.videoControl',['type' => 'single' ,'label' =>
                    trans('template.bannerModule.selectVideo').' <span aria-required="true" class="required"> * </span>','data'=> isset($banners)?$banners:null, 'id' =>
                    'banner_video','videoData'=> isset($videoDataForSingle)?$videoDataForSingle:null, 'name' => 'video_id']) -->

                    <div class="form-group">
                      <label class="form_title" for="description">{!! trans('template.common.description') !!}</label>
                      {!!
                      Form::textarea('description',isset($banners->txtDescription)?$banners->txtDescription:old('description'),
                      array('class' => 'form-control','id'=>'txtDescription')) !!}
                    </div>
                    <h3 class="form-section">{!! trans('template.common.displayinformation') !!}</h3>
                    <div class="row">
                      <div class="col-md-6">
                        @php
                        $display_order_attributes = array('class' => 'form-control','autocomplete'=>'off');
                        @endphp
                        <div
                          class="form-group @if($errors->first('display_order')) has-error @endif form-md-line-input">
                          {!!
                          Form::text('display_order',isset($banners->intDisplayOrder)?$banners->intDisplayOrder:$total_banner,
                          $display_order_attributes) !!}
                          <label class="form_title" for="display_order">{!! trans('template.common.displayorder') !!}
                            <span aria-required="true" class="required"> * </span></label>
                          <span class="help-block">
                            <strong>{{ $errors->first('display_order') }}</strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        @include('powerpanel.partials.displayInfo',['display' =>
                        isset($banners->chrPublish)?$banners->chrPublish:''])
                      </div>
                    </div>
                  </div>
                  <div class="form-actions">
                    <div class="row">
                      <div class="col-md-12">
                        <button type="submit" name="saveandedit" class="btn btn-green-drake" value="saveandedit">{!!
                          trans('template.common.saveandedit') !!}</button>
                        <button type="submit" name="saveandexit" class="btn btn-green-drake" value="saveandexit">{!!
                          trans('template.common.saveandexit') !!}</button>
                        <a class="btn red btn-outline" href="{{ url('powerpanel/banners') }}">{!!
                          trans('template.common.cancel')!!}</a>
                      </div>
                    </div>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="{{ url('resources/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript">
</script>
@include('powerpanel.partials.ckeditor')
<script type="text/javascript">
window.site_url = '{!! url("/") !!}';
var selectedRecord = '{{ isset($banners->fkIntPageId)?$banners->fkIntPageId:'' }}';
var user_action = "{{ isset($blog)?'edit':'add' }}";
</script>
<script src="{{ url('resources/pages/scripts/custom.js') }}" type="text/javascript"></script>>
<script src="{{ url('resources/pages/scripts/banners.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
  type="text/javascript"></script>
<script type="text/javascript">
$('.maxlength-handler').maxlength({
  limitReachedClass: "label label-danger",
  alwaysShow: true,
  threshold: 5,
  twoCharLinebreak: false
});
</script>
@endsection