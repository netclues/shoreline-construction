@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif

@if(!empty($services) && count($services)>0)

<section class="page_section service_listing">
    <div class="container">
        @if (isset($PAGE_CONTENT) && !empty($PAGE_CONTENT) || $services->total() > $services->perPage())
            <!-- <div class="row">
                @if(!empty($PAGE_CONTENT))
                    <div class="col-12 mb-xs-30">
                        {!! $PAGE_CONTENT !!}
                    </div>
                @endif
                @if($services->total() > $services->perPage())
                    <div class="col-12 mb-xs-30">
                        {{ $services->links() }}
                    </div>
                @endif
            </div> -->
        @endif
               
        <div class="row">
            <div class="col-12">
                <div class="tab_overflow">
                    <ul class="nav nav-tabs mb-5" role="tablist">
                        <?php /*<li class="nav-item">
                          <a class="nav-link active" title="Design Services" data-toggle="tab" href="#design-services">{!! $cat !!}</a>
                        </li> */ ?>
                        @foreach($ServiceCategory as $cat)
                        <li class="nav-item">
                          <a class="nav-link" title="{!! $cat !!}" data-toggle="tab" href="#{!! $cat !!}">{!! $cat !!}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        
        
        <div class="tab-content">
            <div id="Design Services" class="tab-pane active"> 
                 <div class="owl-carousel owl-theme owl-nav-absolute">
                	<!-- @foreach($services as  $index => $service)  -->
                        <div class="item"> 
        	                <article class="image_hover">
                                <div class="thumbnail-container">
                                    <a href="{{ route('serviceDetail',$service->alias->varAlias) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}" class="thumbnail">
                                        <picture>
                                            <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($service->fkIntImgId) !!}">
                                            <img src="{!! App\Helpers\resize_image::resize($service->fkIntImgId,360,240) !!}" alt="{{ htmlspecialchars_decode($service->varTitle) }}">
                                        </picture>
                                        <span class="mask"></span>
                                    </a>
                                </div>
                                <div class="content">
                                    @if (isset($service->varFontAwesomeIcon) && !empty($service->varFontAwesomeIcon))
                                        <!-- <div class="icon"><i class="fa {{ $service->varFontAwesomeIcon }}"></i></div> -->
                                    @endif
                                    <h3 class="name"><a href="{{ route('serviceDetail',$service->alias->varAlias) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}">{{ htmlspecialchars_decode(str_limit($service->varTitle, 50)) }}</a></h3>
                                    <!-- <div class="info owl-flex-text">
                                        <p>{{ htmlspecialchars_decode(str_limit($service->txtShortDescription, 150)) }}</p>
                                    </div> -->
                                    <!-- <div><a href="{{ route('serviceDetail',$service->alias->varAlias) }}" title="Read More" class="btn btn-primary mt-xs-15">Read More</a></div> -->
                                </div>
        	                </article> 
                        </div> 
                    <!-- @endforeach -->
                </div> 
            </div>
            @foreach($services as $services)
            <div id="construction-services" class="tab-pane">
                <div class="owl-carousel owl-theme owl-nav-absolute">
                    @foreach($services as  $index => $service)
                        <div class="item">
                            <article class="image_hover">
                                <div class="thumbnail-container">
                                    <a href="{{ route('serviceDetail',$service->alias->varAlias) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}" class="thumbnail">
                                        <picture>
                                            <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($service->fkIntImgId) !!}">
                                            <img src="{!! App\Helpers\resize_image::resize($service->fkIntImgId,360,240) !!}" alt="{{ htmlspecialchars_decode($service->varTitle) }}">
                                        </picture>
                                        <span class="mask"></span>
                                    </a>
                                </div>
                                <div class="content">
                                    @if (isset($service->varFontAwesomeIcon) && !empty($service->varFontAwesomeIcon))
                                        <!-- <div class="icon"><i class="fa {{ $service->varFontAwesomeIcon }}"></i></div> -->
                                    @endif
                                    <h3 class="name"><a href="{{ route('serviceDetail',$service->alias->varAlias) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}">{{ htmlspecialchars_decode(str_limit($service->varTitle, 50)) }}</a></h3>
                                    <!-- <div class="info owl-flex-text">
                                        <p>{{ htmlspecialchars_decode(str_limit($service->txtShortDescription, 150)) }}</p>
                                    </div> -->
                                    <!-- <div><a href="{{ route('serviceDetail',$service->alias->varAlias) }}" title="Read More" class="btn btn-primary mt-xs-15">Read More</a></div> -->
                                </div>
                            </article>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
        
        @if($services->total() > $services->perPage())
            <div class="row mt-xs-30">
                <div class="col-12">
                    {{ $services->links() }}
                </div>
            </div>
        @endif
    </div>
</section>
@else
<section class="page_section service_01">
    <div class="container">
    	<div class="row">
    		<div class="col-12">
    			{!! $PAGE_CONTENT !!}
    		</div>
    	</div>
    </div>
</section>
@endif

@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/services.js') }}"></script>

@endsection

@endsection
@endif