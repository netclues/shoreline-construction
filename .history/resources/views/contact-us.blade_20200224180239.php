@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif



<!-- contact_01 S -->
<section class="page_section contact_01">
	<div class="container">
		<div class="row">
			@if(!empty($PAGE_CONTENT))
				<div class="col-md-12">
				  	{!! $PAGE_CONTENT !!}
				</div>
			@endif

			<div class="col-lg-8 col-md-7 contact-form">
				<!-- <h2 class="nqtitle">Get in Touch</h2> -->
				{!! Form::open(['method' => 'post','class'=>'nqform nq-form-md', 'id'=>'contact_page_form']) !!}
					<div class="row align-items-start">
                        <div class="col-md-12 text-right">
                            <div class="required">* Denotes Required Inputs</div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                            	<label class="nq-label" for="first_name">First Name<span class="star">*</span></label>
                                {!! Form::text('first_name', old('first_name'), array('id'=>'first_name', 'class'=>'form-control nq-input', 'name'=>'first_name', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
                                @if ($errors->has('first_name'))
                                	<span class="error">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="nq-label" for="first_name">Last Name<span class="star">*</span></label>
                                {!! Form::text('first_name', old('first_name'), array('id'=>'first_name', 'class'=>'form-control nq-input', 'name'=>'first_name', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
                                @if ($errors->has('first_name'))
                                    <span class="error">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="nq-label" for="contact_email">Email<span class="star">*</span></label>
                                {!! Form::email('contact_email', old('contact_email'), array('id'=>'contact_email', 'class'=>'form-control nq-input', 'name'=>'contact_email', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
                                @if ($errors->has('contact_email'))
                                    <span class="error">{{ $errors->first('contact_email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="nq-label" for="contact_email">Area of interest<span class="star">*</span></label>
                                {!! Form::email('contact_email', old('contact_email'), array('id'=>'contact_email', 'class'=>'form-control nq-input', 'name'=>'contact_email', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
                                @if ($errors->has('contact_email'))
                                    <span class="error">{{ $errors->first('contact_email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                            	<label class="nq-label" for="phone_number">Phone<span class="star">*</span></label>
                            	{!! Form::text('phone_number', old('phone_number'), array('id'=>'phone_number', 'class'=>'form-control nq-input', 'name'=>'phone_number', 'maxlength'=>"20", 'onpaste'=>'return false;', 'ondrop'=>'return false;', 'onkeypress'=>'javascript: return KeycheckOnlyPhonenumber(event);')) !!}
                            	@if ($errors->has('phone_number'))
                            		<span class="error">{{ $errors->first('phone_number') }}</span>
                            	@endif
                            </div>
                        </div>                        
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                            	<label class="nq-label" for="user_message">Comments</label>
                            	{!! Form::textarea('user_message', old('user_message'), array('class'=>'form-control nq-textarea', 'name'=>'user_message', 'rows'=>'6', 'id'=>'user_message', 'spellcheck'=>'true', 'onpaste'=>'return false;', 'ondrop'=>'return false;' )) !!}
                            	@if ($errors->has('user_message'))
                            		<span class="error">{{ $errors->first('user_message') }}</span>
                            	@endif
                            </div>
                        </div>
                        @if(File::exists(app_path().'/NewsletterLead.php'))
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <div class="nq-checkbox-list">
                                    <label class="nq-checkbox pt-xs-0">
                                        <input name="subscribe" type="checkbox"> Subscribe me to your newsletter as well<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-lg-6 col-md-12 col-sm-6 text-md-center">
                            <div class="form-group">
                              	<div id="contact_html_element" class="g-recaptcha"></div>
                              	<div class="capphitcha" data-sitekey="{{Config::get('Constant.GOOGLE_CAPCHA_KEY')}}">
                                @if ($errors->has('g-recaptcha-response'))
                                	<label class="error help-block">{{ $errors->first('g-recaptcha-response') }}</label>
                                @endif
                              	</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-6 text-lg-right text-md-center text-sm-right form-btn text-center">
                            <div class="form-group">
                                <button type="submit" class="btn-primary" title="Submit">Submit</button>
                            </div>
                        </div>
                    </div>
				{!! Form::close() !!}
			</div>
			<div class="col-lg-4 col-md-5 contact-information">
                @if(!empty($contact_info))
                    @foreach($contact_info as $key => $value) 
                        <h2 class="nqtitle">{{ $value->varTitle }}</h2>
                        @if(!empty($value->txtAddress))
                            <div class="info">
                                <i class="icon fa fa-map-marker" aria-hidden="true"></i>
                                <div class="des">{!! nl2br($value->txtAddress) !!}</div>
                            </div>
                        @endif
                        @if(!empty($value->varPhoneNo))
                        <div class="info">
                            <i class="icon icon-phone" aria-hidden="true"></i>
                            <div class="des">
                                @php $phone = unserialize($value->varPhoneNo); @endphp
                                @foreach($phone as $p)
                                <a href="tel:{{ $p }}" target="_blank" title="Call Us On +1 345 922 Team(8326)"> +1 345 922 Team(8326)</a>
                                @endforeach
                            </div>
                        </div>
                        @endif
                        @if(!empty($value->varEmail))
                        <div class="info">
                            <i class="icon icon-email" aria-hidden="true"></i>
                            <div class="des">
                                @php  $email = unserialize($value->varEmail); @endphp
                                @foreach($email as $e)
                                <a href="mailto:{{ $e }}" target="_blank" title="Email Us On shoreline.ky@outlook.com">shoreline.ky@outlook.com</a>
                                @endforeach
                            </div>
                        </div>
                        @endif
                    @endforeach
                @endif

                @if(
                    !empty(Config::get('Constant.SOCIAL_FB_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_TWITTER_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_YOUTUBE_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_TRIPADVISOR_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_LINKEDIN_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_INSTAGRAM_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_TUMBLR_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_PINTEREST_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_FLICKR_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_DRIBBBLE_LINK')) || 
                    !empty(Config::get('Constant.SOCIAL_RSS_FEED_LINK'))
                )
                    <div class="info contact-social">
                        <div class="title">Follow Us</div>
                        <div class="des">
                            <ul class="nqsocia">
                                @if(!empty(Config::get('Constant.SOCIAL_FB_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_FB_LINK') }}" title="Follow Us On Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                @endif
                                @if(!empty(Config::get('Constant.SOCIAL_INSTAGRAM_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_INSTAGRAM_LINK') }}" title="Follow Us On Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                @endif
                                {{--@if(!empty(Config::get('Constant.SOCIAL_TWITTER_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_TWITTER_LINK') }}" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                @endif
                                @if(!empty(Config::get('Constant.SOCIAL_YOUTUBE_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_YOUTUBE_LINK') }}" title="YouTube" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                @endif
                                @if(!empty(Config::get('Constant.SOCIAL_TRIPADVISOR_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_TRIPADVISOR_LINK') }}" title="Tripadvisor" target="_blank"><i class="fa fa-tripadvisor"></i></a></li>
                                @endif
                                @if(!empty(Config::get('Constant.SOCIAL_LINKEDIN_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_LINKEDIN_LINK') }}" title="Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                @endif                                    
                                @if(!empty(Config::get('Constant.SOCIAL_TUMBLR_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_TUMBLR_LINK') }}" title="Tumblr" target="_blank"><i class="fa fa-tumblr"></i></a></li>
                                @endif
                                @if(!empty(Config::get('Constant.SOCIAL_PINTEREST_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_PINTEREST_LINK') }}" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                                @endif
                                @if(!empty(Config::get('Constant.SOCIAL_FLICKR_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_FLICKR_LINK') }}" title="Flickr" target="_blank"><i class="fa fa-flickr"></i></a></li>
                                @endif
                                @if(!empty(Config::get('Constant.SOCIAL_DRIBBBLE_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_DRIBBBLE_LINK') }}" title="Dribbble" target="_blank"><i class="fa fa-dribbble"></i></a></li>
                                @endif
                                @if(!empty(Config::get('Constant.SOCIAL_RSS_FEED_LINK')))
                                    <li><a href="{{ Config::get('Constant.SOCIAL_RSS_FEED_LINK') }}" title="RSS Feed" target="_blank"><i class="fa fa-rss"></i></a></li>
                                @endif--}}
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
		</div>
	</div>
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d481379.59325254476!2d-80.85443700677934!3d19.508181618093694!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f25863e2fb8aa29%3A0x7045c4d38770715e!2sCayman%20Islands!5e0!3m2!1sen!2sin!4v1582152130753!5m2!1sen!2sin" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</section>
<script type="text/javascript">
  	var sitekey = '{{Config::get("Constant.GOOGLE_CAPCHA_KEY")}}';
  	var onContactloadCallback = function() {
    	grecaptcha.render('contact_html_element', {
      		'sitekey' : sitekey
    	});
  	};

	/* @php
	  	$current_adress = !empty($contact_info->txtAddress)?$contact_info->txtAddress:'';
	  	$pinaddress = explode("*", trim(preg_replace('/\s\s+/', '*', $current_adress)));
	  	$pinaddress = implode('<br/>', $pinaddress);
	@endphp
	var address = "{{ trim(preg_replace('/\s\s+/', ' ',  $current_adress)) }}";
	var pinaddress = "{!! $pinaddress !!}"; */
</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{Config::get('Constant.GOOGLE_MAP_KEY')}}&callback=initMap" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onContactloadCallback&render=explicit"></script>
<!-- contact_01 E -->
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/contact-us.js') }}"></script>

<script src="https://maps.googleapis.com/maps/api/js?key={{Config::get("Constant.GOOGLE_MAP_KEY")}}&callback=initMap" async defer></script>

@endsection

@endsection
@endif