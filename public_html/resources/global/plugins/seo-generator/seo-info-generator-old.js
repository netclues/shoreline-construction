 function generate_seocontent(formname) {

		$('body').loader(loaderConfig);	

		var postdata = $("#" + formname).serialize();
		var description = '';

		if(CKEDITOR.instances.txtDescription != undefined) 
		{
			var html=CKEDITOR.instances.txtDescription.getSnapshot();
			var dom=document.createElement("DIV");
			dom.innerHTML=html;
			var plain_text=(dom.textContent || dom.innerText);
			description = plain_text;
		}

		if (description != '' && description.length > 0) 
		{
				description = encodeURIComponent(description);
				description = description.replace(/^(?:&nbsp;|\xa0|<br \/>)$/, " ");
				description = description.replace(/&#39;/g, "'");
				description = description.replace(/&quot;/g, '"');
				description = description.replace(/&nbsp;/g, " ");
				description = description.replace(/&nbsp;/g, " ");
				description = description.replace(/<p><\/p>/g, " ");

		} else {

				if (document.getElementById('varShortDescription') != null) 
				{
						description = document.getElementById('varShortDescription').value;
						if (description != '' && description.length > 0) {
							description = encodeURIComponent(description);
							description = description.replace(/^(?:&nbsp;|\xa0|<br \/>)$/, " ");
							description = description.replace(/&#39;/g, "'");
							description = description.replace(/&quot;/g, '"');
							description = description.replace(/&nbsp;/g, " ");
							description = description.replace(/&nbsp;/g, " ");
							description = description.replace(/<p><\/p>/g, " ");
						}
				}
		}


		$.ajax({
			type: 'POST',
			url: site_url + '/powerpanel/generate-seo-content',
			data: postdata + '&ajax=Y&description=' + description,
			dataType: "JSON",
			success: function (data) 
			{
				if (data) 
				{
					if (data.meta_title != '') {
						$('#varMetaTitle').val(data.meta_title.replace(/\s+/g, " "));
					}
	
					if (data.meta_title != '') {
						$('#varMetaKeyword').val(data.meta_title.replace(/\s+/g, " "));
					}
	
					if(data.meta_description != '')
					{
						if (data.meta_description.replace(/\s+/g, " ").length < 1) {
							$('#varMetaDescription').val(data.meta_title.replace(/\s+/g, " "));
						} else {
							$('#varMetaDescription').val(data.meta_description.replace(/\s+/g, " "));
						}
					}
				}
	
			},
			complete: function () {
				$.loader.close(true);
			}
		});

}

jQuery(document).ready(function() 
{
		$('.maxlength-handler').maxlength({
			limitReachedClass: "label label-danger",
			alwaysShow: true,
			threshold: 5,
			twoCharLinebreak: false
		});
		CKEDITOR.instances.txtDescription.fire('blur');	

		if(user_action == "add")
		{
			if($('#varMetaTitle').val().length < 1 && $('#varMetaDescription').val().length < 1)
			{
				$('.seoField').on('blur',function() {
					generate_seocontent(seoFormId);
				});
			}
		}

		try {
				user_action;
		} catch (e) {
				if (e.name == "ReferenceError") 
				{
						$('.seoField').blur(function() {
								generate_seocontent(seoFormId);
						});
						$(".seoField").keypress(function(e) {
								if (e.which == 13) {
										$('input[name=title]').trigger('blur');
								}
						});
				}
		}
		
});

// $('form').on('submit',function() {
// 	if($('#varMetaTitle').val().length < 1 && $('#varMetaDescription').val().length < 1){
// 		generate_seocontent(seoFormId);
// 	}
// });



