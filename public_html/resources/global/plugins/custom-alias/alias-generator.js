// code for alias
var editContent = '';
var controller = '';
var stroke = 0;
/* for edit functionality */
var ex_alias = $('.aliasField').val();
if (ex_alias != '') {
    var dataCategoryAlias = $('select[name=parent_category_id]').children('option:selected').data('alias');

    var buidLink = '/' + moduleAlias + '/';

    if (dataCategoryAlias != undefined) {
        buidLink += dataCategoryAlias + '/' + ex_alias;
    } else {
        buidLink += ex_alias;
    }

    var links = site_url + buidLink.replace("//", "/");
    if (moduleAlias != 'static-block') {
        $('.alias-group').removeClass('hide');
    }
    $('.alias').html(links);
    // $('.alias').attr('href',links);
    $('.alias').css('text-decoration', 'none');
    $('.alias').css('cursor', 'text');
}

if (user_action == 'edit') {
    stroke++;
}

// $(document).on('click', 'button[name=saveandedit]', function () {
//     alert('TEST');
//     var ex_alias = $('.aliasField').val();
//     if(ex_alias != ''){
//        alert('Alias Generated.');
//        //onChangeAliasEvent();     
//     }
// });

/* for edit functionality */
$(document).on('change', 'select[name=parent_category_id]', function () {
    var dataCategoryAlias = $(this).children('option:selected').data('alias');
    onChangeAliasEvent(dataCategoryAlias);
});

$(document).on('change', '.hasAlias', function () {
    var categoryAlias = $('select[name=parent_category_id]').length > 0 ? $('select[name=parent_category_id]').children('option:selected').data('alias') : '';
    onChangeAliasEvent(categoryAlias);
});

function onChangeAliasEvent(dataCategoryAlias = '') {
    var str = $('.hasAlias').val();
    if (str.trim().length <= 0) {
        return false;
    } else {
        if (stroke < 1) {
            controller = $('.hasAlias').data('url');
            if ($('select[name=parent_category_id]').length == 0) {
                aliasGenerate(str, dataCategoryAlias);
                stroke++;
            } else {
                if (stroke < 1) {
                    aliasGenerate(str, dataCategoryAlias);
                }
            }
        } else {
            aliasGenerateEdit($('.aliasField').val(), dataCategoryAlias);
        }
    }
}
$('.hasAlias').on('keypress',function (e) {
    if (e.which == 13) {
        $('input[name=title]').trigger('change');
    }
});

function aliasGenerateEdit(str, dataCategoryAlias = '') {
    var buidLink = '/' + moduleAlias + '/';
    buidLink += dataCategoryAlias + '/' + str;
    var links = site_url + buidLink.replace("//", "/");
    editContent = str;
    $('.alias').html(links);
    $('.alias').css('text-decoration', 'none');
    $('.alias').css('cursor', 'text');
    $('.aliasField').val(str);
    $('#new-alias').val(str);
    $('.alias-group').removeClass('hide');
}

function aliasGenerate(str, dataCategoryAlias = '') {
    controller = $('.hasAlias').data('url');
    var oldAlias = $('input[name=oldAlias]').val();
    var ajaxurl = site_url + '/powerpanel/aliasGenerate';
    $.ajax({
        url: ajaxurl,
        data: {
            alias: str,
            oldAlias: oldAlias,
            moduleCode: moduleCode
        },
        type: "POST",
        dataType: "json",
        success: function (data) {
            if (data.alreadyExists) {
                $('#aliasAlert').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }

            var buidLink = '/' + moduleAlias + '/';
            if (dataCategoryAlias != '') {
                buidLink += dataCategoryAlias + '/' + data.alias;
            } else {
                buidLink += data.alias;
            }
            var links = site_url + buidLink.replace("//", "/");
            editContent = data.alias;
            $('.alias').html(links);
            $('.alias').css('text-decoration', 'none');
            $('.alias').css('cursor', 'text');
            $('.aliasField').val(data.alias);
            $('#new-alias').val(data.alias);
            $('.alias-group').removeClass('hide');
            displayShortCode(moduleAlias, data.alias);
          
        },
        error: function () {
            console.log('error!');
        }
    });
}

function updateAlias(newAls, oldAls) {
    controller = $('.hasAlias').data('url');
    var ajaxurl = site_url + '/powerpanel/updateAlias';
    $.ajax({
        url: ajaxurl,
        data: {
            newAls: newAls,
            oldAls: oldAls,
            moduleCode: moduleCode
        },
        type: "POST",
        dataType: "json",
        async: false,
        success: function (data) {
            var buidLink = '/' + moduleAlias + '/' + data.newAls;
            var links = site_url + buidLink.replace("//", "/");
            editContent = data.newAls;
            $('.alias').html(links);
            $('.alias').css('text-decoration', 'none');
            $('.alias').css('cursor', 'text');
            $('.aliasField').val(data.newAls);
            $('#new-alias').val(data.newAls);
            $('input[name=oldAlias]').val(data.newAls);
            $('.alias-group').removeClass('hide');
            displayShortCode(moduleAlias, data.newAls);
        },
        error: function () {
            console.log('error!');
        }
    });
}
$('.editAlias').on('click',function () {
    var updated_alias = $('.aliasField').val();
    var dataCategoryAlias = $('select[name=parent_category_id]').children('option:selected').data('alias');
    var buidLink = '/' + moduleAlias + '/';
    if (dataCategoryAlias != undefined) {
        buidLink += dataCategoryAlias + '/' + updated_alias;
    } else {
        buidLink += updated_alias;
    }
    var links = site_url + buidLink.replace("//", "/");
    var frontURL = '<a href="javascript:void(0);" class="editAlias" title="Edit"></a><a class="without_bg_icon openLink" title="Open Link" target="_blank" href="' + links + '"><i class="fa fa-external-link" aria-hidden="true"></i></a>';
    $('.alias-group').html('<div class="form-group"><label class="form_title" for="site_name">Url :</label> <a href="javascript:void;" class="alias">' + links + '</a><a href="javascript:void(0);" class="editAlias"> <i class="fa fa-edit"></i>  ' + frontURL + '  </a></div><div class="editAliasForm"><div class="form-group form-md-line-input form-md-floating-label"><input placeholder="Alias" class="form-control input-sm edited" name="new-alias" id="new-alias" value="' + updated_alias + '" type="text" maxlength="150"/><label class="form_title" for="site_name">Alias</label></div><a class="btn btn-green-drake btn-xs update-alias" href="javascript:void(0);">Update</a>&nbsp;<a class="btn btn-green-drake btn-xs regenerate-alias" href="javascript:void(0);">Regenerate</a>&nbsp<a class="btn btn-outline red btn-xs cancle-alias" href="javascript:void(0);">Cancel</a></div></div>');
});
$(document).on('click', '.update-alias', function () {
    var newAlias = $('#new-alias').val();
    if ($('#new-alias').parent('div').hasClass('has-error')) {
        return false;
    }
    var oldAlias = $('input[name=oldAlias]').val();

    if (newAlias.length > 0) {
        if ($('select[name=parent_category_id]').length == 0) {
            //aliasGenerate(newAlias);
            if (user_action == 'add') {
                aliasGenerate(newAlias);
            } else {
                updateAlias(newAlias, oldAlias);
            }

        } else {
            var dataCategoryAlias = $('select[name=parent_category_id]').children('option:selected').data('alias');
            aliasGenerate(newAlias, dataCategoryAlias);
        }
    }


    $('.editAliasForm').hide();
});
$(document).on('click', '.editAlias', function () {
    $('.editAliasForm').show();
});
$(document).on('click', '.cancle-alias', function () {
    if ($('#new-alias').parent('div').hasClass('has-error')) {
        $('#new-alias').parent('div').removeClass('has-error');
        $('#new-alias').parent('div').find('span#new-alias-error').html('');
    }
    var oldAlias = $('.aliasField').val();
    $('#new-alias').val(oldAlias);
    $('.editAliasForm').hide();
});
$(document).on('click', '.regenerate-alias', function () {
    if ($('#new-alias').parent('div').hasClass('has-error')) {
        return false;
    }
    stroke = 0;
    if ($('select[name=parent_category_id]').length == 0) {
        $('.hasAlias').trigger('change');
    } else {
        $('select[name=parent_category_id]').trigger('change');
    }

    if ($('#new-alias').parent('div').hasClass('has-error')) {
        $('#new-alias').parent('div').removeClass('has-error');
        $('#new-alias').parent('div').find('span#new-alias-error').html('');
    }

});
// code for alias
/*validation for alias */
jQuery.validator.addMethod("specialCharacterCheck", function (value, element) {
    var re = /[`~!@#$%^&*()_|+\=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(value);
    if (!isSplChar) {
        return true;
    }
}, "Special character not allowed.");

function displayShortCode(moduleAlias, gnAlias) {
    if (moduleAlias == 'static-block') {
        $("#shortCodeDiv").find('#shortCode').html(gnAlias);
        $("#shortCodeDiv").show();
        $(".alias-group").hide();
    } else {
        $(".alias-group").show();
        $("#shortCodeDiv").hide();
    }
}