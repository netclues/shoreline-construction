var pageDelete='Are you sure you want to delete permanently selected page(s)? Deleted page(s) will be removed from the menu.';
var DELETE_ATLEAST_ONE= Lang.get('template.oneRecordDeleted');
var DELETE_CONFIRM_MESSAGE = (window.location.href.indexOf("page") > -1)? pageDelete :  Lang.get('template.selectedDeleted');
var alias = '';
$(document).ready(function() 
{

		$(document).on('click', '.delete', function(e) {    	
				e.preventDefault();
				var controller = $(this).data('controller');
				alias = $(this).data('alias');
				deleteItemModal();
				$('#confirm .delMsg').text(DELETE_CONFIRM_MESSAGE);
				$('#delete').show();
		});


		$(document).on('click', '#delete', function() {
				deleteItem(DELETE_URL, alias);
		});

		$(document).on('click', '#deleteAll', function() {
				Delete_Records();
		});

		$(document).on('click', '.deleteMass', function(e) {
				e.preventDefault();
				var CheckedLength = $("input[type='checkbox'][class='chkDelete']:checked").length;
				if (CheckedLength == 0) {
						$('#confirmForAll .delMsg').text(DELETE_ATLEAST_ONE);
						$('#deleteAll').hide();
				}
				if (CheckedLength > 0) {
						$('#confirmForAll .delMsg').text(DELETE_CONFIRM_MESSAGE);
						$('#deleteAll').show();
				}
				deleteMultiple();
		});

	//select all checkboxes
	$(".group-checkable").on('change',function(){  //"select all" change 
		  $(".chkDelete").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
	});

	//".checkbox" change 
		$(document).on('click', '.chkDelete', function(e) {
  //	$('.chkDelete').on('click',function(){ 
		//uncheck "select all", if one of the listed checkbox item is unchecked
	    if(false == $(this).prop("checked")){ //if this item is unchecked
	    		$(".table-responsive thead .checker span").removeClass();
	        $(".group-checkable").prop('checked', false); //change "select all" checked status to false
	    }
		//check "select all" if all checkbox items are checked
		if ($('.chkDelete:checked').length == $('.chkDelete').length ){
			$(".group-checkable").prop('checked', true);
			$(".table-responsive thead .checker span").addClass('checked');
		}
	});

	$('#image_sortable').sortable({
		//axis: 'x',
		stop: function(event, ui) {
				var oData = $(this).sortable('serialize');
				var aData = $(this).sortable('toArray');
				var imageIdsStrings = aData.join(',');
				var imageIds = imageIdsStrings.replace(/item-/g, '');
				$('.media_manager').parents('.fileinput.fileinput-new').find("input[name^='img_id']").val(imageIds);
		}
	});

});

function deleteItem(ajaxUrl, slug) 
{

		$.ajax({
				url: ajaxUrl,
				data: {
						ids: [slug]
				},
				type: "POST",
				dataType: "HTML",
				success: function(data) {
						$('#confirm').modal('hide');
						var $lmTable = $(".dataTable").dataTable({
								bRetrieve: true
						});
						$lmTable.fnDraw(false);
				},
				error: function() {
						console.log('error!');
				},
				async:false
		});
}

function Delete_Records() 
{
		var matches = [];
		$(".chkDelete:checked").each(function() {
				matches.push(this.value);
		});
		jQuery.ajax({
				type: "POST",
				url: DELETE_URL,
				data: {
						"ids": matches
				},
				async: false,
				success: function(result) {
						$('#confirmForAll').modal('hide');
						var $lmTable = $(".dataTable").dataTable({
								bRetrieve: true
						});
						$lmTable.fnDraw(false);

				}
		});
}

function deleteMultiple() {
		$('#confirmForAll').modal({
				backdrop: 'static',
				keyboard: false
		});
}

function deleteItemModal() {
		$('#confirm').modal({
				backdrop: 'static',
				keyboard: false
		});
}

function moveToModal() {
		$('#moveTo').modal({
				backdrop: 'static',
				keyboard: false
		});
}

$('input[name="display_order"]').on('keypress',function (event) {
            return isNumber(event, this);
});

function isNumber(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (
        (charCode < 48 || charCode > 57)) // allow only digit.
        return false;

    return true;
}

 $(document).on('click', '#go', function() {
		jQuery.ajax({
				type: "POST",
				url: $(this).data('module')+'/swaporder',
				data: {						
						"order": $('#moveTo input[name=order]').val(),
						"exOrder": $('#moveTo input[name=exorder]').val()
				},
				async: false,
				success: function(result) {
					$('#moveTo').modal('hide');
				},
				complete:function(){
					var $lmTable = $(".dataTable").dataTable({
							bRetrieve: true
					});
					$lmTable.fnDraw(false);
				}
		});
});

$(document).on('click', '#refresh', function(e) 
{
  $("#statusfilter, #moduleFilter, #actionFilter, #pageFilter, #bannerFilter, #bannerFilterType,#searchfilter,.categoryFilter,#category_id,#projectStatusfilter").val('').trigger('change');
  $("#testimonialdate").val('');
  $('#dateFilter').trigger('click');
  $('#searchfilter').keyup();
  //$('#searchfilter').on('keyup');
});

//Miltiplease category in corect Way....
$(document).ready(function(){

	$("#category_id").select2({
      placeholder: "Select Category",
      minimumResultsForSearch: 5
   }).on("change.select2", function(e)
   {
      $($(this).children('option:checked')).each(function(){      
          if($(this).val()=='addCat')
          {
            $(this).prop('selected',false);       
            $(this).trigger("change");
            showAddCat();       
          }
        });
      $('#addCatModal input').val(null);
      filterSelectedCatName();
   });  

  $('#addCat').on('click',function() 
  {
      var selectedCat = [];

      var title = $.trim($('#addCatModal input[name=title]').val());
      var parentCategory = $('#parent_category_id').val();
      if($('#category_id').val() != "" && $('#category_id').val() != null)
      {
        var selectedCat = $('#category_id').val();  
      }
      if(parentCategory==""){
          parentCategory = 0;
      }

      if(title.length<1 || title==''){
        $('#addCatModal #catErr').removeClass('hide');
      }else{
        $('#addCatModal #catErr').addClass('hide');
        jQuery.ajax({
            type: "POST",
            url: window.site_url+'/powerpanel/'+$(this).data('module')+'/ajaxCatAdd',
            data: {           
                "varTitle": $('#addCatModal input[name=title]').val(),
                "selectedCat":selectedCat,
                "parent_category_id":parentCategory
            },
            dataType:'json',
            async: false,
            success: function(result) 
            {
             	$('#addCatModal').modal('hide'); 
              	$("#categoryDropdown").html( result.categoriesHtml);  
	           	$("#category_id").select2({
	            	placeholder: "Select Category"
	           	});
               	filterSelectedCatName();
            }
        });
      }
  });
  filterSelectedCatName();
  
});

function showAddCat()
{
  $('#addCatModal').modal({
      backdrop: 'static',
      keyboard: false
  }); 
}

// $("#statusfilter").select2({
//     minimumResultsForSearch: Infinity
// });

function filterSelectedCatName()
{
  $('.select2-selection__choice').each(function(){
    var title = $(this).attr('title');  
    title = title.trim();
    title = title.replace('|_',"");     
      var a=$(this).first().contents().filter(function() {
        return this.nodeType == 3;
      }).replaceWith(title);        
  });
}

