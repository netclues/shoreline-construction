$("select").select2({
    width: '100%'
});
var fieldIdArr = [];
var htmlStr = '<tr class="item" style="display: table-row;"></tr>';
var commonComponent = $(htmlStr).filter("tr").load(window.site_url + '/powerpanel/module-builder/field_template');
$(window).on('load', function() {
    var item = $(commonComponent).clone();
    renderPrimaryData(item);
    initializeCheckbox(item);
    $("#container").append(item);
    var item_title = $(commonComponent).clone();
    renderVarTitleData(item_title);
    initializeCheckbox(item_title);
    $("#container").append(item_title);
    var item_publish = $(commonComponent).clone();
    renderChrPublishData(item_publish);
    initializeCheckbox(item_publish);
    $("#container").append(item_publish);
    var item_delete = $(commonComponent).clone();
    renderChrDeleteData(item_delete);
    initializeCheckbox(item_delete);
    $("#container").append(item_delete);
    var item_created_at = $(commonComponent).clone();
    $(item_created_at).find('.txtFieldName').val("created_at");
    $(item_created_at).find('.txtFieldTitle').val("Created At");
    renderTimeStampData(item_created_at);
    initializeCheckbox(item_created_at);
    $("#container").append(item_created_at);
    var item_updated_at = $(commonComponent).clone();
    $(item_updated_at).find('.txtFieldName').val("updated_at");
    $(item_updated_at).find('.txtFieldTitle').val("Updated At");
    renderTimeStampData(item_updated_at);
    initializeCheckbox(item_updated_at);
    $("#container").append(item_updated_at);
});
$(document).ready(function() {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
    $("#chkDisplayOrder").on("ifChanged", function() {
        var checked = $(this).prop('checked');
        if (checked) {
            var item_display_order = $(commonComponent).clone();
            renderDisplayOrderData(item_display_order);
            initializeCheckbox(item_display_order);
            $("#container").append(item_display_order);
        } else {
            removeField();
        }
    });
    $("#btnAdd").on("click", function() {
        var item = $(commonComponent).clone();
        initializeCheckbox(item);
        $("#container").append(item);
    });
    $("#btnTimeStamps").on("click", function() {
        var item_created_at = $(commonComponent).clone();
        $(item_created_at).find('.txtFieldName').val("created_at");
        renderTimeStampData(item_created_at);
        initializeCheckbox(item_created_at);
        $("#container").append(item_created_at);
        var item_updated_at = $(commonComponent).clone();
        $(item_updated_at).find('.txtFieldName').val("updated_at");
        renderTimeStampData(item_updated_at);
        initializeCheckbox(item_updated_at);
        $("#container").append(item_updated_at);
    });
    $("#btnPrimary").on("click", function() {
        var item = $(commonComponent).clone();
        renderPrimaryData(item);
        initializeCheckbox(item);
        $("#container").append(item);
    });
    $("#btnModelReset").on("click", function() {
        $("#container").html("");
        $('input:text').val("");
        $('input:checkbox').iCheck('uncheck');
    });
    $("#form").on("submit", function() {
        $('body').loader(loaderConfig);
        var fieldArr = [];
        $('.item').each(function() {
            fieldArr.push({
                title: $(this).find('.txtFieldTitle').val(),
                name: $(this).find('.txtFieldName').val(),
                dbType: $(this).find('.txtdbType').val(),
                htmlType: $(this).find('.drdHtmlType').val(),
                validations: $(this).find('.txtValidation').val(),
                searchable: $(this).find('.chkSearchable').prop('checked'),
                primary: $(this).find('.chkPrimary').prop('checked'),
                inForm: $(this).find('.chkInForm').prop('checked')
            });
        });
        var data = {
            moduleTitle: $('#txtModuleTitle').val(),
            modelName: $('#txtModelName').val(),
            tableName: $('#txtCustomTblName').val(),
            options: {
                displayOrder: $('#chkDisplayOrder').prop('checked')
            },
            fields: fieldArr
        };
        data['_token'] = $('#token').val();
        $.ajax({
            url: window.site_url + '/powerpanel/module-builder/generate',
            method: "POST",
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function(result) {
                toastr.options = 
                {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                $.loader.close(true);
                if (result.success) 
                {
                    $('.module_url').css('display', 'block');
                    $('.module_url').html('<button class="close" data-close="alert"></button>' + result.url);
                    toastr.success(result.success);
                } else if (result.error) {
                    toastr.error(result.error);
                }
            },
            error: function(result, error) {
                $.loader.close(true);
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
                toastr.error('Fail! Result');
            }
        });
        return false;
    });
});

function renderPrimaryData(el) {
    $('.chkPrimary').iCheck(getiCheckSelection(false));
    $(el).find('.txtFieldTitle').val("ID");
    $(el).find('.txtFieldName').val("id");
    $(el).find('.txtdbType').val("increments");
    $(el).find('.txtValidation').val('required');
    $(el).find('.chkSearchable').attr('checked', false);
    $(el).find('.chkPrimary').attr('checked', true);
    $(el).find('.chkInForm').attr('checked', true);
    $(el).find('.txtFieldTitle').attr("disabled", "disabled");
    $(el).find('.txtFieldName').attr("disabled", "disabled");
    $(el).find('.txtdbType').attr("disabled", "disabled");
    $(el).find('.txtValidation').attr("disabled", "disabled");
    $(el).find('.drdHtmlType').attr("disabled", "disabled");
    $(el).find('.chkPrimary').attr("disabled", "disabled");
    $(el).find('.chkSearchable').attr("disabled", "disabled");
    $(el).find('.chkInForm').attr("disabled", "disabled");
    $(el).find('.remove').remove();
}

function renderChrPublishData(el) {
    $(el).find('.txtFieldTitle').val("Publish");
    $(el).find('.txtFieldName').val("chrPublish");
    $(el).find('.txtdbType').val("char");
    $(el).find('.drdHtmlType').val("radio");
    $(el).find('.txtValidation').val('required');
    $(el).find('.chkSearchable').attr('checked', false);
    $(el).find('.chkPrimary').attr('checked', false);
    $(el).find('.chkInForm').attr('checked', true);
    $(el).find('.txtFieldTitle').attr("disabled", "disabled");
    $(el).find('.txtFieldName').attr("disabled", "disabled");
    $(el).find('.txtdbType').attr("disabled", "disabled");
    $(el).find('.txtValidation').attr("disabled", "disabled");
    $(el).find('.drdHtmlType').attr("disabled", "disabled");
    $(el).find('.chkSearchable').attr("disabled", "disabled");
    $(el).find('.chkPrimary').attr("disabled", "disabled");
    $(el).find('.chkInForm').attr("disabled", "disabled");
    $(el).find('.remove').remove();
}

function renderVarTitleData(el) {
    $(el).find('.txtFieldTitle').val("Title");
    $(el).find('.txtFieldName').val("varTitle");
    $(el).find('.txtdbType').val("string");
    $(el).find('.drdHtmlType').val("text");
    $(el).find('.txtValidation').val('required');
    $(el).find('.chkSearchable').attr('checked', true);
    $(el).find('.chkPrimary').attr('checked', false);
    $(el).find('.chkInForm').attr('checked', true);
    $(el).find('.txtFieldName').attr("disabled", "disabled");
    $(el).find('.txtdbType').attr("disabled", "disabled");
    $(el).find('.txtValidation').attr("disabled", "disabled");
    $(el).find('.drdHtmlType').attr("disabled", "disabled");
    $(el).find('.chkSearchable').attr("disabled", "disabled");
    $(el).find('.chkPrimary').attr("disabled", "disabled");
    $(el).find('.chkInForm').attr("disabled", "disabled");
    $(el).find('.remove').remove();
}

function renderDisplayOrderData(el) {
    $(el).find('.txtFieldTitle').closest('.item').addClass('display_order')
    $(el).find('.txtFieldTitle').val("Display Order");
    $(el).find('.txtFieldName').val("intDisplayOrder");
    $(el).find('.txtdbType').val("integer");
    $(el).find('.drdHtmlType').val("text");
    $(el).find('.txtValidation').val('required');
    $(el).find('.chkSearchable').attr('checked', false);
    $(el).find('.chkPrimary').attr('checked', false);
    $(el).find('.chkInForm').attr('checked', true);
    $(el).find('.txtFieldName').attr("disabled", "disabled");
    $(el).find('.txtdbType').attr("disabled", "disabled");
    $(el).find('.txtValidation').attr("disabled", "disabled");
    $(el).find('.drdHtmlType').attr("disabled", "disabled");
    $(el).find('.chkSearchable').attr("disabled", "disabled");
    $(el).find('.chkPrimary').attr("disabled", "disabled");
    $(el).find('.chkInForm').attr("disabled", "disabled");
    $(el).find('.remove').remove();
}

function removeField(el) {
    $('.display_order').remove();
}

function renderChrDeleteData(el) {
    $(el).find('.txtFieldTitle').val("Unpublish");
    $(el).find('.txtFieldName').val("chrDelete");
    $(el).find('.txtdbType').val("char");
    $(el).find('.drdHtmlType').val("radio");
    $(el).find('.txtValidation').val('required');
    $(el).find('.chkSearchable').attr('checked', false);
    $(el).find('.chkPrimary').attr('checked', false);
    $(el).find('.chkInForm').attr('checked', false);
    $(el).find('.txtFieldTitle').attr("disabled", "disabled");
    $(el).find('.txtFieldName').attr("disabled", "disabled");
    $(el).find('.txtdbType').attr("disabled", "disabled");
    $(el).find('.txtValidation').attr("disabled", "disabled");
    $(el).find('.drdHtmlType').attr("disabled", "disabled");
    $(el).find('.chkSearchable').attr("disabled", "disabled");
    $(el).find('.chkPrimary').attr("disabled", "disabled");
    $(el).find('.chkInForm').attr("disabled", "disabled");
    $(el).find('.remove').remove();
}

function renderTimeStampData(el) {
    $(el).find('.txtdbType').val("timestamp");
    $(el).find('.txtValidation').val('required');
    $(el).find('.drdHtmlType').val('timestamp');
    $(el).find('.chkSearchable').attr('checked', false);
    $(el).find('.chkPrimary').attr('checked', false);
    $(el).find('.chkInForm').attr('checked', false);
    $(el).find('.txtFieldTitle').attr("disabled", "disabled");
    $(el).find('.txtFieldName').attr("disabled", "disabled");
    $(el).find('.txtdbType').attr("disabled", "disabled");
    $(el).find('.txtValidation').attr("disabled", "disabled");
    $(el).find('.chkSearchable').attr("disabled", "disabled");
    $(el).find('.chkPrimary').attr("disabled", "disabled");
    $(el).find('.chkInForm').attr("disabled", "disabled");
    $(el).find('.drdHtmlType').attr("disabled", "disabled");
    $(el).find('.remove').remove();
}

function initializeCheckbox(el) {
    $(el).find('input:checkbox').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
    });
    $(el).find("select").select2({
        width: '100%'
    });
    // $(el).find(".chkPrimary").on("ifClicked", function() {
    //     $('.chkPrimary').each(function() {
    //         $(this).iCheck('uncheck');
    //     });
    // });
    // $(el).find(".chkPrimary").on("ifChanged", function() {
    //     if ($(this).prop('checked') == true) {
    //         $(el).find(".chkSearchable").iCheck('uncheck');
    //         $(el).find(".chkFillable").iCheck('uncheck');
    //         $(el).find(".chkInForm").iCheck('uncheck');
    //     }
    // });
    // var htmlType = $(el).find('.drdHtmlType');
    // $(htmlType).select2().on('change', function() 
    // {
    //     if ($(htmlType).val() == "radio") {
    //         $(el).find('.radioTitle').show();
    //         $(el).find('.radioValue').show();
    //     } else {
    //         $(el).find('.radioTitle').hide();
    //         $(el).find('.radioValue').hide();
    //     }
    // });
    // $(htmlType).select2().on('change', function() 
    // {
    //     if ($(htmlType).val() == "select") {
    //         $(el).find('.optionTitle').show();
    //         $(el).find('.optionValue').show();
    //     } else {
    //         $(el).find('.optionTitle').hide();
    //         $(el).find('.optionValue').hide();
    //     }
    // });
}

function getiCheckSelection(value) {
    if (value == true) return 'checked';
    else return 'uncheck';
}

function removeItem(e) {
    e.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode);
}