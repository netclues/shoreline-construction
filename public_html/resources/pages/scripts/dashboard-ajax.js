$(document).ready(function(){
	$(".cmsPages").on('click',function(){
		var cmspage_id = this.id;
		$.ajax({
			url: site_url+'/powerpanel/dashboard/ajax',
			data: { type:'cms', id:cmspage_id },
			type: "POST",         
			dataType: "json",
			success: function(data) {
				var html = '';
				if(data != null && data !='') {
					html +=	'<div class="modal-dialog">';
					html +=	'<div class="modal-vertical">';
					html += '<div class="modal-content">';
					html +=	'<div class="modal-header">';
					html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
					html += '<h3 class="modal-title">'+data.varTitle+'</h3>';
					html += '</div>';
					html += '<div class="modal-body">';
					html +=	'<p>'+data.varTitle+'</p>';
					html +=	'<p>'+data.txtDescription+'</p>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					$('.detailsCmsPage').html(html);
					$('.detailsCmsPage').modal('show');
				}
			},
			error: function() {
				console.log('error!');
			}                                 
		});
	});
	$(".contactUsLead").on('click',function()
	{
		var contactuslead_id = this.id;
		$.ajax({
			url: site_url+'/powerpanel/dashboard/ajax',
			data: { type:'contactuslead', id:contactuslead_id },
			type: "POST",         
			dataType: "json",
			success: function(data) {
				var html = '';
				if(data != null && data !='') {
					html +=	'<div class="modal-dialog">';
					html +=	'<div class="modal-vertical">';
					html += '<div class="modal-content">';
					html +=	'<div class="modal-header">';
					html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
					html += '<h3 class="modal-title">'+data.varName+'</h3>';
					html += '</div>';
					html += '<div class="modal-body">';
					html +=	'<p>Email: '+data.varEmail+'</p>';
					if(data.varPhoneNo==null || data.varPhoneNo==''){
						html +=	'<p>Phone No: '+'-'+'</p>';
					}else{
						html +=	'<p>Phone No: '+data.varPhoneNo+'</p>';	
					}
					if(data.txtUserMessage==null || data.txtUserMessage==''){
						html +=	'<p>Message: '+'-'+'</p>';
					}else{
						html +=	'<p>Message: '+data.txtUserMessage+'</p>';	
					}
					
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					$('.detailsContactUsLead').html(html);
					$('.detailsContactUsLead').modal('show');
				}
			},
			error: function() {
				console.log('error!');
			}                                 
		});
	});
	
	$(".requestLead").on('click',function()
	{		
		var requestaquotelead_id = this.id;
		$.ajax({
			url: site_url+'/powerpanel/dashboard/ajax',
			data: { type:'requestlead', id:requestaquotelead_id },
			type: "POST",         
			dataType: "json",
			success: function(data) {
				var html = '';
				if(data != null && data !='') {
					html +=	'<div class="modal-dialog">';
					html +=	'<div class="modal-vertical">';
					html += '<div class="modal-content">';
					html +=	'<div class="modal-header">';
					html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
					html += '<h3 class="modal-title">'+data.varName+'</h3>';
					html += '</div>';
					html += '<div class="modal-body">';
					html +=	'<p>Email: '+data.varEmail+'</p>';
					if(data.varPhoneNo==null || data.varPhoneNo==''){
						html +=	'<p>Phone No: '+'-'+'</p>';
					}else{
						html +=	'<p>Phone No: '+data.varPhoneNo+'</p>';	
					}
					if(data.varAreaOfInterest==null || data.varAreaOfInterest==''){
						html +=	'<p>Area Of Interest: '+'-'+'</p>';
					}else{
						html +=	'<p>Area Of Interest: '+data.varAreaOfInterest+'</p>';	
					}
					if(data.txtUserMessage==null || data.txtUserMessage==''){
						html +=	'<p>Message: '+'-'+'</p>';
					}else{
						html +=	'<p>Message: '+data.txtUserMessage+'</p>';	
					}
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					$('.detailsContactUsLead').html(html);
					$('.detailsContactUsLead').modal('show');
				}
			},
			error: function() {
				console.log('error!');
			}                                 
		});
	});
	
	$(".reservationLead").on('click',function()
	{
		var reservation_id = this.id;
		$.ajax({
			url: site_url+'/powerpanel/dashboard/ajax',
			data: { type:'reservationLead', id:reservation_id },
			type: "POST",         
			dataType: "json",
			success: function(data) {
				var html = '';
				if(data != null && data !='') {
					html +=	'<div class="modal-dialog">';
					html +=	'<div class="modal-vertical">';
					html += '<div class="modal-content">';
					html +=	'<div class="modal-header">';
					html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
					html += '<h3 class="modal-title">'+data.varName+'</h3>';
					html += '</div>';
					html += '<div class="modal-body">';
					html +=	'<p>Email: '+data.varEmail+'</p>';
					if(data.varPhoneNo==null || data.varPhoneNo==''){
						html +=	'<p>Phone No: '+'-'+'</p>';
					}else{
						html +=	'<p>Phone No: '+data.varPhoneNo+'</p>';	
					}
					if(data.intPeople==null || data.intPeople==''){
						html +=	'<p>People: '+' -'+'</p>';
					}else{
						html +=	'<p>People: '+data.intPeople+'</p>';	
					}
					
					
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					$('.detailsReservationLeads').html(html);
					$('.detailsReservationLeads').modal('show');
				}
			},
			error: function() {
				console.log('error!');
			}                                 
		});
	});

	$(".bookanappointmentLead").on('click',function()
	{
		var appointment_id = this.id;
		$.ajax({
			url: site_url+'/powerpanel/dashboard/ajax',
			data: { type:'appointmentlead', id:appointment_id },
			type: "POST",         
			dataType: "json",
			success: function(data) {
				var html = '';
				if(data != null && data !='') {
					html +=	'<div class="modal-dialog">';
					html +=	'<div class="modal-vertical">';
					html += '<div class="modal-content">';
					html +=	'<div class="modal-header">';
					html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
					html += '<h3 class="modal-title">'+data.varName+'</h3>';
					html += '</div>';
					html += '<div class="modal-body">';
					html +=	'<p>Email: '+data.varEmail+'</p>';
					if(data.varPhoneNo==null || data.varPhoneNo==''){
						html +=	'<p>Phone No: '+'-'+'</p>';
					}else{
						html +=	'<p>Phone No: '+data.varPhoneNo+'</p>';	
					}
					if(data.appointmentDate==null || data.appointmentDate==''){
						html +=	'<p>Appointment Date: '+'-'+'</p>';
					}else{
						html +=	'<p>Appointment Date: '+data.appointmentDate+'</p>';	
					}
					if(data.txtUserMessage==null || data.txtUserMessage==''){
						html +=	'<p>Message: '+'-'+'</p>';
					}else{
						html +=	'<p>Message: '+data.txtUserMessage+'</p>';	
					}
					
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					$('.DetailbookanappointmentLead').html(html);
					$('.DetailbookanappointmentLead').modal('show');
				}
			},
			error: function() {
				console.log('error!');
			}                                 
		});
	});

	$(".FaqRecord").on('click',function(){
		var faq_id = this.id;
		$.ajax({
			url: site_url+'/powerpanel/dashboard/ajax',
			data: { type:'faq', id:faq_id },
			type: "POST",         
			dataType: "json",
			success: function(data) {
				var html = '';
				if(data != null && data !='') {
					html +=	'<div class="modal-dialog">';
					html +=	'<div class="modal-vertical">';
					html += '<div class="modal-content">';
					html +=	'<div class="modal-header">';
					html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
					html += '<h3 class="modal-title">FAQ</h3>';
					html += '</div>';
					html += '<div class="modal-body">';
					html +=	'<p>'+data.question+'</p>';
					html +=	'<p>'+data.answer+'</p>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					$('.FAQDetails').html(html);
					$('.FAQDetails').modal('show');
				}
			},
			error: function() {
				console.log('error!');
			}                                 
		});
	});
	$(".BlogRecord").on('click',function(){
		var blog_id = this.id;
		$.ajax({
			url: site_url+'/powerpanel/dashboard/ajax',
			data: { type:'blog', blog_alias:blog_id },
			type: "POST",         
			dataType: "json",
			success: function(data) {
				var html = '';
				if(data != null && data !='') {
					html +=	'<div class="modal-dialog">';
					html +=	'<div class="modal-vertical">';
					html += '<div class="modal-content">';
					html +=	'<div class="modal-header">';
					html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
					html += '<h3 class="modal-title">Blog</h3>';
					html += '</div>';
					html += '<div class="modal-body">';
					html +=	'<p>'+data.title+'</p>';
					html +=	'<p>'+data.description+'</p>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					html +=	'</div>';
					$('.BlogDetails').html(html);
					$('.BlogDetails').modal('show');
				}
			},
			error: function() {
				console.log('error!');
			}
		});
	});
});
