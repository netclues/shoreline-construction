var Custom = function() {
	return {
		//main function
		init: function() {
			//initialize here something.            
		},
		checkVersion: function() {
			//var radioValue = $("input[name='bannerversion']:checked").val();
			//if (radioValue == 'img_banner') {
			//	$('.imguploader').show();
			//	$('.viduploader').addClass('hide');
			//} else {
			//	$('.imguploader').hide();
			//	$('.viduploader').removeClass('hide');
			//}
			$('#modules').select2({
				placeholder: "Select Module",
				width: '100%',
				minimumResultsForSearch: 5
			});
		},
		checkLinkType: function() {
            var checkLinkType = $("input[name='chrLinkType']:checked").val();
            if(checkLinkType=="O"){
                $("#pagetype").hide();
                $("#othertype").show();
            }                
            if(checkLinkType=="C"){
                $("#othertype").hide();
                $("#pagetype").show();
            }            
        },
			}
}();

$("input[name='chrType']").on('click',function(){
    var chrType = $("input[name='chrType']:checked").val();
    
    if(chrType=="I"){
        $("#videotype").hide();
        $("#imagetype").show();
        
    }
    
    if(chrType=="V"){
        $("#imagetype").hide();
        $("#videotype").show();
    }
    
   // alert(chrImageType);}
});

var Validate = function() {
	var handleBanner = function() {
		$("#frmBanner").validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			ignore: [],
			rules: {
				title: {
					required: true,
					noSpace: true
				},
				//description: "required",
				display_order: {
					required: true,
					minStrict: true,
					number: true,
					noSpace: true
				},
				img_id_image_upload: {
					//required: {
					//	depends: function () {
					//		return $('.versionradio[value="img_banner"]:checked').length > 0;
					//	}
					//}
					required: true,
				},
				//video_id: {
				//	required: {
				//		depends: function () {
				//			return $('.versionradio[value="vid_banner"]:checked').length > 0;
				//		}
				//	}
				//}
				sub_title: {
                    xssProtection: true,
                    noSpace: true
                },
                varButtonName: {
                    xssProtection: true,
                    // noSpace: true
                },
                varButtonLink: {
                    xssProtection: true,
                    url: true
                },
			},
			messages: {
				title: Lang.get('validation.required', { attribute: Lang.get('template.title') }),
				img_id_image_upload: Lang.get('validation.required', { attribute: Lang.get('template.imagesbanner') }),
				video_id: Lang.get('validation.required', { attribute: Lang.get('template.videobanner') }),
				display_order: {
					required: Lang.get('validation.required', { attribute: Lang.get('template.displayorder') }),
					minStrict: Lang.get('validation.minStrict', { attribute: Lang.get('template.displayorder') }),
					number: Lang.get('validation.number', { attribute: Lang.get('template.displayorder') }),
					noSpace: Lang.get('validation.noSpace', { attribute: Lang.get('template.displayorder') })
				},
			},
			errorPlacement: function (error, element) { if (element.parent('.input-group').length) { error.insertAfter(element.parent()); } else if (element.hasClass('select2')) { error.insertAfter(element.next('span')); } else { error.insertAfter(element); } },
			invalidHandler: function (event, validator) { //display error alert on form submit
				var errors = validator.numberOfInvalids();
				if (errors) {
					$.loader.close(true);
				}
				$('.alert-danger', $('#frmBanner')).show();
			},
			highlight: function (element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			},
			unhighlight: function (element) {
				$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
			},
			submitHandler: function (form) { // for demo
				$('body').loader(loaderConfig);
				form.submit();
				$("button[type='submit']").attr('disabled','disabled');
				return false;
			}
		});

		$('#frmBanner input').on('keypress',function (e) {
			if (e.which == 13) {
				if ($('#frmBanner').validate().form()) {
					$('#frmBanner').submit(); //form validation success, call ajax form submit
					$("button[type='submit']").attr('disabled','disabled');
				}
				return false;
			}
		});
	}
	return {
		//main function to initiate the module
		init: function () {
			handleBanner();
		}
	};
}();
jQuery(document).ready(function () {
	Custom.init();
	Custom.checkVersion();
	Custom.checkLinkType();
	Validate.init();

	$(document).on('click', '.versionradio', function(e) {
		Custom.checkVersion();
	});
	
	$("input[name='chrLinkType']").on('click',function(){
		Custom.checkLinkType();
	});

	$(document).on('click', '.banner', function(e) {
		Custom.checkType();
	});

	jQuery.validator.addMethod("noSpace", function(value, element) {
		if (value.trim().length <= 0) {
			return false;
		} else {
			return true;
		}
	}, "Space is not allow.");
});
jQuery.validator.addMethod("phoneFormat", function (value, element) {
	// allow any non-whitespace characters as the host part
	return this.optional(element) || /((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}/.test(value);
}, 'Please enter a valid phone number.');

jQuery.validator.addMethod("minStrict", function (value, element) {
	// allow any non-whitespace characters as the host part
	if (value > 0) {
		return true;
	} else {
		return false;
	}
}, 'Display order must be a number higher than zero.');

jQuery.validator.addMethod("noSpace", function (value, element) {
	if (value.trim().length <= 0) {
		return false;
	} else {
		return true;
	}
}, "This field is required");
$('input[name=title]').on('change',function () {
	var title = $(this).val();
	var trim_title = title.trim();
	if (trim_title) {
		$(this).val(trim_title);
		return true;
	}
});




/*********** Remove Image code start Here  *************/
$(document).ready(function () {
	if ($("input[name='img_id_image_upload']").val() == '') {
		$('.removeimg').hide();
		$('.image_thumb .overflow_layer').css('display', 'none');
	} else {
		$('.removeimg').show();
		$('.image_thumb .overflow_layer').css('display', 'block');
	}

	$(document).on('click', '.removeimg', function (e) {
		$("input[name='img_id_image_upload']").val('');
		$("input[name='image_url']").val('');
		$(".fileinput-new div img").attr("src", site_url + '/resources/images/upload_file.gif');

		if ($("input[name='img_id_image_upload']").val() == '') {
			$('.removeimg').hide();
			$('.image_thumb .overflow_layer').css('display', 'none');
		} else {
			$('.removeimg').show();
			$('.image_thumb .overflow_layer').css('display', 'block');
		}
	});
});
/************** Remove Images Code end ****************/


/*********** Remove Video code start Here  *************/
$(document).ready(function () {
	if ($("input[name='video_id']").val() == '') {
		$('.removeVideo').hide();
		$('.video_thumb .overflow_layer').css('display', 'none');
	} else {
		$('.removeVideo').show();
		$('.video_thumb .overflow_layer').css('display', 'block');
	}
  
	$(document).on('click', '.removeVideo', function (e) {
		$("input[name='video_id']").val('');
		$("#video_name").val('');

		$(".video-fileinput div img").attr("src", site_url + '/resources/images/video_upload_file.gif');
		if ($("input[name='video_id']").val() == '') {
			$('.removeVideo').hide();
			$('.video_thumb .overflow_layer').css('display', 'none');
		} else {
			$('.removeVideo').show();
			$('.video_thumb .overflow_layer').css('display', 'block');
		}
	});
});
/************** Remove Video code end ****************/
jQuery.validator.addMethod("xssProtection", function(value, element) {
    return validateXSSInput(value);
}, "Please enter valid input.");

function occurrences(string, substring) {
    var n = 0;
    var pos = 0;
    while (true) {
        pos = string.indexOf(substring, pos);
        if (pos != -1) {
            n++;
            pos += substring.length;
        } else {
            break;
        }
    }
    return (n);
}

function validateXSSInput(value) {
    var count = occurrences(value, '#');
    var value1 = $("<textarea/>").html(value).val();
    for (var i = 1; i <= count; i++) {
        value1 = $("<textarea/>").html(value1).val();
    }
    if (value.match(/((\%3C)|<)((\%2F)|\/)*[a-z0-9\%]+((\%3E)|>)/i)) {
        return false;
    } else if (value.match(/<img|script[^>]+src/i)) {
        return false;
    } else if (value1.match(/((\%3C)|<)((\%2F)|\/)*[a-z0-9\%]+((\%3E)|>)/i)) {
        return false;
    } else if (value1.match(/<img|script[^>]+src/i)) {
        return false;
    } else if (value1.match(/((\%3C)|<)(|\s|\S)+((\%3E)|>)/i)) {
        return false;
    } else {
        return true;
    }
}