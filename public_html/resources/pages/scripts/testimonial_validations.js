/**
 * This method validates testimonial form fields
 * since   2016-12-24
 * author  NetQuick
 */
 var Custom = function () {
	return {
		//main function
		init: function () {
			//initialize here something.            
		},
		checkVersion: function () {
			var radioValue = $("input[name='bannerversion']:checked").val();
			if (radioValue == 'img_banner') {
				$('.imguploader').show();
				$('.viduploader').addClass('hide');
			} else {
				$('.imguploader').hide();
				$('.viduploader').removeClass('hide');
			}
			$('#modules').select2({
				placeholder: "Select Module",
				width: '100%',
				minimumResultsForSearch: 5
			});
		},
	}
}();
 var Validate = function() {
		var handleTestimonial = function() {
				 $("#frmTestimonial").validate({
					errorElement: 'span', //default input error message container
					errorClass: 'help-block', // default input error message class
					ignore:[],
					rules: {					
						testimonialby: {
							required:true,
							noSpace:true
						},					
						testimonial:{
							required:true,
						},
						img_id_image_upload: {
							required: {
								depends: function () {
									return $('.versionradio[value="img_banner"]:checked').length > 0;
								}
							}
						},
						video_id: {
							required: {
								depends: function () {
									return $('.versionradio[value="vid_banner"]:checked').length > 0;
								}
							}
						},
						author_name: {
							required:true,
							noSpace:true
						},
						display_order: {
							required: true,
							minStrict: true,
							number: true,
							noSpace:true
						},	
					},
					messages: {						
						testimonialby: 'Title field is required.',
						author_name: 'Author name is required',
						img_id_image_upload: 'Image field is required',
						video_id: 'video field is required',
						testimonial: Lang.get('validation.required', { attribute: Lang.get('template.description') }),
						display_order: { required: Lang.get('validation.required', { attribute: Lang.get('template.displayorder') }) }
					
					},
					errorPlacement: function (error, element) { 
						if (element.parent('.input-group').length) {
						 error.insertAfter(element.parent()); 
						} else if (element.hasClass('select2')) { 
							error.insertAfter(element.next('span')); 
						}else if(element.attr("id") == "txtDescription"){ 
							error.insertAfter($(".ck-editor")); 
						} else { 
							error.insertAfter(element); 
					   	} 
					},
					invalidHandler: function(event, validator) { //display error alert on form submit
								var errors = validator.numberOfInvalids();
						    if (errors) {
						    	$.loader.close(true);
						    }   
								$('.alert-danger', $('#frmTestimonial')).show();
						},
					highlight: function(element) { // hightlight error inputs
								$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
						},
					unhighlight: function(element) {
								$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
						},
					submitHandler: function (form) {
						$('body').loader(loaderConfig);
						form.submit();
						$("button[type='submit']").attr('disabled','disabled');
						return false;
					}
				});
				$('#frmTestimonial input').on('keypress',function(e) {
						if (e.which == 13) {
								if ($('#frmTestimonial').validate().form()) {
										$('#frmTestimonial').submit(); //form validation success, call ajax form submit
										$("button[type='submit']").attr('disabled','disabled');
								}
								return false;
						}
				});
		}	 
		return {
				//main function to initiate the module
				init: function() {
						handleTestimonial();
				}
		};
}();
jQuery(document).ready(function() {   	 
	 Validate.init();	 	
	 jQuery.validator.addMethod("noSpace", function(value, element){
		if(value.trim().length <= 0){
			return false; 	
		}else{
			return true; 	
		}
	}, "This field is required");	
});
/* added by author */
jQuery(document).ready(function () {
	Custom.init();
	Custom.checkVersion();
	Validate.init();

	$(document).on('click', '.versionradio', function (e) {
		Custom.checkVersion();
	});

	$(document).on('click', '.banner', function (e) {
		Custom.checkType();
	});

	jQuery.validator.addMethod("noSpace", function (value, element) {
		if (value.trim().length <= 0) {
			return false;
		} else {
			return true;
		}
	}, "Space is not allow.");
});
/* added by author */
jQuery.validator.addMethod("phoneFormat", function(value, element) {
	// allow any non-whitespace characters as the host part
	return this.optional( element ) || /((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}/.test( value );
}, 'Please enter a valid phone number.');

jQuery.validator.addMethod("minStrict", function(value, element) {
	// allow any non-whitespace characters as the host part
	if(value>0){
		return true;
	}else{
		return false;
	}
}, 'Display order must be a number higher than zero');
$('input[type=text]').on('change',function(){
	var input = $(this).val();
  var trim_input = input.trim();
  if(trim_input) {
  	$(this).val(trim_input);
  	return true;
 	}
});


/*********** Remove Image code start Here  *************/
/*$(document).ready(function() 
{
	 	if($("input[name='img_id']").val() == ''){  
				$('.removeimg').hide();
				$('.image_thumb .overflow_layer').css('display','none');
		 }else{
			 $('.removeimg').show();
				 $('.image_thumb .overflow_layer').css('display','block');
		 }

	 $(document).on('click', '.removeimg', function(e) 
	 {    	 	
		 $("input[name='img_id']").val('');
		 $("input[name='image_url']").val('');
		$(".fileinput-new div img").attr("src", site_url+ '/resources/images/upload_file.gif');

		if($("input[name='img_id']").val() == '') {  
				$('.removeimg').hide();
				$('.image_thumb .overflow_layer').css('display','none');
			}else{
				$('.removeimg').show();
				 $('.image_thumb .overflow_layer').css('display','block');
			}			 
	});
});*/
/************** Remove Images Code end ****************/

/*********** Remove Image code start Here  *************/
$(document).ready(function () {
	if ($("input[name='img_id_image_upload']").val() == '') {
		$('.removeimg').hide();
		$('.image_thumb .overflow_layer').css('display', 'none');
	} else {
		$('.removeimg').show();
		$('.image_thumb .overflow_layer').css('display', 'block');
	}

	$(document).on('click', '.removeimg', function (e) {
		$("input[name='img_id_image_upload']").val('');
		$("input[name='image_url']").val('');
		$(".fileinput-new div img").attr("src", site_url + '/resources/images/upload_file.gif');

		if ($("input[name='img_id_image_upload']").val() == '') {
			$('.removeimg').hide();
			$('.image_thumb .overflow_layer').css('display', 'none');
		} else {
			$('.removeimg').show();
			$('.image_thumb .overflow_layer').css('display', 'block');
		}
	});
});
/************** Remove Images Code end ****************/


/*********** Remove Video code start Here  *************/
$(document).ready(function () {
	if ($("input[name='video_id']").val() == '') {
		$('.removeVideo').hide();
		$('.video_thumb .overflow_layer').css('display', 'none');
	} else {
		$('.removeVideo').show();
		$('.video_thumb .overflow_layer').css('display', 'block');
	}
  
	$(document).on('click', '.removeVideo', function (e) {
		$("input[name='video_id']").val('');
		$("#video_name").val('');

		$(".video-fileinput div img").attr("src", site_url + '/resources/images/video_upload_file.gif');
		if ($("input[name='video_id']").val() == '') {
			$('.removeVideo').hide();
			$('.video_thumb .overflow_layer').css('display', 'none');
		} else {
			$('.removeVideo').show();
			$('.video_thumb .overflow_layer').css('display', 'block');
		}
	});
});
/************** Remove Video code end ****************/
