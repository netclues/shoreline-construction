/**
 * This method validates contacts's form fields
 * since   2016-12-24
 * author  NetQuick
 */
var Validate = function() {
    var handleContact = function() {
        $("#frmContactUS").validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            rules: {
                name: {
                    required: true,
                    noSpace: true
                },
                order: {
                    required: true,
                    minStrict: true,
                    number: true,
                    noSpace: true
                },
                phone_no: {
                    required: true,
                    maxlength: 25,
                    phonenumber: true,
                    // digits: true,
                    checkallzero: {
                        depends: function () {
                            if ($("#phone_no").val() != '') {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    },
                    phonenumberFormat: {
                        depends: function () {
                            if ($("#phone_no").val() != '') {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    },
    
                },
                address: "required",
                // primary: "required"
            },
            messages: {
                name: Lang.get('validation.required', {
                    attribute: Lang.get('template.name')
                }),
                order: {
                    required: Lang.get('validation.required', {
                        attribute: Lang.get('template.displayorder')
                    })
                },
                phone_no: {
                    required: "Phone number is required.",                    
                    maxlength: "You reach the maximum limit {0}.",
                    // digits: "Only number.",
                },
                address: Lang.get('validation.required', {
                    attribute: Lang.get('template.contactModule.address')
                }),
                // primary: Lang.get('validation.required', {
                //     attribute: Lang.get('template.contactModule.primary')
                // }),
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                var errors = validator.numberOfInvalids();
                if (errors) {
                    $.loader.close(true);
                }
                $('.alert-danger', $('#frmContactUS')).show();
            },
            highlight: function(element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            submitHandler: function(form) {
                $('body').loader(loaderConfig);
                form.submit();
                return false;
            }
        });
        $('#frmContactUS input').on('keypress',function(e) {
            if (e.which == 13) {
                if ($('#frmContactUS').validate().form()) {
                    $('#frmContactUS').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }
    return {
        //main function to initiate the module
        init: function() {
            handleContact();
        }
    };
}();
jQuery(document).ready(function() {
    Validate.init();
    load_validation();
    
    $.validator.addMethod("phonenumber", function(value, element) {
        var newVal = value.replace(/^\D+/g, '');
        if (parseInt(newVal) <= 0 || newVal.match(/\d+/g) == null) {
            return false;
        } else {
            return true;
        }
    }, 'Please enter a valid phone number.');
    jQuery.validator.addMethod("noSpace", function(value, element) {
        if (value.trim().length <= 0) {
            return false;
        } else {
            return true;
        }
    }, "This field is required");
});
jQuery.validator.addMethod("phoneFormat", function(value, element) {
    // allow any non-whitespace characters as the host part
    return this.optional(element) || /((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}/.test(value);
}, 'Please enter a valid phone number.');

jQuery.validator.addMethod("minStrict", function(value, element) {
    // allow any non-whitespace characters as the host part
    if (value > 0) {
        return true;
    } else {
        return false;
    }
}, 'Display order must be a number higher than zero');

$('input[type=text]').on('change',function() {
    var input = $(this).val();
    var trim_input = input.trim();
    if (trim_input) {
        $(this).val(trim_input);
        return true;
    }

});

//########################extra field add js###########################################


$(document).on('click', '.addMoreEmail', function(e) 
{
    e.preventDefault();
    if ($('.emailField').length >= 2) {
        $(this).hide();
    }

    if ($('.emailField').length < 3) {
        emcnt++;
        $('.multi-email').append('<div class="emailField form-group form-md-line-input"> <input class="form-control input-sm" maxlength="100" id="email' + emcnt + '" placeholder="Email" autocomplete="off" name="email[' + emcnt + ']" type="email"> <label class="form_title" for="email[' + emcnt + ']">Email <span aria-required="true" class="required"> * </span></label> <a href="javascript:void(0);" class="removeEmail add_more" title="Remove"><i class="fa fa-times"></i> Remove</a> <span class="help-block"> </span> </div>');
        $('input[name="email[' + emcnt + ']"]').rules("add", {
            required: true,
            emailFormat: true,
            noSpace: true,
            messages: { 
                          required: "Email field is required.",
                       }
        });
    }
});

$(document).on('click', '.removeEmail', function() {
    $(this).parent().remove();
    $('.addMoreEmail').show();
});

$(document).on('click', '.addMorePhone', function(e) {
    e.preventDefault();

    if ($('.phoneField').length >= 2) {
        $(this).hide();
    }

    if ($('.phoneField').length < 3) {
        phcnt++;
        $('.multi-phone').append('<div class="phoneField form-group form-md-line-input"> <input class="form-control input-sm mask_phone_' + phcnt + '" id="phone_no' + phcnt + '" placeholder="Phone No" autocomplete="off" maxlength="20" onkeypress="javascript: return KeycheckOnlyPhonenumber(event);" onpaste="return false;" ondrop="return false;" name="phone_no[' + phcnt + ']" type="text"> <label class="form_title" for="phone_no">Phone No <span aria-required="true" class="required"> * </span></label> <a href="javascript:void(0);" class="removePhone add_more" title="Remove"><i class="fa fa-times"></i> Remove</a> <span class="help-block"> </span> </div>');
        $('input[name="phone_no[' + phcnt + ']"]').rules("add", {
            minlength: 5,
            maxlength: 20,
            required: true,
            phonenumber: {
                depends: function() {
                    if (($(this).val()) != '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
             messages: { 
              //'phone_no[0]':{
                  required: "Phone No field is required.",
                  minlength: "Please enter at least 5 digit.",
                  maxlength: "Please enter no more than 20 digit.",
               //} 
             }
        });
    }
});

$(document).on('click', '.removePhone', function() {
    $(this).parent().remove();
    $('.addMorePhone').show();
});

function load_validation() {
    if ($('.phoneField').length >= 1) {
        for (var i = 0; i <= phcnt; i++) {
            $('input[name="phone_no[' + i + ']"]').rules("add", {
                minlength: 5,
                maxlength: 20,
                phonenumber: {
                    depends: function() {
                        if (($(this).val()) != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
                required: {
                    depends: function() {
                        if ($(this).prop('name') == 'phone_no[0]') {
                            return true;
                        } else {
                            return false;
                        }
                    }
                },
            messages: { 
              //'phone_no[0]':{
                  required: "Phone No field is required.",
                  minlength: "Please enter at least 5 digit.",
                  maxlength: "Please enter no more than 20 digit.",
               //} 
             }
            });
        }

        if ($('.emailField').length >= 0) {

            for (var i = 0; i <= emcnt; i++) {
                $('input[name="email[' + i + ']"]').rules("add", {
                    required: true,
                    emailFormat: true,
                    noSpace: true,
                    messages: { 
                                  required: "Email field is required.",
                               }
                });
            }
        }
    }

    if ($('.emailField').length >= 2) {
        $('.addMoreEmail').hide();
    }

    if ($('.phoneField').length >= 2) {
        $('.addMorePhone').hide();
    }

}
$.validator.addMethod("emailFormat", function(value, element) {
    // allow any non-whitespace characters as the host part
    return this.optional(element) || /^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,4})$/.test(value);
}, 'Enter valid email format');

/*********** Remove Image code start Here  *************/
$(document).ready(function() {
    if ($("input[name='img_id']").val() == '') {
        $('.removeimg').hide();
        $('.image_thumb .overflow_layer').css('display', 'none');
    } else {
        $('.removeimg').show();
        $('.image_thumb .overflow_layer').css('display', 'block');
    }

    $(document).on('click', '.removeimg', function(e) {
        $("input[name='img_id']").val('');
        $("input[name='image_url']").val('');
        $(".fileinput-new div img").attr("src", site_url + "/resources/images/upload_file.gif");

        if ($("input[name='img_id']").val() == '') {
            $('.removeimg').hide();
            $('.image_thumb .overflow_layer').css('display', 'none');
        } else {
            $('.removeimg').show();
            $('.image_thumb .overflow_layer').css('display', 'block');
        }
    });
});
/************** Remove Images Code end ****************/