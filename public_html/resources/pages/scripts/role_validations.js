/**
 * This method validates Role form fields
 * since   2016-12-24
 * author  NetQuick
 */

var modalMenuItemId;
var loaderConfig={
	autoCheck: false,
	size: 16,
	bgColor: 'rgba(0, 0, 0, 0.25)',
	bgOpacity: 0.5,
	fontColor: 'rgba(16, 128, 242, 90)',
	title: 'Loading...'
};

 var Validate = function() {
		var handleRole = function() {
				 $("#frmRole").validate({
					errorElement: 'span', //default input error message container
					errorClass: 'help-block', // default input error message class
					ignore:[],
					rules: {
						name: {
							required:true,
							noSpace:true
						},
						display_name: {
							required:true,
							noSpace:true
						},
						permission: {
				      required: true,
				      req_question: true
				     }						
					},
					messages: {
						name:"Name field is required.",
						display_name: "Display name field is required.",	
						permission: {
						      required: "Permission field is required."						
						   }
					},
					errorPlacement: function (error, element) { 
						if(element.attr("name") == "name" ) {
               $('.nameOfRole').remove();
               error.insertAfter(element); 
            }else if (element.attr("type") == "checkbox") {
              $('#errorMessage').text('Please check at least one.');
            } else if (element.parent('.input-group').length) { 
							error.insertAfter(element.parent()); 
						} else if (element.hasClass('select2')) { 
							error.insertAfter(element.next('span')); 
						} else { 
							error.insertAfter(element); 
						} 
					},
					invalidHandler: function(event, validator) { //display error alert on form submit
								var errors = validator.numberOfInvalids();
						    if (errors) {
						    	$.loader.close(true);
						    }   
								$('.alert-danger', $('#frmRole')).show();
						},
					highlight: function(element) { // hightlight error inputs
								$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
						},
					unhighlight: function(element) {
								$(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
						},
					submitHandler: function (form) {
						$('body').loader(loaderConfig);
						form.submit();
						$("button[type='submit']").attr('disabled','disabled');
						return false;
					}
				});
				$('#frmRole input').on('keypress',function(e) {
						if (e.which == 13) {
								if ($('#frmRole').validate().form()) {
										$('#frmRole').submit(); //form validation success, call ajax form submit
										$("button[type='submit']").attr('disabled','disabled');
								}
								return false;
						}
				});
		}	 
		return {
				//main function to initiate the module
				init: function() {
						handleRole();
				}
		};
}();
jQuery(document).ready(function() {   	 
	 Validate.init();	 
	 jQuery.validator.addMethod("noSpace", function(value, element){
		if(value.trim().length <= 0){
			return false; 	
		}else{
			return true; 	
		}
	}, "Space is not allow");	
});
jQuery.validator.addMethod("phoneFormat", function(value, element) {
	// allow any non-whitespace characters as the host part
	return this.optional( element ) || /((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}/.test( value );
}, 'Please enter a valid phone number.');

$.validator.addMethod('req_question', function(value, elem) {
	 return $('.req_question:checked').length > 0;
}, 'Please check at least one.');

$('input[type=text]').on('change',function(){
	var input = $(this).val();
	var trim_input = input.trim();
	if(trim_input) {
		$(this).val(trim_input);
		return true;
	}
});

var prevent=false;
$('.module-activation').on('switchChange.bootstrapSwitch', function (event, state) {		
		$($(this).parent().parent().parent().parent().parent()).loader(loaderConfig);
		var switchState=$(this).bootstrapSwitch('state');
		if(switchState) {			
			if(!prevent){
				$(this).parent().parent().parent().parent().parent().find('.right_permis input[type=checkbox]').prop('checked', true);
			}
		} else {
			$(this).parent().parent().parent().parent().parent().find('.right_permis input[type=checkbox]').prop('checked', false);
			prevent=false;
		}		
	 setTimeout(function(){ $.loader.close(true); }, 1000);		
});

$('.right_permis input[type=checkbox]').on('click', function (event, state) {		
		if($(this).parent().parent().children().children('input[type=checkbox]:checked').length < 1) {
			$(this).parent().parent().parent().find('.module-activation').bootstrapSwitch('state', false);
			prevent=false;
		}else{
			prevent=true;			
			$(this).parent().parent().parent().find('.module-activation').bootstrapSwitch('state', true);
		}		
});