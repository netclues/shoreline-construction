$(document).on('keyup', '#searchFilter', function() {
	var searchVal = $('#searchFilter').val();
	var length = $("#searchFilter").val().length;	
	if (searchVal != "" && length=='3') {
		$.ajax({
			url: site_url+'/testimonial',
			data: { searchVal:searchVal },
			type: "POST",         
			dataType: "json",
			success: function(data) {
				var html = '';
				if(data != null && data !='') {
					$.each (data, function(key, value) {	
						html += '<div class="item col-md-6 col-sm-6 col-xs-6 col-xs-small grid  animate fadeInUp">';
						html += '<div class="post_bg">';
						html +=	'<div class="col-md-5 col-sm-12 nopadding">';
						html += '<div class="thumbnail_container">';
						html += '<div class="thumbnail">'
						if(value.txt_image_url != null && value.txt_image_url !='') {
		      	 	html += '<img alt="No Image" src="'+value.txt_image_url+'/'+value.txt_image_alt_tag+'.'+value.var_image_extension+'">';
		        } else {
		        	html += '<img alt="No Image" src="'+site_url+''+'/resources/images/user_male4.png"/>';
		        }
						html += '</div>';
						html += '</div>';
						html +=	'</div>';
						html += '<div class="col-md-7 col-sm-12 nopadding">';
						html += '<div class="content">';
						html += '<h3>'+value.testimonialby+'</h3>';
						html += '<p><i class="fa fa-quote-left"></i>'+value.testimonial+'</p>';
						html += '<ul class="inline none">';
						html +=	'<li><a title="Date" href="javascript:;" class="link"><span class="fa fa-calendar"></span> '+value.testimonialdate+'</li>';
						html +=	'<li><a href="#'+value.id+'_collapse" title="Read More" data-toggle="collapse" aria-expanded="false" class="link">Read More <span class="fa fa-caret-right"></span></a></li>';
						html += '</ul>';
						html += '</div>';
						html += '<div class="collapse" id="'+value.id+'_collapse">';
  					html += '<div class="card card-block">';
    				html += ''+value.testimonial+'';
  					html += '</div>';
						html +=	'</div>';
						html += '</div>';
						html +=	'</div>';
						html += '</div>';
					});
				} else {
					html += '<div class="content">';
		      html += '<span>No records found.</span>';
		      html += '</div>';
				}
				$('.testimonialList').html(html);        		
			},
			error: function() {
				console.log('error!');
			}                                 
		});
	}
});
$(document).on('click load', '.pagination a', function (e) {
 	e.preventDefault();
 	var page = $(this).attr('href').split('page=')[1];
  getPosts(page);
});
function getPosts(page) {
  $.ajax({
    url : site_url+'/pagination/testimonial?page='+page,
  }).done(function (data) {
	  $('.testimonial').html(data);
    location.hash = '/page_'+page;
	}).fail(function () {
    alert('Something went to wrong.');
  });
}