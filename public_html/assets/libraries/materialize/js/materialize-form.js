var label, target;

function Form(){}

Form.prototype.alterForm = function(){    
    $('.nq-form-md input, .nq-form-md  textarea').focus(function(e){
        form.setLabel(e.target);
        form.checkFocused();
    });
    $('.nq-form-md input, .nq-form-md textarea').focusout(function(e){
        form.setLabel(e.target);
        form.checkUnfocused(e.target);
    });
};

Form.prototype.setLabel = function(target){
    label= $('label[for='+target.id+']');
};

Form.prototype.getLabel = function(){
    return label;
};

Form.prototype.checkFocused = function(){
    form.getLabel().addClass('nq-active','');
};

Form.prototype.checkUnfocused = function(target){
    if($(target).val().length == 0){
        form.getLabel().removeClass('nq-active');
    }
};

form = new Form();

function initialize(){
    form.alterForm();
}
initialize();

$("form").ready(function() {
    if ( $( this ).hasClass( "nq-form-md" ) ) {
        $(this).find('input[placeholder], input[disabled], input[readonly], textarea[placeholder], textarea[disabled], textarea[readonly], select').parents('.form-group').addClass('nq-active-label');
        if($('input[value], textarea[value]').val().length > 0){
            $(this).find('input[value], textarea[value]').parents('.form-group').children('.nq-label').addClass('nq-active');
        }
    }
});

jQuery.each(jQuery('textarea[data-autoresize]'), function() {
    var offset = this.offsetHeight - this.clientHeight; 
    var resizeTextarea = function(el) {
        jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
    };
    jQuery(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
});