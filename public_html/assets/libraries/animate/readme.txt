https://github.com/daneden/animate.css
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

--------------------

JS File

<script src="assets/libraries/animate-css/js/animate.js"></script>

--------------------

SCSS File

/* Libraries S */
@import "libraries/animate-css/scss/animate";
/* Libraries E */

--------------------

HTML

<div class="animated fadeInUp fast">
</div>

--------------------

Class Name

bounce				flash				pulse				rubberBand
shake				headShake			swing				tada
wobble				jello				bounceIn			bounceInDown
bounceInLeft		bounceInRight		bounceInUp			bounceOut
bounceOutDown		bounceOutLeft		bounceOutRight		bounceOutUp
fadeIn				fadeInDown			fadeInDownBig		fadeInLeft
fadeInLeftBig		fadeInRight			fadeInRightBig		fadeInUp
fadeInUpBig			fadeOut				fadeOutDown			fadeOutDownBig
fadeOutLeft			fadeOutLeftBig		fadeOutRight		fadeOutRightBig
fadeOutUp			fadeOutUpBig		flipInX				flipInY
flipOutX			flipOutY			lightSpeedIn		lightSpeedOut
rotateIn			rotateInDownLeft	rotateInDownRight	rotateInUpLeft
rotateInUpRight		rotateOut			rotateOutDownLeft	rotateOutDownRight
rotateOutUpLeft		rotateOutUpRight	hinge				jackInTheBox
rollIn				rollOut				zoomIn				zoomInDown
zoomInLeft			zoomInRight			zoomInUp			zoomOut
zoomOutDown			zoomOutLeft			zoomOutRight		zoomOutUp
slideInDown			slideInLeft			slideInRight		slideInUp
slideOutDown		slideOutLeft		slideOutRight		slideOutUp
heartBeat

--------------------

Delay Class

Class Name		Delay Time
delay-2s		2s
delay-3s		3s
delay-4s		4s
delay-5s		5s

--------------------

Slow, Slower, Fast, and Faster Class

Class Name		Speed Time
slow			2s
slower			3s
fast			800ms
faster			500ms