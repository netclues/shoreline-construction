function animated() {
    $(".animated").not(".nq-load").each(function (t) {
        var e = $(this),
            n = 1 * t;
        $(window).scrollTop() + $(window).height() >= e.offset().top && setTimeout(function () {
            e.addClass("nq-load")
        }, n)
    })
}
$(document).ready(function () {
    animated()
})
$(window).scroll(function () {
    animated()
})