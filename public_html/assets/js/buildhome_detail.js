$(document).ready(function() {
    var owlClass = '.detail-slider';
    /* OwlCarousel2 Basic S */
        $(owlClass + '.owl-carousel').owlCarousel({
            loop:false,
            rewind:true,
            margin:15,
            /* Show next/prev buttons & dots S */
                nav:true,
                navText: [owlNavTextPrev,owlNavTextNext],
                dots:true,
                dotsEach:true,
            /* Show next/prev buttons & dots E*/
            /* Autoplay S */
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                smartSpeed: 1000,
            /* Autoplay E */
            /* Auto Height S */
                autoHeight:false,
            /* Auto Height E */
            /* Lazy Load S */
                lazyLoad:true,
                lazyLoadEager:1,
            /* Lazy Load E */
            /* Responsive S */
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        dots:true,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    }                    
                },
            /* Responsive E */
            /* Mouse & Touch drag enabled / disabled S */
                mouseDrag:true,
                touchDrag:true,
            /* Mouse & Touch drag enabled / disabled E */
            /* Padding left and right on stage S */
                stagePadding:0,
            /* Padding left and right on stage E */
        });
    /* OwlCarousel2 Basic E */
});

$(document).ready(function() {
    var owlClass = '.detail-gallery-slider';
    /* OwlCarousel2 Basic S */
        $(owlClass + '.owl-carousel').owlCarousel({
            loop:false,
            rewind:true,
            margin:15,
            /* Show next/prev buttons & dots S */
                nav:true,
                navText: [owlNavTextPrev,owlNavTextNext],
                dots:true,
                dotsEach:true,
            /* Show next/prev buttons & dots E*/
            /* Autoplay S */
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                smartSpeed: 1000,
            /* Autoplay E */
            /* Auto Height S */
                autoHeight:false,
            /* Auto Height E */
            /* Lazy Load S */
                lazyLoad:true,
                lazyLoadEager:1,
            /* Lazy Load E */
            /* Responsive S */
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        dots:true,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    },
                    479:{
                        items:2,
                        nav:false,
                        dots:true,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    },
                    767:{
                        items:3,
                        nav:false,
                        dots:true,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    },
                    1199:{
                        items:4,
                        nav:false,
                        dots:true,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    }                    
                },
            /* Responsive E */
            /* Mouse & Touch drag enabled / disabled S */
                mouseDrag:true,
                touchDrag:true,
            /* Mouse & Touch drag enabled / disabled E */
            /* Padding left and right on stage S */
                stagePadding:0,
            /* Padding left and right on stage E */
        });
    /* OwlCarousel2 Basic E */
});