$(document).ready(function() {
	var owlClass = '.banner_01';
	/* OwlCarousel2 Basic S */
	    $(owlClass + ' .owl-carousel').owlCarousel({
	        loop:false,
	        rewind:true,
	        margin:0,
	        /* Show next/prev buttons & dots S */
	            nav:false,
	            navText: [owlNavTextPrev,owlNavTextNext],
	            dots:true,
	            dotsEach:false,
	        /* Show next/prev buttons & dots E*/
	        /* Autoplay S */
	            autoplay:true,
	            autoplayTimeout:5000,
	            autoplayHoverPause:true,
	            smartSpeed: 800,
	        /* Autoplay E */
	        /* Auto Height S */
	            autoHeight:false,
	        /* Auto Height E */
	        /* Lazy Load S */
	            lazyLoad:true,
	            lazyLoadEager:1,
	        /* Lazy Load E */
	        /* Responsive S */
	            responsiveClass:true,
	            responsive:{
	                0:{
	                    items:1,
                        touchDrag:true,
	                    /* loop:($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
	                },
                    1024:{
                        items:1,
                        touchDrag:false
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 3 ? false : true) */
                    }
	            },
	        /* Responsive E */
	        /* Mouse & Touch drag enabled / disabled S */
	            mouseDrag:true,
	            touchDrag:true,
	        /* Mouse & Touch drag enabled / disabled E */
	        /* Padding left and right on stage S */
	            stagePadding:0,
	        /* Padding left and right on stage E */
	    });
	/* OwlCarousel2 Basic E */
});
$(document).ready(function() {
    var owlClass = '.our-service-section';
    /* OwlCarousel2 Basic S */
        $(owlClass + ' .owl-carousel').owlCarousel({
            loop:false,
            rewind:true,
            margin:15,
            /* Show next/prev buttons & dots S */
                nav:true,
                navText: ["<span class='fa fa-angle-left'></span>","<span class='fa fa-angle-right'></span>"],
                dots:true,
                dotsEach:true,
            /* Show next/prev buttons & dots E*/
            /* Autoplay S */
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                smartSpeed: 800,
            /* Autoplay E */
            /* Auto Height S */
                autoHeight:false,
            /* Auto Height E */
            /* Lazy Load S */
                lazyLoad:true,
                lazyLoadEager:1,
            /* Lazy Load E */
            /* Responsive S */
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        dots:true,
                        touchDrag:true
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    },
                    480:{
                        items:2,
                        nav:false,
                        dots:true,
                        touchDrag:true
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 2 ? false : true) */
                    },
                    768:{
                        items:3,
                        nav:false,
                        touchDrag:true,
                        dots:true                        
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 3 ? false : true) */
                    },
                    1024:{
                        items:3,
                        dots:true,
                        nav:false,
                        margin: 30,
                        touchDrag:false,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 3 ? false : true) */
                    }
                },
            /* Responsive E */
            /* Mouse & Touch drag enabled / disabled S */
                mouseDrag:true,
                touchDrag:true,
            /* Mouse & Touch drag enabled / disabled E */
            /* Padding left and right on stage S */
                stagePadding:0,
            /* Padding left and right on stage E */
        });
    /* OwlCarousel2 Basic E */
});
$(document).ready(function() {
    var owlClass = '.recent-work-desc';
    /* OwlCarousel2 Basic S */
        $(owlClass + '.owl-carousel').owlCarousel({
            loop:false,
            rewind:true,
            margin:0,
            /* Show next/prev buttons & dots S */
                nav:true,
                navText: [owlNavTextPrev,owlNavTextNext],
                dots:true,
                dotsEach:true,
            /* Show next/prev buttons & dots E*/
            /* Autoplay S */
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                smartSpeed: 1200,
            /* Autoplay E */
            /* Auto Height S */
                autoHeight:false,
            /* Auto Height E */
            /* Lazy Load S */
                lazyLoad:true,
                lazyLoadEager:1,
            /* Lazy Load E */
            /* Responsive S */
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        dots:true
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    }
                },
            /* Responsive E */
            /* Mouse & Touch drag enabled / disabled S */
                // mouseDrag:true,
                // touchDrag:true,
            /* Mouse & Touch drag enabled / disabled E */
            /* Padding left and right on stage S */
                stagePadding:0,
            /* Padding left and right on stage E */
        });
    /* OwlCarousel2 Basic E */
});

$(document).ready(function() {
    var owlClass = '.building-home';
    /* OwlCarousel2 Basic S */
        $(owlClass + ' .owl-carousel').owlCarousel({
            loop:false,
            rewind:true,
            margin:15,
            /* Show next/prev buttons & dots S */
                nav:true,
                navText: [owlNavTextPrev,owlNavTextNext],
                dots:true,
                dotsEach:true,
            /* Show next/prev buttons & dots E*/
            /* Autoplay S */
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                smartSpeed: 1000,
            /* Autoplay E */
            /* Auto Height S */
                autoHeight:false,
            /* Auto Height E */
            /* Lazy Load S */
                lazyLoad:true,
                lazyLoadEager:1,
            /* Lazy Load E */
            /* Responsive S */
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        dots:true,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    },
                    480:{
                        items:2,
                        nav:false,
                        dots:true,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 2 ? false : true) */
                    },
                    768:{
                        items:3,
                        nav:false,
                        dots:true,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 3 ? false : true) */
                    },
                    1024:{
                        items:4,
                        margin:20,
                        nav:false,
                        dots:true,
                        touchDrag:false,
                        mouseDrag:false,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 4 ? false : true) */
                    },
                    1279:{
                        items:5,
                        margin:30,
                        nav:false,
                        dots:true,
                        touchDrag:false,
                        mouseDrag:false,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 4 ? false : true) */
                    }
                },
            /* Responsive E */
            /* Mouse & Touch drag enabled / disabled S */
                mouseDrag:true,
                touchDrag:true,
            /* Mouse & Touch drag enabled / disabled E */
            /* Padding left and right on stage S */
                stagePadding:0,
            /* Padding left and right on stage E */
        });
    /* OwlCarousel2 Basic E */
});
$(window).on("load resize scroll", function() {
    var owlClass = '.testimonial-section';
    /* OwlCarousel2 Basic S */
        $(owlClass + ' .owl-carousel').owlCarousel({
            loop:false,
            rewind:true,
            margin:15,
            /* Show next/prev buttons & dots S */
                nav:false,
                navText: [owlNavTextPrev,owlNavTextNext],
                dots:true,
                dotsEach:false,
            /* Show next/prev buttons & dots E*/
            /* Autoplay S */
                autoplay:true,
                autoplayTimeout:7000,
                autoplayHoverPause:true,
                smartSpeed: 1200,
            /* Autoplay E */
            /* Auto Height S */
                autoHeight:false,
            /* Auto Height E */
            /* Lazy Load S */
                lazyLoad:true,
                lazyLoadEager:1,
            /* Lazy Load E */
            /* Responsive S */
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        touchDrag:true,
                        mouseDrag:true,
                        /* loop:($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    },
                    1024:{
                        items:1,
                        touchDrag:false,
                        mouseDrag:false,                        
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 3 ? false : true) */
                    }
                },
            /* Responsive E */
            /* Mouse & Touch drag enabled / disabled S */
            /* Mouse & Touch drag enabled / disabled E */
            /* Padding left and right on stage S */
                stagePadding:0,
            /* Padding left and right on stage E */
        });
    /* OwlCarousel2 Basic E */
});