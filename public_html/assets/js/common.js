/* Browser Upgrade Font API S */
    var ie = /MSIE (\d+)/.exec(navigator.userAgent);
    ie = ie? ie[1] : null;
    if(ie && ie <= 9) {
        var script = document.createElement( 'script' );script.type = 'text/javascript';
        script.src = 'assets/js/html5.min.js';$("head").append("<style type='text/css'>.buorg{display:block}</style>");
    }
    if(ie && ie == 8) {  
        $('head').append("<style type='text/css'>.buorg{display:block}</style>");
    }
    else if(ie && ie == 7) {
        $('head').append("<style type='text/css'>.buorg{display:block;}</style>");
    }
/* Browser Upgrade Font API E */

/* Back to Top Scroll S */
    $(window).scroll(function() {
        if ($(this).scrollTop() > 80) {
            $('#back-top').show();
        } else {
            $('#back-top').hide();
        }
    });
    $('#back-top').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 2500);
        return false;
    });
/* Back to Top Scroll E */ 

            // $(document).ready(function() {
            //     $('body').addClass('header_01'); // Mobile Menu Body Add Class        
            //     menuResize(); // Windows Screen Menu Ul LI auto sizing        
            //     fixedHeader(".fix_width",0,"header_fixed",""); // fixedHeader(className,scrollHeight,fixedHeaderClass,animateClassName)
            // });
        
		
			$(document).ready(function(){
				AOS.init({
		        duration: 900,
		        disable: 'mobile',
		        once: true,
		        offset: 0,
		        startEvent: 'DOMContentLoaded',
		        delay: 0
		    });
			    $(window).on('load', function () {
			        AOS.refresh();
			    });
			});
			$(document).ready(function(){	
				$('#cookie_policy').click(function() {
						$.ajax({
						type: "POST",
						url: site_url + "/front/cookiesPopupStore",
						success: function(data) {
								$('.nq-cookies').css('display', 'none');
						}
					});
				});

			    $('#contact_form').validate({
			        errorElement: 'span',
			        errorClass: 'error',
			        ignore: [],
			        rules: {
			            first_name: {
			                required: true,
			                noSpace: true,
			                maxlength: 600,
			                check_special_char: true,
			                no_url: true,
											badwordcheck: true
			            },
			            contact_email: {
			                required: true,
			                maxlength: 100,
			                emailFormat: true,
			            },
			            user_message: {
			                maxlength: 100,
			                no_url: true,
			                check_special_char: true,
											badwordcheck: true
			            },
			            'g-recaptcha-response': {
										required: true
									}
			        },
			        messages: {
			            first_name: {
			                required: "Name is required.",
			                maxlength: "You reach the maximum limit {0}.",
			            },
			            contact_email: {
			                required: "Email is required.",
			                maxlength: "You reach the maximum limit {0}.",
			            },
			            user_message: {
			                maxlength: "You reach the maximum limit {0}.",
			            },
			            'g-recaptcha-response': {
										required: "Please select I'm not a robot.",
									}
			        },
			        onfocusout: function(element) {
			            this.element(element);
			        },
			        errorPlacement: function(error, element) {
			            if (element.attr('id') == 'g-recaptcha-response') {
			                error.insertAfter(element.parent());
			            } else {
			                error.insertAfter(element);
			            }
			        },
			    });
					$("#contact_form").trigger("reset");
			});


			// document.querySelectorAll('oembed[url]').forEach( element => 
			// {

			// 		const iframe = document.createElement('iframe');
			// 		var url  = element.getAttribute('url');
			// 		var videoObj = parseVideo(url);
					
			// 		if(videoObj.type == "youtube") 
			// 		{    
			// 				iframe.setAttribute('src', '//www.youtube.com/embed/'+ videoObj.id);
			// 				iframe.setAttribute('allowfullscreen',true);
							
			// 				$(iframe).insertAfter(element);

			// 		}else if(videoObj.type == "vimeo") {

			// 				iframe.setAttribute('src', '//player.vimeo.com/video/'+videoObj.id);
			// 				iframe.setAttribute('allowfullscreen',true);
			// 				$(iframe).insertAfter(element);

			// 		}else if(videoObj.type == "general") {
			// 				iframe.setAttribute('src', url);
			// 				iframe.setAttribute('allowfullscreen',true);
			// 				$(iframe).insertAfter(element);
			// 		}

			// });

			// function parseVideo (url) 
			// {
			// 		- Supported YouTube URL formats:
			// 		  - http://www.youtube.com/watch?v=My2FRPA3Gf8
			// 		  - http://youtu.be/My2FRPA3Gf8
			// 		  - https://youtube.googleapis.com/v/My2FRPA3Gf8
			// 		- Supported Vimeo URL formats:
			// 		  - http://vimeo.com/25451551
			// 		  - http://player.vimeo.com/video/25451551
			// 		- Also supports relative URLs:
			// 		  - //player.vimeo.com/video/25451551

			// 		var match = url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
					
			// 		if(match != null)
			// 		{
			// 				if (RegExp.$3.indexOf('youtu') > -1) {
			// 						var type = 'youtube';
			// 				} else if (RegExp.$3.indexOf('vimeo') > -1) {
			// 						var type = 'vimeo';
			// 				}    
			// 				return {
			// 						type: type,
			// 						id: RegExp.$6
			// 				};
			// 		}else{
			// 				var type = 'general';
			// 				return {
			// 						type: type
			// 				};
			// 		}
			// }
		
/*Menu Scroll S*/
/*$(document).ready(function(){
    $(".menu__open").click(function(){
        $("html").addClass("body-overflow");
    });
    $(".menu__close").click(function(){
	  	$("html").removeClass("body-overflow");
	});
});*/
/*Menu Scroll E*/