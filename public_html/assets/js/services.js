$(document).ready(function() {
    var owlClass = '.service_listing';
    /* OwlCarousel2 Basic S */
        $(owlClass + ' .owl-carousel').owlCarousel({
            loop:false,
            rewind:true,
            margin:15, 
                nav:false,
                navText: [owlNavTextPrev,owlNavTextNext],
                dots:true,
                dotsEach:true, 
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                smartSpeed: 250, 
                autoHeight:false, 
                lazyLoad:true,
                lazyLoadEager:1, 
            /* Responsive S */
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:false,
                        dots:true,
                        touchDrag:true
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 1 ? false : true) */
                    },
                    480:{
                        items:2,
                        nav:false,
                        dots:true
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 2 ? false : true) */
                    },
                    768:{
                        items:3,
                        nav:false,
                        dots:true
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 3 ? false : true) */
                    },
                    1024:{
                        items:4,
                        dots:true,
                        nav:false,
                        margin: 30,
                        touchDrag:false,
                        /* loop: ($(owlClass + " .owl-carousel .item").length <= 3 ? false : true) */
                    }
                },
            /* Responsive E */ 
                mouseDrag:true,
                touchDrag:true, 
                stagePadding:0, 
        });
    /* OwlCarousel2 Basic E */
});