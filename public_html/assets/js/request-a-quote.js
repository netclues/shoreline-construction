$(document).ready(function () 
{
	$('#request_page_form').validate({
		errorElement: 'span',
		errorClass: 'error',
		ignore: [],
		rules: {
			first_name: {
				required: true,
				xssProtection: true,
				noSpace: true,
				maxlength: 600,
				// check_special_char: true,
				no_url: true,
				badwordcheck: true,
			},
			last_name: {
				required: true,
				xssProtection: true,
				noSpace: true,
				maxlength: 600,
				// check_special_char: true,
				no_url: true,
				badwordcheck: true,
			},
			phone_number: {
				required: true,
				maxlength: 20,
				//digits: true,
				checkallzero: {
					depends: function () {
						if ($("#phone_number").val() != '') {
							return true;
						} else {
							return false;
						}
					}
				},
				phonenumberFormat: {
					depends: function () {
						if ($("#phone_number").val() != '') {
							return true;
						} else {
							return false;
						}
					}
				},

			},
			area_of_interest: {								
				xssProtection: true,
				noSpace: true,								
				no_url: true,
				badwordcheck: true,
			},
			contact_email: {
				required: true,
				maxlength: 100,
				emailFormat: true,
			},
			//area_of_interest: {	
			//	required: true,			
			//	no_url: true,				
			//	badwordcheck: true
			//},
			user_message: {
				maxlength: 600,
				xssProtection: true,
				no_url: true,
				// check_special_char: true,
				badwordcheck: true
			},
			'g-recaptcha-response': {
				required: true
			}
		},
		messages: {
			first_name: {
				required: "Please enter first name.",
				xssProtection: "Please enter valid input",
				maxlength: "You reach the maximum limit {0}.",
			},
			last_name: {
				required: "Please enter last name.",
				xssProtection: "Please enter valid input",
			},
			phone_number: {
				required: "Please enter phone number.",
				maxlength: "You reach the maximum limit {0}.",
				//digits: "Only number.",
			},
			area_of_interest:{				
				xssProtection: "Please enter valid input",
			},
			contact_email: {
				required: "Please enter email.",
				maxlength: "You reach the maximum limit {0}.",
			},
			//area_of_interest: {
			//	required: "This is required.",
			//},
			user_message: {
				maxlength: "You reach the maximum limit {0}.",
				xssProtection: "Please enter valid input",
			},
			'g-recaptcha-response': {
				required: "Please select I'm not a robot.",
			},
		},
		onfocusout: function (element) {
			this.element(element);
		},
		errorPlacement: function (error, element) {
			if (element.attr('id') == 'g-recaptcha-response') {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		},
	});
	$("#request_page_form").trigger("reset");
});

function occurrences(string, substring) {
	var n = 0;
	var pos = 0;
	while (true) {
			pos = string.indexOf(substring, pos);
			if (pos != -1) {
					n++;
					pos += substring.length;
			} else {
					break;
			}
	}
	return (n);
}

function validateXSSInput(value) {
	var count = occurrences(value, '#');
	var value1 = $("<textarea/>").html(value).val();
	for (var i = 1; i <= count; i++) {
			value1 = $("<textarea/>").html(value1).val();
	}
	if (value.match(/((\%3C)|<)((\%2F)|\/)*[a-z0-9\%]+((\%3E)|>)/i)) {
			return false;
	} else if (value.match(/<img|script[^>]+src/i)) {
			return false;
	} else if (value1.match(/((\%3C)|<)((\%2F)|\/)*[a-z0-9\%]+((\%3E)|>)/i)) {
			return false;
	} else if (value1.match(/<img|script[^>]+src/i)) {
			return false;
	} else if (value1.match(/((\%3C)|<)(|\s|\S)+((\%3E)|>)/i)) {
			return false;
	} else {
			return true;
	}
}

jQuery.validator.addMethod("xssProtection", function(value, element) {
	return validateXSSInput(value);
}, "Please enter valid input.");