<?php
namespace App;

use Cache;
use DB;
use Illuminate\Database\Eloquent\Model;

class CmsPage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'cms_page';
    protected $fillable = [
        'id',
        'intAliasId',
        'intFKModuleCode',
        'varTitle',
        'txtDescription',
        'varMetaTitle',
        'varMetaKeyword',
        'varMetaDescription',
        'chrDisplay',
        'chrPublish',
        'chrDelete',
    ];

    /**
     * This method handels retrival of front blog detail
     * @return  Object
     * @since   2017-10-13
     * @author  NetQuick
     */
    public static function getRecordIdByAliasID($aliasID)
    {
        $response = false;
        $response = Self::Select('id')->deleted()->publish()->checkAliasId($aliasID)->first();
        return $response;
    }

    /**
     * This method handels retrival of blog records
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public static function getPageWithAlias($aliasId = false)
    {
        $response = false;
        $cmsPageFields = ['id', 'intAliasId', 'intFKModuleCode'];
        $aliasFields = ['id', 'varAlias', 'intFkModuleCode'];
        $moduleFields = ['id', 'varModuleName'];
        $response = Self::getFrontPageRecord($cmsPageFields, $aliasFields, $moduleFields)->deleted()->publish()->checkAliasId($aliasId)->first();
        return $response;
    }

    /**
     * This method handels retrival of blog records
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public static function getPageContentByPageAlias($cmsPageId)
    {
        $response = false;
        $response = Self::select(['txtDescription'])->deleted()->checkAliasId($cmsPageId)->first();
        return $response;
    }

    /**
     * This method handels retrival of blog records
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public static function getPageByPageId($cmsPageId)
    {
        $response = false;
        $response = Cache::rememberForever('getPageByPageId_' . $cmsPageId, function () use ($cmsPageId) {
            return Self::select(['id', 'intAliasId', 'intFKModuleCode', 'varTitle', 'txtDescription', 'varMetaTitle', 'varMetaKeyword', 'varMetaDescription'])
                ->deleted()
                ->checkAliasId($cmsPageId)
                ->first();
        });
        return $response;
    }

    /**
     * This method handels retrival of blog records
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public static function getFrontPageRecord($cmsPageFields = false, $aliasFields = false, $moduleFields = false)
    {
        $data = [];
        $pageObj = Self::select($cmsPageFields);
        if ($aliasFields != false) {
            $data['alias'] = function ($query) use ($aliasFields) {
                $query->select($aliasFields);
            };
        }
        if ($moduleFields != false) {
            $data['modules'] = function ($query) use ($moduleFields) {
                $query->select($moduleFields);
            };
        }
        if (count($data) > 0) {
            $pageObj = $pageObj->with($data);
        }
        return $pageObj;
    }

    /**
     * This method handels retrival of blog records
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public static function getPowerPanelRecords($cmsPageFields = false, $aliasFields = false, $moduleFields = false, $moduleCode = false, $menuFields = false)
    {
        $data = [];
        $pageObj = Self::select($cmsPageFields);
        if ($aliasFields != false) {
            $data['alias'] = function ($query) use ($aliasFields, $moduleCode) {
                $query->select($aliasFields)->checkModuleCode($moduleCode);
            };
        }

        if ($moduleFields != false) {
            $data['modules'] = function ($query) use ($moduleFields) {
                $query->select($moduleFields);
            };
        }

        if ($menuFields != false) {
            $data['menu'] = function ($query) use ($menuFields) {
                $query->select($menuFields)->publish()->deleted();
            };
        }

        // $data['webhits'] = function ($query)
        // {
        //    $query->select('fkIntAliasId',DB::raw('COUNT(fkIntAliasId) as webhits'))->where('isWeb','Y');
        // };

        if (count($data) > 0) {
            $pageObj = $pageObj->with($data);
        }
        return $pageObj;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordCount($filterArr = false, $returnCounter = false)
    {
        $response = 0;
        $cmsPageFields = ['id'];
        $pageQuery = Self::getPowerPanelRecords($cmsPageFields);
        if ($filterArr != false) {
            $pageQuery = $pageQuery->filter($filterArr, $returnCounter);
        }
        $response = $pageQuery->deleted()->count();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordList($filterArr = false)
    {
        $response = false;
        $cmsPageFields = ['id', 'intAliasId', 'intFKModuleCode', 'varTitle', 'txtDescription', 'varMetaTitle', 'varMetaKeyword', 'varMetaDescription', 'chrPublish', 'chrDelete'];
        $aliasFields = ['id', 'varAlias'];
        $menuFields = ['id', 'intPageId', 'varTitle','chrActive','chrPublish'];

        $response = Self::getPowerPanelRecords($cmsPageFields, $aliasFields, false, false, $menuFields)->deleted()->filter($filterArr)->get();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordsForMenu($moduleCode = false)
    {
        $response = false;
        $cmsPageFields = ['id', 'intAliasId', 'intFKModuleCode', 'varTitle', 'txtDescription', 'varMetaTitle', 'varMetaKeyword', 'varMetaDescription', 'chrPublish', 'chrDelete'];
        $aliasFields = ['id', 'varAlias'];
        $moduleFields = ['id', 'varModuleName'];

        $response = Self::getPowerPanelRecords($cmsPageFields, $aliasFields, $moduleFields, $moduleCode)
            ->deleted()
            ->publish()
            ->orderBy('varTitle', 'ASC')
            ->get();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getPagesWithModule($moduleCode = false)
    {
        $response = false;
        $cmsPageFields = ['id', 'intAliasId', 'intFKModuleCode', 'varTitle', 'txtDescription', 'varMetaTitle', 'varMetaKeyword', 'varMetaDescription', 'chrPublish', 'chrDelete'];
        $moduleFields = ['id', 'varModuleName'];

        $response = Self::getPowerPanelRecords($cmsPageFields, false, $moduleFields, $moduleCode)
            ->deleted()
            ->publish()
            ->orderBy('varTitle')
            ->get();
        return $response;
    }
    public static function getPageWithModuleId($moduleCode = false)
    {
        $response = false;
        $cmsPageFields = ['id', 'intAliasId', 'intFKModuleCode', 'varTitle'];
        $moduleFields = ['id', 'varModuleName'];

        $response = Self::getPowerPanelRecords($cmsPageFields, false, $moduleFields, $moduleCode)
            ->deleted()
            ->publish()
            ->Pages()
            ->first();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordById($id, $cmsPageFields = false, $aliasFields = false, $moduleFields = false)
    {
        $response = false;
        $cmsPageFields = ['id', 'intAliasId', 'intFKModuleCode', 'varTitle', 'txtDescription', 'varMetaTitle', 'varMetaKeyword', 'varMetaDescription', 'chrPublish', 'chrDelete'];
        $aliasFields = ['id', 'varAlias'];
        $moduleFields = ['id', 'varModuleName'];

        $response = Self::getPowerPanelRecords($cmsPageFields, $aliasFields)->deleted()->checkRecordId($id)->first();
        return $response;
    }

    /**
     * This method handels retrival of record for delete
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordsforDeleteById($id)
    {
        $response = false;
        $moduleFields = ['id', 'varTitle'];
        $response = Self::getPowerPanelRecords($moduleFields)->checkRecordId($id)->first();
        return $response;
    }

    /**
     * This method handels retrival of record by id for Log Manage
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordForLogById($id)
    {
        $response = false;
        $cmspageFields = ['id', 'intAliasId', 'intFKModuleCode', 'varTitle', 'txtDescription', 'varMetaTitle', 'varMetaKeyword', 'varMetaDescription', 'chrPublish', 'chrDelete'];
        $aliasFields = ['id', 'varAlias'];
        $moduleFields = ['id', 'varModuleName'];
        $response = Self::getPowerPanelRecords($cmspageFields, $aliasFields, $moduleFields)->deleted()->checkRecordId($id)->first();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    protected static $fetchedOrder = [];
    protected static $fetchedOrderObj = null;
    public static function getRecordByOrder($order = false)
    {
        $response = false;
        $moduleFields = [
            'id',
            'intDisplayOrder',
        ];
        if (!in_array($order, Self::$fetchedOrder)) {
            array_push(Self::$fetchedOrder, $order);
            Self::$fetchedOrderObj = Self::getPowerPanelRecords($moduleFields)
                ->deleted()
                ->orderCheck($order)
                ->first();
        }
        $response = Self::$fetchedOrderObj;
        return $response;
    }

    /**
     * This method handels retrival of record with id and title
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getPagesIdTitle()
    {
        $response = false;
        $cmsPageFields = ['id', 'varTitle'];
        $response = Self::getPowerPanelRecords($cmsPageFields)
            ->deleted()
            ->get();
        return $response;
    }

    /**
     * This method handels retrival of record with id and title
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getHomePage()
    {
        $response = false;
        $cmsPageFields = ['id', 'varTitle', 'intFKModuleCode'];
        $moduleFields = ['id'];
        $response = Self::getPowerPanelRecords($cmsPageFields, false, $moduleFields)
            ->getHomePage()
            ->first();
        return $response;
    }

    /**
     * This method handels retrival of page title by page id
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getPageTitleById($id = false)
    {
        $response = false;
        $cmsPageFields = ['varTitle', 'intFKModuleCode'];
        $response = Self::getPowerPanelRecords($cmsPageFields)
            ->deleted()
            ->checkRecordId($id)
            ->first();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getOptionPageList($filterArr = false)
    {
        $response = false;
        $cmsPageFields = ['id', 'varTitle'];
        $response = Self::getPowerPanelRecords($cmsPageFields)->pluck('varTitle', 'id');
        return $response;
    }

    public static function getMenuData($param)
    {
        $response = DB::table('cms_page')
            ->select('menu.id')
            ->leftJoin('menu', 'menu.intPageId', '=', 'cms_page.id')
            ->where('cms_page.chrDelete', '=', 'N')
            ->where('menu.chrDelete', '=', 'N')
            ->where('menu.chrActive', '=', 'Y')
            ->where('menu.intPageId', $param)
            ->count();

        return $response;
    }

    public static function getRecordByIdForHits($id = false)
     {
        $response = false;
        $cmsPageFields = ['intAliasId', 'intFKModuleCode'];
        $response = Self::getPowerPanelRecords($cmsPageFields)
            ->deleted()
            ->whereIn('id', $id)
            ->get();
        return $response;
     }
    /**
     * This method handels alias relation
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function alias()
    {
        return $this->belongsTo('App\Alias', 'intAliasId', 'id');
    }

    /**
     * This method handels module relation
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function modules()
    {
        return $this->belongsTo('App\Modules', 'intFKModuleCode', 'id');
    }

    /**
     * This method handels module relation
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function menu()
    {
        return $this->belongsTo('App\Menu', 'id', 'intPageId');
    }

    /**
     * This method handels module relation
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */

    // public function webhits()
    // {
    //         return $this->hasMany('App\Pagehit', 'fkIntAliasId', 'intAliasId');
    // }

    /**
     * This method handels retrival of blog records
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public static function getRecords($moduleId = false)
    {
        return self::with(['alias' => function ($query) use ($moduleId) {
            $query->checkModuleCode($moduleId);
        }, 'modules']);
    }

    /**
     * This method handels alias id scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopeCheckAliasId($query, $id)
    {
        return $query->where('intAliasId', $id);
    }
    /**
     * This method handels alias id scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopeCheckModuleId($query, $id)
    {
        return $query->where('intFKModuleCode', $id);
    }
    /**
     * This method handels record id scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopeCheckRecordId($query, $id)
    {
        return $query->where('id', $id);
    }

    /**
     * This method handels order scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopeOrderCheck($query, $order)
    {
        return $query->where('intDisplayOrder', $order);
    }

    /**
     * This method handels home page scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopeGetHomePage($query)
    {
        return $query->where('varTitle', 'Home');
    }

    /**
     * This method handels publish scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopePublish($query)
    {
        return $query->where(['chrPublish' => 'Y']);
    }

    public function scopePages($query)
    {
        return $query->where('varTitle', '=', 'Pages');
    }

    /**
     * This method handels delete scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopeDeleted($query)
    {
        return $query->where(['chrDelete' => 'N']);
    }

    /**
     * This method handels filter scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopeFilter($query, $filterArr = false, $retunTotalRecords = false)
    {
        $response = null;
        if ($filterArr['orderByFieldName'] != null && $filterArr['orderTypeAscOrDesc'] != null) {
            $query = $query->orderBy($filterArr['orderByFieldName'], $filterArr['orderTypeAscOrDesc']);
        } else {
            $query = $query->orderBy('varTitle', 'ASC');
        }

        if (!$retunTotalRecords) {
            if (!empty($filterArr['iDisplayLength']) && $filterArr['iDisplayLength'] > 0) {
                $data = $query->skip($filterArr['iDisplayStart'])->take($filterArr['iDisplayLength']);
            }
        }

        if (!empty($filterArr['statusFilter']) && $filterArr['statusFilter'] != ' ') {
            $data = $query->where('chrPublish', $filterArr['statusFilter']);
        }
        if (!empty($filterArr['searchFilter']) && $filterArr['searchFilter'] != ' ') {
            $data = $query->where('varTitle', 'like', "%" . $filterArr['searchFilter'] . "%");
        }
        if (!empty($filterArr['rangeFilter']) && $filterArr['rangeFilter'] != ' ') {
            $data = $query->whereRaw('DATE(dtStartDateTime) >= DATE("' . date('Y-m-d', strtotime($filterArr['rangeFilter']['from'])) . '")  AND DATE(dtEndDateTime) <= DATE("' . date('Y-m-d', strtotime($filterArr['rangeFilter']['to'])) . '")');
        }
        if (!empty($query)) {
            $response = $query;
        }
        return $response;
    }
    /**
     * This method handels front search scope
     * @return  Object
     * @since   2016-08-09
     * @author  NetQuick
     */
    public function scopeFrontSearch($query, $term = '')
    {
        return $query->where(['varTitle', 'like', '%' . $term . '%']);
    }

    public function banners()
    {
        return $this->hasMany('App\Banner', 'image', 'id');
    }

    

}
