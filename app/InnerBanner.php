<?php
/**
 * The Banner class handels bannner queries
 * ORM implemetation.
 * @package   Netquick powerpanel
 * @license   http://www.opensource.org/licenses/BSD-3-Clause
 * @version   1.1
 * @since       2017-07-20
 */
namespace App;

use Cache;
use DB;
use Illuminate\Database\Eloquent\Model;

class InnerBanner extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'inner_banner';
    protected $fillable = [
        'varTitle',
        'varSubTitle',
        'fkIntImgId',
        'fkModuleId',
        'fkIntPageId',
        'intDisplayOrder',
        'chrPublish',
        'chrDelete',
    ];

    public static function getInnerBannerList($pageId = false, $moduleId = false)
    {
        $response = false;
        $response = Cache::tags(['InnerBanner'])->get('getInnerBannerList');
        if (empty($response)) {
            $moduleFields = ['varTitle', 'varSubTitle', 'fkIntImgId'];
            $response = Self::getFrontRecords($moduleFields)
                ->inRandomOrder()
                ->deleted()
                ->publish();

            if ($pageId) {
                $response = $response->checkByPageId($pageId);
            }
            if ($moduleId) {
                $response = $response->checkModuleId($moduleId);
            }
            $response = $response->first();
            Cache::tags(['InnerBanner'])->forever('getInnerBannerList', $response);
        }
        return $response;
    }

    public static function getDefaultBannerList()
    {
        $response = false;
        $response = Cache::tags(['InnerBanner'])->get('getDefaultBannerList');
        if (empty($response)) {
            $moduleFields = ['fkIntImgId', 'varTitle', 'varSubTitle'];
            $response = Self::getFrontRecords($moduleFields)
                ->displayOrderBy('ASC')
                ->deleted()
                ->publish()
                ->get();
            Cache::tags(['InnerBanner'])->forever('getDefaultBannerList', $response);
        }
        return $response;
    }

    /**
     * This method handels retrival of front banner list
     * @return  Object
     * @since   2017-10-14
     * @author  NetQuick
     */

    public static function getInnerBannerListingPage($pageId = false, $moduleId = false)
    {
        $response = false;

        $moduleFields = ['varTitle', 'fkIntImgId', 'varSubTitle'];
        $response = Self::getFrontRecords($moduleFields)
                    ->publish()
                    ->inRandomOrder()
                    ->deleted();

        if ($pageId) {
            $response = $response->checkByPageId($pageId);
        }
        if ($moduleId) {
            $response = $response->checkModuleId($moduleId);
        }

        if (!empty($response)) {
            $response = $response->first();
        }
        return $response;
    }

    /**
     * This method handels retrival of inner banner record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function innerBannerCount()
    {
        $response = false;
        $moduleFields = ['id'];
        $response = Self::getPowerPanelRecords($moduleFields)
            ->deleted()
            ->count();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordList($filterArr = false)
    {
        $response = false;
        
        $moduleFields = [
            'id',
            'chrPublish',
            'fkIntPageId',
            'fkIntImgId',
            'fkModuleId',
            'varTitle', 
            'varSubTitle',
            'fkIntImgId',
            'intDisplayOrder',
            'created_at'
        ];
        $pageFields = ['id', 'varTitle'];
        $mdlFields = ['id', 'varTitle', 'varTableName'];
        $response =
        $response = Self::getPowerPanelRecords($moduleFields, $pageFields, $mdlFields)

        //Self::getPowerPanelRecords($moduleFields, $pageFields,$mdlFields)

            ->deleted()
            ->filter($filterArr)
            ->get();
            
        return $response;
    }

    public static function getInnerRecordList($filterArr = false)
    {
        $response = false;
        $moduleFields = [
            'id',
            'chrPublish',
            'fkIntPageId',
            'fkIntImgId',
            'fkModuleId',
            'varTitle',
            'varSubTitle',
            'fkIntImgId',
            'intDisplayOrder',
        ];
        $pageFields = ['id', 'varTitle'];
        $mdlFields = ['id', 'varTitle', 'varTableName'];
        $response = Self::getPowerPanelRecords($moduleFields, $pageFields,  $mdlFields)
            ->deleted()
            ->filter($filterArr)
            ->get();

        return $response;
    }

    /**
     * This method handels retrival of record
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordById($id = false)
    {
        $response = false;
        $moduleFields = [
            'id',
            'varTitle',
            'varSubTitle',
            'fkModuleId',
            'fkIntImgId',
            'intDisplayOrder',
            'chrPublish',
            'fkIntPageId',
        ];

        $pageFields = ['id'];
        $response = Self::getPowerPanelRecords($moduleFields, $pageFields)
            ->deleted()
            ->checkRecordId($id)
            ->first();
        return $response;
    }

    /**
     * This method handels retrival of record by id for Log Manage
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordForLogById($id)
    {
        $response = false;
        $moduleFields = [
            'id',
            'varTitle',
            'varSubTitle',
            'fkModuleId',
            'fkIntImgId',
            'intDisplayOrder',
            'chrPublish',
            'fkIntPageId',
        ];

        $response = Self::getPowerPanelRecords($moduleFields)->deleted()->checkRecordId($id)->first();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    protected static $fetchedOrder = [];
    protected static $fetchedOrderObj = null;
    public static function getRecordByOrder($order = false)
    {
        $response = false;
        $moduleFields = [
            'id',
            'intDisplayOrder',
        ];
        if (!in_array($order, Self::$fetchedOrder)) {
            array_push(Self::$fetchedOrder, $order);
            Self::$fetchedOrderObj = Self::getPowerPanelRecords($moduleFields)
                ->deleted()
                ->orderCheck($order)
                ->first();
        }
        $response = Self::$fetchedOrderObj;
        return $response;
    }

    /**
     * This method handels retrival of record for notification
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordNotify($id = false)
    {
        $response = false;
        $imageFields = false;
        $moduleFields = ['varTitle'];
        $response = Self::getPowerPanelRecords($moduleFields)
            ->deleted()
            ->checkRecordId($id)
            ->first();
        return $response;
    }

    public static function getAssignedPageId()
    {
        $response = false;

        $moduleFields = ['fkIntPageId','fkModuleId'];
        $response = Self::select($moduleFields)
            ->publish()
            ->get();
        return $response;
    }

    /**
     * This method handels set/unset of default banner
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function setDefault($id = false, $flagArr = false)
    {
        $response = false;
        $response = Self::where('id', $id)->update($flagArr);
        return $response;
    }

    #Database Configurations========================================
    /**
     * This method handels retrival of front end records
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public static function getFrontRecords($moduleFields = false)
    {
        $data = [];
        $response = false;
        $response = self::select($moduleFields);
        if (count($data) > 0) {
            $response = $response->with($data);
        }
        return $response;
    }

    /**
     * This method handels retrival of backednd records
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public static function getPowerPanelRecords($moduleFields = false, $pageFields = false, $mdlFields = false)
    {
        $data = [];
        $response = false;
        $response = self::select($moduleFields);
        if ($pageFields != false) {
            $data['pages'] = function ($query) use ($pageFields) {$query->select($pageFields);};
        }

        if ($mdlFields != false) {
            $data['modules'] = function ($query) use ($mdlFields) {
                $query->select($mdlFields);
            };
        }

        if (count($data) > 0) {
            $response = $response->with($data);
        }
        return $response;
    }

    /**
     * This method handels image relation
     * @return  Object
     * @since   2017-07-20
     */
    public function image()
    {
        $response = false;
        $response = $this->belongsTo('App\Image', 'fkIntImgId', 'id');
        return $response;
    }

    /**
     * This method handels pages relation
     * @return  Object
     * @since   2017-07-20
     */
    public function pages()
    {
        $response = false;
        $response = $this->belongsTo('App\CmsPage', 'fkIntPageId', 'id');
        return $response;
    }

    /**
     * This method handels pages relation
     * @return  Object
     * @since   2017-07-20
     */
    public function modules()
    {
        $response = false;
        $response = $this->belongsTo('App\Modules', 'fkModuleId', 'id');
        return $response;
    }

    /**
     * This method handels retrival of banners records
     * @return  Object
     * @since   2016-07-20
     */
    public static function getRecords()
    {
        $response = false;
        $response = self::with(['image', 'pages']);
        return $response;
    }

    /**
     * This method handels record id scope
     * @return  Object
     * @since   2016-07-24
     */
    public function scopeCheckRecordId($query, $id)
    {
        $response = false;
        $response = $query->where('id', $id);
        return $response;
    }

    public function scopeCheckByPageId($query, $id)
    {
        $response = false;
        $response = $query->where('fkIntPageId', $id);
        return $response;
    }

    /**
     * This method handels order scope
     * @return  Object
     * @since   2016-07-20
     */
    public function scopeOrderCheck($query, $order)
    {
        $response = false;
        $response = $query->where('intDisplayOrder', $order);
        return $response;
    }

    /**
     * This method handels publish scope
     * @return  Object
     * @since   2016-07-20
     */
    public function scopePublish($query)
    {
        $response = false;
        $response = $query->where(['chrPublish' => 'Y']);
        return $response;
    }

    /**
     * This method handels delete scope
     * @return  Object
     * @since   2016-07-20
     */
    public function scopeDeleted($query)
    {
        $response = false;
        $response = $query->where(['chrDelete' => 'N']);
        return $response;
    }

    /**
     * This method checking default banner
     * @return  Object
     * @since   2016-07-14
     */
    public function scopeDisplayOrderBy($query, $orderBy)
    {
        $response = false;
        $response = $query->orderBy('intDisplayOrder', $orderBy);
        return $response;
    }

    public function scopeCheckModuleId($query, $moduleId)
    {
        $response = false;
        $response = $query->where('fkModuleId', $moduleId);
        return $response;
    }

/**
 * This method handels filter scope
 * @return  Object
 * @since   2016-07-14
 */
    public function scopeFilter($query, $filterArr = false, $retunTotalRecords = false)
    {
        $response = null;
        if ($filterArr['orderByFieldName'] != null && $filterArr['orderTypeAscOrDesc'] != null) {
            $query = $query->orderBy($filterArr['orderByFieldName'], $filterArr['orderTypeAscOrDesc']);
        } else {
            $query = $query->orderBy('created_at', 'desc');
        }

        if (!$retunTotalRecords) {
            if (!empty($filterArr['iDisplayLength']) && $filterArr['iDisplayLength'] > 0) {
                $data = $query->skip($filterArr['iDisplayStart'])->take($filterArr['iDisplayLength']);
            }
        }

        if (!empty($filterArr['statusFilter']) && $filterArr['statusFilter'] != ' ') {
            $data = $query->where('chrPublish', $filterArr['statusFilter']);
        }

        if (!empty($filterArr['searchFilter']) && $filterArr['searchFilter'] != ' ') {
            $data = $query->where('varTitle', 'like', "%" . $filterArr['searchFilter'] . "%");
        }

        if (!empty($filterArr['pageFilter']) && $filterArr['pageFilter'] != ' ') {
            $data = $query->where('fkModuleId', '=', $filterArr['pageFilter']);
        }

        if (!empty($query)) {
            $response = $query;
        }
        return $response;
    }

    /**
     * This method handels filter scope
     * @return  Object
     * @since   2016-07-14
     */
    public static function add_pages()
    {
        $response = false;
        $module_code = DB::table('modules')->where('var_module_name', '=', 'cms-page')->first();
        $response = DB::table('cms_pages')
            ->select('cms_pages.*')
            ->where('cms_pages.chr_delete', '=', 'N')
            ->where('cms_pages.chr_publish', '=', 'Y')
            ->groupBy('cms_pages.id')->get();
        return $response;
    }

}
