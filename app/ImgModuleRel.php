<?php

namespace App;

use App\Modules;
use DB;
use Illuminate\Database\Eloquent\Model;

class ImgModuleRel extends Model
{
    protected $table = 'image_module_rel';
    protected $fillable = [
        'id',
        'varTmpId',
        'intFkImgId',
        'intFkModuleCode',
        'intRecordId',
        'created_at',
        'updated_at',
    ];
    protected static $fetchedID = [];
    protected static $fetchedImg = null;

    public static function getRecord($idArr = null)
    {
        $response = false;
        if (!empty($idArr)) {
            $response = ImgModuleRel::select('intFkImgId')
                ->whereIn('intFkImgId', $idArr)
                ->get();
        }
        return $response;
    }

    public static function addRecord($data = false)
    {
        $response = false;
        if ($data != false && !empty($data)) {
            $recordId = ImgModuleRel::insertGetId($data);
            if ($recordId > 0) {
                $response = $recordId;
            }
        }
        return $response;
    }

    public static function deleteRecord($whereConditions = null)
    {
        $response = false;
        if (!empty($whereConditions)) {
            $response = ImgModuleRel::where($whereConditions)->delete();
        }
        return $response;
    }

    /* Show the name of the used images: */
    public static function getRecordList($idArr = null)
    {
        $response = false;
        if (!empty($idArr)) {
            //SELECT * FROM `nq_image_module_rel` WHERE intFkModuleCode="15" AND intFkImgId="92";
            $collectData = ImgModuleRel::select(['intFkImgId', 'intFkModuleCode', 'intRecordId'])
                ->whereIn('intFkImgId', $idArr)
                ->groupBy('intRecordId')
                ->get()
                ->toArray();

            $CollectAllData = [];
            foreach ($collectData as $key => $value) {
                $tableName = Modules::getModuleTableName($value['intFkModuleCode']);

                if (!empty($tableName->varTableName)) {
                    $response[] = DB::table($tableName->varTableName)->where('id', $value['intRecordId'])->get();
                }
                $response[$key]['moduleName'] = $tableName->varModuleName;
            }
        }
        return $response;
    }

    public static function getRecordListUpdated($idArr = null)
    {
        $response = false;
        if (!empty($idArr)) {

            //SELECT * FROM `nq_image_module_rel` WHERE intFkModuleCode="15" AND intFkImgId="92";
            $collectData = ImgModuleRel::select(['intFkImgId', 'intFkModuleCode', 'intRecordId'])
                ->whereIn('intFkImgId', $idArr)
                ->get()
                ->toArray();

            $CollectAllData = [];
            foreach ($collectData as $key => $value) {

                $tableName = Modules::getModuleTableName($value['intFkModuleCode']);
                $tableNameJSON = json_decode($tableName);

                if (!empty($tableNameJSON->varTableName)) {

										$getRecordImgId = DB::table($tableNameJSON->varTableName)->where('id', $value['intRecordId'])->get();
										if(isset($getRecordImgId['0']))
										{
											$imgIDExplode = $getRecordImgId['0']->fkIntImgId;
											$recordimgArr = explode(',', $imgIDExplode);
											$selectedImgArr = $idArr;
											$imgResult = array_diff($recordimgArr, $selectedImgArr);
											$replceImgArr = implode(',', $imgResult);

											DB::table($tableNameJSON->varTableName)->where('id', $value['intRecordId'])->update(['fkIntImgId' => $replceImgArr]);
											ImgModuleRel::where('intFkImgId', $value['intFkImgId'])->delete();
										}
                }

            }
        }
        return $response;
    }
    /* End code here for used images    */

}
