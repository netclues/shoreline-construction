<?php
namespace App\Helpers;
use App\CommonModel;
use Validator;
use App\Http\Traits\slug;

class AddCategoryAjax {
	static function Add($data, $module){
		$response=false;
		$varTitle = trim($data['varTitle']);
		$selectedCat= (isset($data['selectedCat'])?$data['selectedCat']:array());
		$parentCategory = $data['parent_category_id']; 
		$rules = ['varTitle' => 'required|max:160'];
		$validator = Validator::make($data, $rules);
		
		if ($validator->passes()) {
			$total = CommonModel::getRecordCount();
			$order = $total + 1;
			$newCategoryID = CommonModel::addRecord([
				'varTitle'=>$varTitle,
				'intAliasId' => MyLibrary::insertAlias(slug::create_slug($varTitle, true)),
				'intParentCategoryId'=> $parentCategory,
				'txtShortDescription'=>$varTitle,
				'intDisplayOrder' => $order,
				'chrPublish'=>'Y',
				'chrDelete'=>'N'
			]);
		}
		array_push($selectedCat, (string)$newCategoryID);
		$module = '\\App\\' . $module;
		$category = $module::getCatWithParent();
		$category = CategoryArrayBuilder::getArray($category);		
		$MainMenuCategory = json_encode($category);

		$categoriesHtml=Categories::Parentcategoryhierarchy($selectedCat,false,$module);

		$response['cat'] = $MainMenuCategory;
		$response['selected'] = $selectedCat;
		$response['categoriesHtml'] = $categoriesHtml;
		$response=json_encode($response);	

		return $response;
	}
}