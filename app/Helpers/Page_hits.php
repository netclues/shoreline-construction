<?php
/**
 * This helper generates email sender
 * @package   Netquick
 * @version   1.00
 * @since     2016-11-14
 */
namespace App\Helpers;

use App\Alias;
use App\Helpers\MyLibrary;
use App\Http\Controllers\Controller;
use App\Pagehit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;use Illuminate\Support\Facades\Request;
use Session;
use Config;
use Cookie;
use Crypt;

class Page_hits extends Controller
{
    public static function insertHits($page)
    {
        $aliasID    = null;
        $sever_info = Request::server('HTTP_USER_AGENT');
        $ip_address = MyLibrary::get_client_ip();

        $device     = '';

        if (Config::get('Constant.DEVICE') == 'iPad') 
        {
            $device = 'Y';
        } elseif (Config::get('Constant.DEVICE') == 'mobile') {
            $deviceId = explode(',', $ip_address);
            if (isset($deviceId[0])) {
                $ip_address = $deviceId[0];
            }
            $device = 'N';
        } else {
            $device = 'Y';
        }

        if (isset($page->intAliasId)) {
            $aliasID = $page->intAliasId;
        }

       //Set session cookie to avoid multiple page hits
        $pagehit_cookie = '';
        if (Cookie::get("page_hit") != '') {
            $pagehit_cookie = Crypt::decrypt((Cookie::get("page_hit")), false);
        } else {
            $pagehit_cookie = str_random(30);
            Cookie::queue('page_hit', $pagehit_cookie, 0, "/");
        }

        $userSession = $pagehit_cookie;


        if (!empty($userSession) && !empty($device) && !empty($ip_address) && !empty($aliasID)) {

            $response = Pagehit::select('id')->where(['fkIntAliasId' => $aliasID, 'varSessionId' => $userSession, 'isWeb' => $device, 'varIpAddress' => $ip_address])->first();
            
            if (!isset($response->id)) 
            {
                Pagehit::insert([
                    'fkIntAliasId'   => $aliasID,
                    'varBrowserInfo' => $sever_info,
                    'isWeb'          => $device,
                    'varIpAddress'   => $ip_address,
                    'varSessionId'   => $userSession,
                    'created_at'     => Carbon::now(),
                    'updated_at'     => Carbon::now(),
                ]);
            }
        }
    }

    public static function insertDetailPageHits($segmentTwo)
    {
        $aliasID    = null;
        $sever_info = Request::server('HTTP_USER_AGENT');
        $ip_address = MyLibrary::get_client_ip();

        $device     = '';
        //$session_id = Session::getId();
        //Set session cookie to avoid multiple page hits
        $pagehit_cookie = '';
        if (Cookie::get("page_hit") != '') {
            $pagehit_cookie = Crypt::decrypt((Cookie::get("page_hit")), false);
        } else {
            $pagehit_cookie = str_random(30);
            Cookie::queue('page_hit', $pagehit_cookie, 0, "/");
        }

        $userSession = $pagehit_cookie;


        if (Config::get('Constant.DEVICE') == 'iPad') {
            $device = 'Y';
        } elseif (Config::get('Constant.DEVICE') == 'mobile') {
            $deviceId = explode(',', $ip_address);
            if (isset($deviceId[0])) {
                $ip_address = $deviceId[0];
            }
            $device = 'N';
        } else {
            $device = 'Y';
        }

        if (!empty($segmentTwo)) {
            $aliasObj = Alias::getAlias($segmentTwo);
            if (!empty($aliasObj) && isset($aliasObj->id)) {
                $aliasID = $aliasObj->id;
            }
        }
        if (!empty($userSession) && !empty($device) && !empty($ip_address) && !empty($aliasID)) {
            $isExist = Pagehit::select('id')->where(['fkIntAliasId' => $aliasID, 'varSessionId' => $userSession, 'isWeb' => $device, 'varIpAddress' => $ip_address])->first();
            if (!isset($isExist->id)) {
                Pagehit::insert([
                    'fkIntAliasId'   => $aliasID,
                    'varBrowserInfo' => $sever_info,
                    'isWeb'          => $device,
                    'varIpAddress'   => $ip_address,
                    'varSessionId'   => $userSession,
                    'created_at'     => Carbon::now(),
                    'updated_at'     => Carbon::now(),
                ]);
            }
        }
    }

}
