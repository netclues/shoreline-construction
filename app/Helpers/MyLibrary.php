<?php
namespace App\Helpers;

use App\Alias;
use App\CmsPage;
use App\CommonModel;
use App\Http\Traits\slug;
use App\Log;
use App\Modules;
use App\ModuleSettings;
use App\RecentUpdates;
use Auth;
use Config;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

class MyLibrary
{
    use slug;
    public static function getModelNameSpace()
    {
        $modelName = Config::get('Constant.MODULE.MODEL_NAME');
        $modelNameSpace = '\\App\\' . $modelName;
        return $modelNameSpace;
    }

    public static function setFrontRoutes($segment = false)
    {

        $aliasId = slug::resolve_alias_for_routes($segment);
        $response = CmsPage::getPageWithAlias($aliasId);

        return $response;
    }

    public static function setConstants($segmentsArr = false)
    {
        Config::set('Constant.APP_KEY', env('APP_KEY'));
        if (empty($segmentsArr)) {
            $module = 'home';
        } elseif ($segmentsArr[0] != 'powerpanel') {
            $module = $segmentsArr[0];
        } elseif (isset($segmentsArr[1])) {
            $module = $segmentsArr[1];
        }

        $response = false;
        if (!empty($segmentsArr) && isset($module)) {

            $objModules = Modules::getAllModuleData($module);

            if (empty($objModules) && $segmentsArr[0] != 'powerpanel') {
                $aliasId = slug::resolve_alias_for_routes($module);
                $response = CmsPage::getPageWithAlias($aliasId);
                if (isset($response->modules->varModuleName)) {
                    $module = $response->modules->varModuleName;
                    $objModules = Modules::getAllModuleData($module);
                }
            }

            if (!empty($objModules)) {
                $settings = ModuleSettings::getSettings($objModules->id);
                Config::set('Constant.MODULE.ID', $objModules->id);
                Config::set('Constant.MODULE.TITLE', $objModules->varTitle);
                Config::set('Constant.MODULE.NAME', $objModules->varModuleName);
                Config::set('Constant.MODULE.TABLE_NAME', $objModules->varTableName);
                Config::set('Constant.MODULE.MODEL_NAME', $objModules->varModelName);
                Config::set('Constant.MODULE.CONTROLLER', $objModules->varModuleClass);
                $settings = isset($settings->txtSettings) ? $settings->txtSettings : json_encode(null);
                Config::set('Constant.MODULE.SETTINGS', $settings);

                $endSegment = collect($segmentsArr)->last();
                $action = '';
                $notificationAction = '';
                if ($endSegment == $objModules->varModuleName) {
                    $action = 'list';
                    $notificationAction = 'listed';
                } else if ($endSegment == 'add' || $endSegment == 'create') {
                    $action = 'add';
                    $notificationAction = 'added';
                } else if ($endSegment == 'edit' || $endSegment == 'update_status' || $endSegment == 'update') {
                    Config::set('Constant.RECORD.ALIAS', $segmentsArr[2]);
                    $action = 'edit';
                    $notificationAction = 'updated';
                } else if ($endSegment == "destroy" || $endSegment == "DeleteRecord") {
                    $action = 'delete';
                    $notificationAction = 'deleted';
                }
                Config::set('Constant.MODULE.ACTION', $action);
                Config::set('Constant.NOTIFICATION.ACTION', $notificationAction);

                $response = true;
            }
        }

        return $response;
    }

    public static function logData($id = false, $moduleId = false)
    {
        if ($moduleId == false) {
            $moduleId = Config::get('Constant.MODULE.ID');
        }
        $response = array();
        if (!empty($id)) {
            $logArr = array();
            $logArr['fk_record_id'] = $id;
            $logArr['userId'] = Auth::id();
            $logArr['moduleCode'] = $moduleId;
            $logArr['ipAddress'] = self::get_client_ip();
            $logArr['chr_publish'] = 'Y';
            $logArr['chr_delete'] = 'N';
            $logArr['action'] = Config::get('Constant.MODULE.ACTION');
            $response = $logArr;
        }
        return $response;
    }
    public static function notificationData($id = false, $data = false, $moduleId = false)
    {
        if ($moduleId == false) {
            $moduleId = Config::get('Constant.MODULE.ID');
        }
        $response = array();
        if (!empty($id) && !empty($data)) {
            $verb = '';
            if (Config::get('Constant.MODULE.ACTION') == "delete") {
                $verb = 'from';
            } else if (Config::get('Constant.MODULE.ACTION') == "add") {
                $verb = 'to';
            } else if (Config::get('Constant.MODULE.ACTION') == "edit") {
                $verb = 'at';
            }
            if (isset($data->varTitle)) {
                $title = $data->varTitle;
            } else {
                $title = $data->name;
            }
            $notification = "%s " . Config::get('Constant.MODULE.ACTION') . ' ' . $title . ' ' . $verb . ' ' . Config::get('Constant.MODULE.NAME') . '.';
            $recentNotification = '%s ' . Config::get('Constant.NOTIFICATION.ACTION') . ' ' . $title . ' ' . $verb . ' ' . Config::get('Constant.MODULE.NAME') . '.';
            $notificationArr['fkIntRecordCode'] = $id;
            $notificationArr['fkIntUserId'] = Auth::id();
            $notificationArr['varIpAddress'] = self::get_client_ip();
            $notificationArr['fkIntModuleId'] = $moduleId;
            $notificationArr['txtNotification'] = $notification;
            $notificationArr['txtRecentNotification'] = $recentNotification;
            $notificationArr['chrRecordDelete'] = $verb == 'from' ? 'Y' : 'N';

            $response = $notificationArr;
        }
        return $response;
    }

    public static function deleteMultipleRecords($data = false)
    {
        $response = false;
        $responseAr = [];
        if (!empty($data)) {

            $modelNameSpace = self::getModelNameSpace();
            // $updateFields = ['chrDelete' => 'Y', 'chrPublish' => 'N'];
            // $whereINConditions = $data['ids'];

            // $update = CommonModel::updateMultipleRecords($whereINConditions, $updateFields);

            foreach ($data['ids'] as $key => $id) {
                //if ($update) {
                    $objModule = CommonModel::getRecordsForDeleteById($id);
                    // var_dump($objModule->toArray());
                    // die();

                    if (isset($objModule->intDisplayOrder)) {
                        CommonModel::updateOrder($objModule);
                        $updateFields['intDisplayOrder'] = 1;
                    }

                    if (isset($objModule->intAliasId)) {
                        Alias::deleteAlias($objModule->intAliasId, Config::get('Constant.MODULE.ID'));
                    }

                    if (!empty($id)) {
                        $logArr = self::logData($id);
                        $title = '-';
                        if (isset($objModule->varTitle)) {
                            $title = $objModule->varTitle;
                        } else if (isset($objModule->varName)) {
                            $title = $objModule->varName;
                        } else if (isset($objModule->name)) {
                            $title = $objModule->name;
                        }
                        $logArr['varTitle'] = $title;
                        Log::recordLog($logArr);
                        array_push($responseAr, $objModule->id);
                        $updateRecentUpdatesFilelds = ['chrRecordDelete' => 'Y'];
                        if (Auth::user()->can('recent-updates-list')) {
                            $notificationUpdate = RecentUpdates::updateRecords($id, $updateRecentUpdatesFilelds);
                            if ($notificationUpdate) {
                                $notificationArr = self::notificationData($id, $objModule);
                                RecentUpdates::setNotification($notificationArr);
                            }
                        }
                    }
                //}
            }

            $whereINConditions = $data['ids'];
            CommonModel::deleteMultipleRecords($whereINConditions);

            $response = $responseAr;
        }

        return $response;
    }

    public static function setPublishUnpublish($alias = false, $val)
    {
        $response = false;
        if (!empty($alias) && !empty($val)) {
            $modelNameSpace = self::getModelNameSpace();
            if (is_numeric($alias)) {
                $id = $alias;
                $whereConditions = ['id' => $id];
                //$objModule       = $modelNameSpace::getRecordById($id);
            }
            $logArr = self::logData($id);
            if (!empty($val) && ($val == 'Unpublish')) {
                $updateField = ['chrPublish' => 'N'];

                $update = CommonModel::updateRecords($whereConditions, $updateField);
                if ($update) {
                    $logArr['action'] = 'Unpublish';
                    Log::recordLog($logArr);
                    $response = $update;
                }
            }
            if (!empty($val) && ($val == 'Publish')) {
                $updateField = ['chrPublish' => 'Y'];
                $update = CommonModel::updateRecords($whereConditions, $updateField);
                if ($update) {
                    $logArr['action'] = 'publish';
                    Log::recordLog($logArr);
                    $response = $update;
                }
            }
        }
        return $response;
    }

    /**
     * This method generates events seo content
     * @return  Meta values
     * @since   2016-10-25
     * @author  NetQuick
     */
    public static function generateSeocontent($title = false, $description = false, $fromajax = false)
    {

        $response = array();

        if (strlen($description) > 0) 
        {
            if ($fromajax) {
                $description = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 &]/', '', urldecode(html_entity_decode(strip_tags($description))))));
            } else {
                $description = trim(preg_replace('/ +/', ' ', preg_replace('/[^A-Za-z0-9 &]/', '', urldecode(html_entity_decode(strip_tags($description))))));
            }
        }
        
        $meta_title = (!empty($title)?$title:'');
        $meta_title = substr($meta_title, 0, 60);

        $meta_description = (!empty($description)?$description:'');
        $meta_description = substr($meta_description, 0, 160);
        

        $response['meta_title'] = $meta_title;
        $response['meta_description'] = $meta_description;

        return $response;
    }

    /**
     * This method handels swapping of available order record while editing
     * @param      order
     * @return  order
     * @since   2016-12-23
     * @author  NetQuick
     */

    public static function swapOrderEdit($order = null, $id = null)
    {        
        $modelNameSpace = self::getModelNameSpace();        
        $existingRecord = CommonModel::getRecordByOrder($modelNameSpace, $order);
        // print_r($existingRecord);die;
        $totalRecordCount = CommonModel::getTotalRecordCount($modelNameSpace);

        $currentRecord = $modelNameSpace::getRecordById($id);

        if ($currentRecord->intDisplayOrder != $order) {
            if (!empty($existingRecord) > 0) {

                $whereConditions = ['id' => $currentRecord->id];
                CommonModel::updateRecords($whereConditions, ['intDisplayOrder' => $order]);

                CommonModel::updateOrderAfterEdit($currentRecord->intDisplayOrder, $existingRecord->intDisplayOrder, $currentRecord->id);

            } else {

                if ($totalRecordCount > 0) {
                    if ($order > $totalRecordCount) {
/*code for find last record orderID */
                        $lastrecordDetail = CommonModel::getRecordByOrder($modelNameSpace, $totalRecordCount);
                        if (!empty($lastrecordDetail) > 0) {
                            $whereConditions = ['id' => $currentRecord->id];
                            CommonModel::updateRecords($whereConditions, ['intDisplayOrder' => $totalRecordCount]);
                            CommonModel::updateOrderAfterEdit($currentRecord->intDisplayOrder, $lastrecordDetail->intDisplayOrder, $currentRecord->id);
                        }
                    }
                }
            }
        }
    }

    // public static function swapOrderEdit($order = null, $id = null)
    // {
    //     $modelNameSpace = self::getModelNameSpace();
    //     $recEx          = CommonModel::getRecordByOrder($modelNameSpace, $order);

    //     if (count($recEx) > 0)
    //     {
    //         $recCur = $modelNameSpace::getRecordById($id);
    //         if ($recCur->intDisplayOrder != $recEx->intDisplayOrder)
    //         {
    //             $whereConditionsForEx = ['id' => $recEx['id']];
    //             CommonModel::updateRecords($whereConditionsForEx, ['intDisplayOrder' => $recCur->intDisplayOrder]);
    //             $whereConditionsForCur = ['id' => $recCur['id']];
    //             CommonModel::updateRecords($whereConditionsForCur, ['intDisplayOrder' => $recEx->intDisplayOrder]);
    //         }
    //     }
    // }

    /**
     * This method handels swapping of available order record while editing
     * @param      order
     * @return  order
     * @since   2016-12-23
     * @author  NetQuick
     */
    public static function swapOrder($order = null, $exOrder = null)
    {
        $modelNameSpace = self::getModelNameSpace();
        $recEx = CommonModel::getRecordByOrder($modelNameSpace, $exOrder);
        if (!empty($recEx)) {
            $recCur = CommonModel::getRecordByOrder($modelNameSpace, $order);
            if ($recCur->intDisplayOrder != $recEx->intDisplayOrder) {
                $whereConditionsForEx = ['id' => $recEx['id']];
                CommonModel::updateRecords($whereConditionsForEx, ['intDisplayOrder' => $recCur->intDisplayOrder]);
                $whereConditionsForCur = ['id' => $recCur['id']];
                CommonModel::updateRecords($whereConditionsForCur, ['intDisplayOrder' => $recEx->intDisplayOrder]);
            }
        }
    }

    /**
     * This method handels swapping of available order record while adding
     * @param      order
     * @return  order
     * @since   2016-10-21
     * @author  NetQuick
     */
    public static function swapOrderAdd($order = null)
    {
       
        $response = false;
        $modelNameSpace = self::getModelNameSpace();
        $rec = CommonModel::getRecordByOrder($modelNameSpace, $order);
        
        $total = CommonModel::getRecordCount();

        if (!empty($rec)) {
            
            $whereConditions = ['intDisplayOrder' => $order];
            CommonModel::updateRecords($whereConditions, ['intDisplayOrder' => $order + 1]);

            $updatedOrder = $order + 1;
            CommonModel::updateOrderAfterAdd($updatedOrder, $rec->id);

            $response = $order;

        } else {
            $response = $total + 1;
        }

        return $response;
    }

    // /**
    //  * This method reorders events
    //  * @return  events index view data
    //  * @since   2016-10-11
    //  * @author  NetQuick
    //  */

    // static function reorder($data) {
    //         if (isset($data['order'])) {
    //                 $data = array_filter($data['order'], function ($value) {
    //                         return $value !== '';
    //                 });
    //                 foreach ($data as $key => $value) {
    //                         if ((int)$key != 0) {
    //                                 $whereConditions = ['id'=> $key];
    //                                 CommonModel::updateRecords($whereConditions, ['intDisplayOrder' => $value]);
    //                         }
    //                 }
    //         }
    // }

    public static function insertAlias($alias = false, $moduleCode = false)
    {
        $response = false;
        $moduleCode = ($moduleCode == false) ? Config::get('Constant.MODULE.ID') : $moduleCode;

        $response = Alias::addAlias($alias, $moduleCode);
        return $response;
    }

    public static function get_client_ip()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP')) {
            $ipaddress = getenv('HTTP_CLIENT_IP');
        } else if (getenv('HTTP_X_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        } else if (getenv('HTTP_X_FORWARDED')) {
            $ipaddress = getenv('HTTP_X_FORWARDED');
        } else if (getenv('HTTP_FORWARDED_FOR')) {
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        } else if (getenv('HTTP_FORWARDED')) {
            $ipaddress = getenv('HTTP_FORWARDED');
        } else if (getenv('REMOTE_ADDR')) {
            $ipaddress = getenv('REMOTE_ADDR');
        } else {
            $ipaddress = 'UNKNOWN';
        }

        return $ipaddress;
    }

    /**
     * This method handels Encrypted code with AES Algorythm Custom Method
     * @return  Object
     * @since   2018-07-25
     * @author  NetQuick
     */

    public static function getEncryptedString($plaintext, $getAppKeyFromEnv = false)
    {
        $encryptedSring = '';
        if (!empty($plaintext)) {

            $envKey = Config::get('Constant.ENV_APP_KEY');
            if ($getAppKeyFromEnv) {
                $envKey = env('APP_KEY');
            }
            $method = 'aes-256-cbc';

            // Must be exact 32 chars (256 bit)
            $secureEnvKey = substr(hash('sha256', $envKey, true), 0, 32);

            // IV must be exact 16 chars (128 bit)
            $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

            $encryptedSring = base64_encode(openssl_encrypt($plaintext, $method, $secureEnvKey, OPENSSL_RAW_DATA, $iv));
        }
        return $encryptedSring;
    }

    /**
     * This method handels Decrypted code with AES Algorythm Custom Method
     * @return  Object
     * @since   2018-07-25
     * @author  NetQuick
     */

    public static function getDecryptedString($encrypted, $getAppKeyFromEnv = false)
    {
        $decryptedSring = '';
        if (!empty($encrypted)) {

            $envKey = Config::get('Constant.ENV_APP_KEY');
            if ($getAppKeyFromEnv) {
                $envKey = env('APP_KEY');
            }
            $method = 'aes-256-cbc';

            // Must be exact 32 chars (256 bit)
            $secureEnvKey = substr(hash('sha256', $envKey, true), 0, 32);

            // IV must be exact 16 chars (128 bit)
            $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);

            $decryptedSring = openssl_decrypt(base64_decode($encrypted), $method, $secureEnvKey, OPENSSL_RAW_DATA, $iv);
        }
        return $decryptedSring;
    }

    public static function getLaravelEncryptedString($plaintext)
    {
        $encryptedSring = '';
        if (!empty($plaintext)) {
            $encryptedSring = Crypt::encrypt($plaintext);
        }
        return $encryptedSring;
    }

    public static function getLaravelDecryptedString($encrypted)
    {
        $decryptedSring = '';
        if (!empty($encrypted)) {
            try {
                $decryptedSring = Crypt::decrypt($encrypted);
            } catch (DecryptException $e) {
                $decryptedSring = '';
            }
        }
        return $decryptedSring;
    }

}
