<?php
/**
 * This helper give description for static block section by alias.
 * @package   Netquick
 * @version   1.00
 * @since     2016-12-07
 * @author    Vishal Agrawal
 */
namespace App\Helpers;
use App\Http\Traits\slug;
use App\StaticBlocks;
use App\Modules;
use App\Alias;


class static_block
{
    use slug;
    public static function static_block($alias, $onlyParentRecords = false, $childWithParent = false)
    {
        if (!empty($alias)) 
        {
           $moduleObj = Modules::getModule('static-block');
           $aliasObj = Alias::getAliasByModuleId($alias,$moduleObj->id);
            if (!empty($aliasObj->id) && isset($aliasObj->id)) 
            {
                $staticBlockObj = StaticBlocks::getStaticBlockList($aliasObj->id);
                if (!empty($staticBlockObj)) {
                    $staticData = StaticBlocks::getStaticBlockId($staticBlockObj->id, $onlyParentRecords, $childWithParent);
                    return $staticData;
                }
            }
        }
    }

}
