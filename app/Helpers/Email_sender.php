<?php
/**
 * The Email_sender class handels email functions
 * configuration  process (ORM code Updates).
 * @package   Netquick powerpanel
 * @license   http://www.opensource.org/licenses/BSD-3-Clause
 * @version   1.1
 * @since     2017-08-17
 * @author    NetQuick
 */
namespace App\Helpers;

use App\EmailLog;
use App\EmailType;
use App\ServiceCategory;
use App\BuildingCategory;
use App\GeneralSettings;
use App\Helpers\MyLibrary;
use Config;
use Mail;
use Request;

class Email_sender
{
/**
 * This method handels test email process
 * @return  JSON Object
 * @since   2017-08-17
 * @author  NetQuick
 */
    public static function testMail()
    {

        $settings = Self::getSettings();

        $settings["subject"] = "Test email";
        $settings['emailType'] = EmailType::getRecords()->checkEmailType('General')->first()->id;
        $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
        $settings['to'] = MyLibrary::getDecryptedString($settings['DEFAULT_ADMIN_EMAIL']);
        $settings['cc_email'] = !empty($settings['DEFAULT_CC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_CC_EMAIL']) : '';
        $settings['bcc_email'] = !empty($settings['DEFAULT_BCC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_BCC_EMAIL']) : '';
        $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
        $settings['sender'] = $settings['SMTP_SENDER_NAME'];
        $settings['receiver'] = $settings['SMTP_SENDER_NAME'];
        $settings['txtBody'] = view('emails.default', $settings)->render();
        $logId = Self::recodLog($settings);

        //unset($settings['txtBody']);
        Self::sendEmail('emails.default', $settings, $logId);
    }
/**
 * This method handels contact email process for admin and user
 * @return  Flag contactUs
 * @since   2017-08-17
 * @author  NetQuick
 */
    public static function contactUs($data = null)
    {
        if ($data != null) {

            $settings = Self::getSettings();
            $settings["user"] = 'admin';
            $settings["first_name"] = $data["first_name"];
			$settings["last_name"] = $data["last_name"];
			$settings["full_name"] = $data["first_name"].' '.$data["last_name"];
            $settings["email"] = $data["contact_email"];
            $settings["phone_number"] = (isset($data["phone_number"]) ? $data["phone_number"] : '');
            $settings["user_message"] = (!empty($data["user_message"]) ? $data["user_message"] : '');

            $data['user'] = 'admin';
            $settings["subject"] = "New Contact Enquiry Received";
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('Contact Us Lead')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
            $settings['to'] = MyLibrary::getDecryptedString($settings['DEFAULT_CONTACT_EMAIL']);
            $settings['cc_email'] = !empty($settings['DEFAULT_CC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_CC_EMAIL']) : '';
            $settings['bcc_email'] = !empty($settings['DEFAULT_BCC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_BCC_EMAIL']) : '';
            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['SMTP_SENDER_NAME'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('emails.contactmail', $settings)->render();
			// print_r($settings['txtBody']);die;
            $logId = Self::recodLog($settings);
            Self::sendEmail('emails.contactmail', $settings, $logId);

        }

    }
	
/**
 * This method handels request a quote email process for admin and user
 * @return  Flag contactUs
 * @since   2020-03-03
 * @author  NetQuick
 */
    public static function requestQuote($data = null)
    {
        if ($data != null) {

            // $area_of_interest_id = $data["area_of_interest"];              
            // $area_of_interest_json = ServiceCategory::getRequestQuoteList($area_of_interest_id);
            // $area_of_interest = json_decode($area_of_interest_json);

            // print_r($area_of_interest->varTitle);die;
            $settings = Self::getSettings();
            $settings["user"] = 'admin';
            $settings["first_name"] = $data["first_name"];
			$settings["last_name"] = $data["last_name"];
			$settings["full_name"] = $data["first_name"].' '.$data["last_name"];
            $settings["email"] = $data["contact_email"];
            // $settings["area_of_interest"] = $area_of_interest->varTitle;
            $settings["area_of_interest"] = $data["area_of_interest"];
            $settings["phone_number"] = (isset($data["phone_number"]) ? $data["phone_number"] : '');
            $settings["user_message"] = (!empty($data["user_message"]) ? $data["user_message"] : '');

            $data['user'] = 'admin';
            $settings["subject"] = "New Request a Quote Enquiry Received";
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('Request A Quote Lead')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
            $settings['to'] = MyLibrary::getDecryptedString($settings['DEFAULT_REQUEST_EMAIL']);
            $settings['cc_email'] = !empty($settings['DEFAULT_CC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_CC_EMAIL']) : '';
            $settings['bcc_email'] = !empty($settings['DEFAULT_BCC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_BCC_EMAIL']) : '';
            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['SMTP_SENDER_NAME'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('emails.requestmail', $settings)->render();
			// print_r($settings['txtBody']);die;
            $logId = Self::recodLog($settings);
            Self::sendEmail('emails.requestmail', $settings, $logId);

        }

    }	
/**
* This method handels request a quote email process for admin and user
* @return  Flag contactUs
* @since   2020-03-03
* @author  NetQuick
*/
public static function requestQuoteSidebar($data = null)
{
	if ($data != null) {
        $area_of_interest_id = $data["ArearOfInterest"]; 
        $area_of_interest_json = ServiceCategory::getRequestQuoteList($area_of_interest_id);
        $area_of_interest = json_decode($area_of_interest_json);
        // echo '<pre>'; print_r($area_of_interest_json);die;
		// $area_of_interest_id = $data["ArearOfInterest"];              
		// $area_of_interest_json = ServiceCategory::getRequestQuoteList($area_of_interest_id);
		// $area_of_interest = json_decode($area_of_interest_json);
		
		
		$settings = Self::getSettings();
		$settings["user"] = 'admin';
		$settings["first_name"] = $data["first_name"];
		$settings["last_name"] = $data["last_name"];
		$settings["full_name"] = $data["first_name"].' '.$data["last_name"];
        $settings["email"] = $data["contact_email"];
        $settings["area_of_interest"] = $data['ArearOfInterest'];
        
        $settings["area_of_interest"] = $area_of_interest->varTitle;
        // print_r($settings["area_of_interest"]);die;
		$settings["phone_number"] = (isset($data["phone_number"]) ? $data["phone_number"] : '');
		$settings["user_message"] = (!empty($data["user_message"]) ? $data["user_message"] : '');

		$data['user'] = 'admin';
		$settings["subject"] = "New Service Request a Quote Enquiry Received";
		$settings['emailType'] = EmailType::getRecords()->checkEmailType('Request A Quote Lead')->first()->id;
		$settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
		$settings['to'] = MyLibrary::getDecryptedString($settings['DEFAULT_REQUEST_EMAIL']);
		$settings['cc_email'] = !empty($settings['DEFAULT_CC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_CC_EMAIL']) : '';
		$settings['bcc_email'] = !empty($settings['DEFAULT_BCC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_BCC_EMAIL']) : '';
		$settings['sender'] = $settings['SMTP_SENDER_NAME'];
		$settings['receiver'] = $settings['SMTP_SENDER_NAME'];
		$settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
		$settings['txtBody'] = view('emails.requestmail', $settings)->render();
		// print_r($settings['txtBody']);die;
		$logId = Self::recodLog($settings);
		Self::sendEmail('emails.requestmail', $settings, $logId);

	}

}	

/**
* This method handels request a quote email process for building home request leads.
* @return  Flag contactUs
* @since   2020-03-11
* @author  NetQuick
*/
public static function requestQuoteBuilding($data = null)
{
	if ($data != null) {
        $area_of_interest_id = $data["ArearOfInterest"]; 
        $area_of_interest_json = BuildingCategory::getRequestQuoteList($area_of_interest_id);
        $area_of_interest = json_decode($area_of_interest_json);
        // echo '<pre>'; print_r($area_of_interest_json);die;
		// $area_of_interest_id = $data["ArearOfInterest"];              
		// $area_of_interest_json = ServiceCategory::getRequestQuoteList($area_of_interest_id);
		// $area_of_interest = json_decode($area_of_interest_json);
		
		
		$settings = Self::getSettings();
		$settings["user"] = 'admin';
		$settings["first_name"] = $data["first_name"];
		$settings["last_name"] = $data["last_name"];
		$settings["full_name"] = $data["first_name"].' '.$data["last_name"];
        $settings["email"] = $data["contact_email"];
        $settings["area_of_interest"] = $data['ArearOfInterest'];
        
        $settings["area_of_interest"] = $area_of_interest->varTitle;
        // print_r($settings["area_of_interest"]);die;
		$settings["phone_number"] = (isset($data["phone_number"]) ? $data["phone_number"] : '');
		$settings["user_message"] = (!empty($data["user_message"]) ? $data["user_message"] : '');

		$data['user'] = 'admin';
		$settings["subject"] = "New Building Request a Quote Enquiry Received";
		$settings['emailType'] = EmailType::getRecords()->checkEmailType('Request A Quote Lead')->first()->id;
		$settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
		$settings['to'] = MyLibrary::getDecryptedString($settings['DEFAULT_REQUEST_EMAIL']);
		$settings['cc_email'] = !empty($settings['DEFAULT_CC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_CC_EMAIL']) : '';
		$settings['bcc_email'] = !empty($settings['DEFAULT_BCC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_BCC_EMAIL']) : '';
		$settings['sender'] = $settings['SMTP_SENDER_NAME'];
		$settings['receiver'] = $settings['SMTP_SENDER_NAME'];
		$settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
		$settings['txtBody'] = view('emails.requestmail', $settings)->render();
        // print_r($settings['txtBody']);die;
		$logId = Self::recodLog($settings);
		Self::sendEmail('emails.requestmail', $settings, $logId);

	}

}	
/**
 * This method handels contact email process for admin and user
 * @return  Flag contactUs
 * @since   2017-08-17
 * @author  NetQuick
 */
    public static function bookAppointment($data = null)
    {
        if ($data != null) {
            $settings = Self::getSettings();
            $settings["user"] = 'admin';
            $settings["first_name"] = $data["first_name"];
            $settings["email"] = $data["contact_email"];
            $settings["phone_number"] = (isset($data["phone_number"]) ? MyLibrary::getDecryptedString($data["phone_number"]) : '');
            $settings["user_message"] = $data["user_message"];
            $settings["service_name"] = $data["service_name"];
            $settings["appointment_date"] = isset($data['appointment_date']) ? date('' . Config::get('Constant.DEFAULT_DATE_FORMAT')) : '';
#Admin Email================================
            $data['user'] = 'admin';
            $settings["subject"] = "New Appointment Enquiry Received";
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('Appointment Lead')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
            $settings['to'] = MyLibrary::getDecryptedString($settings['DEFAULT_ADMIN_EMAIL']);
            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['SMTP_SENDER_NAME'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('emails.appointmentmail', $settings)->render();
            $logId = Self::recodLog($settings);
            //unset($settings['txtBody']);
            Self::sendEmail('emails.appointmentmail', $settings, $logId);
#User Email================================
            $settings['user'] = 'user';
            $settings["subject"] = "Thank you! Appointment Request Received for " . $settings["appointment_date"] . "";
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('Appointment Lead')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
            $settings['to'] = $settings['email'];
            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['first_name'];
            $settings['txtBody'] = view('emails.appointmentmail', $settings)->render();
            $logId = Self::recodLog($settings);
            //unset($settings['txtBody']);
            Self::sendEmail('emails.appointmentmail', $settings, $logId);
        }
    }
/**
 * This method handels newsletter subscription email process
 * @return  Flag
 * @since   2017-08-17
 * @author  NetQuick
 */
    public static function newsletter($data = null, $id = null)
    {
        if ($data != null && $id != null) {
            $settings = Self::getSettings();
            $settings['first_name'] = ucfirst(explode('@', Mylibrary::getDecryptedString($data["varEmail"]))[0]);
            $settings["email"] = Mylibrary::getDecryptedString(trim($data["varEmail"]));
            $settings['user_subscribe'] = url("news-letter/subscription/subscribe/" . $id);
            $settings['subject'] = 'Please Confirm Your Email Subscription for Our Newsletter';
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('General')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString(Config::get('Constant.SMTP_SENDER_EMAIL'));
            $settings['to'] = Mylibrary::getDecryptedString(trim($data["varEmail"]));
            $settings['sender'] = Config::get('Constant.SMTP_SENDER_NAME');
            $settings['receiver'] = $settings['first_name'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('emails.subscription', $settings)->render();
            $logId = Self::recodLog($settings);
            //unset($settings['txtBody']);
            Self::sendEmail('emails.subscription', $settings, $logId);
        }
    }
/**
 * This method handels newsletter subscribed email process
 * @return  Flag
 * @since   2017-08-17
 * @author  NetQuick
 */
    public static function newsletterSubscribed($data = null, $id = null)
    {
        if ($data != null && $id != null) {
            $settings = Self::getSettings();
            $settings['first_name'] = ucfirst(explode('@', Mylibrary::getDecryptedString($data["varEmail"]))[0]);
            $settings["email"] = Mylibrary::getDecryptedString($data["varEmail"]);
            $settings["phone_number"] = isset($data["phone_number"]) ? $data["phone_number"] : '';
            $settings['user_unsubscribe'] = url("news-letter/subscription/unsubscribe/" . $id);
            $settings['subject'] = 'Congratulations! Your subscription has been confirmed';
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('General')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
            $settings['to'] = Mylibrary::getDecryptedString($data["varEmail"]);
            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['first_name'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('emails.subscription', $settings)->render();
            $logId = Self::recodLog($settings);
            //unset($settings['txtBody']);
            Self::sendEmail('emails.subscription', $settings, $logId);
        }
    }
/**
 * This method sends email for Restaurant reservation
 * @return  Flag
 * @since   2018-07-24
 * @author  NetQuick
 */
    public static function reservation($data = null)
    {
        if ($data != null) {
            $settings = Self::getSettings();
            $settings["user"] = 'admin';
            $settings["first_name"] = $data["first_name"];
            $settings["email"] = $data["contact_email"];
            $settings["phone_number"] = (isset($data["phone_number"]) ? $data["phone_number"] : '');
            $settings["number_of_people"] = (isset($data["people"]) ? $data["people"] : null);
            $settings["user_message"] = (isset($data["user_message"]) && !empty($data["user_message"]) ? $data["user_message"] : '');
            $settings["reservation_date"] = (isset($data['reservation_date']) ? date('d-m-Y', strtotime($data['reservation_date'])) : '');
            $settings["reservation_time"] = (isset($data['reservation_time'])?$data['reservation_time']:'');
            #Admin Email================================
            $data['user'] = 'admin';
            $settings["subject"] = "New Reservation Enquiry Received";
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('Reservation Lead')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
            $settings['to'] = MyLibrary::getDecryptedString($settings['DEFAULT_ADMIN_EMAIL']);
            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['SMTP_SENDER_NAME'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('emails.reservation', $settings)->render();
            $logId = Self::recodLog($settings);
            //unset($settings['txtBody']);
            Self::sendEmail('emails.reservation', $settings, $logId);
            #User Email================================
            $settings['user'] = 'user';
            $settings["subject"] = "Congratulations! Your booking has been confirmed";
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('Reservation Lead')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
            $settings['to'] = $settings['email'];
            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['first_name'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('emails.reservation', $settings)->render();
            $logId = Self::recodLog($settings);
            //unset($settings['txtBody']);
            Self::sendEmail('emails.reservation', $settings, $logId);
        }
    }

    public static function forgotPassword($data = null)
    {
        if ($data != null) {

            $settings = Self::getSettings();
            $settings["user"] = $data['user'];
            $settings["resetToken"] = $data['resetToken'];
            #Forgot Password Email To User ================================
            $settings["subject"] = "Your Password Reset Link";
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('Forgot Password')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);

            if (!empty($data['user']->personalId)) {
                $settings['to'] = Mylibrary::getDecryptedString($data['user']->personalId);
            } else {
                $settings['to'] = $data['user']->email;
            }

            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['SMTP_SENDER_NAME'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('auth.emails.password', $settings)->render();
            $logId = Self::recodLog($settings);
            //unset($settings['txtBody']);
            Self::sendEmail('auth.emails.password', $settings, $logId);
        }
    }

    public static function privacyRemovalEmailConfirmation($data = null, $id)
    {
        if ($data != null) {
            $settings = Self::getSettings();
            if (!empty($data["first_name"]) || !empty($data["last_name"])) {
                $name = $data["first_name"] . ' ' . $data["last_name"];
            } else {
                $name = explode('@', $data["email"])[0];
            }

            $settings["name"] = $name;
            $settings["email"] = $data["email"];
            $settings["link"] = url('/confirm-email-address') . '?token=' . MyLibrary::getLaravelEncryptedString($id);

            // $settings["reason"] = !empty($data["reason"])?$data["reason"]:'-';
            // $settings["authorized"] = isset($data["authorized"])?'Yes':'No';

            $settings["subject"] = "Please confirm data removal request";
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('Privacy Removal')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
            $settings['to'] = $settings["email"];
            $settings['cc_email'] = !empty($settings['DEFAULT_CC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_CC_EMAIL']) : '';
            $settings['bcc_email'] = !empty($settings['DEFAULT_BCC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_BCC_EMAIL']) : '';
            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['SMTP_SENDER_NAME'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('emails.privacy-removal-email-confirmation', $settings)->render();
            $logId = Self::recodLog($settings);

            Self::sendEmail('emails.privacy-removal-email-confirmation', $settings, $logId);
        }
    }

    public static function privacyRemovalAdminEmail($data = null)
    {
        if ($data != null) {
            $settings = Self::getSettings();
            $email = MyLibrary::getDecryptedString($data->varEmail);
            if (!empty($data->varName)) {
                $user_name = $data->varName;
            } else {
                $user_name = explode('@', $email)[0];
            }

            $settings["name"] = (!empty($data->varName) ? $data->varName : 'N/A');
            $settings["user_name"] = $user_name;
            $settings["email"] = $email;
            $settings["reason"] = MyLibrary::getDecryptedString($data->txtReason);
            $settings["authorized"] = ($data->chrIsAuthorized == 'Y') ? 'Yes' : 'No';
            $settings["email_verification"] = ($data->chrIsEmailVerified == 'Y') ? 'Yes' : 'No';
            $settings["link"] = url('powerpanel/privacy-removal-leads/');

            $settings["subject"] = "Data removal request received from " . $user_name;
            $settings['emailType'] = EmailType::getRecords()->checkEmailType('Privacy Removal')->first()->id;
            $settings['from'] = MyLibrary::getDecryptedString($settings['SMTP_SENDER_EMAIL']);
            $settings['to'] = MyLibrary::getDecryptedString($settings['DEFAULT_ADMIN_EMAIL']);
            $settings['cc_email'] = !empty($settings['DEFAULT_CC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_CC_EMAIL']) : '';
            $settings['bcc_email'] = !empty($settings['DEFAULT_BCC_EMAIL']) ? MyLibrary::getDecryptedString($settings['DEFAULT_BCC_EMAIL']) : '';
            $settings['sender'] = $settings['SMTP_SENDER_NAME'];
            $settings['receiver'] = $settings['SMTP_SENDER_NAME'];
            $settings['ReplyToEmail'] = MyLibrary::getDecryptedString($settings["DEFAULT_REPLYTO_EMAIL"]);
            $settings['txtBody'] = view('emails.privacy-removal-admin-email', $settings)->render();
            $logId = Self::recodLog($settings);

            Self::sendEmail('emails.privacy-removal-admin-email', $settings, $logId);
        }
    }

    /**
     * This method sends email
     * @return  Flag
     * @since   2017-08-17
     * @author  NetQuick
     */
    public static function sendEmail($view = null, $settings = null, $logId = null)
    {
        if (Config::get('Constant.MAILER') == 'smtp' || Config::get('Constant.MAILER') == 'log') {
            Self::smtp($view, $settings, $logId);
        } else {
            Self::mailAPI($settings, $logId);
        }

    }

    /**
     * This method sends email using mail api
     * @return  Flag
     * @since   2017-08-17
     * @author  NetQuick
     */
    public static function mailAPI($settings = null, $logId = null)
    {
        if (!empty($settings) && $logId > 0) 
        {

            $emailPara = array(
                'PageUrl' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null,
                'SiteName' => $settings['SITE_NAME'],
                'FormName' => $settings['from'],
                'EmailTo' => $settings['to'],
                'CcEmail' => (isset($settings["cc_email"]) ? $settings["cc_email"] : ''),
                'BccEmail' => (isset($settings["bcc_email"]) ? $settings["bcc_email"] : ''),
                'ReplyToEmail' => (isset($settings["ReplyToEmail"]) ? $settings["ReplyToEmail"] : ''),
                'Message' => $settings['txtBody'],
                'Subject' => $settings['subject'],
                'TestMail' => '',
                'AllowAttachment' => '',
                'Attachment' => '',
            );

            if (isset($settings["attachement"]) && $settings["attachement"] != "") {
                $emailPara['AllowAttachment'] = 'png,jpg,DOC'; //optional if you want to send only pdf,doc then put like 'png,DOC'
                $emailPara['Attachment'] = $settings["attachement"]; // array('http://www.example.com/upimages/Banner/softtest1.png');
            }

            $ReturnArray = array(
                'EmailPara' => json_encode($emailPara, true),
                'SiteId' => Config::get('Constant.MAIL_API_SITE_ID'), //Define in constant
                'SiteTocken' => Config::get('Constant.MAIL_API_TOKEN'), //Define in constant
            );

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => Config::get('Constant.MAIL_API_URL'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $ReturnArray,
                CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
                CURLOPT_USERPWD => Config::get('Constant.MAIL_API_USERNAME') . ":" . Config::get('Constant.MAIL_API_PASSWORD'),
                CURLOPT_HTTPHEADER => array(
                    "ES-API-KEY:" . Config::get('Constant.MAIL_API_TOKEN'), //Define in constant
                ),
                CURLOPT_SSL_VERIFYPEER => false,
            ));
            
            $response = curl_exec($curl);
            curl_close($curl);

            if ($response == 'true') {
                EmailLog::updateEmailLog(['id' => $logId], ['chrIsSent' => 'Y']);
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * This method sends email using smtp
     * @return  Flag
     * @since   2017-08-17
     * @author  NetQuick
     */
    public static function smtp($view = null, $settings = null, $logId = null)
    {
        if (!empty($settings) && $logId > 0 && $view != null) {
            $mailconfigData = Config::get('mail');
            $sent = Mail::send($view, $settings, function ($message) use ($settings) {

                $message->from($settings['from'], $settings['sender']);
                $message->subject($settings['subject']);

                if (isset($settings['to']) && !empty($settings['to'])) {
                    $toEmailArr = explode(",", $settings['to']);
                    $toEmailArr    =  array_map('trim',$toEmailArr);
                    $message->to($toEmailArr, $settings['receiver']);
                }

                if (isset($settings['cc_email']) && !empty($settings['cc_email'])) {
                    $ccEmailArr = explode(",", $settings['cc_email']);
                    $ccEmailArr    =  array_map('trim',$ccEmailArr);
                    $message->cc($ccEmailArr, null);
                }

                if (isset($settings['bcc_email']) && !empty($settings['bcc_email'])) {
                    $bccEmailArr = explode(",", $settings['bcc_email']);
                    $bccEmailArr    =  array_map('trim',$bccEmailArr);
                    $message->cc($bccEmailArr, null);
                }

                if (isset($settings['ReplyToEmail']) && !empty($settings['ReplyToEmail'])) {
                    $replyToEmailArr = explode(',', $settings['ReplyToEmail']);
                    $replyToEmailArr    =  array_map('trim',$replyToEmailArr);
                    $message->replyTo($replyToEmailArr, null);
                }
                
                //$message->attachData($pdf, 'invoice.pdf');
                //$message->attachData($pdf, 'invoice.pdf', ['mime' => $mime]);
            });

            if (count(Mail::failures()) == 0) {
                EmailLog::updateEmailLog(['id' => $logId], ['chrIsSent' => 'Y']);
            }

        }

    }

    /**
     * This method loads general settings
     * @return  Settings array
     * @since   2017-08-17
     * @author  NetQuick
     */
    public static function getSettings()
    {
        $settings = [];
        $generalSettings = GeneralSettings::getSettings();
        if (!empty($generalSettings)) {
            foreach ($generalSettings as $key => $value) {
                $settings[$value['fieldName']] = $value['fieldValue'];
            }
        }
        return $settings;
    }

    /**
     * This method stores email log data in database
     * @return  Flag
     * @since   2017-08-17
     * @author  NetQuick
     */
    public static function recodLog($mailData = null)
    {
        if ($mailData != null) {
            $logData = [];
            $logData['intFkUserId'] = isset($mailData['userId']) ? $mailData['userId'] : 1;
            $logData['intFkEmailType'] = $mailData['emailType'];
            $logData['chrReceiverType'] = isset($mailData['chrReceiverType']) ? $mailData['chrReceiverType'] : '-';
            $logData['intFkModuleId'] = isset($mailData['intFkModuleId']) ? $mailData['intFkModuleId'] : 1;
            $logData['intFkRecordId'] = isset($mailData['chrReceiverType']) ? $mailData['chrReceiverType'] : 1;
            $logData['varFrom'] = Mylibrary::getEncryptedString($mailData['from']);
            $logData['txtTo'] = Mylibrary::getEncryptedString($mailData['to']);
            $logData['txtCc'] = isset($mailData['cc']) ? Mylibrary::getEncryptedString($mailData['cc']) : '-';
            $logData['txtBcc'] = isset($mailData['bcc']) ? Mylibrary::getEncryptedString($mailData['bcc']) : '-';
            $logData['txtSubject'] = $mailData['subject'];
            $logData['chrAttachment'] = isset($mailData['attachment']) ? $mailData['attachment'] : '-';
            $logData['txtBody'] = isset($mailData['txtBody']) ? Mylibrary::getEncryptedString($mailData['txtBody']) : '-';
            $logData['chrIsSent'] = 'N';
            $logData['chrPublish'] = 'Y';
            $logData['chrDelete'] = 'N';
            $logData['chrIpAddress'] = MyLibrary::get_client_ip();
            $logData['varBrowserInfo'] = Request::header('User-Agent');
            $logData['created_at'] = date('Y-m-d H:i:s');
            $recordId = EmailLog::logEmail($logData);
            return $recordId;
        }
    }
}
