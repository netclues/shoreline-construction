<?php
namespace App\Http\Controllers;
use App\BuildingHome;
use App\BuildingCategory;
use App\Rules\ValidateBadWord;
use App\Rules\ValidRecaptcha;
use Validator;
use App\RequestLead;
use App\Video;
use Config;
use App\Helpers\MyLibrary;
use App\Helpers\Email_sender;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use File;


class BuildingHomeController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads Building Home list view
		 * @return  View
		 * @since   2020-02-07
		 * @author  NetQuick
		 */
		public function index() 
		{
			
			$data     = array();
			$page     = 1;
			$paginate = 8;
			if (!empty(Request::get('page'))) {
				$page = Request::get('page');
			}
			$buildingcategory         = BuildingCategory::getCategoryList($paginate, $page);
			$data['buildingcategory'] = $buildingcategory;
			return view('building-home', $data);
		}



				/**
		* This method loads Building Home detail view
		* @param   Alias of record
		* @return  View
		* @since   2020-03-03
		* @author  NetQuick
		*/ 
		public function detail($alias) 
		{
			$id      = slug::resolve_alias($alias);
			$buildinghome = BuildingHome::getFrontDetail($id);
			if (!empty($buildinghome)) {
				$buildinghomeCategory = null;
				$categoryIds     = unserialize($buildinghome->txtCategories);
				if (!empty($categoryIds)) {
					$buildinghomeCategory = BuildingCategory::getRecordByIds($categoryIds);
				}
				$videoIDAray             = explode(',', $buildinghome->fkIntVideoId);
				$videoObj                = Video::getVideoData($videoIDAray);
				$metaInfo                = array('varMetaTitle' => $buildinghome->varMetaTitle, 'varMetaKeyword' => $buildinghome->varMetaKeyword, 'varMetaDescription' => $buildinghome->varMetaDescription);
				$data                    = array();

				$breadcrumb              = [];
                $segmentArr  = Request::segments();
                // echo '<pre>';print_r($segmentArr);die;
				$url = '';
				foreach ($segmentArr as $key => $value) 
				{
					$url .= $value.'/';
					$breadcrumb[$key]['title'] =  ucwords(str_replace('-', ' ', $value));
					$breadcrumb[$key]['url'] =  rtrim($url,'/');
				} 
				$buildingothercategory = BuildingCategory::getFrontCategories($categoryIds);
				$similarBuildingHome =  BuildingHome::getSimilarRecordList($buildinghome->id);   
				$data['buildinghome']         = $buildinghome;
				$data['alias']           = $alias;
				$data['similarBuildingHome'] = $similarBuildingHome;
				$data['metaInfo']        = $metaInfo;
                $data['breadcrumb']      = $breadcrumb;
               
				$data['buildinghomeCategory'] = $buildinghomeCategory;
				$data['buildingothercategory'] = $buildingothercategory;
				$data['videoObj']        = $videoObj;

                if(isset($buildinghome->fkIntImgId) && !empty($buildinghome->fkIntImgId)) {
                    $imageArr = explode(',', $buildinghome->fkIntImgId);
                    view()->share('SHARE_IMG', $imageArr[0]); 
                }            

				if (isset($buildinghome->varMetaTitle) && !empty($buildinghome->varMetaTitle)) {
					view()->share('META_TITLE', $buildinghome->varMetaTitle);
				}

				if (isset($buildinghome->varMetaKeyword) && !empty($buildinghome->varMetaKeyword)) {
					view()->share('META_KEYWORD', $buildinghome->varMetaKeyword);
				}
				if (isset($buildinghome->varMetaDescription) && !empty($buildinghome->varMetaDescription)) {
					view()->share('META_DESCRIPTION', substr(trim($buildinghome->varMetaDescription), 0, 500));
				}				
				return view('building-home-detail', $data);				
			}else{
				abort(404);
			}
		}
		/**
		* This method stores Request a quote leads
		* @param   NA
		* @return  Redirection to Thank You page
		* @since   2020-02-07
		* @author  NetQuick
		*/
		public function store() {
            $data = Request::all();
            $messsages = array(
                'first_name.required' => 'Name field is required',
                'first_name.handle_xss' => 'Please enter valid input',
                'first_name.no_url' => 'URL is not allowed',
                'user_message.handle_xss' => 'Please enter valid input',
                'user_message.valid_input' => 'Please enter valid input',
                'user_message.no_url' => 'URL is not allowed',
                // 'area_of_interest.required' => 'area_of_interest is required',
                'area_of_interest.handle_xss' => 'Please enter valid input',
                'area_of_interest.valid_input' => 'Please enter valid input',
                'area_of_interest.no_url' => 'URL is not allowed',
                'contact_email.required' => 'Email is required',
                'g-recaptcha-response.required' => 'Captcha is required',
                'phone_number.required' => 'Phone is required',
            );

            $rules = array(
                'first_name' => ['required', 'handle_xss', 'no_url', new ValidateBadWord],
                'contact_email' => 'required|email',
                'user_message' => ['handle_xss', 'no_url', new ValidateBadWord],
                'area_of_interest' => ['handle_xss', 'no_url', new ValidateBadWord],    
            );

            if (isset($data['phone_number'])) {
                $rules['phone_number'] = 'required';
            }           

            $rules['g-recaptcha-response'] = ['required', new ValidRecaptcha];

            $validator = Validator::make($data, $rules, $messsages);

        if ($validator->passes()) {
            // echo '<pre>'; print_r($data);die;
            $buildingcategory = null;

            $contactus_lead = new RequestLead;
            $first_name = strip_tags($data['first_name']);
            $last_name = strip_tags($data['last_name']);
            $buildingID = strip_tags($data['building_id']);
            $varName = $first_name . ' '.$last_name;    
            $contactus_lead->varName = $varName;
            $contactus_lead->varEmail = MyLibrary::getEncryptedString($data['contact_email']);
            

            
            $service = BuildingHome::getRecordById($buildingID);            
            $categoryIds     = unserialize($service->txtCategories);            
            if (!empty($categoryIds)) {
                $buildingcategory = BuildingCategory::getRecordByIds($categoryIds);
                $serviceCat = $buildingcategory->toArray();  
                
            }
            foreach ($buildingcategory as $key => $value) {
                $area_of_interest_id = $value->id;                
                $area_of_interest = $value->varTitle;                
            }

            
            
            $contactus_lead->varAreaOfInterest = MyLibrary::getEncryptedString($area_of_interest);
            // echo '<pre>';print_r($contactus_lead->varAreaOfInterest);die;

            if (isset($data['phone_number'])) {
                $contactus_lead->varPhoneNo = MyLibrary::getEncryptedString($data['phone_number']);
            } else {
                $contactus_lead->varPhoneNo = '';
            }
            if (isset($data['user_message'])) {
                $contactus_lead->txtUserMessage = MyLibrary::getEncryptedString(strip_tags($data['user_message']));
            } else {
                $contactus_lead->txtUserMessage = '';
            }
            
            // echo '<pre>'; print_r($contactus_lead);die;
            $contactus_lead->varIpAddress = MyLibrary::get_client_ip();
            $contactus_lead->save();

            /*Start this code for message*/
            if (!empty($contactus_lead->id)) {
                $recordID = $contactus_lead->id;
                $data['ArearOfInterest'] = $area_of_interest_id;
                Email_sender::requestQuoteBuilding($data, $contactus_lead->id);

                if (File::exists(app_path() . '/NewsletterLead.php')) {
                    if (isset($data['subscribe']) && $data['subscribe'] == "on") {

                        $emalExists = NewsletterLead::getRecords()->publish()->deleted()->where('varEmail', "=", Mylibrary::getEncryptedString($data['contact_email']))->first();
                        if (empty($emalExists)) {
                            $subscribeArr = [];
                            $subscribeArr['varEmail'] = Mylibrary::getEncryptedString($data['ArearOfInterest']);
                            $subscribeArr['varName'] = strip_tags($data['first_name']);
                             $subscribeArr['varAreaOfInterest'] = Mylibrary::getEncryptedString($data['contact_email']);
                            $subscribeArr['varIpAddress'] = MyLibrary::get_client_ip();
                            $subscribeArr['created_at'] = date('Y-m-d h:i:s');
                            $subscribe = NewsletterLead::insertGetId($subscribeArr);

                            $newsLetterData = NewsletterLead::getRecords()->publish()->deleted()->checkRecordId($subscribe);
                            if ($newsLetterData->count() > 0) {
                                $newsLetterData = $newsLetterData->first()->toArray();
                                $id = Crypt::encrypt($newsLetterData['id']);
                                Email_sender::newsletter($newsLetterData, $id);
                            }

                        } else {

                            if ($emalExists->chrSubscribed == "N") {
                                $newsLetterData = $emalExists->toArray();
                                $id = Crypt::encrypt($newsLetterData['id']);
                                Email_sender::newsletter($newsLetterData, $id);
                            }
                        }
                    }
                }

                if (Request::ajax()) {
                    return json_encode(['success' => 'Thank you for contacting us, We will get back to you shortly.']);
                } else {
                    return redirect()->route('thank-you')->with(['form_submit' => true, 'message' => 'Thank you for contacting us, We will get back to you shortly.']);
                }

            } else {
                return redirect('/');
            }

                } else {

                    //return contact form with errors
                    if (!empty($data['back_url'])) {
                        return redirect($data['back_url'] . '#contact_form')->withErrors($validator)->withInput();
                    } else {
                        return Redirect::route('request-a-quote')->withErrors($validator)->withInput();
                    }

                }
            }
	}