<?php
namespace App\Http\Controllers;
use App\Services;
use App\ServiceCategory;
use App\Video;
use Config;
use App\Rules\ValidateBadWord;
use App\Rules\ValidRecaptcha;
use Validator;
use App\RequestLead;
use App\Helpers\MyLibrary;
use App\Helpers\Email_sender;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Redirect;
use File;


class ServicesController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads services list view
		 * @return  View
		 * @since   2020-02-07
		 * @author  NetQuick
		 */
		public function index() 
		{
            $ServiceCategory      		= ServiceCategory::getFrontList()->toArray();
            $ServiceCategory[' '] 	   	= '--Select Category--';
            asort($ServiceCategory);            

            $page     = 2;
            $paginate = 2;
            if (!empty(Request::get('page'))) {
                $page = Request::get('page');
            }
            // $ServiceCategoryFilter         = ServiceCategory::getCategoryList($paginate, $page);

            $data                      	= array();
            // $filterArr                 	= [];
            // $filterArr['catFilter']    	= (null !== Request::get('catFilter')) ? Request::get('catFilter') : null;
            // $filterArr['searchFilter'] 	= (null !== Request::get('searchFilter')) ? Request::get('searchFilter') : null;
            // $services                  	= Services::getFrontList($filterArr, Request::get('page'));
            // $data['services']          	= $services;
            // $data['ServiceCategory']   	= $ServiceCategory;
            // $data['ServiceFilter']   	= $ServiceCategoryFilter;


            $serCatTypeArr = ServiceCategory::getFrontList($paginate, $page);		
		
            foreach ($serCatTypeArr as $index => $serCatType) {
                $serviceObj[$index]['type'] = $serCatType->toArray();
                $catId = $serviceObj[$index]['type']['id'];
                $typeWiseService = Services::getFrontList($catId);
                $serviceObj[$index]['services'] = $typeWiseService->toArray();		
            }
            $data['services'] = $serviceObj;
            $data['ServiceCategory']   	= $ServiceCategory;
            // $data['ServiceFilter']   	= $ServiceCategoryFilter;
        
            return view('services', $data);
        }
        /**
		* This method loads services detail view
		* @param   Alias of record
		* @return  View
		* @since   2020-02-07
		* @author  NetQuick
		*/ 
        public function detail($alias) 
		{
            $id      = slug::resolve_alias($alias);
            $service = Services::getFrontDetail($id);
            if (!empty($service)) {
                $serviceCategory = null;
                $categoryIds     = unserialize($service->txtCategories);
                // print_r($categoryIds);die;
                if (!empty($categoryIds)) {
                    $serviceCategory = ServiceCategory::getRecordByIds($categoryIds);
                }
                $videoIDAray             = explode(',', $service->fkIntVideoId);
                $videoObj                = Video::getVideoData($videoIDAray);
                $metaInfo                = array('varMetaTitle' => $service->varMetaTitle, 'varMetaKeyword' => $service->varMetaKeyword, 'varMetaDescription' => $service->varMetaDescription);
                $data                    = array();
                
                $breadcrumb              = [];
                $segmentArr  = Request::segments();
                $url = '';
                foreach ($segmentArr as $key => $value) 
                {
                    $url .= $value.'/';
                    $breadcrumb[$key]['title'] =  ucwords(str_replace('-', ' ', $value));
                    $breadcrumb[$key]['url'] =  rtrim($url,'/');
                } 

                $similarServices =  Services::getSimilarRecordList($service->id);   
                $data['service']         = $service;
                $data['alias']           = $alias;
                $data['similarServices'] = $similarServices;
                $data['metaInfo']        = $metaInfo;
                $data['breadcrumb']      = $breadcrumb;
                $data['serviceCategory'] = $serviceCategory;
                $data['service_id'] = $categoryIds;
                $data['videoObj']        = $videoObj;

                if(isset($service->fkIntImgId) && !empty($service->fkIntImgId)) {
                    $imageArr = explode(',', $service->fkIntImgId);
                    view()->share('SHARE_IMG', $imageArr[0]); 
                }            

                if (isset($service->varMetaTitle) && !empty($service->varMetaTitle)) {
                    view()->share('META_TITLE', $service->varMetaTitle);
                }
                
                if (isset($service->varMetaKeyword) && !empty($service->varMetaKeyword)) {
                    view()->share('META_KEYWORD', $service->varMetaKeyword);
                }
                if (isset($service->varMetaDescription) && !empty($service->varMetaDescription)) {
                    view()->share('META_DESCRIPTION', substr(trim($service->varMetaDescription), 0, 500));
                }				
                return view('services-detail', $data);				
			}else{
				abort(404);
			}
        }
        /* service detail sidebar form */
        
		/**
		* This method stores Request a quote leads
		* @param   NA
		* @return  Redirection to Thank You page
		* @since   2020-02-07
		* @author  NetQuick
		*/
		public function store() {
            $data = Request::all();
            $messsages = array(
                'first_name.required' => 'Name field is required',
                'first_name.handle_xss' => 'Please enter valid input',
                'first_name.no_url' => 'URL is not allowed',
                'user_message.handle_xss' => 'Please enter valid input',
                'user_message.valid_input' => 'Please enter valid input',
                'user_message.no_url' => 'URL is not allowed',
                // 'area_of_interest.required' => 'area_of_interest is required',
                'area_of_interest.handle_xss' => 'Please enter valid input',
                'area_of_interest.valid_input' => 'Please enter valid input',
                'area_of_interest.no_url' => 'URL is not allowed',
                'contact_email.required' => 'Email is required',
                'g-recaptcha-response.required' => 'Captcha is required',
                'phone_number.required' => 'Phone is required',
            );

            $rules = array(
                'first_name' => ['required', 'handle_xss', 'no_url', new ValidateBadWord],
                'contact_email' => 'required|email',
                'user_message' => ['handle_xss', 'no_url', new ValidateBadWord],
                'area_of_interest' => ['handle_xss', 'no_url', new ValidateBadWord],    
            );

            if (isset($data['phone_number'])) {
                $rules['phone_number'] = 'required';
            }
            // if (isset($data['area_of_interest'])) {
            //     $rules['area_of_interest'] = 'required';
            // }

            $rules['g-recaptcha-response'] = ['required', new ValidRecaptcha];

            $validator = Validator::make($data, $rules, $messsages);

        if ($validator->passes()) {
            // echo '<pre>'; print_r($data);die;
            $serviceCategory = null;

            $contactus_lead = new RequestLead;
            $first_name = strip_tags($data['first_name']);
            $last_name = strip_tags($data['last_name']);
            $serviceID = strip_tags($data['service_id']);
            $varName = $first_name . ' '.$last_name;    
            $contactus_lead->varName = $varName;
            $contactus_lead->varEmail = MyLibrary::getEncryptedString($data['contact_email']);
            

            
            $service = Services::getRecordById($serviceID);            
            $categoryIds     = unserialize($service->txtCategories);            
            if (!empty($categoryIds)) {
                $serviceCategory = ServiceCategory::getRecordByIds($categoryIds);
                $serviceCat = $serviceCategory->toArray();  
                
            }
            foreach ($serviceCategory as $key => $value) {
                $area_of_interest_id = $value->id;                
                $area_of_interest = $value->varTitle;                
            }

            
            
            $contactus_lead->varAreaOfInterest = MyLibrary::getEncryptedString($area_of_interest);
            // echo '<pre>';print_r($contactus_lead->varAreaOfInterest);die;

            if (isset($data['phone_number'])) {
                $contactus_lead->varPhoneNo = MyLibrary::getEncryptedString($data['phone_number']);
            } else {
                $contactus_lead->varPhoneNo = '';
            }
            if (isset($data['user_message'])) {
                $contactus_lead->txtUserMessage = MyLibrary::getEncryptedString(strip_tags($data['user_message']));
            } else {
                $contactus_lead->txtUserMessage = '';
            }
            
            // echo '<pre>'; print_r($contactus_lead);die;
            $contactus_lead->varIpAddress = MyLibrary::get_client_ip();
            $contactus_lead->save();

            /*Start this code for message*/
            if (!empty($contactus_lead->id)) {
                $recordID = $contactus_lead->id;
                $data['ArearOfInterest'] = $area_of_interest_id;
                Email_sender::requestQuoteSidebar($data, $contactus_lead->id);

                if (File::exists(app_path() . '/NewsletterLead.php')) {
                    if (isset($data['subscribe']) && $data['subscribe'] == "on") {

                        $emalExists = NewsletterLead::getRecords()->publish()->deleted()->where('varEmail', "=", Mylibrary::getEncryptedString($data['contact_email']))->first();
                        if (empty($emalExists)) {
                            $subscribeArr = [];
                            $subscribeArr['varEmail'] = Mylibrary::getEncryptedString($data['ArearOfInterest']);
                            $subscribeArr['varName'] = strip_tags($data['first_name']);
                             $subscribeArr['varAreaOfInterest'] = Mylibrary::getEncryptedString($data['contact_email']);
                            $subscribeArr['varIpAddress'] = MyLibrary::get_client_ip();
                            $subscribeArr['created_at'] = date('Y-m-d h:i:s');
                            $subscribe = NewsletterLead::insertGetId($subscribeArr);

                            $newsLetterData = NewsletterLead::getRecords()->publish()->deleted()->checkRecordId($subscribe);
                            if ($newsLetterData->count() > 0) {
                                $newsLetterData = $newsLetterData->first()->toArray();
                                $id = Crypt::encrypt($newsLetterData['id']);
                                Email_sender::newsletter($newsLetterData, $id);
                            }

                        } else {

                            if ($emalExists->chrSubscribed == "N") {
                                $newsLetterData = $emalExists->toArray();
                                $id = Crypt::encrypt($newsLetterData['id']);
                                Email_sender::newsletter($newsLetterData, $id);
                            }
                        }
                    }
                }

                if (Request::ajax()) {
                    return json_encode(['success' => 'Thank you for contacting us, We will get back to you shortly.']);
                } else {
                    return redirect()->route('thank-you')->with(['form_submit' => true, 'message' => 'Thank you for contacting us, We will get back to you shortly.']);
                }

            } else {
                return redirect('/');
            }

                } else {

                    //return contact form with errors
                    if (!empty($data['back_url'])) {
                        return redirect($data['back_url'] . '#contact_form')->withErrors($validator)->withInput();
                    } else {
                        return Redirect::route('request-a-quote')->withErrors($validator)->withInput();
                    }

                }
            }
}