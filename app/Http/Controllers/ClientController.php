<?php
namespace App\Http\Controllers;
		use \App\Client;
	use Config;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


class ClientController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads client list view
		 * @return  View
		 * @since   2020-02-07
		 * @author  NetQuick
		 */
		public function index() 
		{
						$paginate 				= 12;
$page 					= (!empty(Request::get('page'))?Request::get('page'):'1');
$client         		= Client::getFrontList($paginate,$page);
$currentPage     		= $client->currentPage();
$data            		= [];
$data['client']         = $client;
$data['currentPage']    = $currentPage;
										return view('client', $data);
					}



		}