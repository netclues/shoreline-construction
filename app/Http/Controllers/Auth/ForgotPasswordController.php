<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\MyLibrary;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
     */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request)
    {

        $this->validateEmail($request);

        $requestedEmail = $request->only('email');

        $request->merge(['email' => MyLibrary::getEncryptedString($requestedEmail['email'])]);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
        ? $this->sendResetLinkResponse($request, $response)
        : $this->sendResetLinkFailedResponse($request, $response);
    }

    protected function sendResetLinkResponse($request, $response)
    {
        $emailSentID = '';
        $userData = User::getRecordByEmailID($request->email);
        if (!empty(MyLibrary::getDecryptedString($userData->personalId))) {
            $emailSentID = 'Password Reset link has been sent to your PERSONAL Email ID: ' . MyLibrary::getDecryptedString($userData->personalId) . '';
        } else {
            $emailSentID = 'Password Reset link has been sent to your REGISTERED Email ID: ' . MyLibrary::getDecryptedString($userData->email) . '';
        }

        return back()->with('status', $emailSentID);
    }

}
