<?php
namespace App\Http\Controllers\Auth;

use App\Helpers\MyLibrary;
use App\Http\Controllers\PowerpanelController;
use App\User;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class PasswordController extends PowerpanelController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
     */
    use ResetsPasswords;
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    protected $redirectTo = '/powerpanel/dashboard';
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('guest:user');
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }

    protected function getResetValidationRules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6|max:20|check_passwordrules',
            'password_confirmation' => 'required|confirmed|min:6|max:20|check_passwordrules',
        ];
    }

    public function sendResetLinkAjax(Request $request, PasswordBroker $passwords)
    {

        if ($request->ajax()) {

            $this->validate($request, ['email' => 'required|email']);

            $response = $passwords->sendResetLink($request->only('email'), function ($m) {
                $m->subject('Your Password Reset Link');
            });

            switch ($response) {
                case PasswordBroker::RESET_LINK_SENT:
                    return [
                        'error' => 'false',
                        'msg' => 'A password link has been sent to your email address',
                    ];

                case PasswordBroker::INVALID_USER:
                    return [
                        'error' => 'true',
                        'msg' => "We can't find a user with that email address",
                    ];
            }
        }
        return false;
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendResetLinkEmail(Request $request, PasswordBroker $passwords)
    {
        $anotherEmailForSendResetLink = false;
        $em = [];
        $requestedEmail = $request->only('email');
        $requestedEmail = MyLibrary::getDecryptedString($requestedEmail['email']);
        $em['email'] = $requestedEmail;
        $request = $em;

        $this->validateSendResetLinkEmail($request);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );

        switch ($response) {
            case Password::RESET_LINK_SENT:
                $mailSuccessResponse = $this->getSendResetLinkEmailSuccessResponse($response);
                return $mailSuccessResponse;
            case Password::INVALID_USER:
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    }

}
