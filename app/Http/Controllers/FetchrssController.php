<?php
/**
* The MenuController class handels news
* configuration  process.
* @package   Netquick powerpanel
* @license   http://www.opensource.org/licenses/BSD-3-Clause
* @version   1.00
* @since     2017-04-10
* @author    NetQuick
*/
namespace App\Http\Controllers;
use App\News;
use App\NewsCategory;
use App\Alias;
use App\Http\Traits\slug;
use Validator;
use Request;
use Response;
use Input;
use DB;
use Feeds;
use Auth;
use Illuminate\Routing\UrlGenerator;
use App\Helpers\resize_image;
use Carbon\Carbon;
use App\Image;

class FetchrssController extends FrontController {
	use slug;
	public $_APP_URL;
	protected $url;	
	public function __construct(UrlGenerator $url) {
		$this->url = $url->to('/');
		$this->_APP_URL = env('APP_URL');
		parent::__construct();
	}
 /**
 * This method handels load process of news
 * @return  view
 * @since   2017-04-10
 * @author  NetQuick
 */
	public function index($start=null,$offset=null) {		
		$url = 'http://cayman27.ky/api/appservice.php/postbycategory';
		$c = curl_init();

		if($start == null || $offset == null){
			$start=0;
			$offset=1;
		}
		
		curl_setopt($c, CURLOPT_USERAGENT,  'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:11.0) Gecko/20100101 Firefox/11.0'); // empty user agents probably not accepted
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($c, CURLOPT_AUTOREFERER,    1);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

		curl_setopt($c, CURLOPT_URL, $url );
		curl_setopt($c, CURLOPT_REFERER, $url);
		curl_setopt($c, CURLOPT_POST, true);
		curl_setopt($c,CURLOPT_POSTFIELDS,["categoryid"=>13,"start"=>$start,"offset"=>$offset]);
		$sResponse = curl_exec($c);
		$objNavigation=json_decode($sResponse)->Details;		

		//print_r($objNavigation);exit();

		if(!empty($objNavigation)){

			//var_dump($objNavigation);exit();

			$cnt=0;			
			foreach ($objNavigation as $nav_item) {				
				preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', (string)$nav_item->image, $match);
				//preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', (string)$nav_item->link, $linkVisit);

				$exists = News::where('title','=',trim((string)$nav_item->title))->count();
				
				if($exists == 0){
					$imageId=$this->upload_image($match[0][0]);						
					if($imageId>0){
						$news  = new news;
						$news->title = trim((string)$nav_item->title);
						$news->img_id = $imageId;
						$news->category_id = 1;
						$news->description = strip_tags((string)$nav_item->description, '<p>');
						$news->display_order = self::get_order();
						$news->chr_publish = 'Y';
						$news->varMetaTitle = (string)$nav_item->title;
						$news->varMetaKeyword = (string)$nav_item->title;
						$news->varMetaDescription = strip_tags((string)$nav_item->description, '<p>');
						$news->link='';//$linkVisit[0][0];
						$news->created_at=date('Y-m-d H:i:s',strtotime((string)$nav_item->date));
						$news->updated_at=date('Y-m-d H:i:s',strtotime((string)$nav_item->date));
						$news->save();
						$post = new Alias( [ 'alias' => slug::create_slug($news->title) ]);
						$module_code =  DB::table('modules')->where('var_module_name','=','news')->first();
						$post->fk_module_code = $module_code->int_code;
						$post->fk_record_code = $news->id;
						$post->save();
						$cnt++;
					}
				}
			}
			echo $cnt.' Feeds migrated to news!';
		}else{ echo 'empty'; }
	}

	public static function get_order()
	{	
		$rec = News::where(['chr_publish'=>'Y','chr_delete'=>'N'])->count();
		$response=$rec+1;
		return $response;
	}

	public function upload_image($url=false) 
	{
		$respose = false;

		$context  = stream_context_create(array('http' => array('header' => 'Accept: image/jpeg')));		
		$file = @file_get_contents($url, false, $context);	

		if($file === FALSE){
			$response =  0;	
		}else{			

			$timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
			$path = parse_url($url, PHP_URL_PATH);  
			$fileExtract = basename($path);

			$fileExtract=explode('.', $fileExtract);
			$pathinfo['filename']=$fileExtract[0];
			$pathinfo['extension']=$fileExtract[1];
			//$pathinfo = pathinfo($file->getClientOriginalName());
			
			$name = $timestamp.'-'.self::clean($pathinfo['filename']);

			file_put_contents(public_path().'/assets/images/'.$name.'.'.$pathinfo['extension'],$file);

			$slug = slug::create_slug($name);			

			$image = new Image;
			$image->fk_image_category_id = 0;
			$image->fk_user_id = 1;
			$image->txt_image_url = $this->url.'/assets/images';
			$image->txt_thumb_url = $this->url.'/assets/images/thumb_200_200';
			$image->txt_image_path = public_path().'/assets/images';
			$image->txt_thumb_path = public_path().'/assets/images/thumb_200_200';
			$image->var_image_slug = $slug;
			$image->var_image_extension = $pathinfo['extension'];	
			$image->txt_image_alt_tag = $name;
			$image->chr_is_user_uploaded = 'Y';
			$image->save();
			$response = $image->id;	
		}
		return $response;
		exit;
	}

	public static function clean($string) 
	{
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   return preg_replace('/[^A-Za-z0-9\-.]/', '', $string); // Removes special chars.
	}

		
}