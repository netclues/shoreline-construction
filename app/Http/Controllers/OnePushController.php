<?php
namespace App\Http\Controllers;
use App\Http\Controllers\PowerpanelController;
use Request;
use App\GeneralSettings;
use App\Alias;
use App\Image;
use Abraham\TwitterOAuth\TwitterOAuth;
use DB;
use File;
use App\Http\Traits\slug;
use Config;
use App\Helpers\MyLibrary;
use App\Helpers\Google;
use App\Helpers\Instagram_share;
use App\Helpers\LinkedInHelper;
use App\Helpers\Facebook;
use App\Helpers\Twitter;
use App\Helpers\resize_image;
use Cache;

class OnePushController extends PowerpanelController
{
	public function ShareonSocialMedia(Request $request)
	{		
			if(null !==(Request::get('socialmedia'))) {			
				$formPost=Request::get();
				$request->session()->put('socialShare', $formPost);				
			}else{
				$formPost=$request->session()->get('socialShare');
			}

			if(in_array('facebook', $formPost['socialmedia'])){
				$this->fbShare($formPost);
			}

			if(in_array('twitter', $formPost['socialmedia'])){
				$this->twitterShare($formPost);
			}

			if(in_array('googleplus', $formPost['socialmedia'])){
				$this->gPshare($formPost);	
			}			

			if(in_array('linkedin', $formPost['socialmedia'])){
				$this->linkedinShare($formPost);
			}

			if(in_array('instagram', $formPost['socialmedia'])){				
				Instagram_share::share($formPost);
			}

	}

	public function twitterShare($formPost){
		Twitter::shareStory($formPost);
	}

	public function fbShare($formPost){
		Facebook::shareStory($formPost);
	}


	public function gPshare($formPost){
		$code = Config::get('Constant.SOCIAL_SHARE_GOOGLE_PLUS_ACCESS_TOKEN');
		$content = [
			'status' => $formPost['varTitle'].': '.$formPost['txtDescription'],
			'url' => $formPost['frontLink']
		];
		if(!empty($formPost['frontImg']) && isset($formPost['frontImg'])) {
			$img=Image::getImg($formPost['frontImg']);
			$content['media'] = public_path('assets/images/'.$img->txtImageName.'.'.$img->varImageExtension);
		}
		Google::shareStory($code, $content);
	}

	public function linkedinShare($formPost){
		$content = [
			'status' => $formPost['varTitle'].': '.$formPost['txtDescription'],
			'url' => $formPost['frontLink']
		];
		if(!empty($formPost['frontImg']) && isset($formPost['frontImg'])) {					
			$content['media'] = resize_image::resize($formPost['frontImg']);
		}
		LinkedInHelper::share($content);
	}

	public function gPlusCallBack(Request $request){		
		if(Request::get('code')){
			$formPost=$request->session()->get('socialShare');
			$token = Google::generateAccessToken(Request::get('code'))['access_token'];			
			GeneralSettings::checkByFieldName('SOCIAL_SHARE_GOOGLE_PLUS_GETCODE')->update(['fieldValue' => Request::get('code')]);
			GeneralSettings::checkByFieldName('SOCIAL_SHARE_GOOGLE_PLUS_ACCESS_TOKEN')->update(['fieldValue' => $token]);
			Config::set('Constant.SOCIAL_SHARE_GOOGLE_PLUS_ACCESS_TOKEN',$token);
			Config::set('Constant.SOCIAL_SHARE_GOOGLE_PLUS_GETCODE',Request::get('code'));
			Cache::tags('genralSettings')->flush();
			$this->gPshare($formPost);
			echo "<script>window.close();</script>";
		}
	}

	public function LinkedInCallBack(Request $request){
		if(Request::get('code')){
			$formPost=$request->session()->get('socialShare');
			$token = LinkedInHelper::generateAccessToken(Request::get('code'));			
			GeneralSettings::checkByFieldName('SOCIAL_SHARE_LINKEDIN_GETCODE')->update(['fieldValue' => Request::get('code')]);
			GeneralSettings::checkByFieldName('SOCIAL_SHARE_LINKEDIN_ACCESS_TOKEN')->update(['fieldValue' => $token]);
			Config::set('Constant.SOCIAL_SHARE_LINKEDIN_ACCESS_TOKEN',$token);
			Config::set('Constant.SOCIAL_SHARE_LINKEDIN_GETCODE',Request::get('code'));
			Cache::tags('genralSettings')->flush();
			$this->linkedinShare($formPost);			
			echo "<script>window.close();</script>";
		}
	}

	
	public function getRecord() {
			
		$id = (int) Request::get('alias');
		$modal = Request::get('modal');
		$modelNameSpace = '\\App\\' . $modal;
		$record =$modelNameSpace::select(['fkIntImgId','varMetaTitle','varMetaDescription'])
		->where('id',$id)
		->get();
		$record[0]->imgsrc=resize_image::resize($record[0]->fkIntImgId,200,200);		
		echo json_encode($record);
	}
}