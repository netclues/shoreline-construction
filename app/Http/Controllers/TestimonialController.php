<?php
namespace App\Http\Controllers;
		use App\Testimonial;
	use Config;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


class TestimonialController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

	/**
	 * This method loads testimonial list view
	 * @return  View
	 * @since   2020-02-07
	 * @author  NetQuick
	 */
	public function index() 
	{
		$rangeFilter['from']       	= (null !== Request::get('dateRangeFrom')) ? Request::get('dateRangeFrom') : null;
		$rangeFilter['to']         	= (null !== Request::get('dateRangeTo')) ? Request::get('dateRangeTo') : null;
		$filterArr['rangeFilter']  	= $rangeFilter;
		$filterArr['searchFilter'] 	= (null !== Request::get('searchFilter')) ? Request::get('searchFilter') : null;
		$currentPage               	= (null !== (Request::get('page'))) ? Request::get('page') : 1;
		$paginate 					= 6;
		$testimonialArr            	= Testimonial::getFrontList($paginate, $currentPage);
		
		$data                      	= array();
		$data['testimonialArr']    	= $testimonialArr;
		return view('testimonial', $data);
	}
}