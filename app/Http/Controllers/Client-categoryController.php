<?php
namespace App\Http\Controllers;
		use \App\ClientCategory;
		use \App\Client;
		use \App\Http\Traits\slug;
	use Config;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


class Client-categoryController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads client-category list view
		 * @return  View
		 * @since   2020-02-07
		 * @author  NetQuick
		 */
		public function index() 
		{
						$data 						= array();
$page 						= 1;
$paginate 					= 12;

if (!empty(Request::get('page'))) {
	$page = Request::get('page');
}

$clientCategory         	= ClientCategory::getCategoryList($paginate, $page);
$data['clientCategory'] 	= $clientCategory;
										return view('client-category', $data);
					}



				/**
		* This method loads client-category detail view
		* @param   Alias of record
		* @return  View
		* @since   2020-02-07
		* @author  NetQuick
		*/ 
				public function detail($alias) 
		{
				$aliasArr = explode('/', $alias);
foreach ($aliasArr as $key => $value) {
  slug::resolve_alias($value);
}

$alias        = end($aliasArr);
$id           = slug::resolve_alias($alias);
$paginate     = 12;
$page         = 1;
$categoryPage = 1;

if (!empty(Request::get('page'))) {
    $page = Request::get('page');
}

if (!empty(Request::get('categoryPage'))) {
    $categoryPage = Request::get('categoryPage');            
}

$clientCategory = ClientCategory::getFrontDetail($id);


if (!empty($clientCategory) && count($clientCategory) > 0) 
{
    $subCategoryList         = ClientCategory::getSubCategoryList($clientCategory->id, $paginate, $categoryPage);   
    $clientObj = Client::getListByCategory($clientCategory->id, $paginate, $page);
   
    $metaInfo                = array('varMetaTitle' => $clientCategory->varMetaTitle, 'varMetaKeyword' => $clientCategory->varMetaKeyword, 'varMetaDescription' => $clientCategory->varMetaDescription);
    $breadcrumb              = [];
    $data                    = array();


    $breadcrumb              = [];
    $segmentArr  = Request::segments();
    $url = '';
    foreach ($segmentArr as $key => $value) 
    {
        $url .= $value.'/';
        $breadcrumb[$key]['title'] =  ucwords(str_replace('-', ' ', $value));
        $breadcrumb[$key]['url'] =  rtrim($url,'/');
    }
    
    $data['clientObj']      = $clientObj;
    $data['alias']           = $alias;
    $data['metaInfo']        = $metaInfo;
    $data['clientCategory']  = $clientCategory;
    $data['breadcrumb'] = $breadcrumb;
    $data['subCategoryList'] = $subCategoryList;  
    $data['clientCategory'] = $clientCategory;  


    if (isset($clientCategory->varMetaTitle) && !empty($clientCategory->varMetaTitle)) {
        view()->share('META_TITLE', $clientCategory->varMetaTitle);
    }

    if (isset($clientCategory->varMetaKeyword) && !empty($clientCategory->varMetaKeyword)) {
        view()->share('META_KEYWORD', $clientCategory->varMetaKeyword);
    }
    
    if (isset($clientCategory->varMetaDescription) && !empty($clientCategory->varMetaDescription)) {
        view()->share('META_DESCRIPTION', substr(trim($clientCategory->varMetaDescription), 0, 500));
    }				
								return view('client-category-detail', $data);				
			}else{
				abort(404);
			}
		}
		}