<?php
namespace App\Http\Controllers;

use DB;
use App\Banner;
use App\StaticBlocks;
use App\Helpers\static_block;
use App\Http\Traits\slug;
use App\Testimonial;
use App\Services;
use \App\Client;
use \App\Video;
use \App\OurProcess;
use \App\BuildingCategory;
use \App\ServiceCategory;
use \App\OurWorks;
use Config;
use App\Helpers\MyLibrary;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


class HomeController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}
	/**
	* Show the application dashboard.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index() {
		$data = array();	
		$page     = 1;
		$paginate = 5; 
		$filterArr = [];
		
		$data      = array();
		$bannerObj = Banner::getHomeBannerList();
		if (!empty($bannerObj) && count($bannerObj) > 0) {
			$data['bannerData'] = $bannerObj;
		}

		$buildingcategory         = BuildingCategory::getCategoryList($paginate, $page);
		if (!empty($buildingcategory) && count($buildingcategory) > 0) {
			$data['buildingcategory'] = $buildingcategory;
		}
		$staticDataWelcome  = StaticBlocks::getStaticBlockIdRecord(8);		
		if (!empty($staticDataWelcome)) {
			$data['staticDataWelcome'] = $staticDataWelcome;
		}

		$staticCongratulations  = StaticBlocks::getStaticBlockIdRecord(9);		
		if (!empty($staticCongratulations)) {
			$data['staticCongratulations'] = $staticCongratulations;
		}
		
		$testimonialObj = Testimonial::getLatestList();
		if (!empty($testimonialObj)) {
			$data['testimonialArr'] = $testimonialObj;
		}

		// $page     = 1;
		// $paginate = 2;
		// if (!empty(Request::get('page'))) {
		// 	$page = Request::get('page');
		// }
		// $serviceCategory = ServiceCategory::getCategoryList($paginate, $page);
		// $serviceObj = Services::getFrontList($filterArr, Request::get('page'));
		// if (!empty($serviceObj) && count($serviceObj) > 0) {			
		// 	$data['services'] = $serviceObj;
		// 	$data['serviceCategory'] = $serviceCategory;
		// }

		//$data = $serviceObj = array(); 

		/** services listing start **/
		$page     = 2;
		$paginate = 2;
		if (!empty(Request::get('page'))) {
			$page = Request::get('page');
		}       
		$serCatTypeArr = ServiceCategory::getFrontList($paginate, $page);		
		
        foreach ($serCatTypeArr as $index => $serCatType) {
			$serviceObj[$index]['type'] = $serCatType->toArray();
			$catId = $serviceObj[$index]['type']['id'];
			$typeWiseService = Services::getFrontList($catId);
			$serviceObj[$index]['services'] = $typeWiseService->toArray();		
		}
		$data['services'] = $serviceObj;
		/** services listing end **/
		
		$ourprocessArr   = OurProcess::getHomeList($paginate=8, $page);		
		if (!empty($ourprocessArr) && count($ourprocessArr) > 0) {
			$data['ourprocessArr'] = $ourprocessArr;
		}

		$ourworksArr = '';
		$paginatework = 4;
		$currentPage  = (null !== (Request::get('page'))) ? Request::get('page') : 1;
		$ourworksArr = OurWorks::getOurFrontList($paginatework, $currentPage);		
		if (!empty($ourworksArr) && count($ourworksArr) > 0) {
			$data['ourworksArr'] = $ourworksArr;
		}

		$clientObj = Client::getFeaturedList(3);
		if (!empty($clientObj) && count($clientObj) > 0) {
			$data['clientObj'] = $clientObj;
		}
		$staticBlocks = static_block::static_block('section-15');
		if (!empty($staticBlocks)) {
			$data['staticBlocks'] = $staticBlocks;
		}
		$site_monitor = DB::table('site_monitor')->select('varTitle')->where('chrDelete', 'N')->first();
		if (!empty($site_monitor)) {
			$data['site_monitor'] = $site_monitor;
		}	
		return view('index',$data);
	}
}