<?php
namespace App\Http\Controllers\Powerpanel;

use App\Helpers\MyLibrary;
use App\Http\Controllers\PowerpanelController;
use App\PrivacyRemovalLead;
use Config;
use DB;
use Request;
use Schema;
use App\Exports\PrivacyLeadExport;
use Excel;



class PrivacyRemovalLeadsController extends PowerpanelController
{


    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }

    public function index()
    {
        $iTotalRecords = PrivacyRemovalLead::getRecordCount();
        $this->breadcrumb['title'] = trans('template.privacyRemovalLeadModule.manageLeads');
        return view('powerpanel.privacy_removal_leads.list', ['iTotalRecords' => $iTotalRecords, 'breadcrumb' => $this->breadcrumb]);
    }

    public function get_list()
    {
        $filterArr = [];
        $records = [];
        $records["data"] = [];
        $filterArr['orderColumnNo'] = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');
        $filterArr['orderByFieldName'] = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');
        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';
        $filterArr['iDisplayLength'] = intval(Request::get('length'));
        $filterArr['iDisplayStart'] = intval(Request::get('start'));

        $sEcho = intval(Request::get('draw'));

        $arrResults = PrivacyRemovalLead::getRecordList($filterArr);
        $iTotalRecords = PrivacyRemovalLead::getRecordCount($filterArr, true);

        $end = $filterArr['iDisplayStart'] + $filterArr['iDisplayLength'];
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        if (!empty($arrResults)) {
            foreach ($arrResults as $key => $value) {
                $records["data"][] = $this->tableData($key, $value);
            }
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK";
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;

    }


    public function DeleteRecord()
    {
        $data = Request::all();
        if (Schema::hasTable('contact_lead')) {
            $delete = DB::table('contact_lead')->where('varEmail', $data['ids'][0])->delete();
        }

        if (Schema::hasTable('request_lead')) {
            $delete = DB::table('request_lead')->where('varEmail', $data['ids'][0])->delete();
        }

        if (Schema::hasTable('newsletter_lead')) {
            $delete = DB::table('newsletter_lead')->where('varEmail', $data['ids'][0])->delete();
        }

        if (Schema::hasTable('newsletter_lead')) {
            $delete = DB::table('appointment_lead')->where('varEmail', $data['ids'][0])->delete();
        }

        if (Schema::hasTable('event_lead')) {
            $delete = DB::table('event_lead')->where('varEmail', $data['ids'][0])->delete();
        }

        if (Schema::hasTable('email_log')) {
            $delete = DB::table('email_log')->where('txtTo', $data['ids'][0])->delete();
        }

        if (Schema::hasTable('privacy_removal_leads')) {
            $delete = DB::table('privacy_removal_leads')->where('varEmail', $data['ids'][0])->delete();
        }

        echo json_encode($delete);
        exit;
    }

    public function tableData($key, $value)
    {
        
        $phoneNo = '';
        $details = '';

        $moduleList = $this->getModulesName($key, $value->varEmail);

        $linkedModule = '';

        if (!empty($moduleList)) {
            $linkedModule .= '<div class="pro-act-btn">';
            $linkedModule .= '<a href="javascript:void(0)" class="without_bg_icon" onclick="return hs.htmlExpand(this,{width:300,headingText:\'Linked Modules\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="icon-envelope"></span></a>';
            $linkedModule .= '<div class="highslide-maincontent">';

            foreach ($moduleList as $m_key => $m_value) {
                $linkedModule .= '<a href="' . url('/powerpanel/' . $m_value['moduleSlug']) . '">' . $m_value['moduleName'] . '</a> ('. $m_value['count'].')<br/>';
            }
            $linkedModule .= '</div></div>';

        } else {
            $linkedModule .= '-';
        }

        if (!empty($value->txtReason)) {
            $details .= '<div class="pro-act-btn">';
            $details .= '<a href="javascript:void(0)" class="without_bg_icon" onclick="return hs.htmlExpand(this,{width:300,headingText:\'Reason\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="icon-envelope"></span></a>';
            $details .= '<div class="highslide-maincontent">' . MyLibrary::getDecryptedString(nl2br($value->txtReason)) . '</div>';
            $details .= '</div>';
        } else {
            $details .= '-';
        }

        if($value->chrIsEmailVerified == 'Y'){
            $actions = '&nbsp;<a class="without_bg_icon delete" title="' . trans("template.common.delete") . '" data-controller="privacy-removal-leads" data-alias = "' . $value->varEmail . '"><i class="fa fa-times"></i></a>';
        }else{
            $actions = '-';
        }
        

        $records = array(
            '<input type="checkbox" name="delete[]" class="chkDelete" value="' . $value->id . '">',
            !empty($value->varName)?$value->varName:'-',
            MyLibrary::getDecryptedString($value->varEmail),
            $details,
            $linkedModule,
            ($value->chrIsEmailVerified == 'Y' ? 'Yes' : 'No'),
            date('' . Config::get('Constant.DEFAULT_DATE_FORMAT') . ' ' . Config::get('Constant.DEFAULT_TIME_FORMAT') . '', strtotime($value->created_at)),
            $actions,
        );

        return $records;
    }

    public function getModulesName($key, $email)
    {

        $moduleArray = array();
        if (Schema::hasTable('contact_lead')) {
            $contactLeads = DB::table('contact_lead')->where('varEmail', $email)->count();
            if ($contactLeads > 0) {
                $moduleArray['contact_lead']['moduleName'] = 'Contact Us';
                $moduleArray['contact_lead']['moduleSlug'] = 'contact-us';
                $moduleArray['contact_lead']['count'] = $contactLeads;
            }
        }

        if (Schema::hasTable('request_lead')) {
            $requestLeads = DB::table('request_lead')->where('varEmail', $email)->count();
            if ($requestLeads > 0) {
                $moduleArray['request_lead']['moduleName'] = 'Request a Quote';
                $moduleArray['request_lead']['moduleSlug'] = 'request-a-quote';
                $moduleArray['request_lead']['count'] = $requestLeads;
            }
        }

        if (Schema::hasTable('newsletter_lead')) {
            $newsLetterLeads = DB::table('newsletter_lead')->where('varEmail', $email)->count();
            if ($newsLetterLeads > 0) {
                $moduleArray['newsletter_lead']['moduleName'] = 'Newsletter Leads';
                $moduleArray['newsletter_lead']['moduleSlug'] = 'newsletter-lead';
                $moduleArray['newsletter_lead']['count'] = $newsLetterLeads;
            }
        }

        if (Schema::hasTable('appointment_lead')) {
            $appointmentLeads = DB::table('appointment_lead')->where('varEmail', $email)->count();
            if ($appointmentLeads > 0) {
                $moduleArray['appointment_lead']['moduleName'] = 'Appointment Leads';
                $moduleArray['appointment_lead']['moduleSlug'] = 'appointment-lead';
                $moduleArray['appointment_lead']['count'] = $appointmentLeads;
            }
        }

        if (Schema::hasTable('event_lead')) {
            $eventLeads = DB::table('event_lead')->where('varEmail', $email)->count();
            if ($eventLeads > 0) {
                $moduleArray['event_lead']['moduleName'] = 'Event Leads';
                $moduleArray['event_lead']['moduleSlug'] = 'event-lead';
                $moduleArray['event_lead']['count'] = $eventLeads;
            }
        }

        if (Schema::hasTable('email_log')) {
            $emailLogs = DB::table('email_log')->where('txtTo', $email)->count();
            if ($emailLogs > 0) {
                $moduleArray['email_log']['moduleName'] = 'Email Logs';
                $moduleArray['email_log']['moduleSlug'] = 'email-log';
                $moduleArray['email_log']['count'] = $emailLogs;
            }
        }

        return $moduleArray;

    }

    public function ExportRecord()
    {

        return Excel::download(new PrivacyLeadExport, Config::get('Constant.SITE_NAME') . '-' . trans("template.privacyRemovalLeadModule.privacyRemovalLeads") . '-' . date("dmy-h:i") . '.xlsx');

    }

}
