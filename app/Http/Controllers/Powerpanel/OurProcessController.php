<?php
/**
 * The MenuController class handels our_process
 * configuration  process.
 * @package   Netquick powerpanel
 * @license   http://www.opensource.org/licenses/BSD-3-Clause
 * @version   1.00
 * @since     2020-02-28
 * @author    NetQuick
 */
namespace App\Http\Controllers\Powerpanel;

use App\Alias;
use App\OurProcess;
use App\CommonModel;
use App\Helpers\AddCategoryAjax;
use App\Helpers\Category_builder;
use App\Helpers\MyLibrary;
use App\Http\Controllers\PowerpanelController;
use App\Log;
use App\RecentUpdates;
use Auth;
use Cache;
use Carbon\Carbon;
use Config;
use Request;
use App\Modules;

use Illuminate\Support\Facades\Redirect;
use Validator;

class OurProcessController extends PowerpanelController
{

    public $catModule;
    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }
    /**
     * This method handels load process of OurProcess
     * @return  View
     * @since   2020-02-28
     * @author  NetQuick
     */
    public function index()
    {
        $iTotalRecords = CommonModel::getRecordCount();        
        $this->breadcrumb['title'] = trans('template.ourprocessModule.manageOurProcess');
        $breadcrumb = $this->breadcrumb;
        return view('powerpanel.our_process.index', compact('iTotalRecords', 'breadcrumb'));
    }
    /**
     * This method loads service edit view
     * @param   Alias of record
     * @return  View
     * @since   2020-02-28
     * @author  NetQuick
     */
    public function edit($id = false)
    {      
        
        if (!is_numeric($id)) {
            //Add Record
            
            $total = CommonModel::getRecordCount();
            $total = $total + 1;
            $this->breadcrumb['title'] = trans('template.ourprocessModule.addOurProcess');
            $this->breadcrumb['module'] = trans('template.ourprocessModule.manageOurProcess');
            $this->breadcrumb['url'] = 'powerpanel/ourprocess';
            $this->breadcrumb['inner_title'] = trans('template.ourprocessModule.addOurProcess');
            $breadcrumb = $this->breadcrumb;
            $data = compact('total', 'breadcrumb');
        } else {

            //Edit Record
            $ourprocess = OurProcess::getRecordById($id);   
            $total = CommonModel::getRecordCount();         

            if (empty($ourprocess)) {
                return redirect()->route('powerpanel.our-process.add');
            }

            $metaInfo = array('varMetaTitle' => $ourprocess->varMetaTitle, 'varMetaKeyword' => $ourprocess->varMetaKeyword, 'varMetaDescription' => $ourprocess->varMetaDescription);
            $this->breadcrumb['title'] = trans('template.ourprocessModule.editOurProcess') . ' - ' . $ourprocess->varTitle;
            $this->breadcrumb['module'] = trans('template.ourprocessModule.manageOurProcess');
            $this->breadcrumb['url'] = 'powerpanel/ourprocess';
            $this->breadcrumb['inner_title'] = trans('template.ourprocessModule.editOurProcess') . ' - ' . $ourprocess->varTitle;
            $breadcrumb = $this->breadcrumb;
            $data = compact('ourprocess','total', 'metaInfo', 'breadcrumb');
        }
        return view('powerpanel.our_process.actions', $data);
    }

    /**
     * This method stores service modifications
     * @return  View
     * @since   2020-02-28
     * @author  NetQuick
     */
    public function handlePost(Request $request)
    {

        $data = Request::all();       

        $actionMessage = trans('template.common.oppsSomethingWrong');
        $settings = json_decode(Config::get("Constant.MODULE.SETTINGS"));

        //    $messsages = array('display_order.greater_than_zero'=>'Display order must greater than zero');
        $rules = array(
            'title' => 'required|max:160',
            'display_order' => 'required|greater_than_zero',
            'chrMenuDisplay' => 'required',
            //'short_description' => 'required|max:' . (isset($settings) ? $settings->short_desc_length : 400),
            //'alias' => 'required',
        );
        // $rules['varMetaTitle'] = 'required|max:' . $this->metaLength;
        // $rules['varMetaKeyword'] = 'required|max:' . $this->metaLength;
        // $rules['varMetaDescription'] = 'required|max:' . $this->metaDescriptionLength;

        $messsages = array(
            'display_order.greater_than_zero' => trans('template.ourprocessModule.displayGreaterThan'),
            //'short_description.required' => trans('template.ourprocessModule.shortDescription'),
            // 'varMetaTitle.required' => trans('template.ourprocessModule.metaTitle'),
            // 'varMetaKeyword.required' => trans('template.ourprocessModule.metaKeyword'),
            // 'varMetaDescription.required' => trans('template.ourprocessModule.metaDescription'),
        );

        //$data['short_description'] = trim(preg_replace('/\s\s+/', ' ', $data['short_description']));

        $validator = Validator::make($data, $rules, $messsages);
        if ($validator->passes()) {
            $ourprocessArr = [];
            $ourprocessArr['varTitle'] = trim($data['title']);            
            $ourprocessArr['txtDescription'] = $data['description'];
            //$ourprocessArr['txtCategories'] = isset($data['category_id']) ? serialize($data['category_id']) : null;
            //$ourprocessArr['varPreferences'] = '';
            //$ourprocessArr['chrfeaturedOurProcess'] = $data['featuredOurProcess'];
            $ourprocessArr['chrPublish'] = $data['chrMenuDisplay'];
            $ourprocessArr['created_at'] = Carbon::now();

            // $ourprocessArr['varMetaTitle'] = trim($data['varMetaTitle']);
            // $ourprocessArr['varMetaKeyword'] = trim($data['varMetaKeyword']);
            // $ourprocessArr['varMetaDescription'] = trim($data['varMetaDescription']);

            $id = Request::segment(3);
            if (is_numeric($id)) {
                #Edit post Handler=======
                // if ($data['oldAlias'] != $data['alias']) {
                //     Alias::updateAlias($data['oldAlias'], $data['alias']);
                // }
                $ourprocess = OurProcess::getRecordForLogById($id);
                $whereConditions = ['id' => $ourprocess->id];
                $update = CommonModel::updateRecords($whereConditions, $ourprocessArr);
                if ($update) {
                    if (!empty($id)) {
                        self::swap_order_edit($data['display_order'], $ourprocess->id);

                        $logArr = MyLibrary::logData($ourprocess->id);
                        if (Auth::user()->can('log-advanced')) {
                            $newOurProcessObj = OurProcess::getRecordForLogById($ourprocess->id);
                            $oldRec = $this->recordHistory($ourprocess);
                            $newRec = $this->recordHistory($newOurProcessObj);
                            $logArr['old_val'] = $oldRec;
                            $logArr['new_val'] = $newRec;
                        }
                        $logArr['varTitle'] = trim($data['title']);
                        Log::recordLog($logArr);
                        if (Auth::user()->can('recent-updates-list')) {
                            if (!isset($newOurProcessObj)) {
                                $newOurProcessObj = OurProcess::getRecordForLogById($ourprocess->id);
                            }
                            $notificationArr = MyLibrary::notificationData($ourprocess->id, $newOurProcessObj);
                            RecentUpdates::setNotification($notificationArr);
                        }
                    }
                    self::flushCache();
                    $actionMessage = trans('template.ourprocessModule.updateMessage');
                }
            } else {
                #Add post Handler=======
                // $ourprocessArr['intAliasId'] = MyLibrary::insertAlias($data['alias']);
                $ourprocessArr['intDisplayOrder'] = self::swap_order_add($data['display_order']);
                $ourprocessID = CommonModel::addRecord($ourprocessArr);
                if (!empty($ourprocessID)) {
                    $id = $ourprocessID;
                    $newOurProcessObj = OurProcess::getRecordForLogById($id);

                    $logArr = MyLibrary::logData($id);
                    $logArr['varTitle'] = $newOurProcessObj->varTitle;
                    Log::recordLog($logArr);
                    if (Auth::user()->can('recent-updates-list')) {
                        $notificationArr = MyLibrary::notificationData($id, $newOurProcessObj);
                        RecentUpdates::setNotification($notificationArr);
                    }
                    self::flushCache();
                    $actionMessage = trans('template.ourprocessModule.addedMessage');
                }
            }            
            if (!empty($data['saveandexit']) && $data['saveandexit'] == 'saveandexit') {
                return redirect()->route('powerpanel.our-process.index')->with('message', $actionMessage);
            } else {
                return redirect()->route('powerpanel.our-process.edit', $id)->with('message', $actionMessage);
            }
        } else {
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }
    /**
     * This method loads ourprocess table data on view
     * @return  View
     * @since   2020-02-28
     * @author  NetQuick
     */
    public function get_list()
    {
        $filterArr = [];
        $records = [];
        $records["data"] = [];
        $filterArr['orderColumnNo'] = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');
        $filterArr['orderByFieldName'] = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');
        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['statusFilter'] = !empty(Request::get('statusValue')) ? Request::get('statusValue') : '';
        $filterArr['catFilter'] = !empty(Request::get('catValue')) ? Request::get('catValue') : '';
        $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';
        $filterArr['iDisplayLength'] = intval(Request::get('length'));
        $filterArr['iDisplayStart'] = intval(Request::get('start'));
        /**** Delete record then redirect to approriate pagination **/
        $currentrecordcountstart = intval(Request::get('start'));
        $currentpaging = intval(Request::get('length'));

        $totalRecords_old = CommonModel::getTotalRecordCount();
        if ($totalRecords_old > $currentrecordcountstart) {
            $filterArr['iDisplayStart'] = intval(Request::get('start'));
        } else {
            $filterArr['iDisplayStart'] = intval(0);
        }
        /**** Delete record then redirect to approriate pagination **/
        $sEcho = intval(Request::get('draw'));
        $arrResults = OurProcess::getRecordList($filterArr);
        //echo "<pre>"; print_r($arrResults); exit;
        $iTotalRecords = CommonModel::getRecordCount($filterArr, true);
        $totalRecords = CommonModel::getTotalRecordCount();
        $end = $filterArr['iDisplayStart'] + $filterArr['iDisplayLength'];
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        if (!empty($arrResults)) {
            //$this->catModule = Modules::getModule('building-category')->id;
            foreach ($arrResults as $key => $value) {
                $records["data"][] = $this->tableData($value, $totalRecords);
            }

        }

        $records["customActionStatus"] = "OK";
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return json_encode($records);
    }
    /**
     * This method delete multiples ourprocess
     * @return  true/false
     * @since   2017-07-15
     * @author  NetQuick
     */
    public function DeleteRecord(Request $request)
    {
        $data = Request::all('ids');
        $update = MyLibrary::deleteMultipleRecords($data);
        self::flushCache();
        echo json_encode($update);
        exit;
    }
    /**
     * This method reorders banner position
     * @return  Banner index view data
     * @since   2016-10-26
     * @author  NetQuick
     */
    public function reorder()
    {
        $order = Request::get('order');
        $exOrder = Request::get('exOrder');

        MyLibrary::swapOrder($order, $exOrder);
        self::flushCache();
    }
    /**
     * This method handels swapping of available order record while adding
     * @param   order
     * @return  order
     * @since   2016-10-21
     * @author  NetQuick
     */
    public static function swap_order_add($order = null)
    {
        $response = false;
        if ($order != null) {
            $response = MyLibrary::swapOrderAdd($order);
            self::flushCache();
        }
        return $response;
    }
    /**
     * This method handels swapping of available order record while editing
     * @param   order
     * @return  order
     * @since   2016-12-23
     * @author  NetQuick
     */
    public static function swap_order_edit($order = null, $id = null)
    {
        MyLibrary::swapOrderEdit($order, $id);
        self::flushCache();
    }

    public function makeFeatured()
    {
        $id = Request::get('id');
        $featured = Request::get('featured');
        $whereConditions = ['id' => $id];
        $update = CommonModel::updateRecords($whereConditions, ['chrfeaturedOurProcess' => $featured]);
        self::flushCache();
        echo json_encode($update);
    }

    /**
     * This method destroys Service in multiples
     * @return  Service index view
     * @since   2016-10-25
     * @author  NetQuick
     */
    public function publish(Request $request)
    {
        $alias = Request::get('alias');
        $val = Request::get('val');
        $update = MyLibrary::setPublishUnpublish($alias, $val);
        self::flushCache();
        echo json_encode($update);
        exit;
    }
    /**
     * This method handels logs History records
     * @param   $data
     * @return  HTML
     * @since   2017-07-21
     * @author  NetQuick
     */
    public function recordHistory($data = false)
    {
        $returnHtml = '';
        $returnHtml .= '<table class="new_table_desing table table-striped table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>' . trans("template.common.title") . '</th>                           
                            <th>' . trans("template.common.displayorder") . '</th>  
                            <th>' . trans("template.common.description") . '</th>
                            <th>' . trans("template.common.metatitle") . '</th>
                            <th>' . trans("template.common.metakeyword") . '</th>
                            <th>' . trans("template.common.metadescription") . '</th>
                            <th>' . trans("template.common.publish") . '</th>
                          </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>' . $data->varTitle . '</td>';
        
                              $returnHtml .= '<td>' . ($data->intDisplayOrder) . '</td>
                                <td>' . $data->txtDescription . '</td>
                               
                                <td>' . $data->varMetaTitle . '</td>
                                <td>' . $data->varMetaKeyword . '</td>
                                <td>' . $data->varMetaDescription . '</td>
                                <td>' . $data->chrPublish . '</td>
                            </tr>
                        </tbody>
                    </table>';
        return $returnHtml;
    }

    public function tableData($value = false, $catModuleID = false, $totalRecord = false)
    {

        // $publish_action = '';       
        // if (Auth::user()->can('our-process-edit')) {
        //     $details = '<a class="without_bg_icon" title="' . trans("template.common.edit") . '" href="' . route('powerpanel.our-process.edit', array('alias' => $value->id)) . '"><i class="fa fa-pencil"></i></a>';
        // }
        // if (Auth::user()->can('our-process-delete')) {
        //     $details .= '&nbsp;<a class="without_bg_icon delete" title="' . trans("template.common.delete") . '" data-controller="our-process" data-alias = "' . $value->id . '"><i class="fa fa-times"></i></a>';
        // }       

        // if (Auth::user()->can('our-process-publish')) {
        //     if (!empty($value->chrPublish) && ($value->chrPublish == 'Y')) {
        //         $publish_action .= '<input data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/ourprocess" title="' . trans("template.common.publishedRecord") . '" data-value="Unpublish" data-alias="' . $value->id . '">';
        //     } else {
        //         $publish_action .= '<input checked="" data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/ourprocess" title="' . trans("template.common.unpublishedRecord") . '" data-value="Publish" data-alias="' . $value->id . '">';
        //     }
        // }

        $publish_action = '';
        if (Auth::user()->can('our-process-edit')) {
            $details = '<a class="without_bg_icon" title="' . trans("template.common.edit") . '" href="' . route('powerpanel.our-process.edit', array('alias' => $value->id)) . '"><i class="fa fa-pencil"></i></a>';
        }
        if (Auth::user()->can('our-process-delete')) {
            $details .= '&nbsp;<a class="without_bg_icon delete" title="' . trans("template.common.delete") . '" data-controller="our-process" data-alias = "' . $value->id . '"><i class="fa fa-times"></i></a>';
        }

        if (Auth::user()->can('our-process-publish')) {
            if (!empty($value->chrPublish) && ($value->chrPublish == 'Y')) {
                $publish_action .= '<input data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/our-process" title="' . trans("template.common.publishedRecord") . '" data-value="Unpublish" data-alias="' . $value->id . '">';
            } else {
                $publish_action .= '<input checked="" data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/our-process" title="' . trans("template.common.unpublishedRecord") . '" data-value="Publish" data-alias="' . $value->id . '">';
            }
        }

        /* $details .='<a class="without_bg_icon share" title="Share" data-modal="our-process" data-alias="'.$value->id.'"  data-images="'.$value->fkIntImgId.'" data-link = "'.url('/our-process/'.$value->alias['varAlias']).'" data-toggle="modal" data-target="#confirm_share">
        <i class="fa fa-share-alt"></i></a>';*/

        if (Auth::user()->can('our-process-edit')) {
            $title = '<a class="" title="Edit" href="' . route('powerpanel.our-process.edit', array('alias' => $value->id)) . '">' . $value->varTitle . '</a>';
        } else {
            $title = $value->varTitle;
        }       

        if (Auth::user()->can('our-process-edit')) {
            $title = '<a class="" title="' . trans("template.common.edit") . '" href="' . route('powerpanel.our-process.edit', array('alias' => $value->id)) . '">' . $value->varTitle . '</a>';
        } else {
            $title = $value->varTitle;
        }

        $orderArrow = '';
        $orderArrow .= '<span class="pageorderlink">';
        if ($totalRecord != $value->intDisplayOrder) {
            $orderArrow .= '<a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveUp"> <i class="fa fa-plus " aria-hidden="true"></i></a>';
        }
        $orderArrow .= $value->intDisplayOrder . ' ';
        if ($value->intDisplayOrder != 1) {
            $orderArrow .= '<a href="javascript:;"  data-order="' . $value->intDisplayOrder . '" class="moveDwn"><i class="fa fa-minus" aria-hidden="true"></i></a>';
        }
        $orderArrow .= '</span>';

        $records = array(
            '<input type="checkbox" name="delete" class="chkDelete" value="' . $value->id . '">',
            $title,
            '<div class="pro-act-btn">
          <a href="javascript:void(0)" class="without_bg_icon" onclick="return hs.htmlExpand(this,{width:300,headingText:\'' . trans("template.common.shortdescription") . '\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>
            <div class="highslide-maincontent">' . htmlspecialchars_decode($value->txtDescription) . '</div>
          </div>',
            
           
           
            $orderArrow,
            $publish_action,
            $details,
            $value->intDisplayOrder,
        );
        return $records;
    }

    public static function flushCache()
    {
        Cache::tags('OurProcess')->flush();
       // Cache::tags('BuildingCategory')->flush();
    }

}
