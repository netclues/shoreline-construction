<?php
namespace App\Http\Controllers\Powerpanel;

use App\Alias;
use App\Banner;
use App\CommonModel;
use App\Helpers\AddImageModelRel;
use App\Helpers\MyLibrary;
use App\Helpers\resize_image;
use App\Http\Controllers\PowerpanelController;
use App\Log;
use App\RecentUpdates;
use App\Video;
use Auth;
use Cache;
use Illuminate\Support\Facades\Redirect;
use Request;
use Validator;
use App\CmsPage;

class BannerController extends PowerpanelController
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }
    /**
     * This method handels load banner grid
     * @return  View
     * @since   2017-07-20
     * @author  NetQuick
     */
    public function index()
    {
        $total = CommonModel::getRecordCount();
        $this->breadcrumb['title'] = trans('template.bannerModule.manage');
        return view('powerpanel.banners.list', ['total' => $total, 'breadcrumb' => $this->breadcrumb]);
    }
    /**
     * This method handels list of banner with filters
     * @return  View
     * @since   2017-07-20
     * @author  NetQuick
     */
    public function get_list()
    {
        /*Start code for sorting*/
        $filterArr = [];
        $records = array();
        $records["data"] = array();
        $filterArr['orderColumnNo'] = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');
        $filterArr['orderByFieldName'] = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');
        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';
        $filterArr['statusFilter'] = !empty(Request::get('customActionName')) ? Request::get('customActionName') : '';
        $filterArr['bannerFilter'] = !empty(Request::get('bannerFilter')) ? Request::get('bannerFilter') : '';
        $filterArr['bannerFilterType'] = !empty(Request::get('bannerFilterType')) ? Request::get('bannerFilterType') : '';
        $filterArr['pageFilter'] = !empty(Request::get('pageFilter')) ? Request::get('pageFilter') : '';
        $filterArr['iDisplayLength'] = intval(Request::get('length'));
        $filterArr['iDisplayStart'] = intval(Request::get('start'));
        /**** Delete record then redirect to approriate pagination **/
        $currentrecordcountstart = intval(Request::get('start'));
        $currentpaging = intval(Request::get('length'));

        $totalRecords_old = CommonModel::getTotalRecordCount();
        if ($totalRecords_old > $currentrecordcountstart) {
            $filterArr['iDisplayStart'] = intval(Request::get('start'));
        } else {
            $filterArr['iDisplayStart'] = intval(0);
        }
        /**** Delete record then redirect to approriate pagination **/
        $sEcho = intval(Request::get('draw'));
        $arrResults = Banner::getRecordList($filterArr);
        $totalRecords = CommonModel::getTotalRecordCount();
        $iTotalRecords = CommonModel::getRecordCount($filterArr, true);
        $end = $filterArr['iDisplayStart'] + $filterArr['iDisplayLength'];
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        if (count($arrResults) > 0 && !empty($arrResults)) {
            foreach ($arrResults as $key => $value) {
                $records["data"][] = $this->tableData($value, $totalRecords);
            }
        }

        $records["customActionStatus"] = "OK";
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    /**
     * This method loads banner edit view
     * @param   Alias of record
     * @return  View
     * @since   2017-11-10
     * @author  NetQuick
     */
    public function edit($id = false)
    {
		$pagesList = CmsPage::getRecordList();
        if (empty($id)) {
            $total = CommonModel::getRecordCount();
            $total = $total + 1;
            $this->breadcrumb['title'] = trans('template.bannerModule.add');
            $this->breadcrumb['module'] = trans('template.bannerModule.manage');
            $this->breadcrumb['url'] = 'powerpanel/banners';
            $this->breadcrumb['inner_title'] = trans('template.bannerModule.add');
            $breadcrumb = $this->breadcrumb;
            $data = ['total_banner' => $total, 'breadcrumb' => $this->breadcrumb, 'imageManager' => true, 'videoManager' => true, 'pagesList' => $pagesList];
        } else {			
            $banners = Banner::getRecordById($id);
            if (empty($banners)) {
                return redirect()->route('powerpanel.banners.add');
            }
            //$videoID = $banners->fkIntVideoId;
            //$videoDataForSingle = video::getVideoDataForSingleVidoe($videoID);
            //print_r($videoDataForSingle); exit();

            $this->breadcrumb['title'] = trans('template.common.edit') . ' - ' . $banners->varTitle;
            $this->breadcrumb['module'] = trans('template.bannerModule.manage');
            $this->breadcrumb['url'] = 'powerpanel/banners';
            $this->breadcrumb['inner_title'] = trans('template.common.edit') . ' - ' . $banners->varTitle;
            $breadcrumb = $this->breadcrumb;
			// $data = ['banners' => $banners, 'breadcrumb' => $this->breadcrumb, 'imageManager' => true, 'videoManager' => true,'videoDataForSingle' => $videoDataForSingle,  'pagesList' => $pagesList];
			$data = ['banners' => $banners, 'breadcrumb' => $this->breadcrumb, 'imageManager' => true, 'videoManager' => true, 'pagesList' => $pagesList];
        }
        return view('powerpanel.banners.actions', $data);
    }

    /**
     * This method stores banner modifications
     * @return  View
     * @since   2017-11-10
     * @author  NetQuick
     */
    public function handlePost(Request $request)
    {
        $postArr = Request::all();
        $bannerFields = [];
        $actionMessage = trans('template.common.oppsSomethingWrong');

        $rules = array(
            'title' => 'required|max:160',
            'display_order' => 'required|greater_than_zero',
			'chrMenuDisplay' => 'required',
			'varButtonName' => 'handle_xss|no_url',
        );
        // $rules['bannerversion'] = 'required';
        // if ($postArr['bannerversion'] == 'vid_banner') {
        //     $rules['video_id'] = 'required';
        // } else {
            $rules['img_id_image_upload'] = 'required';
        // }

        $messsages = array(
            'img_id_image_upload.required' => trans('template.bannerModule.bannerValidation'),
			'display_order.greater_than_zero' => trans('template.bannerModule.displayGreaterThan'),
			'varButtonName.handle_xss' => 'Please enter valid input',
            'varButtonName.no_url' => 'URL is not allowed',
        );

        $validator = Validator::make($postArr, $rules, $messsages);
        if ($validator->passes()) {
            $bannerFields['fkIntImgId'] = $postArr['img_id_image_upload'];
            $bannerFields['fkIntVideoId'] = null;

            // if ($postArr['bannerversion'] == 'vid_banner') {
            //     $bannerFields['fkIntVideoId'] = $postArr['video_id'];
            //     $bannerFields['fkIntImgId'] = null;
            // } else {
                $bannerFields['fkIntImgId'] = $postArr['img_id_image_upload'];
				$bannerFields['fkIntVideoId'] = null;
			// }
			
			$bannerFields['chrLinkType'] = trim($postArr['chrLinkType']);
            if ($postArr['chrLinkType'] == 'O') {
                $bannerFields['varButtonLink'] = trim($postArr['varButtonLink']);
                $bannerFields['fkPageId'] = null;
            } else {
                $bannerFields['varButtonLink'] = null;
                $bannerFields['fkPageId'] = trim($postArr['fkPageId']);
			}
			
            $bannerFields['varTitle'] = trim($postArr['title']);
            $bannerFields['varSubTitle'] = trim($postArr['sub_title']);
            // $bannerFields['varBannerVersion'] = $postArr['bannerversion'];
			// $bannerFields['txtDescription'] = $postArr['description'];
			$bannerFields['varButtonName'] = trim($postArr['varButtonName']);
			$bannerFields['chrPublish'] = $postArr['chrMenuDisplay'];

            $id = Request::segment(3);

            if (is_numeric($id)) {
                #Edit post Handler=======
                $banner = Banner::getRecordForLogById($id);
                $whereConditions = ['id' => $id];
                $update = CommonModel::updateRecords($whereConditions, $bannerFields);

                if ($update) {
                    if (!empty($id)) {
                        self::swap_order_edit($postArr['display_order'], $id);
                        $logArr = MyLibrary::logData($id);
                        if (Auth::user()->can('log-advanced')) {
                            $newBannerObj = Banner::getRecordForLogById($id);
                            $oldRec = $this->recordHistory($banner);
                            $newRec = $this->recordHistory($newBannerObj);
                            $logArr['old_val'] = $oldRec;
                            $logArr['new_val'] = $newRec;
                        }
                        $logArr['varTitle'] = trim($postArr['title']);
                        Log::recordLog($logArr);
                        if (Auth::user()->can('recent-updates-list')) {
                            if (!isset($newBannerObj)) {
                                $newBannerObj = Banner::getRecordForLogById($id);
                            }
                            $notificationArr = MyLibrary::notificationData($id, $newBannerObj);
                            RecentUpdates::setNotification($notificationArr);
                        }
                    }
                    self::flushCache();
                    $actionMessage = trans('template.bannerModule.updateMessage');
                }
            } else {
                #Add post Handler=======
                $bannerFields['intDisplayOrder'] = self::swap_order_add($postArr['display_order']);
                $bannerID = CommonModel::addRecord($bannerFields);
                if (!empty($bannerID)) {
                    $id = $bannerID;
                    $newBannerObj = Banner::getRecordForLogById($id);

                    $logArr = MyLibrary::logData($id);
                    $logArr['varTitle'] = $newBannerObj->varTitle;
                    Log::recordLog($logArr);
                    if (Auth::user()->can('recent-updates-list')) {
                        $notificationArr = MyLibrary::notificationData($id, $newBannerObj);
                        RecentUpdates::setNotification($notificationArr);
                    }
                    self::flushCache();
                    $actionMessage = trans('template.bannerModule.addedMessage');
                }
            }

            AddImageModelRel::sync(explode(',', $postArr['img_id_image_upload']), $id);
            if (!empty($postArr['saveandexit']) && $postArr['saveandexit'] == 'saveandexit') {
                return redirect()->route('powerpanel.banners.index')->with('message', $actionMessage);
            } else {
                return redirect()->route('powerpanel.banners.edit', $id)->with('message', $actionMessage);
            }
        } else {
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }
    /**
     * This method destroys Banner in multiples
     * @return  Banner index view
     * @since   2016-10-25
     * @author  NetQuick
     */
    public function DeleteRecord()
    {
        $data = Request::all('ids');
        $update = MyLibrary::deleteMultipleRecords($data);
        self::flushCache();
        echo json_encode($update);
        exit;
    }
    /**
     * This method destroys Banner in multiples
     * @return  Banner index view
     * @since   2016-10-25
     * @author  NetQuick
     */
    public function publish()
    {
        $alias = (int) Request::get('alias');
        $val = Request::get('val');
        $update = MyLibrary::setPublishUnpublish($alias, $val);
        self::flushCache();
        echo json_encode($update);
        exit;
    }
    /**
     * This method reorders banner position
     * @return  Banner index view data
     * @since   2016-10-26
     * @author  NetQuick
     */
    public function reorder()
    {
        $order = Request::get('order');
        $exOrder = Request::get('exOrder');
        MyLibrary::swapOrder($order, $exOrder);
        self::flushCache();
    }
    /**
     * This method assigns default banner flag
     * @return  Banner index view data
     * @since   2016-12-10
     * @author  NetQuick
     */
    public function makeDefault()
    {
        $id = Request::get('alias');
        $val = Request::get('val');
        if (!empty($val) && ($val == 'rm-default')) {
            $update = Banner::setDefault($id, ['chrDefaultBanner' => 'N']);
            if ($update) {
                $logArr = MyLibrary::logData($id);
                $logArr['action'] = 'remove-default-banner';
                Log::recordLog($logArr);
                self::flushCache();
            }
        }
        if (!empty($val) && ($val == 'default')) {
            $update = Banner::setDefault($id, ['chrDefaultBanner' => 'Y']);
            if ($update) {
                $logArr = MyLibrary::logData($id);
                $logArr['action'] = 'made-default-banner';
                Log::recordLog($logArr);
                self::flushCache();
            }
        }
        echo json_encode($update);
    }
/**
 * This method handels swapping of available order record while adding
 * @param      order
 * @return  order
 * @since   2016-10-21
 * @author  NetQuick
 */
    public static function swap_order_add($order = null)
    {
        $response = false;
        if ($order != null) {
            $response = MyLibrary::swapOrderAdd($order);
            self::flushCache();
        }
        return $response;
    }
    /**
     * This method handels swapping of available order record while editing
     * @param      order
     * @return  order
     * @since   2016-12-23
     * @author  NetQuick
     */
    public static function swap_order_edit($order = null, $id = null)
    {
        MyLibrary::swapOrderEdit($order, $id);
        self::flushCache();
    }

    public function tableData($value, $totalRecords)
    {

        $image = '';
        $actions = '';
        // $txtDescription = '';
        $checkbox = '';
        $publish_action = '';

        if (Auth::user()->can('banners-edit')) {
            $actions .= '<a class="without_bg_icon" title="' . trans("template.common.edit") . '" href="' . route('powerpanel.banners.edit', array('alias' => $value->id)) . '">
                <span><i class="fa fa-pencil"></i></a>';
        }

        if (($totalRecords > 1)) {
            if (Auth::user()->can('banners-delete')) {
                $actions .= '<a class="without_bg_icon delete" title="' . trans("template.common.delete") . '" data-controller="banners" data-alias="' . $value->id . '"><i class="fa fa-times"></i></a>';
            }
            $checkbox = '<input type="checkbox" name="delete" class="chkDelete" value="' . $value->id . '">';
        } else {
            $checkbox = '<a href="javascript:;" data-toggle="tooltip" data-placement="right" data-toggle="tooltip" title="This is default banner so can&#39;t be deleted."><i style="color:red" class="fa fa-exclamation-triangle"></i></a>';
        }

        if (Auth::user()->can('banners-publish')) {
            if ($value->chrPublish == 'Y') {
                $publish_action .= '<input  data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/banners" title="' . trans("template.common.publishedRecord") . '" data-value="Unpublish" data-alias="' . $value->id . '">';
            } else {
                $publish_action .= '<input checked="" data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/banners" title="' . trans("template.common.unpublishedRecord") . '" data-value="Publish" data-alias="' . $value->id . '">';
            }
        }

        $image .= '<div class="text-center">';
        if (!empty($value->image)) {
            $image .= '<a  href="' . resize_image::resize($value->fkIntImgId) . '" class="fancybox-buttons" data-fancybox="fancybox-buttons">';
            $image .= '<img height="30" width="30" title="' . preg_replace('/[^A-Za-z0-9\-]/', '-', $value->varTitle) . '" src="' . resize_image::resize($value->fkIntImgId, 50, 50) . '"/>';
            $image .= '</a>';
        } else {
            $image .= '<span class="glyphicon glyphicon-minus"></span>';
        }
        $image .= '</div>';

        if (Auth::user()->can('banners-edit')) {
            $title = '<a title="Edit" href="' . route('powerpanel.banners.edit', array('alias' => $value->id)) . '">' . $value->varTitle . '</a>';
        } else {
            $title = $value->varTitle;
        }

        $orderArrow = '';
        $orderArrow .= '<span class="pageorderlink">';
        if ($totalRecords != $value->intDisplayOrder) {
            $orderArrow .= '<a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveUp"><i class="fa fa-plus" aria-hidden="true"></i></a> ';
        }
        $orderArrow .= '<a href="javascript:;"  data-order="' . $value->intDisplayOrder . '" class="moveTo" data-module="services">' . $value->intDisplayOrder . '</a> ';
        if ($value->intDisplayOrder != 1) {
            $orderArrow .= ' <a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveDwn"><i class="fa fa-minus" aria-hidden="true"></i></a>';
        }
        $orderArrow .= '</span>';

        $subTitle = '<span class="glyphicon glyphicon-minus"></span>';
        if(!empty($value->varSubTitle)){
        $subTitle =  '<div class="pro-act-btn">
                    <a href="javascript:void(0)" class="without_bg_icon" onclick="return hs.htmlExpand(this,{width:300,headingText:\'' . "Sub Title " . '\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>
                        <div class="highslide-maincontent">' . html_entity_decode($value->varSubTitle) . '</div>
                    </div>';
         }  

        // $Description = '<span class="glyphicon glyphicon-minus"></span>';
        // if(!empty($value->txtDescription)){
        //   $Description = '<div class="pro-act-btn">
        //      <a href="javascript:void(0)" class="without_bg_icon" onclick="return hs.htmlExpand(this,{width:300,headingText:\'' . trans("template.common.description") . '\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>
        //         <div class="highslide-maincontent">' . html_entity_decode($value->txtDescription) . '</div>
        //     </div>';
        // }

        $records = array(
            $checkbox,
            $title,
            $subTitle,
            // $Description,     
            $image,
            $orderArrow,
            $publish_action,
            $actions,
        );
        return $records;
    }
    /**
     * This method handels logs old records
     * @param      $data
     * @return  order
     * @since   2017-07-21
     * @author  NetQuick
     */
    public function recordHistory($data = false)
    {

        $bannerVersion = ($data->varBannerVersion == "vid_banner") ? "Video Banner" : "Image Banner";
        if ($data->varBannerVersion == "vid_banner" && $data->fkIntVideoId != null) {
            $videoDetail = Video::getVideoTitleById($data->fkIntVideoId);
            $videoTitle = ($videoDetail->varVideoName != "") ? $videoDetail->varVideoName . "." . $videoDetail->varVideoExtension : '-';
        } else {
            $videoTitle = "-";
        }

        $returnHtml = '';
        $returnHtml .= '<table class="new_table_desing table table-striped table-bordered table-hover">
                            <thead>
                                    <tr>
                                            <th>' . trans("template.common.title") . '</th>
                                            <th>' . trans("template.common.image") . '</th>
                                            <th>' . trans("template.bannerModule.version") . '</th>
                                            <th>' . trans("template.common.video") . '</th>                                            
                                            <th>' . trans("template.common.displayorder") . '</th>
                                            <th>' . trans("template.common.publish") . '</th>
                                    </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                            <td>' . $data->varTitle . '</td>';
        if ($data->fkIntImgId > 0) {
            $returnHtml .= '<td>' . '<img height="50" width="50" src="' . resize_image::resize($data->fkIntImgId) . '" />' . '</td>';
        } else {
            $returnHtml .= '<td>-</td>';
        }
        $returnHtml .= '<td>' . $bannerVersion . '</td>
                                            <td>' . $videoTitle . '</td>                                            
                                            <td>' . ($data->intDisplayOrder) . '</td>
                                            <td>' . $data->chrPublish . '</td>
                                    </tr>
                            </tbody>
                    </table>';
        return $returnHtml;
    }
    public static function flushCache()
    {
        Cache::tags('Banner')->flush();
    }
}
