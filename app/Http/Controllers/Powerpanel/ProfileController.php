<?php
namespace App\Http\Controllers\Powerpanel;

use App\Helpers\MyLibrary;
use App\Helpers\resize_image;
use App\Http\Controllers\PowerpanelController;
use App\Image;
use App\Modules;
use App\RecentUpdates;
use App\User;
use Auth;
use Hash;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Validator;

class ProfileController extends PowerpanelController
{

    public function __construct(UrlGenerator $url)
    {
        parent::__construct();
        $this->url = $url;
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }
    public function index(Guard $auth)
    {

        $userEmailID = $auth->user()['email'];
        $user_data = User::getRecordByEmailID($userEmailID);
        $user_data->email = MyLibrary::getDecryptedString($user_data->email);
        $user_data->personalId = MyLibrary::getDecryptedString($user_data->personalId);

        $this->breadcrumb['title'] = trans('template.header.myProfile');
        return view('powerpanel.profile.change_profile', ['user_data' => $user_data, 'breadcrumb' => $this->breadcrumb, 'imageManager' => true]);

    }
    public static function changeprofile(Request $request, Guard $auth)
    {
        $data = Request::all();
        $data['email'] = MyLibrary::getEncryptedString($data['email']);
        $rules = array(
            'name' => 'required|max:150',
            'email' => 'required|max:160|unique:users,email,' . $auth->user()['id'],
            'personalId' => 'required|email|max:100',
        );
        $validator = Validator::make($data, $rules);
        $userEmailID = $auth->user()['email'];
        if ($validator->passes()) {
            $data = [
                'name' => $data['name'],
                'email' => $data['email'],
                'personalId' => MyLibrary::getEncryptedString($data['personalId']),
                'fkIntImgId' => (!empty($data['img_id_user_photo']) ? $data['img_id_user_photo'] : null),
            ];

            $user = User::updateUserRecordByEmail($userEmailID, $data);
            return Redirect::route('powerpanel/changeprofile')->with('message', 'The record has been successfully edited and saved.');
        } else {
            return Redirect::route('powerpanel/changeprofile')->withErrors($validator)->withInput();
        }
    }
    public function changepassword()
    {
        $this->breadcrumb['title'] = trans('template.header.changePassword');
        return view('powerpanel.profile.change_password', ['breadcrumb' => $this->breadcrumb]);
    }
    public function handle_changepassword(Request $request, Guard $auth)
    {
        $data = Request::all();
        $moduleCode = Modules::getModule('users');
        $rules = array(
            'old_password' => 'required|max:20',
            'new_password' => 'required|max:20|min:6|check_passwordrules',
            'confirm_password' => 'required|same:new_password|max:20|min:6|check_passwordrules',
        );
        $validator = Validator::make($data, $rules);
        $userEmailID = $auth->user()['email'];
        $user_data = User::getRecordByEmailID($userEmailID);
        if ($validator->passes()) {
            if (Hash::check($data['old_password'], $user_data->password)) {
                if ($data['old_password'] != $data['new_password']) {
                    $data = ['password' => bcrypt($data['new_password'])];
                    $user = User::updateUserRecordByEmail($userEmailID, $data);
                    if ($user) {
                        $userObj = User::getRecordByIdWithoutRole($user_data->id);
                        if (Auth::user()->can('recent-updates-list')) {
                            $notificationArr = MyLibrary::notificationData($user_data->id, $userObj, $moduleCode->id);
                            RecentUpdates::setNotification($notificationArr);
                        }
                    }
                } else {
                    return Redirect::route('powerpanel/changepassword')->with('error', 'Password is already exists please choose another password.');
                }
            } else {
                return Redirect::route('powerpanel/changepassword')->with('error', 'Old Password is not valid please enter valid password.');
            }
            return Redirect::route('powerpanel/changepassword')->with('message', 'The record has been successfully edited and saved.');
        } else {
            return Redirect::route('powerpanel/changepassword')->withErrors($validator)->withInput();
        }
    }
}
