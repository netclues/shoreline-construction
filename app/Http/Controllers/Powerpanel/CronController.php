<?php

namespace App\Http\Controllers\Powerpanel;

use App\EmailLog;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class CronController extends Controller
{
    public static function deleteEmailLogs()
    {
        $date          = Carbon::today()->subDays(30);
        $LastMonthData = EmailLog::where('created_at', '<', $date)->delete();
    }
}