<?php
namespace App\Http\Controllers\Powerpanel;

use App\Alias;
use App\CommonModel;
use App\Helpers\AddImageModelRel;
use App\Helpers\MyLibrary;
use App\Helpers\resize_image;
use App\Http\Controllers\PowerpanelController;
use App\Log;
use App\RecentUpdates;
use App\Testimonial;
use App\Video;
use Auth;
use Cache;
use Carbon\Carbon;
use Config;
use Illuminate\Support\Facades\Redirect;
use Request;
use Validator;

class TestimonialController extends PowerpanelController
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }

    /**
     * This method handels load testimonial grid
     * @return  View
     * @since   2017-07-20
     * @author  NetQuick
     */
    public function index()
    {
        $total = CommonModel::getRecordCount();
        $this->breadcrumb['title'] = trans('template.testimonialModule.manageTestimonials');
        return view('powerpanel.testimonial.list_testimonial', ['total' => $total, 'breadcrumb' => $this->breadcrumb]);
    }

    /**
     * This method handels list of testimonial with filters
     * @return  View
     * @since   2017-07-20
     * @author  NetQuick
     */
    public function get_list()
    {

        /*Start code for sorting*/
        $filterArr = [];
        $records = array();

        $records["data"] = array();
        $filterArr['orderColumnNo'] = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');
        $filterArr['orderByFieldName'] = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');
        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';
        $filterArr['statusFilter'] = !empty(Request::get('statusFilter')) ? Request::get('statusFilter') : '';
        $filterArr['dateFilter'] = !empty(Request::get('dateValue')) ? Request::get('dateValue') : '';
        $filterArr['iDisplayLength'] = intval(Request::get('length'));
        $filterArr['iDisplayStart'] = intval(Request::get('start'));

        $sEcho = intval(Request::get('draw'));

        $arrResults = Testimonial::getRecordList($filterArr);
        $iTotalRecords = CommonModel::getRecordCount($filterArr, true);
        $end = $filterArr['iDisplayStart'] + $filterArr['iDisplayLength'];
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        if ($arrResults->count() > 0 && !empty($arrResults)) {
            foreach ($arrResults as $key => $value) {
                $records["data"][] = $this->tableData($value);
            }
        }
        $records["customActionStatus"] = "OK";
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;

    }

    /**
     * This method loads testimonial edit view
     * @param      Alias of record
     * @return  View
     * @since   2017-07-21
     * @author  NetQuick
     */

    public function edit($alias = false)
    {
        $total = CommonModel::getRecordCount();
        $total = $total + 1;

        $imageManager = true;
        $videoManager = true;
        if (!is_numeric($alias)) {
            $total = CommonModel::getRecordCount();
            $total = $total + 1;
            $this->breadcrumb['title'] = trans('template.testimonialModule.addTestimonial');
            $this->breadcrumb['module'] = trans('template.testimonialModule.manageTestimonials');
            $this->breadcrumb['url'] = 'powerpanel/testimonial';
            $this->breadcrumb['inner_title'] = trans('template.testimonialModule.addTestimonial');
            $data = [
                'total' => $total,
                'breadcrumb' => $this->breadcrumb,
                'imageManager' => 'imageManager',
                'videoManager' => true,
            ];
        } else {
            $id = $alias;
            $testimonial = Testimonial::getRecordById($id);
            $total = CommonModel::getRecordCount(); 
            if (empty($testimonial)) {
                return redirect()->route('powerpanel.testimonial.add');
            }

            $videoID = $testimonial->fkIntVideoId;
            $videoDataForSingle = video::getVideoDataForSingleVidoe($videoID);

            $this->breadcrumb['title'] = trans('template.testimonialModule.editTestimonial') . ' - ' . $testimonial->varTitle;
            $this->breadcrumb['module'] = trans('template.testimonialModule.manageTestimonials');
            $this->breadcrumb['url'] = 'powerpanel/testimonial';
            $this->breadcrumb['inner_title'] = trans('template.testimonialModule.editTestimonial') . ' - ' . $testimonial->varTitle;
            $data = [
                'testimonials' => $testimonial,
                'id' => $id,
                'breadcrumb' => $this->breadcrumb,
                'imageManager' => 'imageManager',
                'videoManager' => 'videoManager',
                'total' => $total,
                'videoDataForSingle' => $videoDataForSingle,
            ];
        }
        return view('powerpanel.testimonial.actions', $data);
    }

    /**
     * This method stores testimonial modifications
     * @return  View
     * @since   2017-07-21
     * @author  NetQuick
     */

    public function handlePost(Request $request)
    {
        $postArr = Request::all();
        
        $rules = array(
            'testimonialby' => 'required|max:160',
            'author_name' => 'required|max:160',
            'testimonial' => 'required',
            'display_order' => 'required|greater_than_zero',
            'chrMenuDisplay' => 'required',
        );
        $rules['bannerversion'] = 'required';
        if ($postArr['bannerversion'] == 'vid_banner') {
            $rules['video_id'] = 'required';
        } else {
            $rules['img_id_image_upload'] = 'required';
        }
        $messsages = array(
            'img_id_image_upload.required' => trans('template.bannerModule.bannerValidation'),
            'display_order.greater_than_zero' => trans('template.ourprocessModule.displayGreaterThan'),
            'testimonialby.required' => trans('template.testimonialModule.testimonialByMessage'),
            'author_name.required' => trans('template.testimonialModule.authorMessage'),
            
        );
        $validator = Validator::make($postArr, $rules, $messsages);
        if ($validator->passes()) {      
            
            $id = Request::segment(3);
           
            $actionMessage = trans('template.common.oppsSomethingWrong');
            if (is_numeric($id)) { #Edit post Handler=======
            
                $testimonial = Testimonial::getRecordForLogById($id);                
                $updateTestimonialFields = [];

                $updateTestimonialFields['fkIntImgId'] = $postArr['img_id_image_upload'];
                $updateTestimonialFields['fkIntVideoId'] = null;

                // echo '<pre>'; print_r($updateTestimonialFields);die;

                if ($postArr['bannerversion'] == 'vid_banner') {
                    $updateTestimonialFields['fkIntVideoId'] = $postArr['video_id'];
                    $updateTestimonialFields['fkIntImgId'] = null;
                } else {
                    $updateTestimonialFields['fkIntImgId'] = $postArr['img_id_image_upload'];
                    $updateTestimonialFields['fkIntVideoId'] = null;
                }
                
                $updateTestimonialFields['varTitle'] = trim($postArr['testimonialby']);
                $updateTestimonialFields['varAuthor'] = trim($postArr['author_name']);
                $updateTestimonialFields['varBannerVersion'] = $postArr['bannerversion'];
                // $updateTestimonialFields['fkIntImgId'] = !empty($postArr['img_id']) ? $postArr['img_id'] : null;
                $updateTestimonialFields['txtDescription'] = $postArr['testimonial'];
               // $updateTestimonialFields['dtStartDateTime'] = date('Y-m-d', strtotime(str_replace('/', '-', $postArr['testimonialdate'])));
                $updateTestimonialFields['chrPublish'] = $postArr['chrMenuDisplay'];
                
                $ourprocess = Testimonial::getRecordForLogById($id);                
                $whereConditions = ['id' => $id];                
                $update = CommonModel::updateRecords($whereConditions, $updateTestimonialFields);                
                if ($update) {
                                       
                    if ($id > 0 && !empty($id)) { 
                                             
                        self::swap_order_edit($postArr['display_order'], $ourprocess->id);                        
                        $logArr = MyLibrary::logData($id);
                        if (Auth::user()->can('log-advanced')) {
                            $newTestimonialObj = Testimonial::getRecordForLogById($id);
                            print_r($newTestimonialObj);die; 
                            $oldRec = $this->recordHistory($testimonial);
                            $newRec = $this->recordHistory($newTestimonialObj);
                            $logArr['old_val'] = $oldRec;
                            $logArr['new_val'] = $newRec;
                        }

                        $logArr['varTitle'] = trim($postArr['testimonialby']);
                        $logArr['varAuthor'] = trim($postArr['author_name']);
                        Log::recordLog($logArr);
                        if (Auth::user()->can('recent-updates-list')) {
                            if (!isset($newTestimonialObj)) {
                                $newTestimonialObj = Testimonial::getRecordForLogById($id);
                            }
                            $notificationArr = MyLibrary::notificationData($id, $newTestimonialObj);
                            RecentUpdates::setNotification($notificationArr);
                        }
                        self::flushCache();
                        $actionMessage = trans('template.testimonialModule.updateMessage');
                    }
                }
            } else { #Add post Handler=======
                // echo '<pre>'; print_r($postArr['display_order']);die;
                
                // echo '<pre>'; print_r( $updateTestimonialFields['intDisplayOrder']);die;

                $testimonialArr['varTitle'] = trim($postArr['testimonialby']);
                $testimonialArr['varAuthor'] = trim($postArr['author_name']);
                $testimonialArr['fkIntImgId'] = !empty($postArr['img_id_image_upload']) ? $postArr['img_id_image_upload'] : null;
                $testimonialArr['fkIntVideoId'] = !empty($postArr['video_id']) ? $postArr['video_id'] : null;
                $testimonialArr['txtDescription'] = $postArr['testimonial'];
                $testimonialArr['intDisplayOrder'] = self::swap_order_add($postArr['display_order']);
                // $testimonialArr['dtStartDateTime'] = date('Y-m-d', strtotime(str_replace('/', '-', $postArr['testimonialdate'])));
                $testimonialArr['chrPublish'] = $postArr['chrMenuDisplay'];
                $testimonialArr['created_at'] = Carbon::now();
                // echo '<pre>'; print_r($testimonialArr);die;
                $testimonialID = CommonModel::addRecord($testimonialArr);
                if (!empty($testimonialID)) {
                    $id = $testimonialID;
                    $newTestimonialObj = Testimonial::getRecordForLogById($id);

                    $logArr = MyLibrary::logData($id);
                    $logArr['varTitle'] = $newTestimonialObj->varTitle;
                    $logArr['varAuthor'] = $newTestimonialObj->varAuthor;
                    Log::recordLog($logArr);
                    if (Auth::user()->can('recent-updates-list')) {
                        $notificationArr = MyLibrary::notificationData($id, $newTestimonialObj);
                        RecentUpdates::setNotification($notificationArr);
                    }
                    self::flushCache();
                    $actionMessage = trans('template.testimonialModule.addMessage');
                }

            }
            // AddImageModelRel::sync(explode(',', $postArr['img_id']), $id);
            AddImageModelRel::sync(explode(',', $postArr['img_id_image_upload']), $id);            
            if (!empty($postArr['saveandexit']) && $postArr['saveandexit'] == 'saveandexit') {
                return redirect()->route('powerpanel.testimonial.index')->with('message', $actionMessage);
            } else {
                return redirect()->route('powerpanel.testimonial.edit', $id)->with('message', $actionMessage);
            }

        } else {
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }

    /**
     * This method destroys Testimonial in multiples
     * @return  Testimonial index view
     * @since   2016-10-25
     * @author  NetQuick
     */
    public function DeleteRecord(Request $request)
    {
        $data = Request::all('ids');
        $update = MyLibrary::deleteMultipleRecords($data);
        self::flushCache();
        echo json_encode($update);
        exit;
    }

    /**
     * This method destroys Testimonial in multiples
     * @return  Testimonial index view
     * @since   2016-10-25
     * @author  NetQuick
     */
    public function publish(Request $request)
    {
        $alias = Request::get('alias');
        $val = Request::get('val');
        $update = MyLibrary::setPublishUnpublish($alias, $val);
        self::flushCache();
        echo json_encode($update);
        exit;
    }

    public function tableData($value,$totalRecord = false)
    {
        $details = '';
        $actions = '';
        $author = '';
        $publish_action = '';

        $imgIcon = '';
        if (!empty($value->fkIntImgId) && $value->fkIntImgId > 0) {
            $imgIcon .= '<a href="' . resize_image::resize($value->fkIntImgId) . '" class="fancybox-buttons" data-fancybox="fancybox-buttons">';
            $imgIcon .= '<img height="30" width="30" title="' . preg_replace('/[^A-Za-z0-9\-]/', '-', $value->varTitle) . '" src="' . resize_image::resize($value->fkIntImgId, 50, 50) . '"/>';
            $imgIcon .= '</a>';
        } else {
            $imgIcon .= '<span class="glyphicon glyphicon-minus"></span>';
        }

        if (Auth::user()->can('testimonial-edit')) {
            $actions .= '<a class="without_bg_icon" title="' . trans("template.common.edit") . '" href="' . route('powerpanel.testimonial.edit', array('alias' => $value->id)) . '">
					<i class="fa fa-pencil"></i></a>';
        }
        if (Auth::user()->can('testimonial-delete')) {
            $actions .= '&nbsp;<a class="without_bg_icon delete" title="' . trans("template.common.delete") . '" data-controller="testimonial" data-alias = "' . $value->id . '"><i class="fa fa-times"></i></a>';
        }

        if (Auth::user()->can('testimonial-publish')) {
            if ($value->chrPublish == 'Y') {
                $publish_action .= '<input data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/testimonial" title="' . trans("template.common.publishedRecord") . '" data-value="Unpublish" data-alias="' . $value->id . '">';
            } else {
                $publish_action .= '<input checked="" data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/testimonial" title="' . trans("template.common.unpublishedRecord") . '" data-value="Publish" data-alias="' . $value->id . '">';
            }
        }

        if (Auth::user()->can('testimonial-edit')) {
            $title = '<a class="" title="' . trans("template.common.edit") . '" href="' . route('powerpanel.testimonial.edit', array('alias' => $value->id)) . '">' . $value->varTitle . '</a>';
        } else {
            $title = $value->varTitle;
        }

        if (!empty($value->varAuthor)) {
            $author = $value->varAuthor; 
        } else {
            $author = 'N/A';
        }

        $orderArrow = '';
        $orderArrow .= '<span class="pageorderlink">';
        if ($totalRecord != $value->intDisplayOrder) {
            $orderArrow .= '<a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveUp"> <i class="fa fa-plus " aria-hidden="true"></i></a>';
        }
        $orderArrow .= $value->intDisplayOrder . ' ';
        if ($value->intDisplayOrder != 1) {
            $orderArrow .= '<a href="javascript:;"  data-order="' . $value->intDisplayOrder . '" class="moveDwn"><i class="fa fa-minus" aria-hidden="true"></i></a>';
        }
        $orderArrow .= '</span>';

        // $dateTimeTestimonial = '';
        // if(!empty($value->dtStartDateTime)){
        //   $dateTimeTestimonial = date(Config::get('Constant.DEFAULT_DATE_FORMAT'), strtotime($value->dtStartDateTime));    
        // }else{
        //   $dateTimeTestimonial = '-';   
        // }        

        $records = array(
            '<input type="checkbox" name="delete" class="chkDelete" value="' . $value->id . '">',
            $title,            
            '<div class="pro-act-btn">
                <a href="javascript:void(0)" class="without_bg_icon highslide-active-anchor" onclick="return hs.htmlExpand(this,{width:300,headingText:\'' . trans("template.common.author") . '\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>
                <div class="highslide-maincontent">' . htmlspecialchars_decode($author) . '</div>
            </div>',
            '<div class="pro-act-btn">
                <a href="javascript:void(0)" class="without_bg_icon highslide-active-anchor" onclick="return hs.htmlExpand(this,{width:300,headingText:\'' . trans("template.common.description") . '\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>
                <div class="highslide-maincontent">' . htmlspecialchars_decode($value->txtDescription) . '</div>
            </div>',            
            $imgIcon,
            $orderArrow,
            // $dateTimeTestimonial,
            $publish_action,
            $actions,
        );
        return $records;
    }

    /**
     * This method handels logs History records
     * @param   $data
     * @return  HTML
     * @since   2017-07-21
     * @author  NetQuick
     */
    public function recordHistory($data = false)
    {

        $bannerVersion = ($data->varBannerVersion == "vid_banner") ? "Video Banner" : "Image Banner";
        if ($data->varBannerVersion == "vid_banner" && $data->fkIntVideoId != null) {
            $videoDetail = Video::getVideoTitleById($data->fkIntVideoId);
            $videoTitle = ($videoDetail->varVideoName != "") ? $videoDetail->varVideoName . "." . $videoDetail->varVideoExtension : '-';
        } else {
            $videoTitle = "-";
        }

        $returnHtml = '';
        $returnHtml .= '<table class="new_table_desing table table-striped table-bordered table-hover">
				<thead>
					<tr>
                        <th>' . trans("template.testimonialModule.testimonialBy") . '</th>
                        <th>' . trans("template.common.image") . '</th>
                        <th>' . trans("template.bannerModule.version") . '</th>
                        <th>' . trans("template.common.video") . '</th>
                        <th>' . trans("template.testimonialModule.author") . '</th>
						// <th>' . trans("template.testimonialModule.testimonialDate") . '</th>
						<th>' . trans("template.common.description") . '</th>
						<th>' . trans("template.common.publish") . '</th>
					</tr>
				</thead>
				<tbody>
					<tr>
                        <td>' . $data->varTitle . '</td>';
                        if ($data->fkIntImgId > 0) {
                            $returnHtml .= '<td>' . '<img height="50" width="50" src="' . resize_image::resize($data->fkIntImgId) . '" />' . '</td>';
                        } else {
                            $returnHtml .= '<td>-</td>';
                        }

                $returnHtml .= '<td>' . $bannerVersion . '</td> 
                        <td>' . $videoTitle . '</td>
                        <td>' . $data->varAuthor . '</td>
						// <td>' . date(Config::get('Constant.DEFAULT_DATE_FORMAT'), strtotime($data->dtStartDateTime)) . '</td>
						<td>' . $data->txtDescription . '</td>
						<td>' . $data->chrPublish . '</td>
					</tr>
				</tbody>
			</table>';

        return $returnHtml;
    }
    
    /**
     * This method handels swapping of available order record while editing
     * @param   order
     * @return  order
     * @since   2016-12-23
     * @author  NetQuick
     */
    public static function swap_order_edit($order = null, $id = null)
    {        
        MyLibrary::swapOrderEdit($order, $id);
        self::flushCache();
    }
    /**
     * This method handels swapping of available order record while adding
     * @param   order
     * @return  order
     * @since   2016-10-21
     * @author  NetQuick
     */
    public static function swap_order_add($order = null)
    {
        
        $response = false;
        if ($order != null) {
            
            $response = MyLibrary::swapOrderAdd($order);
            // print_r($response);die;
            self::flushCache();
        }
        return $response;
    }
    /**
     * This method reorders banner position
     * @return  Banner index view data
     * @since   2016-10-26
     * @author  NetQuick
     */
    public function reorder()
    {
        $order = Request::get('order');
        $exOrder = Request::get('exOrder');

        MyLibrary::swapOrder($order, $exOrder);
        self::flushCache();
    }
    public static function flushCache()
    {
        Cache::tags('Testimonial')->flush();
    }
}
