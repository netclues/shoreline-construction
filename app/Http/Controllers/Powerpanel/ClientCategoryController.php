<?php
/**
 * The MenuController class handels client_category
 * configuration  process.
 * @package   Netquick powerpanel
 * @license   http://www.opensource.org/licenses/BSD-3-Clause
 * @version   1.00
 * @since     2017-01-31
 * @author    NetQuick
 */
namespace App\Http\Controllers\Powerpanel;

use App\Alias;
use App\ClientCategory;
use App\Client;
use App\CommonModel;
use App\Helpers\AddCategoryAjax;
use App\Helpers\Category_builder;
use App\Helpers\MyLibrary;
use App\Http\Controllers\PowerpanelController;
use App\Log;
use App\RecentUpdates;
use Auth;
use Cache;
use Carbon\Carbon;
use Config;
use Request;

use Illuminate\Support\Facades\Redirect;
use Validator;

class ClientCategoryController extends PowerpanelController
{

    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }
    /**
     * This method handels load process of client_category
     * @return  view
     * @since   2017-01-31
     * @author  NetQuick
     */
    public function index()
    {
        $iTotalRecords             = CommonModel::getRecordCount();
        $this->breadcrumb['title'] = trans('template.clientCategoryModule.manageClientCategory');
        $breadcrumb                = $this->breadcrumb;
        return view('powerpanel.client_category.index', compact('iTotalRecords', 'breadcrumb'));
    }

    /**
     * This method loads client_category edit view
     * @param   Alias of record
     * @return  View
     * @since   2017-01-31
     * @author  NetQuick
     */
    public function edit($alias = false)
    {
        $isParent = 0;
        if (!is_numeric($alias)) 
        {
            
            $total                           = CommonModel::getRecordCount();
            $total                           = $total + 1;
            $this->breadcrumb['title']       = trans('template.clientCategoryModule.addClientCategory');
            $this->breadcrumb['inner_title'] = trans('template.clientCategoryModule.addClientCategory');
            $this->breadcrumb['module']      = trans('template.clientCategoryModule.manageClientCategory');
            $this->breadcrumb['url']         = 'powerpanel/client-category';
            $breadcrumb                      = $this->breadcrumb;
            $hasRecords                      = 0;
            $metaInfo                        = array('varMetaTitle' => '', 'varMetaKeyword' => '', 'varMetaDescription' => '');
            $categories                      = Category_builder::Parentcategoryhierarchy('intParentCategoryId', '', '', 'listbox', 'client_category');
            $data                            = compact('total', 'breadcrumb', 'categories', 'hasRecords', 'isParent');

        } else {
              $id           = $alias;
              $clientCategory = ClientCategory::getRecordById($id);
              
              if (empty($clientCategory)) {
                return redirect()->route('powerpanel.client-category.add');
              }

              $this->breadcrumb['title']       = trans('template.common.edit') . ' - ' . $clientCategory->varTitle;
              $this->breadcrumb['inner_title'] = trans('template.common.edit') . ' - ' . $clientCategory->varTitle;
              $this->breadcrumb['module']      = trans('template.clientCategoryModule.manageClientCategory');
              $this->breadcrumb['url']         = 'powerpanel/client-category';
              $breadcrumb                      = $this->breadcrumb;
              $hasRecords                      = Client::getCountById($clientCategory->id);
              $isParent                        = ClientCategory::getCountById($clientCategory->id);
              $metaInfo                        = array('varMetaTitle' => $clientCategory->varMetaTitle, 'varMetaKeyword' => $clientCategory->varMetaKeyword, 'varMetaDescription' => $clientCategory->varMetaDescription);
              $categories                      = Category_builder::Parentcategoryhierarchy($clientCategory->intParentCategoryId, $id);
              $alias                           = $clientCategory->alias->varAlias;
              $data                            = compact('metaInfo', 'clientCategory', 'hasRecords', 'isParent', 'alias', 'breadcrumb', 'categories');
        }
        return view('powerpanel.client_category.actions', $data);
    }
    /**
     * This method stores client_category modifications
     * @return  View
     * @since   2017-01-31
     * @author  NetQuick
     */

    public function handlePost(Request $request)
    {
        $data     = Request::all();
        $settings = json_decode(Config::get("Constant.MODULE.SETTINGS"));
        $rules    = array(
            'title'             => 'required|max:160',
            'display_order'     => 'required|greater_than_zero',
            'alias'             => 'required',
            'short_description' => 'required|max:' . (isset($settings) ? $settings->short_desc_length : 400)
        );
        $messsages = array(
            'display_order.required'          => trans('template.clientCategoryModule.displayOrder'),
            'display_order.greater_than_zero' => trans('template.clientCategoryModule.displayGreaterThan')
        );
        $data['short_description'] = trim(preg_replace('/\s\s+/', ' ', $data['short_description']));
        $validator                 = Validator::make($data, $rules, $messsages);
        if ($validator->passes()) {
            $id            = Request::segment(3);
            $actionMessage = trans('template.common.oppsSomethingWrong');
            if (is_numeric($id)) {
                #Edit post Handler=======

                if ($data['oldAlias'] != $data['alias']) {
                    Alias::updateAlias($data['oldAlias'], $data['alias']);
                }
                $clientCategory             = ClientCategory::getRecordForLogById($id);
                $updateClientCategoryFields = [
                    'varTitle'            => trim($data['title']),
                    'intParentCategoryId' => $data['parent_category_id'],
                    'chrPublish'          => isset($data['chrMenuDisplay']) ? $data['chrMenuDisplay'] : 'Y',
                    'txtDescription'      => $data['description'],
                    'txtShortDescription' => $data['short_description'],
                    'varMetaTitle'        => $data['varMetaTitle'],
                    'varMetaKeyword'      => $data['varMetaKeyword'],
                    'varMetaDescription'  => $data['varMetaDescription'],
                ];

                $whereConditions = ['id' => $clientCategory->id];
                $update          = CommonModel::updateRecords($whereConditions, $updateClientCategoryFields);
                if ($update) {
                    if (!empty($id)) {
                        self::swap_order_edit($data['display_order'], $clientCategory->id);

                        $logArr = MyLibrary::logData($clientCategory->id);
                        if (Auth::user()->can('log-advanced')) {
                            $newClientCategoryObj = ClientCategory::getRecordForLogById($clientCategory->id);
                            $oldRec             = $this->recordHistory($clientCategory);
                            $newRec             = $this->recordHistory($newClientCategoryObj);
                            $logArr['old_val']  = $oldRec;
                            $logArr['new_val']  = $newRec;
                        }

                        $logArr['varTitle'] = trim($data['title']);
                        Log::recordLog($logArr);
                        if (Auth::user()->can('recent-updates-list')) {
                            if (!isset($newClientCategoryObj)) {
                                $newClientCategoryObj = ClientCategory::getRecordForLogById($clientCategory->id);
                            }
                            $notificationArr = MyLibrary::notificationData($clientCategory->id, $newClientCategoryObj);
                            RecentUpdates::setNotification($notificationArr);
                        }
                    }

                    Self::flushCache();
                    $actionMessage = trans('template.clientCategoryModule.updateMessage');

                }

            } else {

                $clientCategoryArr                        = [];
                $clientCategoryArr['intAliasId']          = MyLibrary::insertAlias($data['alias']);
                $clientCategoryArr['varTitle']            = trim($data['title']);
                $clientCategoryArr['intParentCategoryId'] = isset($data['parent_category_id']) ? $data['parent_category_id'] : 1;
                $clientCategoryArr['intDisplayOrder']     = self::swap_order_add($data['display_order']);
                $clientCategoryArr['chrPublish']          = $data['chrMenuDisplay'];
                $clientCategoryArr['txtDescription']      = $data['description'];
                $clientCategoryArr['txtShortDescription'] = $data['short_description'];
                $clientCategoryArr['varMetaTitle']        = $data['varMetaTitle'];
                $clientCategoryArr['varMetaKeyword']      = $data['varMetaKeyword'];
                $clientCategoryArr['varMetaDescription']  = $data['varMetaDescription'];
                $clientCategoryArr['created_at']          = Carbon::now();

                $clientCategoryID = CommonModel::addRecord($clientCategoryArr);

                if (!empty($clientCategoryID)) {
                    $id                 = $clientCategoryID;
                    $newClientCategoryObj = ClientCategory::getRecordForLogById($id);
                    $logArr             = MyLibrary::logData($id);
                    $logArr['varTitle'] = $newClientCategoryObj->varTitle;
                    Log::recordLog($logArr);

                    if (Auth::user()->can('recent-updates-list')) {
                        $notificationArr = MyLibrary::notificationData($id, $newClientCategoryObj);
                        RecentUpdates::setNotification($notificationArr);
                    }

                    $actionMessage = trans('template.clientCategoryModule.addedMessage');
                }

                Self::flushCache();
            }
            if (!empty($data['saveandexit']) && $data['saveandexit'] == 'saveandexit') {
                return redirect()->route('powerpanel.client-category.index')->with('message', $actionMessage);
            } else {
                return redirect()->route('powerpanel.client-category.edit', $id)->with('message', $actionMessage);
            }

        } else {
            return Redirect::back()->withErrors($validator)->withInput();
        }

    }
    /**
     * This method loads client_category table data on view
     * @return  View
     * @since   2017-01-31
     * @author  NetQuick
     */

    public function get_list()
    {
        $filterArr                  = [];
        $records                    = [];
        $records["data"]            = [];
        $filterArr['orderColumnNo'] = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');

        $filterArr['orderByFieldName'] = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');

        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['statusFilter']       = !empty(Request::get('customActionName')) ? Request::get('customActionName') : '';

        $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';

        $filterArr['iDisplayLength'] = intval(Request::get('length'));
        $filterArr['iDisplayStart']  = intval(Request::get('start'));

        $sEcho      = intval(Request::get('draw'));
        $arrResults = ClientCategory::getRecordList($filterArr);

        $iTotalRecords                      = CommonModel::getRecordCount($filterArr, true);
        $totalRecords                       = CommonModel::getTotalRecordCount();
        
        if (!empty($arrResults)) {
            foreach ($arrResults as $key => $value) {
                $records["data"][] = $this->tableData($value,$totalRecords);
            }

        }
        $records["customActionStatus"] = "OK";
        $records["draw"]               = $sEcho;
        $records["recordsTotal"]       = $iTotalRecords;
        $records["recordsFiltered"]    = $iTotalRecords;

        return json_encode($records);
    }

    public function publish(Request $request)
    {
        $alias  = Request::get('alias');
        $val  = Request::get('val');
        $update = MyLibrary::setPublishUnpublish($alias, $val);
        Self::flushCache();
        echo json_encode($update);
        exit;
    }

    /**
     * This method reorders banner position
     * @return  Banner index view data
     * @since   2016-10-26
     * @author  NetQuick
     */
    public function reorder()
    {
        $order   = Request::get('order');
        $exOrder = Request::get('exOrder');
        MyLibrary::swapOrder($order, $exOrder);
        Self::flushCache();
    }

    /**
     * This method delete multiples Team
     * @return  true/false
     * @since   2017-07-22
     * @author  NetQuick
     */
    public function DeleteRecord(Request $request)
    {
        $data   = Request::all('ids');
        $update = MyLibrary::deleteMultipleRecords($data);
        Self::flushCache();
        echo json_encode($update);
        exit;
    }

    /**
     * This method handels swapping of available order record while adding
     * @param   order
     * @return  order
     * @since   2017-07-24
     * @author  NetQuick
     */
    public static function swap_order_add($order = null)
    {
        $response = false;
        if ($order != null) {
            $response = MyLibrary::swapOrderAdd($order);
            Self::flushCache();
        }
        return $response;
    }

    /**
     * This method handels swapping of available order record while editing
     * @param      order
     * @return  order
     * @since   2017-07-22
     * @author  NetQuick
     */
    public static function swap_order_edit($order = null, $id = null)
    {
        MyLibrary::swapOrderEdit($order, $id);
        Self::flushCache();
    }

    /**
     * This method handels logs History records
     * @param   $data
     * @return  HTML
     * @since   2017-07-21
     * @author  NetQuick
     */

    public function recordHistory($data = false)
    {
        $returnHtml = '';
        $returnHtml .= '<table class="new_table_desing table table-striped table-bordered table-hover">
																					<thead>
																							<tr>
																								<th>' . trans("template.common.title") . '</th>
																								<th>' . trans("template.common.parentCategory") . '</th>
																								<th>' . trans("template.common.shortDescription") . '</th>
																								<th>' . trans("template.common.description") . '</th>
																								<th>' . trans("template.common.displayorder") . '</th>
																								<th>' . trans("template.common.metatitle") . '</th>
																								<th>' . trans("template.common.metakeyword") . '</th>
																								<th>' . trans("template.common.metadescription") . '</th>
																								<th>' . trans("template.common.publish") . '</th>
																							</tr>
																					</thead>
																					<tbody>
																							<tr>
																									<td>' . $data->varTitle . '</td>';
        if ($data->intParentCategoryId > 0) {
            $catIDS[]       = $data->intParentCategoryId;
            $parentCateName = ClientCategory::getParentCategoryNameBycatId($catIDS);
            $parentCateName = $parentCateName[0]->varTitle;
            $returnHtml .= '<td>' . $parentCateName . '</td>';
        } else {
            $returnHtml .= '<td>-</td>';
        }

        $returnHtml .= '<td>' . $data->txtShortDescription . '</td>
																									<td>' . $data->txtDescription . '</td>
																									<td>' . ($data->intDisplayOrder) . '</td>
																									<td>' . $data->varMetaTitle . '</td>
																									<td>' . $data->varMetaKeyword . '</td>
																									<td>' . $data->varMetaDescription . '</td>
																									<td>' . $data->chrPublish . '</td>
																							</tr>
																					</tbody>
																			</table>';
        return $returnHtml;

    }

    public function tableData($value = false,$totalRecords)
    {
        $hasRecords           = Client::getCountById($value->id);
        $isParent             = ClientCategory::getCountById($value->id);
        $details              = '';
        $parent_category_name = ' ';
        $publish_action       = '';
        $titleData            = "";
        $details              = '<a class="without_bg_icon" href="' . url('powerpanel/client/add?category=' . $value->id) . '" title="' . trans("template.clientCategoryModule.addClient") . '"><i class="icon-notebook"></i></a>';
        if (Auth::user()->can('client-category-edit')) {
            $details .= '<a class="without_bg_icon" title="' . trans("template.common.edit") . '" href="' . route('powerpanel.client-category.edit', array('alias' => $value->id)) . '"><i class="fa fa-pencil"></i></a>';
        }
        if (Auth::user()->can('client-category-delete') && $hasRecords == 0 && $isParent == 0) {
            $details .= '&nbsp;<a class="without_bg_icon delete" title="' . trans("template.common.delete") . '" data-controller="client-category" data-alias = "' . $value->id . '"><i class="fa fa-times"></i></a>';
        }

        if (Auth::user()->can('client-category-publish')) {
            if ($hasRecords == 0 && $isParent == 0) {
                if ($value->chrPublish == 'Y') {
                    $publish_action .= '<input data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/client-category" title="' . trans("template.common.publishedRecord") . '" data-value="Unpublish" data-alias="' . $value->id . '">';
                } else {
                    $publish_action .= '<input checked="" data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/client-category" title="' . trans("template.common.unpublishedRecord") . '" data-value="Publish" data-alias="' . $value->id . '">';
                }
            } else {
                $publish_action = '-';
            }
        }

        if ($hasRecords > 0) {
            $titleData = 'This category is selected in ' . $hasRecords . ' record(s) so it can&#39;t be deleted or unpublished.';
        }

        if ($isParent > 0) {
            $titleData = 'This category is selected as Parent Category in ' . $isParent . ' record(s) so it can&#39;t be deleted or unpublished.';
        }
        $checkbox = '<a href="javascript:;" data-toggle="tooltip" data-placement="right" data-toggle="tooltip" data-original-title="' . $titleData . '" title="' . $titleData . '"><i style="color:red" class="fa fa-exclamation-triangle"></i></a>';

        $parentCategoryTitle = '-';
        if (!empty($value->intParentCategoryId) && $value->intParentCategoryId > 0) {
            $catIDS[]            = $value->intParentCategoryId;
            $parentCategoryName  = ClientCategory::getParentCategoryNameBycatId($catIDS);
            $parentCategoryTitle = $parentCategoryName[0]->varTitle;
        }

        $orderArrow = '';   
        $orderArrow .= '<span class="pageorderlink">'; 
        if($totalRecords != $value->intDisplayOrder) {
          $orderArrow .= '<a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveUp"><i class="fa fa-plus" aria-hidden="true"></i></a> ';
        }
        $orderArrow .= $value->intDisplayOrder.' ';
        if($value->intDisplayOrder != 1) {
          $orderArrow .= ' <a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveDwn"><i class="fa fa-minus" aria-hidden="true"></i></a>';
        } 
        $orderArrow .= '</span>'; 

        $records = array(
            ($hasRecords == 0 && $isParent == 0) ? '<input type="checkbox" name="delete" class="chkDelete" value="' . $value->id . '">' : $checkbox,
            '<a class="without_bg_icon" title="Edit" href="' . route('powerpanel.client-category.edit', array('alias' => $value->id)) . '">' . $value->varTitle . '</a>',
            '<a href="javascript:void(0)" class="without_bg_icon" onclick="return hs.htmlExpand(this,{width:300,headingText:\'' . trans("template.common.shortdescription") . '\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>
					<div class="highslide-maincontent">' . nl2br($value->txtShortDescription) . '</div>
				</div>',
            $parentCategoryTitle,
            ($hasRecords > 0) ? '<a href="' . url('powerpanel/client?category=' . $value->id) . '">' . trans("template.common.view") . '(' . $hasRecords . ')</a>' : '-',
            $orderArrow,
            $publish_action,
            $details,
            $value->intDisplayOrder,
        );
        return $records;
    }

    public function addCatAjax()
    {
        $data = Request::all();
        return AddCategoryAjax::Add($data, 'ClientCategory');
    }

    public static function flushCache()
    {
        Cache::tags(['ClientCategory', 'Client'])->flush();
    }
}
