<?php
namespace App\Http\Controllers\Powerpanel;

use App\CommonModel;
use App\ContactLead;
use App\RequestLead;
use App\Currency;
use App\EmailLog;
use App\GeneralSettings;
use App\Helpers\Email_sender;
use App\Helpers\MyLibrary;
use App\Http\Controllers\PowerpanelController;
use App\Modules;
use App\ModuleSettings;
use App\NewsletterLead;
use App\Pagehit;
use App\Timezone;
use Artisan;
use Cache;
use Config;
use File;
use Illuminate\Support\Facades\Redirect;
use Request;
use Session;
use Validator;

class SettingsController extends PowerpanelController
{
    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }
    public function index()
    {
        /********* PHP INI FILE CONTENT **********/
        $phpIniPath = public_path() . '/.user.ini';
        $phpIniFileExist = $this->filePathExist($phpIniPath);
        if (!$phpIniFileExist) {
            $phpIniContent = '';
            File::put($phpIniPath, $phpIniContent);
        }
        $phpIniContent = File::get($phpIniPath);
        /********* End of PHP INI FILE CONTENT **********/
        /********* Robot FILE CONTENT **********/
        $robotFilepath = public_path() . '/robots.txt';
        $robotFileExist = $this->filePathExist($robotFilepath);
        if (!$robotFileExist) {
            $robotFileContent = '';
            File::put($robotFilepath, $robotFileContent);
        }
        $robotFileContent = File::get($robotFilepath);
        /*********End of Robot FILE CONTENT **********/
        $BingSiteAuthFilePath = public_path() . '/BingSiteAuth.xml';
        $bingFileName = 'BingSiteAuth.xml';
        $bingFileExist = $this->filePathExist($BingSiteAuthFilePath);
        if (!$bingFileExist) {
            GeneralSettings::checkByFieldName('BING_FILE_PATH')->update(['fieldValue' => null]);
            Config::set('Constant.BING_FILE_PATH', null);
        }

        if (!empty(Session::get('tab'))) {
            $tabSessionValue = Session::get('tab');
        } else {
            $tabSessionValue = '';
        }

        $timezone = Timezone::orderBy('zone_name', 'ASC')->get();
        $currencyList = Currency::getCurrency();

        $this->breadcrumb['title'] = trans('template.header.settings');

        return view('powerpanel.general_setting.settings', [
            'tab_value' => $tabSessionValue,
            'timezone' => $timezone,
            'currencyList' => $currencyList,
            'breadcrumb' => $this->breadcrumb,
            'phpIniContent' => $phpIniContent,
            'imageManager' => true,
            'robotFileContent' => $robotFileContent,
        ]);
    }
    public static function testMail()
    {
        Email_sender::testMail();
        echo '<div class="alert alert-info">Test email has been sent in your login email</div>';
    }
    public static function update_settings()
    {

        $data = Request::all();
        $BingSiteAuthFilePath = public_path() . '/BingSiteAuth.xml';
        $bingFileName = 'BingSiteAuth.xml';
        $BingfileError = false;
        if (Request::file('xml_file')) {
            $file = Request::file('xml_file');
            $pathinfo = pathinfo($file->getClientOriginalName());
            $uploadedFileExtention = $pathinfo['extension'];

            if ($uploadedFileExtention != 'xml') {
                $BingfileError = true;
            }
            if ($BingfileError == false) {
                if (self::filePathExist($BingSiteAuthFilePath)) {
                    unlink($BingSiteAuthFilePath);
                }
                $file->move(public_path(), $bingFileName);
            }
        }

        $phpIniPath = public_path() . '/.user.ini';
        $robotFilepath = public_path() . '/robots.txt';
        Session::forget('tab');
        Session::put('tab', Request::get('tab'));
        $tab_val = Request::get('tab');

        $message = array();
        $rules = array();

        switch ($tab_val) {
            case 'general_settings':
                $message = array(
                    'front_logo_id.required' => 'The front logo field is required.',
                );
                $rules = array(
                    'site_name' => 'required|max:160',
                    'front_logo_id' => 'required',
                );
                break;
            case 'banner_settings':
                $message = array(
                    'banner_id' => 'The Default Banner Image is required.',
                );
                $rules = array(
                    'mail_api_url' => 'max:160',
                );
            break;
            case 'smtp_settings':
                if(Request::get('mailer') == "smtp" || Request::get('mailer') == "log"){
                    $rules = array(
                        'mailer' => 'required',
                        'smtp_server' => 'required',
                        'smtp_username' => 'required',
                        'smtp_password' => 'required',
                        'smtp_encryption' => 'required',
                        'smtp_authenticattion' => 'required',
                        'smtp_port' => 'required',
                        'smtp_sender_name' => 'required',
                        'smtp_sender_id' => 'required'
                    );
                    $message = array(
                        'mailer' => 'Mailer is required',
                        'smtp_server' => 'Host is required',
                        'smtp_username' => 'Username is required',
                        'smtp_password' => 'Password is required',
                        'smtp_encryption' => 'Encryption is required',
                        'smtp_authenticattion' => 'Authentication is required',
                        'smtp_port' => 'Port is required',
                        'smtp_sender_name' => 'Sender Name is required',
                        'smtp_sender_id' => 'Sender email is required',
                    );
                }else{
                    $rules = array(
                        'mailer' => 'required',
                        'mail_api_url' => 'required',
                        'mail_api_username' => 'required',
                        'mail_api_pwd' => 'required',
                        'mail_api_token' => 'required',
                        'mail_api_site_id' => 'required'
                    );
                    $message = array(
                        'mailer' => 'Mailer is required',
                        'mail_api_url' => 'API URL is required',
                        'mail_api_username' => 'Username is required',
                        'mail_api_pwd' => 'Password is required',
                        'mail_api_token' => 'Token is required',
                        'mail_api_site_id' => 'Site ID is required'
                    );
                }
            break;
            case 'seo_settings':
                $rules = array(
                    'meta_title' => 'required',
                    'meta_description' => 'required',
                );
                $message = array();
                if ($BingfileError) {
                    $rules['xml_file'] = 'required';
                    $message = array(
                        'xml_file.required' => 'Please upload only xml file',
                    );
                }
                break;
            case 'currency_settings':
                $rules = array(
                    'currency' => 'required',
                );
                $message = array(
                    'currency.required' => 'Please choose currency',
                );
                break;
            case 'social_settings':
                $rules = array();
                $message = array();
                break;
            case 'social_media_share':
                $rules = array(
                    'fb_id' => 'required',
                    'fb_api' => 'required',
                    'fb_secret_key' => 'required',
                    'fb_access_token' => 'required',
                    'twitter_api' => 'required',
                    'twitter_secret_key' => 'required',
                    'twitter_access_token' => 'required',
                    'twitter_access_token_key' => 'required',
                    'linkedin_api' => 'required',
                    'linkedin_secret_key' => 'required',
                    'linkedin_access_token' => 'required',
                    'linkedin_access_token_key' => 'required',
                );
                break;
            case 'other_settings':
                $rules = array(
                    'google_capcha_key' => 'required',
                    'google_map_key' => 'required',
                    'php_ini_content' => 'handle_xss',
                );
                $message = array(
                    'php_ini_content.handle_xss' => 'Enter valid input.',
                );
                break;
            case 'maintenance':
                $message = array(
                    'reset.required' => 'Please select an option to reset.',
                );
                $rules = array(
                    'reset' => 'required',
                );
                break;

        }
        $validator = Validator::make($data, $rules, $message);
        if ($validator->passes()) {
            switch ($tab_val) {
                case 'general_settings':
                    $arrGeneralSettings = array(
                        'SITE_NAME' => trim(Request::get('site_name')),
                        'FRONT_LOGO_ID' => Request::get('front_logo_id'),
                    );
                    break;
                case 'email_settings':
                    $arrGeneralSettings = array(
                        'DEFAULT_NEWSLETTER_EMAIL' => MyLibrary::getEncryptedString(Request::get('default_newsletter_email')),
                        'DEFAULT_REPLYTO_EMAIL' => MyLibrary::getEncryptedString(trim(Request::get('default_replyto_email'))),
                        'DEFAULT_ADMIN_EMAIL' => MyLibrary::getEncryptedString(trim(Request::get('default_admin_email'))),
                        'DEFAULT_CONTACT_EMAIL' => MyLibrary::getEncryptedString(trim(Request::get('default_contact_email'))),
                        'DEFAULT_REQUEST_EMAIL' => MyLibrary::getEncryptedString(trim(Request::get('default_request_email'))),
                        'DEFAULT_CC_EMAIL' => MyLibrary::getEncryptedString(trim(Request::get('default_cc_email'))),
                        'DEFAULT_BCC_EMAIL' => MyLibrary::getEncryptedString(trim(Request::get('default_bcc_email'))),
                    );
                    break;
                case 'banner_settings':
                    $arrGeneralSettings = array(
                        'DEFAULT_BANNER_TITLE' => Request::get('DEFAULT_BANNER_TITLE'),
                        'DEFAULT_BANNER_SUBTITLE' => Request::get('DEFAULT_BANNER_SUBTITLE'),
                        'DEFAULT_HOME_BANNER' => Request::get('default_home_banner'),
                        'DEFAULT_INNER_BANNER' => Request::get('default_inner_banner'),
                    );
                    break;
                case 'smtp_settings':
                    $arrGeneralSettings = array(
                        'MAILER' => Request::get('mailer'),
                        'MAIL_API_URL' => trim(Request::get('mail_api_url')),
                        'MAIL_API_USERNAME' => trim(Request::get('mail_api_username')),
                        'MAIL_API_PASSWORD' => trim(Request::get('mail_api_pwd')),
                        'MAIL_API_TOKEN' => trim(Request::get('mail_api_token')),
                        'MAIL_API_SITE_ID' => trim(Request::get('mail_api_site_id')),
                        'SMTP_SERVER' => trim(Request::get('smtp_server')),
                        'SMTP_USERNAME' => trim(Request::get('smtp_username')),
                        'SMTP_PASSWORD' => Request::get('smtp_password'),
                        'SMTP_ENCRYPTION' => Request::get('smtp_encryption'),
                        'SMTP_AUTHENTICATION' => Request::get('smtp_authenticattion'),
                        'SMTP_PORT' => trim(Request::get('smtp_port')),
                        'SMTP_SENDER_NAME' => trim(Request::get('smtp_sender_name')),
                        'SMTP_SENDER_EMAIL' => MyLibrary::getEncryptedString(trim(Request::get('smtp_sender_id')))
                    );
                break;
                case 'seo_settings':
                    $arrGeneralSettings = array(
                        'GOOGLE_ANALYTIC_CODE' => Request::get('google_analytic_code'),
                        'GOOGLE_TAG_MANAGER_FOR_BODY' => Request::get('google_tag_manager_for_body'),
                        'DEFAULT_META_TITLE' => trim(Request::get('meta_title')),
                        'DEFAULT_META_DESCRIPTION' => Request::get('meta_description'),
                        'META_TAG' => trim(Request::get('meta_tag')),
                        'ROBOT_TXT_CONTENT' => Request::get('robotfile_content'),
                        'BING_FILE_PATH' => $bingFileName,
                    );
                    break;
                case 'currency_settings':
                    $arrGeneralSettings = array(
                        'CURRENCY' => Request::get('currency'),
                    );
                    break;
                case 'social_settings':
                    $arrGeneralSettings = array(
                        'SOCIAL_FB_LINK' => trim(Request::get('fb_link')),
                        'SOCIAL_TWITTER_LINK' => trim(Request::get('twitter_link')),
                        'SOCIAL_YOUTUBE_LINK' => trim(Request::get('youtube_link')),
                        'SOCIAL_LINKEDIN_LINK' => trim(Request::get('linkedin_link')),
                        'SOCIAL_INSTAGRAM_LINK' => trim(Request::get('instagram_link')),
                        'SOCIAL_TUMBLR_LINK' => trim(Request::get('tumblr_link')),
                        'SOCIAL_PINTEREST_LINK' => trim(Request::get('pinterest_link')),
                        'SOCIAL_FLICKR_LINK' => trim(Request::get('flickr_link')),
                        'SOCIAL_DRIBBBLE_LINK' => trim(Request::get('dribbble_link')),
                        'SOCIAL_RSS_FEED_LINK' => trim(Request::get('rss_feed_link')),
                    );
                    break;
                case 'social_media_share':
                    $arrGeneralSettings = array(
                        'SOCIAL_SHARE_FB_ID' => Request::get('fb_id'),
                        'SOCIAL_SHARE_FB_API_KEY' => trim(Request::get('fb_api')),
                        'SOCIAL_SHARE_FB_SECRET_KEY' => trim(Request::get('fb_secret_key')),
                        'SOCIAL_SHARE_FB_ACCESS_TOKEN' => trim(Request::get('fb_access_token')),
                        'SOCIAL_SHARE_TWITTER_API_KEY' => trim(Request::get('twitter_api')),
                        'SOCIAL_SHARE_TWITTER_SECRET_KEY' => trim(Request::get('twitter_secret_key')),
                        'SOCIAL_SHARE_TWITTER_ACCESS_TOKEN' => trim(Request::get('twitter_access_token')),
                        'SOCIAL_SHARE_TWITTER_ACCESS_SECRET_KEY' => trim(Request::get('twitter_access_token_key')),
                        'SOCIAL_SHARE_LINKEDIN_API_KEY' => trim(Request::get('linkedin_api')),
                        'SOCIAL_SHARE_LINKEDIN_SECRET_KEY' => trim(Request::get('linkedin_secret_key')),
                        'SOCIAL_SHARE_LINKEDIN_ACCESS_TOKEN' => trim(Request::get('linkedin_access_token')),
                        'SOCIAL_SHARE_LINKEDIN_ACCESS_SECRET_KEY' => trim(Request::get('linkedin_access_token_key')),
                    );
                    break;
                case 'other_settings':
                    $available_social_links_for_team = array();
                    $available_social_links_for_team_data = Request::get('available_social_links_for_team');
                    $i = 0;
                    if (is_array($available_social_links_for_team_data)) {
                        foreach ($available_social_links_for_team_data as $key => $value) {
                            if ($value['title'] != "") {
                                $value['key'] = 'social_link_' . $i;
                                $available_social_links_for_team[$i] = $value;
                                $i++;
                            }
                        }
                    }

                    $available_social_links_for_team = serialize($available_social_links_for_team);
                    $arrGeneralSettings = array(
                        'DEFAULT_PAGE_SIZE' => 9,
                        'DEFAULT_TIME_ZONE' => Request::get('timezone'),
                        'DEFAULT_DATE_FORMAT' => Request::get('default_date_format'),
                        'DEFAULT_TIME_FORMAT' => Request::get('time_format'),
                        'GOOGLE_MAP_KEY' => trim(Request::get('google_map_key')),
                        'GOOGLE_CAPCHA_KEY' => trim(Request::get('google_capcha_key')),
                        'BAD_WORDS' => Request::get('bad_words'),
                        'PHP_INI_CONTENT' => Request::get('php_ini_content'),
                        'AVAILABLE_SOCIAL_LINKS_FOR_TEAM_MEMBER' => $available_social_links_for_team,
                    );
                    break;
                case 'maintenance':
                    foreach (Request::get('reset') as $key => $value) {
                        switch ($value) {
                            case "moblihits":
                                Pagehit::where('isWeb', '=', 'N')->delete();
                                break;
                            case "webhits":
                                Pagehit::where('isWeb', '=', 'Y')->delete();
                                Cache::forget('checkPageHits');
                                Cache::forget('checkInnerPageHits');
                                break;
                            case "contactleads":
                                ContactLead::truncate();
                                break;
                            case "requestleads":
                                RequestLead::truncate();
                                break;
                            case "newsletterleads":
                                NewsletterLead::truncate();
                                break;
                            case "emaillog":
                                EmailLog::truncate();
                                break;
                            case "flushAllCache":
                                Cache::flush();
                                break;
                        }
                    }
                    break;
            }
            if ($tab_val != 'maintenance') {
                foreach ($arrGeneralSettings as $key => $value) {
                    if ($key != 'PHP_INI_CONTENT' || $key != 'ROBOT_TXT_CONTENT') {
                        GeneralSettings::checkByFieldName($key)->update(['fieldValue' => $value]);
                    }
                    if ($key == 'PHP_INI_CONTENT') {
                        $fileexist = self::filePathExist($phpIniPath);
                        if ($fileexist) {
                            $phpIniContent = $value;
                            File::put($phpIniPath, $phpIniContent);
                        }
                    }
                    if ($key == 'ROBOT_TXT_CONTENT') {
                        $fileexist = self::filePathExist($robotFilepath);
                        if ($fileexist) {
                            $robotFileContent = $value;
                            File::put($robotFilepath, $robotFileContent);
                        }
                    }
                }
            }
            self::flushCache();
        } else {
            return Redirect::route('powerpanel/settings')->withErrors($validator)->withInput();
        }
        if ($tab_val == 'maintenance') {
            return Redirect::route('powerpanel/settings')->with('message', 'The data has been successfully reset.');
        } else {
            return Redirect::route('powerpanel/settings')->with('message', 'The record has been successfully edited and saved.	');
        }
    }
    public static function flushCache()
    {
        Cache::tags('genralSettings')->flush();
    }

    public function getDBbackUp()
    {
        $message = trans('template.common.oppsSomethingWrong');
        Artisan::call('backup:run');
        Session::put('tab', 'maintenance');
        $filename = base_path('storage/laravel-backups/temp/' . env('DB_DATABASE') . '.sql');
        $bytes = File::size($filename);
        if ($bytes > 0 && self::filePathExist($filename)) {
            $message = 'Database has been backed up!';
            GeneralSettings::deleteLogs();
        }
        return Redirect::route('powerpanel/settings')->with('message', $message);
    }

    public static function filePathExist($filepath = false)
    {
        $response = false;
        if (file_exists($filepath)) {
            $response = true;
        }
        return $response;
    }

    public static function saveModuleSettings()
    {
        $data = Request::all();
        $id = $data['moduleId'];
        unset($data['_token']);
        $settings = json_encode($data);
        $exists = ModuleSettings::getSettings($id);
        $settings = ['intModuleId' => $id, 'txtSettings' => $settings, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')];
        if (empty($exists)) {
            CommonModel::addRecord($settings, '\\App\\ModuleSettings');
        } else {
            $whereCondArr = ['intModuleId' => $id];
            CommonModel::updateRecords($whereCondArr, $settings, false, '\\App\\ModuleSettings');
        }
        echo '<div class="alert alert-success">' . ucwords($data['moduleName']) . ' Settings saved successfully</div>';
    }

    public function getModuleSettings()
    {
        $response = false;
        $data = Request::all();
        $id = $data['moduleId'];
        Session::put('moduleSettting', $id);
        $response = ModuleSettings::getSettings($id);
        $response = isset($response->txtSettings) ? $response->txtSettings : null;
        if (!empty($response)) {
            $response = $response;
        }
        return $response;
    }

    public function getModulesAjax()
    {
        $term = Request::get('term');
        Session::put('tab', 'module');
        $modules = Modules::getModuleListForSettings($term);
        if (null == Session::get('moduleSettting')) {
            Session::put('moduleSettting', $modules[0]->id);
        }
        return view('powerpanel.partials.modulesettingtabs', ['modules' => $modules])->render();
    }
}
