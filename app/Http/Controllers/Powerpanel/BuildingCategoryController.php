<?php
/**
 * The MenuController class handels building_category
 * configuration  process.
 * @package   Netquick powerpanel
 * @license   http://www.opensource.org/licenses/BSD-3-Clause
 * @version   1.00
 * @since     2017-01-31
 * @author    NetQuick
 */
namespace App\Http\Controllers\Powerpanel;

use App\Alias;
use App\BuildingCategory;
use App\BuildingHome;
use App\CommonModel;
use App\Helpers\AddImageModelRel;
use App\Helpers\AddCategoryAjax;
use App\Helpers\Category_builder;
use App\Helpers\MyLibrary;
use App\Helpers\resize_image;
use App\Http\Controllers\PowerpanelController;
use App\Log;
use App\RecentUpdates;
use Auth;
use Cache;
use Carbon\Carbon;
use Config;
use Request;

use Illuminate\Support\Facades\Redirect;
use Validator;

class BuildingCategoryController extends PowerpanelController
{

    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }
    /**
     * This method handels load process of building_category
     * @return  view
     * @since   2017-01-31
     * @author  NetQuick
     */
    public function index()
    {
        $iTotalRecords             = CommonModel::getRecordCount();
        $this->breadcrumb['title'] = trans('template.buildingCategoryModule.manageBuildingCategory');
        $breadcrumb                = $this->breadcrumb;
        return view('powerpanel.building_category.index', compact('iTotalRecords', 'breadcrumb'));
    }

    /**
     * This method loads building_category edit view
     * @param   Alias of record
     * @return  View
     * @since   2017-01-31
     * @author  NetQuick
     */
    public function edit($alias = false)
    {
        $imageManager = true;
        $isParent = 0;
        if (!is_numeric($alias)) 
        {
            
            $total                           = CommonModel::getRecordCount();
            $total                           = $total + 1;
            $this->breadcrumb['title']       = trans('template.buildingCategoryModule.addBuildingCategory');
            $this->breadcrumb['inner_title'] = trans('template.buildingCategoryModule.addBuildingCategory');
            $this->breadcrumb['module']      = trans('template.buildingCategoryModule.manageBuildingCategory');
            $this->breadcrumb['url']         = 'powerpanel/building-category';
            $breadcrumb                      = $this->breadcrumb;
            $hasRecords                      = 0;
            $metaInfo                        = array('varMetaTitle' => '', 'varMetaKeyword' => '', 'varMetaDescription' => '');
            $categories                      = Category_builder::Parentcategoryhierarchy('intParentCategoryId', '', '', 'listbox', 'building_category');
            $data                            = compact('total', 'breadcrumb', 'categories', 'hasRecords', 'isParent','imageManager');

        } else {
              $id           = $alias;
              $buildingCategory = BuildingCategory::getRecordById($id);
              $total                           = CommonModel::getRecordCount();
               //echo "<pre>"; print_r($buildingCategory); exit;             
              if (empty($buildingCategory)) {
                return redirect()->route('powerpanel.building-category.add');
              }

              $this->breadcrumb['title']       = trans('template.common.edit') . ' - ' . $buildingCategory->varTitle;
              $this->breadcrumb['inner_title'] = trans('template.common.edit') . ' - ' . $buildingCategory->varTitle;
              $this->breadcrumb['module']      = trans('template.buildingCategoryModule.manageBuildingCategory');
              $this->breadcrumb['url']         = 'powerpanel/building-category';
              $breadcrumb                      = $this->breadcrumb;
              $hasRecords                      = BuildingHome::getCountById($buildingCategory->id);
              $isParent                        = BuildingCategory::getCountById($buildingCategory->id);
              $metaInfo                        = array('varMetaTitle' => $buildingCategory->varMetaTitle, 'varMetaKeyword' => $buildingCategory->varMetaKeyword, 'varMetaDescription' => $buildingCategory->varMetaDescription);
              $categories                      = Category_builder::Parentcategoryhierarchy($buildingCategory->intParentCategoryId, $id);
              $alias                           = $buildingCategory->alias->varAlias;
              $data                            = compact('total','metaInfo', 'buildingCategory', 'hasRecords', 'isParent', 'alias', 'breadcrumb', 'categories','imageManager');
        }
        return view('powerpanel.building_category.actions', $data);
    }
    /**
     * This method stores building_category modifications
     * @return  View
     * @since   2017-01-31
     * @author  NetQuick
     */

    public function handlePost(Request $request)
    {
        $data     = Request::all();
        $settings = json_decode(Config::get("Constant.MODULE.SETTINGS"));
        $rules    = array(
            'title'             => 'required|max:160',
            'display_order'     => 'required|greater_than_zero',
            'alias'             => 'required',
            'short_description' => 'required|max:' . (isset($settings) ? $settings->short_desc_length : 400)
        );
        $messsages = array(
            'display_order.required'          => trans('template.buildingCategoryModule.displayOrder'),
            'display_order.greater_than_zero' => trans('template.buildingCategoryModule.displayGreaterThan')
        );
        $data['short_description'] = trim(preg_replace('/\s\s+/', ' ', $data['short_description']));
        $validator                 = Validator::make($data, $rules, $messsages);
        if ($validator->passes()) {
            $id            = Request::segment(3);
            $actionMessage = trans('template.common.oppsSomethingWrong');
            if (is_numeric($id)) {
                #Edit post Handler=======

                if ($data['oldAlias'] != $data['alias']) {
                    Alias::updateAlias($data['oldAlias'], $data['alias']);
                }
                $BuildingCategory             = BuildingCategory::getRecordForLogById($id);
                $updateBuildingCategoryFields = [
                    'varTitle'            => trim($data['title']),
                    'fkIntImgId'          => !empty($data['img_id']) ? $data['img_id'] : null,                    
                    'intParentCategoryId' => isset($data['parent_category_id']) ? $data['parent_category_id'] : 0,
                    'chrPublish'          => isset($data['chrMenuDisplay']) ? $data['chrMenuDisplay'] : 'Y',
                    'txtDescription'      => $data['description'],
                    'txtShortDescription' => $data['short_description'],
                    'varMetaTitle'        => $data['varMetaTitle'],
                    'varMetaKeyword'      => $data['varMetaKeyword'],
                    'varMetaDescription'  => $data['varMetaDescription'],
                ];

                $whereConditions = ['id' => $BuildingCategory->id];
                $update          = CommonModel::updateRecords($whereConditions, $updateBuildingCategoryFields);
                if ($update) {
                    if (!empty($id)) {
                        self::swap_order_edit($data['display_order'], $BuildingCategory->id);

                        $logArr = MyLibrary::logData($BuildingCategory->id);
                        if (Auth::user()->can('log-advanced')) {
                            $newBuildingCategoryObj = BuildingCategory::getRecordForLogById($BuildingCategory->id);
                            $oldRec             = $this->recordHistory($BuildingCategory);
                            $newRec             = $this->recordHistory($newBuildingCategoryObj);
                            $logArr['old_val']  = $oldRec;
                            $logArr['new_val']  = $newRec;
                        }

                        $logArr['varTitle'] = trim($data['title']);
                        Log::recordLog($logArr);
                        if (Auth::user()->can('recent-updates-list')) {
                            if (!isset($newBuildingCategoryObj)) {
                                $newBuildingCategoryObj = BuildingCategory::getRecordForLogById($BuildingCategory->id);
                            }
                            $notificationArr = MyLibrary::notificationData($BuildingCategory->id, $newBuildingCategoryObj);
                            RecentUpdates::setNotification($notificationArr);
                        }
                    }

                    Self::flushCache();
                    $actionMessage = trans('template.buildingCategoryModule.updateMessage');

                }

            } else {

                $BuildingCategoryArr                        = [];
                $BuildingCategoryArr['intAliasId']          = MyLibrary::insertAlias($data['alias']);
                $BuildingCategoryArr['fkIntImgId']          = !empty($data['img_id']) ? $data['img_id'] : null;
                $BuildingCategoryArr['varTitle']            = trim($data['title']);
                $BuildingCategoryArr['intParentCategoryId'] = isset($data['parent_category_id']) ? $data['parent_category_id'] : 0;
                $BuildingCategoryArr['intDisplayOrder']     = self::swap_order_add($data['display_order']);
                $BuildingCategoryArr['chrPublish']          = $data['chrMenuDisplay'];
                $BuildingCategoryArr['txtDescription']      = $data['description'];
                $BuildingCategoryArr['txtShortDescription'] = $data['short_description'];
                $BuildingCategoryArr['varMetaTitle']        = $data['varMetaTitle'];
                $BuildingCategoryArr['varMetaKeyword']      = $data['varMetaKeyword'];
                $BuildingCategoryArr['varMetaDescription']  = $data['varMetaDescription'];
                $BuildingCategoryArr['created_at']          = Carbon::now();

                $BuildingCategoryID = CommonModel::addRecord($BuildingCategoryArr);

                if (!empty($BuildingCategoryID)) {
                    $id                 = $BuildingCategoryID;
                    $newBuildingCategoryObj = BuildingCategory::getRecordForLogById($id);
                    $logArr             = MyLibrary::logData($id);
                    $logArr['varTitle'] = $newBuildingCategoryObj->varTitle;
                    Log::recordLog($logArr);

                    if (Auth::user()->can('recent-updates-list')) {
                        $notificationArr = MyLibrary::notificationData($id, $newBuildingCategoryObj);
                        RecentUpdates::setNotification($notificationArr);
                    }

                    $actionMessage = trans('template.buildingCategoryModule.addedMessage');
                }

                Self::flushCache();
            }
            AddImageModelRel::sync(explode(',', $data['img_id']), $id);            
            if (!empty($data['saveandexit']) && $data['saveandexit'] == 'saveandexit') {
                return redirect()->route('powerpanel.building-category.index')->with('message', $actionMessage);
            } else {
                return redirect()->route('powerpanel.building-category.edit', $id)->with('message', $actionMessage);
            }

        } else {
            return Redirect::back()->withErrors($validator)->withInput();
        }

    }
    /**
     * This method loads building_category table data on view
     * @return  View
     * @since   2017-01-31
     * @author  NetQuick
     */

    public function get_list()
    {
        $filterArr                  = [];
        $records                    = [];
        $records["data"]            = [];
        $filterArr['orderColumnNo'] = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');

        $filterArr['orderByFieldName'] = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');

        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['statusFilter']       = !empty(Request::get('customActionName')) ? Request::get('customActionName') : '';

        $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';

        $filterArr['iDisplayLength'] = intval(Request::get('length'));
        $filterArr['iDisplayStart']  = intval(Request::get('start'));

        $sEcho      = intval(Request::get('draw'));
        $arrResults = BuildingCategory::getRecordList($filterArr);

        $iTotalRecords                      = CommonModel::getRecordCount($filterArr, true);
        $totalRecords                       = CommonModel::getTotalRecordCount();
        
        if (!empty($arrResults)) {
            foreach ($arrResults as $key => $value) {
                $records["data"][] = $this->tableData($value,$totalRecords);
            }

        }
        $records["customActionStatus"] = "OK";
        $records["draw"]               = $sEcho;
        $records["recordsTotal"]       = $iTotalRecords;
        $records["recordsFiltered"]    = $iTotalRecords;

        return json_encode($records);
    }

    public function publish(Request $request)
    {
        $alias  = Request::get('alias');
        $val  = Request::get('val');
        $update = MyLibrary::setPublishUnpublish($alias, $val);
        Self::flushCache();
        echo json_encode($update);
        exit;
    }

    /**
     * This method reorders banner position
     * @return  Banner index view data
     * @since   2016-10-26
     * @author  NetQuick
     */
    public function reorder()
    {
        $order   = Request::get('order');
        $exOrder = Request::get('exOrder');
        MyLibrary::swapOrder($order, $exOrder);
        Self::flushCache();
    }

    /**
     * This method delete multiples Team
     * @return  true/false
     * @since   2017-07-22
     * @author  NetQuick
     */
    public function DeleteRecord(Request $request)
    {
        $data   = Request::all('ids');
        $update = MyLibrary::deleteMultipleRecords($data);
        Self::flushCache();
        echo json_encode($update);
        exit;
    }

    /**
     * This method handels swapping of available order record while adding
     * @param   order
     * @return  order
     * @since   2017-07-24
     * @author  NetQuick
     */
    public static function swap_order_add($order = null)
    {
        $response = false;
        if ($order != null) {
            $response = MyLibrary::swapOrderAdd($order);
            Self::flushCache();
        }
        return $response;
    }

    /**
     * This method handels swapping of available order record while editing
     * @param      order
     * @return  order
     * @since   2017-07-22
     * @author  NetQuick
     */
    public static function swap_order_edit($order = null, $id = null)
    {
        MyLibrary::swapOrderEdit($order, $id);
        Self::flushCache();
    }

    /**
     * This method handels logs History records
     * @param   $data
     * @return  HTML
     * @since   2017-07-21
     * @author  NetQuick
     */

    public function recordHistory($data = false)
    {
        $returnHtml = '';
        $returnHtml .= '<table class="new_table_desing table table-striped table-bordered table-hover">
                                                                                    <thead>
                                                                                            <tr>
                                                                                                <th>' . trans("template.common.title") . '</th>
                                                                                                <th>' . trans("template.common.parentCategory") . '</th>
                                                                                                <th>' . trans("template.common.shortDescription") . '</th>
                                                                                                <th>' . trans("template.common.description") . '</th>
                                                                                                <th>' . trans("template.common.displayorder") . '</th>
                                                                                                <th>' . trans("template.common.metatitle") . '</th>
                                                                                                <th>' . trans("template.common.metakeyword") . '</th>
                                                                                                <th>' . trans("template.common.metadescription") . '</th>
                                                                                                <th>' . trans("template.common.publish") . '</th>
                                                                                            </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                            <tr>
                                                                                                    <td>' . $data->varTitle . '</td>';
        if ($data->intParentCategoryId > 0) {
            $catIDS[]       = $data->intParentCategoryId;
            $parentCateName = BuildingCategory::getParentCategoryNameBycatId($catIDS);
            $parentCateName = $parentCateName[0]->varTitle;
            $returnHtml .= '<td>' . $parentCateName . '</td>';
        } else {
            $returnHtml .= '<td>-</td>';
        }

        $returnHtml .= '<td>' . $data->txtShortDescription . '</td>
                                                                                                    <td>' . $data->txtDescription . '</td>
                                                                                                    <td>' . ($data->intDisplayOrder) . '</td>
                                                                                                    <td>' . $data->varMetaTitle . '</td>
                                                                                                    <td>' . $data->varMetaKeyword . '</td>
                                                                                                    <td>' . $data->varMetaDescription . '</td>
                                                                                                    <td>' . $data->chrPublish . '</td>
                                                                                            </tr>
                                                                                    </tbody>
                                                                            </table>';
        return $returnHtml;

    }

    public function tableData($value = false,$totalRecords)
    {
        $hasRecords           = BuildingHome::getCountById($value->id);
        $isParent             = BuildingCategory::getCountById($value->id);
        $details              = '';
        $parent_category_name = ' ';
        $publish_action       = '';
        $titleData            = "";
        $imgIcon = '';
        if (!empty($value->fkIntImgId) && $value->fkIntImgId > 0) {
            $imgIcon .= '<a href="' . resize_image::resize($value->fkIntImgId) . '" class="fancybox-buttons" data-fancybox="fancybox-buttons">';
            $imgIcon .= '<img height="30" width="30" title="' . preg_replace('/[^A-Za-z0-9\-]/', '-', $value->varTitle) . '" src="' . resize_image::resize($value->fkIntImgId, 50, 50) . '"/>';
            $imgIcon .= '</a>';
        } else {
            $imgIcon .= '<span class="glyphicon glyphicon-minus"></span>';
        } 
        $details              = '<a class="without_bg_icon" href="' . url('powerpanel/buildinghome/add?category=' . $value->id) . '" title="' . trans("template.buildingCategoryModule.addBuildingCategory") . '"><i class="icon-notebook"></i></a>';
        if (Auth::user()->can('building-category-edit')) {
            $details .= '<a class="without_bg_icon" title="' . trans("template.common.edit") . '" href="' . route('powerpanel.building-category.edit', array('alias' => $value->id)) . '"><i class="fa fa-pencil"></i></a>';
        }
        if (Auth::user()->can('building-category-delete') && $hasRecords == 0 && $isParent == 0) {
            $details .= '&nbsp;<a class="without_bg_icon delete" title="' . trans("template.common.delete") . '" data-controller="building-category" data-alias = "' . $value->id . '"><i class="fa fa-times"></i></a>';
        }

        if (Auth::user()->can('building-category-publish')) {
            if ($hasRecords == 0 && $isParent == 0) {
                if ($value->chrPublish == 'Y') {
                    $publish_action .= '<input data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/building-category" title="' . trans("template.common.publishedRecord") . '" data-value="Unpublish" data-alias="' . $value->id . '">';
                } else {
                    $publish_action .= '<input checked="" data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/building-category" title="' . trans("template.common.unpublishedRecord") . '" data-value="Publish" data-alias="' . $value->id . '">';
                }
            } else {
                $publish_action = '-';
            }
        }

        if ($hasRecords > 0) {
            $titleData = 'This category is selected in ' . $hasRecords . ' record(s) so it can&#39;t be deleted or unpublished.';
        }

        if ($isParent > 0) {
            $titleData = 'This category is selected as Parent Category in ' . $isParent . ' record(s) so it can&#39;t be deleted or unpublished.';
        }
        $checkbox = '<a href="javascript:;" data-toggle="tooltip" data-placement="right" data-toggle="tooltip" data-original-title="' . $titleData . '" title="' . $titleData . '"><i style="color:red" class="fa fa-exclamation-triangle"></i></a>';

        $parentCategoryTitle = '-';
        if (!empty($value->intParentCategoryId) && $value->intParentCategoryId > 0) {
            $catIDS[]            = $value->intParentCategoryId;
            $parentCategoryName  = BuildingCategory::getParentCategoryNameBycatId($catIDS);
            $parentCategoryTitle = $parentCategoryName[0]->varTitle;
        }

        $orderArrow = '';   
        $orderArrow .= '<span class="pageorderlink">'; 
        if($totalRecords != $value->intDisplayOrder) {
          $orderArrow .= '<a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveUp"><i class="fa fa-plus" aria-hidden="true"></i></a> ';
        }
        $orderArrow .= $value->intDisplayOrder.' ';
        if($value->intDisplayOrder != 1) {
          $orderArrow .= ' <a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveDwn"><i class="fa fa-minus" aria-hidden="true"></i></a>';
        } 
        $orderArrow .= '</span>'; 

        $records = array(
            ($hasRecords == 0 && $isParent == 0) ? '<input type="checkbox" name="delete" class="chkDelete" value="' . $value->id . '">' : $checkbox,
            '<a class="without_bg_icon" title="Edit" href="' . route('powerpanel.building-category.edit', array('alias' => $value->id)) . '">' . $value->varTitle . '</a>',
            '<a href="javascript:void(0)" class="without_bg_icon" onclick="return hs.htmlExpand(this,{width:300,headingText:\'' . trans("template.common.shortdescription") . '\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>
                    <div class="highslide-maincontent">' . nl2br($value->txtShortDescription) . '</div>
                </div>',
            $imgIcon,
            //$parentCategoryTitle,
            ($hasRecords > 0) ? '<a href="' . url('powerpanel/buildinghome?category=' . $value->id) . '">' . trans("template.common.view") . ' (' . $hasRecords . ')</a>' : '-',
            $orderArrow,
            $publish_action,
            $details,
            $value->intDisplayOrder,
        );
        return $records;
    }

    public function addCatAjax()
    {
        $data = Request::all();
        return AddCategoryAjax::Add($data, 'BuildingCategory');
    }

    public static function flushCache()
    {
        Cache::tags(['BuildingCategory', 'Building'])->flush();
    }
}
