<?php
namespace App\Http\Controllers\Powerpanel;

use App\ContactLead;
use App\RequestLead;
use App\ServiceCategory;
use App\Dashboard;
use App\Helpers\MyLibrary;
use App\Http\Controllers\PowerpanelController;
use App\Pagehit;
use File;
use Request;
use Config;
use App\RestaurantReservations;

class DashboardController extends PowerpanelController
{

    /*
    |--------------------------------------------------------------------------
    | Dashboard Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling dashboard stats.
    |
    |
    |
     */
    /**
     * Create a new Dashboard controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }
    public function index()
    {

        $hits = Pagehit::getHits()->toArray();
        $currentMonth = Pagehit::getHitsCurrentMonth()->toArray();
        $currentYear = Pagehit::getHitsCurrentYear()->toArray();

        $leads = ContactLead::getRecordList();
        $contactLeadCount = count($leads);

        $requestleads = RequestLead::getRecordList();        
        $requestLeadCount = count($requestleads);
        // echo '<pre>'; print_r($requestLeadCount);die;

        $reservationleads = '';
        $reservationLeadCount = 0;
        if (File::exists(app_path() . '/NewsletterLead.php') != null) 
        {
            $reservationleads = RestaurantReservations::getRecordList();
            $reservationLeadCount = count($reservationleads);
        }

        $newsleads = "";
        $subscriberLeadCount = 0;
        $currentMonthNewsLetterCount = 0;
        $currentYearNewsLetterCount = 0;
        if (File::exists(app_path() . '/NewsletterLead.php') != null) {
            $newsleads = \App\NewsletterLead::getRecordList();
            $currentMonthNewsLetterCount = \App\NewsletterLead::getCurrentMonthCount();
            $currentYearNewsLetterCount = \App\NewsletterLead::getCurrentYearCount();
            $subscriberLeadCount = count($newsleads);
        }

        $bookAnAppointmentleads = "";
        if (File::exists(app_path() . '/AppointmentLead.php') != null) {
            $bookAnAppointmentleads = \App\AppointmentLead::getRecordList();
        }

        $currentMonthContactCount = ContactLead::getCurrentMonthCount();
        $currentYearContactCount = ContactLead::getCurrentYearCount();

        $currentMonthRequestCount = RequestLead::getCurrentMonthCount();
        $currentYearRequestCount = RequestLead::getCurrentYearCount();

        $currentMonthNewsLetterCount = !empty($currentMonthNewsLetterCount) ? $currentMonthNewsLetterCount : 0;
        $currentYearNewsLetterCount = !empty($currentYearNewsLetterCount) ? $currentYearNewsLetterCount : 0;

        return view('powerpanel.dashboard.dashboard', compact(
            'hits',
            'currentMonth',
            'currentYear',
            'leads',
            'requestleads',
            'newsleads',
            'bookAnAppointmentleads',
            'contactLeadCount',
            'requestLeadCount',
            'subscriberLeadCount',
            'reservationleads',
            'reservationLeadCount',
            'currentMonthNewsLetterCount',
            'currentYearNewsLetterCount',
            'currentMonthRequestCount',
            'currentYearRequestCount',
            'currentMonthContactCount',
            'currentYearContactCount')
        );
    }

    public function ajaxcall()
    {
        $data = Request::all();
        switch ($data['type']) {
            case 'contactuslead':
                $contactusleadID = $data['id'];
                $contactusLeadRecord = ContactLead::getRecordById($contactusleadID);
                if ($contactusLeadRecord['varEmail'] != '') {
                    $contactusLeadRecord['varEmail'] = MyLibrary::getDecryptedString($contactusLeadRecord['varEmail']);
                }
                if ($contactusLeadRecord['varPhoneNo'] != '') {
                    $contactusLeadRecord['varPhoneNo'] = MyLibrary::getDecryptedString($contactusLeadRecord['varPhoneNo']);
                }
                if ($contactusLeadRecord['txtUserMessage'] != '') {
                    $contactusLeadRecord['txtUserMessage'] = MyLibrary::getDecryptedString($contactusLeadRecord['txtUserMessage']);
                }

                echo json_encode($contactusLeadRecord);
                break;

            case 'requestlead':
                    $requestleadID = $data['id'];
                    $requestLeadRecord = RequestLead::getRecordById($requestleadID);
                    if ($requestLeadRecord['varEmail'] != '') {
                        $requestLeadRecord['varEmail'] = MyLibrary::getDecryptedString($requestLeadRecord['varEmail']);
                    }
                    if ($requestLeadRecord['varPhoneNo'] != '') {
                        $requestLeadRecord['varPhoneNo'] = MyLibrary::getDecryptedString($requestLeadRecord['varPhoneNo']);
                    }
                    if ($requestLeadRecord['txtUserMessage'] != '') {
                        $requestLeadRecord['txtUserMessage'] = MyLibrary::getDecryptedString($requestLeadRecord['txtUserMessage']);
                    }

                    if ($requestLeadRecord['varAreaOfInterest'] != '') {
                        $requestLeadRecord['varAreaOfInterest'] = MyLibrary::getDecryptedString($requestLeadRecord['varAreaOfInterest']);
                    }

                    // if ($requestLeadRecord['varAreaOfInterest'] != '') {
                    //     $aof_id = $requestLeadRecord['varAreaOfInterest'];                                      
                    //     $AreaOfInterest = ServiceCategory::getRequestQuoteList($aof_id);                        
                    //     $requestLeadRecord['varAreaOfInterest'] = $AreaOfInterest->varTitle;
                    // }
    
                    echo json_encode($requestLeadRecord);
                    break;
            
            case 'appointmentlead':
					$appointmentID = $data['id'];										
                    $appointmentRecord = \App\AppointmentLead::getRecordById($appointmentID);
                    if ($appointmentRecord['varEmail'] != '') {
                        $appointmentRecord['varEmail'] = MyLibrary::getDecryptedString($appointmentRecord['varEmail']);
                    }
                    if ($appointmentRecord['varPhoneNo'] != '') {
                        $appointmentRecord['varPhoneNo'] = MyLibrary::getDecryptedString($appointmentRecord['varPhoneNo']);
										}
										
                    if ($appointmentRecord['appointmentDate'] != '') {
                        $appointmentRecord['appointmentDate'] = date('' . Config::get('Constant.DEFAULT_DATE_FORMAT') . ' ' . Config::get('Constant.DEFAULT_TIME_FORMAT') . '', strtotime($appointmentRecord['appointmentDate']));
										}
										
                    echo json_encode($appointmentRecord);
                break;
              case 'reservationLead':
                    $reservationID = $data['id'];                                       
                    $reservationRecord = \App\RestaurantReservations::getRecordById($reservationID);
                    if ($reservationRecord['varEmail'] != '') {
                        $reservationRecord['varEmail'] = MyLibrary::getDecryptedString($reservationRecord['varEmail']);
                    }
                    if ($reservationRecord['varPhoneNo'] != '') {
                        $reservationRecord['varPhoneNo'] = MyLibrary::getDecryptedString($reservationRecord['varPhoneNo']);
                                        }
                    if ($reservationRecord['dtDateTime'] != '') {
                        $reservationRecord['dtDateTime'] = date('' . Config::get('Constant.DEFAULT_DATE_FORMAT') . ' ' . Config::get('Constant.DEFAULT_TIME_FORMAT') . '', strtotime($reservationRecord['dtDateTime']));
                                        }
                                        
                    echo json_encode($reservationRecord);
                break;  
            default:
                echo "error";
                break;
        }
    }
}
