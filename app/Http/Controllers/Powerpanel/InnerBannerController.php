<?php
namespace App\Http\Controllers\Powerpanel;

use App\Alias;
use App\CmsPage;
use App\CommonModel;
use App\Helpers\AddImageModelRel;
use App\Helpers\MyLibrary;
use App\Helpers\resize_image;
use App\Http\Controllers\PowerpanelController;
use App\InnerBanner;
use App\Log;
use App\Modules;
use App\RecentUpdates;
use Auth;
use Cache;
use DB;
use Illuminate\Support\Facades\Redirect;
use Request;
use Validator;

class InnerBannerController extends PowerpanelController
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }
    /**
     * This method handels load banner grid
     * @return  View
     * @since   2017-07-20
     * @author  NetQuick
     */
    public function index()
    {
        $total = CommonModel::getRecordCount();
        $cms_pages = $total > 0 ? CmsPage::getPagesWithModule() : null;

        $moduleObj = Modules::getFrontModuleList();

        $this->breadcrumb['title'] = trans('Manage Banners');
        return view('powerpanel.inner_banners.list', ['total' => $total, 'moduleObj' => $moduleObj, 'cms_pages' => $cms_pages, 'breadcrumb' => $this->breadcrumb]);
    }
    /**
     * This method handels list of banner with filters
     * @return  View
     * @since   2017-07-20
     * @author  NetQuick
     */
    public function get_list()
    {
        /*Start code for sorting*/
        $filterArr = [];
        $records = array();
        $records["data"] = array();
        $filterArr['orderColumnNo'] = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');
        $filterArr['orderByFieldName'] = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');
        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';
        $filterArr['statusFilter'] = !empty(Request::get('customActionName')) ? Request::get('customActionName') : '';
        $filterArr['bannerFilter'] = !empty(Request::get('bannerFilter')) ? Request::get('bannerFilter') : '';
        $filterArr['bannerFilterType'] = !empty(Request::get('bannerFilterType')) ? Request::get('bannerFilterType') : '';
        $filterArr['pageFilter'] = !empty(Request::get('pageFilter')) ? Request::get('pageFilter') : '';
        $filterArr['iDisplayLength'] = intval(Request::get('length'));
        $filterArr['iDisplayStart'] = intval(Request::get('start'));
        /**** Delete record then redirect to approriate pagination **/
        $currentrecordcountstart = intval(Request::get('start'));
        $currentpaging = intval(Request::get('length'));

        $totalRecords_old = CommonModel::getTotalRecordCount();
        if ($totalRecords_old > $currentrecordcountstart) {
            $filterArr['iDisplayStart'] = intval(Request::get('start'));
        } else {
            $filterArr['iDisplayStart'] = intval(0);
        }
        /**** Delete record then redirect to approriate pagination **/
        $sEcho = intval(Request::get('draw'));
        $arrResults = InnerBanner::getRecordList($filterArr);
        $totalRecords = CommonModel::getTotalRecordCount();
        $iTotalRecords = CommonModel::getRecordCount($filterArr, true);
        $end = $filterArr['iDisplayStart'] + $filterArr['iDisplayLength'];
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        if ($arrResults->count() > 0 && !empty($arrResults)) {
            foreach ($arrResults as $key => $value) {
                $records["data"][] = $this->tableData($value, $totalRecords);
            }
        }
        $records["customActionStatus"] = "OK";
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
        exit;
    }
    /**
     * This method loads banner edit view
     * @param   Alias of record
     * @return  View
     * @since   2017-11-10
     * @author  NetQuick
     */
    public function edit($id = false)
    {

        $module = Modules::getFrontModuleList();
        if (!is_numeric($id)) {
            $total = CommonModel::getRecordCount();
            $total = $total + 1;
            $this->breadcrumb['title'] = trans('Add Inner Banner');
            $this->breadcrumb['module'] = trans('Manage Inner Banner Module');
            $this->breadcrumb['url'] = 'powerpanel/inner-banner';
            $this->breadcrumb['inner_title'] = trans('Add Inner Banner');
            $breadcrumb = $this->breadcrumb;
            $data = ['modules' => $module, 'total_banner' => $total, 'breadcrumb' => $this->breadcrumb, 'imageManager' => true];
        } else {
            $banners = InnerBanner::getRecordById($id);
            if (empty($banners)) {
                return redirect()->route('powerpanel.InnerBannerModule.add');
            }
            $this->breadcrumb['title'] = trans('template.common.edit') . ' - ' . $banners->varTitle;
            $this->breadcrumb['module'] = trans('Manage Banners');
            $this->breadcrumb['url'] = 'powerpanel/inner-banner';
            $this->breadcrumb['inner_title'] = trans('template.common.edit') . ' - ' . 'Banners';
            $breadcrumb = $this->breadcrumb;
            $data = ['banners' => $banners, 'modules' => $module, 'breadcrumb' => $this->breadcrumb, 'imageManager' => true];
        }
        return view('powerpanel.inner_banners.actions', $data);
    }
    /**
     * This method stores banner modifications
     * @return  View
     * @since   2017-11-10
     * @author  NetQuick
     */
    public function handlePost(Request $request)
    {
        $postArr = Request::all();
        $bannerFields = [];
        $actionMessage = trans('template.common.oppsSomethingWrong');

        $rules = array(
            'title' => 'required|max:160',
            'chrMenuDisplay' => 'required',
            'img_id' => 'required',
        );

        $messsages = array(
            'img_id.required' => trans('template.InnerBannerModule.bannerValidation'),
            'modules.required' => trans('template.InnerBannerModule.moduleValidationMessage'),
        );

        $validator = Validator::make($postArr, $rules, $messsages);
        if ($validator->passes()) {
            //$pageId = 0;

            $moduleId = $postArr['modules'];
            $bannerFields['fkIntImgId'] = $postArr['img_id'];
            $bannerFields['varTitle'] = trim($postArr['title']);
            $bannerFields['varSubTitle'] = trim($postArr['sub_title']);
            $bannerFields['fkModuleId'] = $moduleId;
            $bannerFields['chrPublish'] = $postArr['chrMenuDisplay'];

            $id = Request::segment(3);
            if (is_numeric($id)) {
                #Edit post Handler=======
                $banner = InnerBanner::getRecordForLogById($id);
                $pageId = isset($postArr['foritem']) ? $postArr['foritem'] : $banner->fkIntPageId;
                $bannerFields['fkIntPageId'] = $pageId;

                $whereConditions = ['id' => $id];
                $update = CommonModel::updateRecords($whereConditions, $bannerFields);
                if ($update) {
                    if (!empty($id)) {
                        self::swap_order_edit($postArr['display_order'], $id);
                        $logArr = MyLibrary::logData($id);
                        if (Auth::user()->can('log-advanced')) {
                            $newBannerObj = InnerBanner::getRecordForLogById($id);
                            $oldRec = $this->recordHistory($banner);
                            $newRec = $this->recordHistory($newBannerObj);
                            $logArr['old_val'] = $oldRec;
                            $logArr['new_val'] = $newRec;
                        }
                        $logArr['varTitle'] = trim($postArr['title']);
                        Log::recordLog($logArr);
                        if (Auth::user()->can('recent-updates-list')) {
                            if (!isset($newBannerObj)) {
                                $newBannerObj = InnerBanner::getRecordForLogById($id);
                            }
                            $notificationArr = MyLibrary::notificationData($id, $newBannerObj);
                            RecentUpdates::setNotification($notificationArr);
                        }
                    }
                    self::flushCache();
                    $actionMessage = trans('template.InnerBannerModule.updateMessage');
                }
            } else {

                #Add post Handler=======
                $pageId = $postArr['foritem'];
                $bannerFields['fkIntPageId'] = $pageId;

                $bannerID = CommonModel::addRecord($bannerFields);
                if (!empty($bannerID)) {
                    $id = $bannerID;
                    $newBannerObj = InnerBanner::getRecordForLogById($id);
                    $logArr = MyLibrary::logData($id);
                    $logArr['varTitle'] = $newBannerObj->varTitle;
                    Log::recordLog($logArr);
                    if (Auth::user()->can('recent-updates-list')) {
                        $notificationArr = MyLibrary::notificationData($id, $newBannerObj);
                        RecentUpdates::setNotification($notificationArr);
                    }
                    self::flushCache();
                    $actionMessage = trans('template.InnerBannerModule.addedMessage');
                }
            }

            AddImageModelRel::sync(explode(',', $postArr['img_id']), $id);

            if (!empty($postArr['saveandexit']) && $postArr['saveandexit'] == 'saveandexit') {
                return redirect()->route('powerpanel.inner-banner.index')->with('message', $actionMessage);
            } else {
                return redirect()->route('powerpanel.inner-banner.edit', $id)->with('message', $actionMessage);
            }

            return Redirect::back()->withErrors($validator)->withInput();
        }
    }
    /**
     * This method destroys Banner in multiples
     * @return  Banner index view
     * @since   2016-10-25
     * @author  NetQuick
     */
    public function DeleteRecord(Request $request)
    {
        $data = Request::all('ids');
        $update = MyLibrary::deleteMultipleRecords($data);
        self::flushCache();
        echo json_encode($update);
        exit;
    }
    /**
     * This method destroys Banner in multiples
     * @return  Banner index view
     * @since   2016-10-25
     * @author  NetQuick
     */
    public function publish(Request $request)
    {
        $alias = (int) Request::get('alias');
        $val = Request::get('val');
        $update = MyLibrary::setPublishUnpublish($alias, $val);
        self::flushCache();
        echo json_encode($update);
        exit;
    }
    /**
     * This method reorders banner position
     * @return  Banner index view data
     * @since   2016-10-26
     * @author  NetQuick
     */
    public function reorder()
    {
        $order = Request::get('order');
        $exOrder = Request::get('exOrder');
        MyLibrary::swapOrder($order, $exOrder);
        self::flushCache();
    }
    /**
     * This method assigns default banner flag
     * @return  Banner index view data
     * @since   2016-12-10
     * @author  NetQuick
     */
    public function makeDefault()
    {
        $id = Request::get('alias');
        $val = Request::get('val');
        if (!empty($val) && ($val == 'rm-default')) {
            $update = InnerBanner::setDefault($id, ['chrDefaultBanner' => 'N']);
            if ($update) {
                $logArr = MyLibrary::logData($id);
                $logArr['action'] = 'remove-default-banner';
                Log::recordLog($logArr);
                self::flushCache();
            }
        }
        if (!empty($val) && ($val == 'default')) {
            $update = InnerBanner::setDefault($id, ['chrDefaultBanner' => 'Y']);
            if ($update) {
                $logArr = MyLibrary::logData($id);
                $logArr['action'] = 'made-default-banner';
                Log::recordLog($logArr);
                self::flushCache();
            }
        }
        echo json_encode($update);
    }
/**
 * This method handels swapping of available order record while adding
 * @param      order
 * @return  order
 * @since   2016-10-21
 * @author  NetQuick
 */
    public static function swap_order_add($order = null)
    {
        $response = false;
        if ($order != null) {
            $response = MyLibrary::swapOrderAdd($order);
            self::flushCache();
        }
        return $response;
    }

    /**
     * This method handels swapping of available order record while editing
     * @param      order
     * @return  order
     * @since   2016-12-23
     * @author  NetQuick
     */
    public static function swap_order_edit($order = null, $id = null)
    {
        MyLibrary::swapOrderEdit($order, $id);
        self::flushCache();
    }

    /**
     * This method handels getting category and it's records (ajax)
     * @return      JSON object
     * @since       2016-12-23
     * @author      NetQuick
     */
    public static function selectRecords()
    {
        $data = Request::all();
        $module = $data['module'];

        $selected = $data['selected'];
        $model = '\\App\\' . $data['model'];
        $module = Modules::getModule($module);
        $recordSelect = '<option value="">--' . trans('template.InnerBannerModule.selectPage') . '--</option>';

        $assignedPageId = InnerBanner::getAssignedPageId();

        $pageIdArr = array();
        $moduleIdArr = array();
        foreach ($assignedPageId as $key => $value) {
            $pageIdArr[$key] = $value->fkIntPageId;
            $moduleIdArr[$key] = $value->fkModuleId;
        }

        if ($module->varModuleName == "pages") {

            $moduleRec = $model::getPagesWithModule();
            foreach ($moduleRec as $record) {
                if (strtolower($record->varTitle) != 'home') {
                    if (Auth::user()->can($record->modules->varModuleName . '-list')) {

                        $exist = false;
                        if (in_array($record->id, $pageIdArr) && in_array($module->id, $moduleIdArr)) {
                            $exist = true;
                        }
                        if ($data['user_action'] == 'add') {
                            if (!$exist) {
                                $recordSelect .= '<option  data-moduleid="' . $module->id . '" value="' . $record->id . '" ' . ($record->id == $selected ? 'selected' : '') . '>' . ucwords($record->varTitle) . '</option>';
                            }
                        } else if ($data['user_action'] == 'edit') {
                            if ($exist) {
                                $recordSelect .= '<option disabled="disabled"  data-moduleid="' . $module->id . '" value="' . $record->id . '" ' . ($record->id == $selected ? 'selected' : '') . '>' . ucwords($record->varTitle) . '</option>';
                            } else {
                                $recordSelect .= '<option  data-moduleid="' . $module->id . '" value="' . $record->id . '" ' . ($record->id == $selected ? 'selected' : '') . '>' . ucwords($record->varTitle) . '</option>';
                            }
                        }
                    }
                }
            }

        } else {

            $moduleRec = $model::getRecordList();
            foreach ($moduleRec as $record) {
                if (strtolower($record->varTitle) != 'home') {
                    $exist = false;
                    if (in_array($record->id, $pageIdArr) && in_array($module->id, $moduleIdArr)) {
                        $exist = true;
                    }

                    if ($data['user_action'] == 'add') {
                        if (!$exist) {
                            $recordSelect .= '<option data-moduleid="' . $module->id . '" value="' . $record->id . '" ' . ($record->id == $selected ? 'selected' : '') . '>' . ucwords($record->varTitle) . '</option>';
                        }
                    } else {
                        if ($exist) {
                            $recordSelect .= '<option disabled="disabled" data-moduleid="' . $module->id . '" value="' . $record->id . '" ' . ($record->id == $selected ? 'selected' : '') . '>' . ucwords($record->varTitle) . '</option>';
                        } else {
                            $recordSelect .= '<option data-moduleid="' . $module->id . '" value="' . $record->id . '" ' . ($record->id == $selected ? 'selected' : '') . '>' . ucwords($record->varTitle) . '</option>';
                        }
                    }

                }
            }

        }

        return $recordSelect;
    }

    public function tableData($value, $totalRecords)
    {

        $image = '';
        $actions = '';
        $checkbox = '';
        $publish_action = '';
        $pageName = '-';
        $recordTitle = '-';

        if (Auth::user()->can('banners-edit')) {
            $actions .= '<a class="without_bg_icon" title="' . trans("template.common.edit") . '" href="' . route('powerpanel.inner-banner.edit', array('alias' => $value->id)) . '"><span><i class="fa fa-pencil"></i></a>';
        }

        if (($totalRecords > 1)) {
            if (Auth::user()->can('inner-banner-delete')) {
                $actions .= '<a class="without_bg_icon delete" title="' . trans("template.common.delete") . '" data-controller="inner-banner" data-alias = "' . $value->id . '"><i class="fa fa-times"></i></a>';
            }
            $checkbox = '<input type="checkbox" name="delete" class="chkDelete" value="' . $value->id . '">';
        } else {
            $checkbox = '<a href="javascript:;" data-toggle="tooltip" data-placement="right" data-toggle="tooltip" title="This is default banner so can&#39;t be deleted."><i style="color:red" class="fa fa-exclamation-triangle"></i></a>';
        }

        $pageName = isset($value->modules->varTitle) && strlen($value->modules->varTitle) > 0 ? $value->modules->varTitle : 'Default';

        if (isset($value->modules->varTableName) && !empty($value->modules->varTableName)) {
            if ($value->modules->varTableName == 'appointment_lead') {
                $recordObj = DB::table($value->modules->varTableName)->select('varName')->where('id', $value->fkIntPageId)->first();
            } else {
                $recordObj = DB::table($value->modules->varTableName)->select('varTitle')->where('id', $value->fkIntPageId)->first();
            }
        }

        if (isset($recordObj->varTitle) && !empty($recordObj)) {
            $recordTitle = $recordObj->varTitle;
        }

        $image .= '<div class="text-center">';
        if (!empty($value->fkIntImgId)) {
            $imageArr = explode(',', $value->fkIntImgId);
            if (count($imageArr) > 1) {
                $image .= '<div class="multi_image_thumb">';
                foreach ($imageArr as $ikey => $ivalue) {
                    $image .= '<a href="' . resize_image::resize($ivalue) . '" class="fancybox-thumb" rel="fancybox-thumb-' . $value->id . '" data-fancybox="fancybox-thumb">';
                    $image .= '<img height="30" width="30" title="' . preg_replace('/[^A-Za-z0-9\-]/', '-', $value->varTitle) . '" src="' . resize_image::resize($ivalue, 50, 50) . '"/>';
                    $image .= '</a>';
                }
                $image .= '</div>';
            } else {
                $image .= '<div class="multi_image_thumb">';
                $image .= '<a href="' . resize_image::resize($value->fkIntImgId) . '" class="fancybox-buttons"  data-fancybox="fancybox-buttons">';
                $image .= '<img height="30" width="30" title="' . preg_replace('/[^A-Za-z0-9\-]/', '-', $value->varTitle) . '" src="' . resize_image::resize($value->fkIntImgId, 50, 50) . '"/>';
                $image .= '</a>';
                $image .= '</div>';
            }

        } else {
            $image .= '<span class="glyphicon glyphicon-minus"></span>';
        }

        $image .= '</div>';

        if (Auth::user()->can('banners-edit')) {
            $title = '<a title="Edit" href="' . route('powerpanel.inner-banner.edit', array('alias' => $value->id)) . '">' . $value->varTitle . '</a>';
        } else {
            $title = $value->varTitle;
        }

        if (Auth::user()->can('banners-publish')) {
            if ($value->chrPublish == 'Y') {
                $publish_action .= '<input  data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/inner-banner" title="' . trans("template.common.publishedRecord") . '" data-value="Unpublish" data-alias="' . $value->id . '">';
            } else {
                $publish_action .= '<input checked="" data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/inner-banner" title="' . trans("template.common.unpublishedRecord") . '" data-value="Publish" data-alias="' . $value->id . '">';
            }
        }
        $subTitle = '<span class="glyphicon glyphicon-minus"></span>';
        if (!empty($value->varSubTitle)) {
            $subTitle = '<div class="pro-act-btn">
                    <a href="javascript:void(0)" class="without_bg_icon" onclick="return hs.htmlExpand(this,{width:300,headingText:\'' . "Sub Title " . '\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>
                        <div class="highslide-maincontent">' . html_entity_decode($value->varSubTitle) . '</div>
                    </div>';
        }

        $records = array(
            $checkbox,
            $title,
            $subTitle,
            $image,
            $pageName,
            $recordTitle,
            $publish_action,
            $actions,
            $value->created_at

        );

        return $records;
    }
    /**
     * This method handels logs old records
     * @param      $data
     * @return  order
     * @since   2017-07-21
     * @author  NetQuick
     */
    public function recordHistory($data = false)
    {

        $pageTitle = 'Default';
        if (strlen($data->fkIntPageId) > 0) {
            $pageDetail = CmsPage::getPageTitleById($data->fkIntPageId);
            $pageTitle = $pageDetail->varTitle;
        }

        $returnHtml = '';
        $returnHtml .= '<table class="new_table_desing table table-striped table-bordered table-hover">
              <thead>
                  <tr>
                      <th>' . trans("template.common.title") . '</th>
                      <th>' . trans("template.InnerBannerModule.page") . '</th>
                      <th>' . trans("template.common.image") . '</th>
                      <th>' . trans("template.InnerBannerModule.version") . '</th>
                      <th>' . trans("template.common.description") . '</th>
                      <th>' . trans("template.common.intDisplayOrder") . '</th>
                      <th>' . trans("template.common.publish") . '</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>' . $data->varTitle . '</td>
                      <td>' . $pageTitle . '</td>';
        if ($data->fkIntImgId > 0) {
            $returnHtml .= '<td>' . '<img height="50" width="50" src="' . resize_image::resize($data->fkIntImgId) . '" />' . '</td>';
        } else {
            $returnHtml .= '<td>-</td>';
        }
        $returnHtml .= '<td>' . $bannerVersion . '</td>

                      <td>' . $data->txtDescription . '</td>
                      <td>' . ($data->intDisplayOrder) . '</td>
                      <td>' . $data->chrPublish . '</td>
                  </tr>
              </tbody>
          </table>';
        return $returnHtml;
    }

    public static function flushCache()
    {
        Cache::tags('InnerBanner')->flush();
    }
}
