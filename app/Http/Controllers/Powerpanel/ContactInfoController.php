<?php
namespace App\Http\Controllers\Powerpanel;

use App\CommonModel;
use App\ContactInfo;
use App\Helpers\AddImageModelRel;
use App\Helpers\MyLibrary;
use App\Helpers\resize_image;
use App\Http\Controllers\PowerpanelController;
use App\Log;
use App\RecentUpdates;
use Auth;
use Cache;
use Illuminate\Support\Facades\Redirect;
use Request;
use Validator;

class ContactInfoController extends PowerpanelController
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }

    /**
     * This method handels view of listing
     * @return  view
     * @since   2017-08-02
     * @author  NetQuick
     */
    public function index()
    {
        $total = CommonModel::getRecordCount();

        $this->breadcrumb['title'] = trans('template.contactModule.managecontacts');
        $client_role = Auth::user()->hasRole('client_roles');
        return view('powerpanel.contact_info.list', ['total' => $total, 'breadcrumb' => $this->breadcrumb, 'client_role' => $client_role]);
    }

    /**
     * This method loads contactInfo edit view
     * @param   Alias of record
     * @return  View
     * @since   2017-11-10
     * @author  NetQuick
     */
    public function edit($id = false)
    {
        $imageManager = true;
         $primaryContactCount = ContactInfo::where('chrIsPrimary', 'Y')->count();
        //  print_r($primaryContactCount);die;
       
        if (!is_numeric($id)) {
            $total = CommonModel::getRecordCount();
            $total = $total + 1;
            $this->breadcrumb['title'] = trans('template.contactModule.addnewcontact');
            $this->breadcrumb['module'] = trans('template.contactModule.managecontacts');
            $this->breadcrumb['url'] = 'powerpanel/contact-info';
            $this->breadcrumb['inner_title'] = trans('template.contactModule.addnewcontact');
            $breadcrumb = $this->breadcrumb;
            $data = ['total' => $total, 'breadcrumb' => $this->breadcrumb, 'imageManager' => $imageManager];
        } else {
            $contactInfo = ContactInfo::getRecordById($id);
            if (empty($contactInfo)) {
                return redirect()->route('powerpanel.contact-info.add');
            }
            $this->breadcrumb['title'] = trans('template.common.edit') . ' - ' . $contactInfo->varTitle;
            $this->breadcrumb['module'] = trans('template.contactModule.managecontacts');
            $this->breadcrumb['url'] = 'powerpanel/contact-info';
            $this->breadcrumb['inner_title'] = trans('template.common.edit') . ' - ' . $contactInfo->varTitle;
            $breadcrumb = $this->breadcrumb;
             $data = ['contactInfo' => $contactInfo, 'breadcrumb' => $this->breadcrumb, 'imageManager' => $imageManager, 'primaryContactCount' => $primaryContactCount];
            // $data = ['contactInfo' => $contactInfo, 'breadcrumb' => $this->breadcrumb, 'imageManager' => $imageManager];
        }
        return view('powerpanel.contact_info.actions', $data);
    }

/**
 * This method handels post of edit
 * @return  view
 * @since   2017-08-02
 * @author  NetQuick
 */
    public function handlePost(Request $request)
    {
        $postArr = Request::all();
         
        $rules = $this->serverSideValidationRules();
        $actionMessage = 'Opps... Something went wrong!';
        $validator = Validator::make($postArr, $rules);

        if ($validator->passes()) {
            // echo '<pre>'; print_r($postArr);die;
            // echo '<pre>'; print_r('in valid');die;
            // foreach ($postArr['phone_no'] as $key => $value) {
            //     if (is_null($value) || $value == '') {
            //         unset($postArr['phone_no'][$key]);
            //     }
            // }

            // foreach ($postArr['email'] as $key => $value) {
            //     if (is_null($value) || $value == '') {
            //         unset($postArr['email'][$key]);
            //     }
            // }

            $contactInfoArr['varTitle'] = trim($postArr['name']);
            $contactInfoArr['varEmail'] = (!empty($postArr['email']) ? serialize($postArr['email']) : null);
            $contactInfoArr['varPhoneNo'] = $postArr['phone_no'];
            $contactInfoArr['fkIntImgId'] = !empty($postArr['img_id']) ? $postArr['img_id'] : null;
            $contactInfoArr['txtAddress'] = trim($postArr['address']);
            // $contactInfoArr['chrIsPrimary'] = $postArr['primary'];
            // $contactInfoArr['chrPublish'] = $postArr['chrMenuDisplay'];
            $contactInfoArr['varLatitude'] = !empty($postArr['lattitude']) ? $postArr['lattitude'] : '19.321187240779548';
            $contactInfoArr['varLongitude'] = !empty($postArr['longitude']) ? $postArr['longitude'] : '-81.2274169921875';
            $contactInfoArr['created_at'] = date('Y-m-d H:i:s');
            $contactInfoArr['updated_at'] = date('Y-m-d H:i:s');

            $id = Request::segment(3);
            if (is_numeric($id)) { #Edit post Handler=======

                $contactInfo = ContactInfo::getRecordForLogById($id);
                $whereConditions = ['id' => $id];
                $update = CommonModel::updateRecords($whereConditions, $contactInfoArr);

                if ($update) {
                    if (!empty($id)) {
                        self::swap_order_edit($postArr['order'], $id);

                        $logArr = MyLibrary::logData($id);
                        if (Auth::user()->can('log-advanced')) {
                            $newContactInfo = ContactInfo::getRecordForLogById($id);
                            $oldRec = $this->recordHistory($contactInfo);
                            $newRec = $this->recordHistory($newContactInfo);
                            $logArr['old_val'] = $oldRec;
                            $logArr['new_val'] = $newRec;
                        }
                        $logArr['varTitle'] = trim($postArr['name']);
                        Log::recordLog($logArr);
                        if (Auth::user()->can('recent-updates-list')) {
                            if (!isset($newContactInfo)) {
                                $newContactInfo = ContactInfo::getRecordForLogById($id);
                            }
                            $notificationArr = MyLibrary::notificationData($id, $newContactInfo);
                            RecentUpdates::setNotification($notificationArr);
                        }
                        
                        // if($postArr['primary'] == "Y")
                        // {
                        //     ContactInfo::where('id', '!=', $id)->update(['chrIsPrimary' => 'N']);
                        // }
                    }
                    self::flushCache();
                    $actionMessage = 'Contact has been successfully updated.';
                }
            } else { #Add post Handler=======
            $contactInfoArr['intDisplayOrder'] = self::swap_order_add($postArr['order']);

                // if($postArr['primary'] == "Y")
                // {
                //     ContactInfo::where(['chrIsPrimary' => 'Y'])->update(['chrIsPrimary' => 'N']);
                // }

                $contactInfoID = CommonModel::addRecord($contactInfoArr);
                if (!empty($contactInfoID)) {
                    $id = $contactInfoID;
                    $newContactObj = ContactInfo::getRecordForLogById($id);

                    $logArr = MyLibrary::logData($id);
                    $logArr['varTitle'] = $newContactObj->varTitle;
                    Log::recordLog($logArr);
                    if (Auth::user()->can('recent-updates-list')) {
                        $notificationArr = MyLibrary::notificationData($id, $newContactObj);
                        RecentUpdates::setNotification($notificationArr);
                    }
                    self::flushCache();
                    $actionMessage = 'Contact has been successfully added.';
                }
            }

            AddImageModelRel::sync(explode(',', $postArr['img_id']), $id);
            if (!empty(Request::get('saveandexit')) && Request::get('saveandexit') == 'saveandexit') {
                return redirect()->route('powerpanel.contact-info.index')->with('message', $actionMessage);
            } else {
                return redirect()->route('powerpanel.contact-info.edit', $id)->with('message', $actionMessage);
            }
        } else {
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }

    /**
     * This method handels listing
     * @return  view
     * @since   2017-08-02
     * @author  NetQuick
     */
    public function get_list()
    {
        $filterArr = [];
        $records = [];
        $records["data"] = [];

        $filterArr['orderColumnNo'] = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');
        $filterArr['orderByFieldName'] = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');
        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['statusFilter'] = !empty(Request::get('statusValue')) ? Request::get('statusValue') : '';
        $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';
        $filterArr['iDisplayLength'] = intval(Request::get('length'));
        $filterArr['iDisplayStart'] = intval(Request::get('start'));

        $sEcho = intval(Request::get('draw'));
        $arrResults = ContactInfo::getRecordList($filterArr);
        $iTotalRecords = CommonModel::getRecordCount($filterArr, true);
        $totalRecords = CommonModel::getTotalRecordCount();

        $end = $filterArr['iDisplayStart'] + $filterArr['iDisplayLength'];
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        if (!empty($arrResults)) {
            foreach ($arrResults as $key => $value) {
                $records['data'][] = $this->tableData($value, $totalRecords);
            }
        }

        if (!empty(Request::get("customActionType")) && Request::get("customActionType") == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    /**
     * This method handels Publish/Unpublish
     * @return  view
     * @since   2017-08-02
     * @author  NetQuick
     */
    public function publish(Request $request)
    {
        $alias = Request::get('alias');
        $val = Request::get('val');
        $update = MyLibrary::setPublishUnpublish($alias, $val);
        self::flushCache();
        echo json_encode($update);
        exit;
    }

    /**
     * This method reorders position
     * @return  Banner index view data
     * @since   2016-10-26
     * @author  NetQuick
     */
    public function reorder()
    {
        $order = Request::get('order');
        $exOrder = Request::get('exOrder');
        MyLibrary::swapOrder($order, $exOrder);
        self::flushCache();
    }

    /**
     * This method destroys multiples records
     * @return  true/false
     * @since   03-08-2017
     * @author  NetQuick
     */
    public function DeleteRecord(Request $request)
    {
        $data = Request::all('ids');
        $update = MyLibrary::deleteMultipleRecords($data);
        self::flushCache();
        echo json_encode($update);
        exit;
    }

    /**
     * This method handels swapping of available order record while adding
     * @param      order
     * @return  order
     * @since   2017-07-22
     * @author  NetQuick
     */
    public static function swap_order_add($order = null)
    {
        $response = false;
        if ($order != null) {
            $response = MyLibrary::swapOrderAdd($order);
            self::flushCache();
        }
        return $response;
    }

    /**
     * This method handels swapping of available order record while editing
     * @param      order
     * @return  order
     * @since   2017-07-22
     * @author  NetQuick
     */
    public static function swap_order_edit($order = null, $id = null)
    {
        MyLibrary::swapOrderEdit($order, $id);
        self::flushCache();
    }

    /**
     * This method datatable grid data
     * @return  array
     * @since   03-08-2017
     * @author  NetQuick
     */
    public function tableData($value = false, $totalRecords)
    {
        $actions = '';
        $publish_action = '';

        $image = '<div class="text-center">';
        if (!empty($value->fkIntImgId)) {
            $image .= '<a href="' . resize_image::resize($value->fkIntImgId) . '" class="fancybox-buttons" data-fancybox="fancybox-buttons">';
            $image .= '<img height="30" width="30" title="' . preg_replace('/[^A-Za-z0-9\-]/', '-', $value->varTitle) . '" src="' . resize_image::resize($value->fkIntImgId, 50, 50) . '"/>';
            $image .= '</a>';
        } else {
            $image .= '<span class="glyphicon glyphicon-minus"></span>';
        }
        $image .= '</div>';

        if (Auth::user()->can('contact-info-edit')) {
            $actions .= '<a class="without_bg_icon" title="Edit" href="' . route('powerpanel.contact-info.edit', $value->id) . '">
				<i class="fa fa-pencil"></i></a>';
        }

        if (Auth::user()->can('contact-info-delete')) {
            $actions .= '&nbsp;<a class="without_bg_icon delete" title="Delete" data-controller="contact-info" data-alias = "' . $value->id . '"><i class="fa fa-times"></i></a>';
        }

        // if (Auth::user()->can('contact-info-publish')) {
        //     if ($value->chrPublish == 'Y') {
        //         $publish_action .= '<input data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/contact-info" title="' . trans("template.common.publishedRecord") . '" data-value="Unpublish" data-alias="' . $value->id . '">';
        //     } else {
        //         $publish_action .= '<input checked="" data-off-text="No" data-on-text="Yes" class="make-switch publish" class="make-switch publish" data-off-color="info" data-on-color="primary" type="checkbox" data-controller="powerpanel/contact-info" title="' . trans("template.common.unpublishedRecord") . '" data-value="Publish" data-alias="' . $value->id . '">';
        //     }
        // }

        $details = '';
        $details .= '<a href="javascript:void(0)" class="highslide-active-anchor" onclick="return hs.htmlExpand(this,{width:300,headingText:\'Address\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-map-marker"></span></a>';
        $details .= '<div class="highslide-maincontent">' . nl2br($value->txtAddress) . '</div>';

        // if ($value->chrIsPrimary == 'Y') {
        //     $primary = 'Yes';
        // } else {
        //     $primary = 'No';
        // }

        if (Auth::user()->can('contact-info-edit')) {
            $title = '<a class="" title="Edit" href="' . route('powerpanel.contact-info.edit', $value->id) . '">' . ucwords($value->varTitle) . '</a>';
        } else {
            $title = ucwords($value->varTitle);
        }

        $emails = '<span class="glyphicon glyphicon-minus"></span>';
        if (!empty($value->varEmail)) {
            $emails = unserialize($value->varEmail);
            $emails = (count($emails) > 1 ? implode('<br/>', $emails) : $emails[0]);
        }

        $orderArrow = '';
        $orderArrow .= '<span class="pageorderlink">';
        if ($totalRecords != $value->intDisplayOrder) {
            $orderArrow .= '<a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveUp"><i class="fa fa-plus" aria-hidden="true"></i></a> ';
        }
        $orderArrow .= $value->intDisplayOrder . ' ';
        if ($value->intDisplayOrder != 1) {
            $orderArrow .= ' <a href="javascript:;" data-order="' . $value->intDisplayOrder . '" class="moveDwn"><i class="fa fa-minus" aria-hidden="true"></i></a>';
        }
        $orderArrow .= '</span>';
        $client_role = Auth::user()->hasRole('client_roles');
        $contactinfoID = '';
        if ($client_role) {
            $contactinfoID .= '';
        } else {
            $contactinfoID .= '<input type="checkbox" name="delete" class="chkDelete" value="' . $value->id . '">';
        }

        $primaryContact = '';
        if (!empty($value->chrIsPrimary)) {
            if ($value->chrIsPrimary == 'Y') {
                $primaryContact .= '<a href="javascript:makeFeatured(' . $value->id . ',\'N\');"><i class="fa fa-star" aria-hidden="true"></i></a>';
            } else {
                $primaryContact .= '<a href="javascript:makeFeatured(' . $value->id . ',\'Y\');"><i class="fa fa-star-o" aria-hidden="true"></i></a>';
            }
        } else {
            $primaryContact .= '<a href="javascript:makeFeatured(' . $value->id . ',\'Y\');"><i class="fa fa-star-o" aria-hidden="true"></i></a>';
        }

        $records = array(
            $contactinfoID,
            $title,
            $emails,
            $details,
            $primaryContact,
            $publish_action,
            $actions,
        );
        return $records;
    }

    /**
     * This method handle severside validation rules
     * @return  array
     * @since   03-08-2017
     * @author  NetQuick
     */
    public function serverSideValidationRules()
    {
        $rules = array(
            'name' => 'required|max:255',
            'order' => 'required|greater_than_zero',
            'address' => 'required',
            // 'primary' => 'required',
        );
        return $rules;
    }

    /**
     * This method handle notification old record
     * @return  array
     * @since   2016-10-25
     * @author  NetQuick
     */
    public function recordHistory($data = false)
    {
        $displayEmails = '-';
        if (!empty($data->varEmail)) {
            $emails = unserialize($data->varEmail);
            $displayEmails = count($emails) > 1 ? implode('<br/>', $emails) : $emails[1];
        }

        $displayPhones = "-";
        if (!empty($data->varPhoneNo)) {
            $phones = unserialize($data->varPhoneNo);
            $displayPhones = count($phones) > 1 ? implode('<br/>', $phones) : $phones[1];
        }

        $returnHtml = '';
        $returnHtml .= '<table class="new_table_desing table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>' . trans("template.common.title") . '</th>
					<th>' . trans("template.common.email") . '</th>
					<th>' . trans("template.common.phoneno") . '</th>
					<th>' . trans("template.common.address") . '</th>					
					<th>' . trans("template.common.publish") . '</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>' . $data->varTitle . '</td>
					<td>' . $displayEmails . '</td>
					<td>' . $displayPhones . '</td>
					<td>' . $data->txtAddress . '</td>					
					<td>' . $data->chrPublish . '</td>
				</tr>
			</tbody>
		</table>';
        return $returnHtml;
    }

    public static function flushCache()
    {
        Cache::tags('ContactInfo')->flush();
    }

    public function makeFeatured()
    {
        $id = Request::get('id');
        $featured = Request::get('featured');
        $whereConditions = ['id' => $id];
        $update = ContactInfo::where($whereConditions)->update(['chrIsPrimary' => $featured]);
        ContactInfo::where('id', '!=', $id)->update(['chrIsPrimary' => 'N']);
        self::flushCache();
        echo json_encode($update);
    }

}
