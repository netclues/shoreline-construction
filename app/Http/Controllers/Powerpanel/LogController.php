<?php
namespace App\Http\Controllers\Powerpanel;

use App\Helpers\MyLibrary;
use App\Http\Controllers\PowerpanelController;
use App\Log;
use App\Modules;
use Config;
use DB;
use Excel;
use Request;
use App\Exports\LogExport;

class LogController extends PowerpanelController
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }

    /**
     * This method handels load log grid
     * @return  View
     * @since   2017-07-20
     * @author  NetQuick
     */
    public function index()
    {
        $total = Log::getRecords('')->deleted()->count();
        $this->breadcrumb['title'] = trans('template.logManagerModule.manage');
        $modules = Modules::getFrontModuleListForLog();

        return view('powerpanel.logmanager.log_manager', ['total' => $total, 'breadcrumb' => $this->breadcrumb, 'modules' => $modules]);
    }

    /**
     * This method handels list of log with filters
     * @return  View
     * @since   2017-07-20
     * @author  NetQuick
     */
    public function get_list()
    {
        /*Start code for sorting*/
        $filterArr = [];
        $records = array();
        $records["data"] = array();
        $filterArr['orderColumnNo'] = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');
        $filterArr['orderByFieldName'] = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');
        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';
        $filterArr['moduleFilter'] = !empty(Request::get('moduleFilter')) ? Request::get('moduleFilter') : '';
        $filterArr['actionFilter'] = !empty(Request::get('actionFilter')) ? Request::get('actionFilter') : '';
        $filterArr['iDisplayLength'] = intval(Request::get('length'));
        $filterArr['iDisplayStart'] = intval(Request::get('start'));
        $sEcho = intval(Request::get('draw'));
        $arrResults = Log::getRecords()->deleted()->filter($filterArr)->get();
        $iTotalRecords = Log::getRecords()->deleted()->filter($filterArr, true)->count();
        $end = $filterArr['iDisplayStart'] + $filterArr['iDisplayLength'];
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        if ($arrResults->count() > 0 && !empty($arrResults)) {
            foreach ($arrResults as $key => $value) {
                if (isset($value->user)) {
                    $records["data"][] = $this->tableData($value);
                }
            }
        }
        $records["customActionStatus"] = "OK";
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;
    }

    /**
     * This method destroys Log in multiples
     * @return  Log index view
     * @since   2016-10-25
     * @author  NetQuick
     */
    public function DeleteRecord()
    {
        $data = Request::get('ids');
        $update = Log::deleteRecordsPermanent($data);
        exit;
    }

    public function tableData($value)
    {

        $old_val = '';
        $new_val = '';
        $link = '';
        $recordLink = '';
        if (!empty($value->txtOldVal) && strtolower($value->varAction) == 'edit') {
            $old_val .= '<a href="javascript:void(0)" class="without_bg_icon " onclick="return hs.htmlExpand(this,{width:300,headingText:\'Old Value\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>';
            $old_val .= '<div class="highslide-maincontent">' . $value->txtOldVal . '</div>';
        } else { $old_val .= '-';}
        if (!empty($value->txtNewVal) && strtolower($value->varAction) == 'edit') {
            $new_val .= '<a href="javascript:void(0)" class="without_bg_icon" onclick="return hs.htmlExpand(this,{width:300,headingText:\'New Value\',wrapperClassName:\'titlebar\',showCredits:false});"><span aria-hidden="true" class="fa fa-file-text-o"></span></a>';
            $new_val .= '<div class="highslide-maincontent">' . $value->txtNewVal . '</div>';
        } else { $new_val .= '-';}

        // if($value->module->varTitle=='contact') {
        //     $link    .= '<a href="'.url('powerpanel/'.'contacts').'">'.ucfirst($value->module->varTitle).'</a>';
        // }elseif ($value->module->varTitle=='testimonial') {
        //     $link    .= '<a href="'.url('powerpanel/'.'testimonials').'">'.ucfirst($value->module->varTitle).'</a>';
        // }else{
        //     $link .= '<a href="'.url('powerpanel/'.$value->module->varModuleName).'">'.ucfirst($value->module->varTitle).'</a>';
        // }

        if ($value->module->id == $value->fkIntModuleId) {
            $link .= '<a href="' . url('powerpanel/' . $value->module->varModuleName) . '">' . ucfirst($value->module->varTitle) . '</a>';
        } else {
            $link .= '-';
        }

        if ($value->module->varTableName == "pages") {
            $chrDeleteData = DB::table('cms_page')->where('id', $value->intRecordId)->first();
            if (!empty($chrDeleteData) && isset($chrDeleteData->chrDelete)) {
                $chrDeleteAction = $chrDeleteData->chrDelete;
            }
        } else {
            $chrDeleteData = DB::table($value->module->varTableName)->where('id', $value->intRecordId)->first();
            if (!empty($chrDeleteData) && isset($chrDeleteData->chrDelete)) {
                $chrDeleteAction = $chrDeleteData->chrDelete;
            } else {
                $chrDeleteAction = "Y";
            }
        }

        if ($value->varTitle != null && ($value->varAction == "add" || $value->varAction == "edit") && $chrDeleteAction == "N") {
            if ($value->fkIntModuleId == $value->module->id) {
                $recordLink .= '<a href="' . url('powerpanel/' . $value->module->varModuleName . '/' . $value->intRecordId . '/edit') . '">' . ucfirst($value->varTitle) . '</a>';
            }
        } elseif ($value->varTitle != null) {
            $recordLink .= '<span>' . ucfirst($value->varTitle) . '</span>';
        } else {
            $recordLink .= '-';
        }

        $records = array(
            '<input type="checkbox" name="delete" class="chkDelete" value="' . $value->id . '">',
            $value->user->name . ' &lt' . MyLibrary::getDecryptedString($value->user->email) . '&gt',
            $link,
            $old_val,
            $new_val,
            $recordLink,
            ucfirst($value->varAction),
            $value->varIpAddress,
            date('' . Config::get('Constant.DEFAULT_DATE_FORMAT') . ' ' . Config::get('Constant.DEFAULT_TIME_FORMAT') . '', strtotime($value->created_at)),
        );
        return $records;
    }

    /**
     * This method handels export process of Logs
     * @return  xls file
     * @since   2016-10-18
     * @author  NetQuick
     */
    public function ExportRecord()
    {
        return Excel::download(new LogExport, Config::get('Constant.SITE_NAME') . '-' . 'logdata' . '-' . date("dmy-h:i").'.xlsx');
    }

}
