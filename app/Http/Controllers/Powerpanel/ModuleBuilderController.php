<?php

namespace App\Http\Controllers\Powerpanel;

use App\Http\Controllers\PowerpanelController;
use App\Modules;
use Artisan;
use Auth;
use DB;
use Request;
use App\CommonModel;



class ModuleBuilderController extends PowerpanelController
{

    public function __construct()
    {
        parent::__construct();
        if (isset($_COOKIE['locale'])) {
            app()->setLocale($_COOKIE['locale']);
        }
    }

    public function index()
    {
        $iTotalRecords             = Modules::getRecords()->deleted()->count();
        $this->breadcrumb['title'] = 'Modules';
        return view('powerpanel.module_builder.list', ['iTotalRecords' => $iTotalRecords, 'breadcrumb' => $this->breadcrumb]);
    }

    public function get_list()
    {
        $filterArr                       = [];
        $records                         = [];
        $records["data"]                 = [];
        $filterArr['orderColumnNo']      = (!empty(Request::get('order')[0]['column']) ? Request::get('order')[0]['column'] : '');
        $filterArr['orderByFieldName']   = (!empty(Request::get('columns')[$filterArr['orderColumnNo']]['name']) ? Request::get('columns')[$filterArr['orderColumnNo']]['name'] : '');
        $filterArr['orderTypeAscOrDesc'] = (!empty(Request::get('order')[0]['dir']) ? Request::get('order')[0]['dir'] : '');
        $filterArr['searchFilter']       = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';
        $filterArr['iDisplayLength']     = intval(Request::get('length'));
        $filterArr['iDisplayStart']      = intval(Request::get('start'));
                        /**** Delete record then redirect to approriate pagination **/ 
                $currentrecordcountstart = intval(Request::get('start'));
        $currentpaging = intval(Request::get('length'));

        $totalRecords_old                = CommonModel::getTotalRecordCount();
        if($totalRecords_old > $currentrecordcountstart ){
            $filterArr['iDisplayStart']  = intval(Request::get('start'));
        }else{
            $filterArr['iDisplayStart']  = intval(0);
        }
        /**** Delete record then redirect to approriate pagination **/ 

        $sEcho = intval(Request::get('draw'));

        $arrResults    = Modules::getAllModuleList($filterArr);
        $iTotalRecords = Modules::getRecordCount($filterArr, true);

        $end = $filterArr['iDisplayStart'] + $filterArr['iDisplayLength'];
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        if (!empty($arrResults)) {
            foreach ($arrResults as $key => $value) {
                $records["data"][] = $this->tableData($value);
            }

        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode($records);
        exit;

    }

    public function tableData($value)
    {

        $actions = '';
        $actions .= '<a target="_blank" class="without_bg_icon" href="' . url('powerpanel/') . '/' . $value->varModuleName . '" title="Module URL"><i class="fa fa-external-link" aria-hidden="true"></i></a>';
         
        // if (Auth::user()->can('modules-delete')) {
        //     if ($value->chrIsGenerated == 'Y') {
        //         $actions .= '&nbsp;<a class="without_bg_icon delete" title="' . trans("template.common.delete") . '" data-controller="modules" data-alias="' . $value->id . '"><i class="fa fa-times"></i></a>';
        //     }
        // }

        $records = array(
            '<input type="checkbox" name="delete[]" class="chkDelete" value="' . $value->id . '">',
            $value->varTitle,
            $value->varModuleName,
            $value->chrIsFront,
            $value->chrIsPowerpanel,
            $value->chrIsGenerated,
            $actions,
        );
        return $records;
    }

    public function create()
    {
        $this->breadcrumb['title']       = 'Modules';
        $this->breadcrumb['module']      = 'Manage Module';
        $this->breadcrumb['url']         = 'powerpanel/modules';
        $this->breadcrumb['inner_title'] = 'Create Module';
        $breadcrumb                      = $this->breadcrumb;
        return view('powerpanel.module_builder.module_builder_form', compact('breadcrumb'));
    }

    public function fieldTemplate()
    {
        return view('powerpanel.module_builder.field-template');
    }

    public function generate()
    {

        $response = array();
        $data     = Request::all();

        $pluralModuleName = strtolower(str_plural($data['moduleTitle']));
        $moduleSlug       = strtolower(str_slug($data['moduleTitle'], '-'));

        $moduleExists = Modules::select('id')
            ->where('varModuleName', '=', $pluralModuleName)
            ->orWhere('varModuleName', '=', $moduleSlug)
            ->count();

        if ($moduleExists == 0) {
            $res = Artisan::call('crud:generator', ['name' => $data['modelName'],
                'data'                                         => json_encode($data),
            ]);

            $result = Artisan::output();

            if ($result) {

                $moduleFields                    = array();
                $moduleFields['varTitle']        = $data['moduleTitle'];
                $moduleFields['varModuleName']   = strtolower(str_slug($data['moduleTitle'], '-'));
                $moduleFields['varTableName']    = strtolower(str_slug($data['tableName'], '_'));
                $moduleFields['varModelName']    = preg_replace('/\s+/', '', ucwords($data['modelName']));
                $moduleFields['varModuleClass']  = preg_replace('/\s+/', '', ucwords($data['modelName'])) . 'Controller';
                $moduleFields['decVersion']      = '1.0';
                $moduleFields['varPermissions']  = 'list, create, edit, delete, publish';
                $moduleFields['intDisplayOrder'] = 0;
                $moduleFields['chrIsGenerated']  = 'Y';
                $moduleFields['chrIsFront']      = 'N';
                $moduleFields['chrIsPowerpanel'] = 'Y';
                $moduleFields['chrPublish']      = 'Y';
                $moduleFields['chrDelete']       = 'N';

                $id = Modules::insertGetId($moduleFields);
                if ($id) {
                    DB::table('permissions')->truncate();
                    Artisan::call('db:seed', ['--class' => 'PermissionTableSeeder']);
                }
                $response['url']     = url('/powerpanel') . '/' . strtolower(str_slug($data['moduleTitle'], '-'));
                $response['success'] = "Module Generated Successfully.";

            } else {
                $response['error'] = "Something went wrong";
            }

        } else {
            $response['error'] = "This module is already exists";
        }

        echo json_encode($response);
        exit;

    }

    /**
     * This method delete multiples blog
     * @return  true/false
     * @since   2017-07-15
     * @author  NetQuick
     */
    public function DeleteRecord(Request $request)
    {
        $data = Request::all('ids');
        if (isset($data['ids']) && !empty($data['ids'])) {
            foreach ($data['ids'] as $key => $value) 
            {
                $moduleObj = Modules::getModuleDataById($value);
                $isDelete = Modules::deleteRecordById($value);
                if ($isDelete) 
                {
                    if (file_exists(base_path('app/Http/Controllers/Powerpanel/') . $moduleObj->varModuleClass . '.php')) {
                        unlink(base_path('app/Http/Controllers/Powerpanel/') . $moduleObj->varModuleClass . '.php');
                    }

                    if (file_exists(base_path('app/') . $moduleObj->varModelName . '.php')) {
                        unlink(base_path('app/') . $moduleObj->varModelName . '.php');
                    }

                    // if (is_dir(resource_path('/views/powerpanel/') . $moduleObj->varModuleName)) {
                    //     unlink(resource_path('/views/powerpanel/') . $moduleObj->varModuleName);
                    // }

                    if (file_exists(base_path('/public_html/resources/pages/scripts/') . $moduleObj->varModuleName . '-validations.js')) {
                        unlink(base_path('/public_html/resources/pages/scripts/') . $moduleObj->varModuleName . '-validations.js');
                    }
                }
            }
        }
        echo json_encode($isDelete);
        exit;
    }

}
