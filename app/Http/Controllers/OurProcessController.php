<?php
namespace App\Http\Controllers;
use App\OurProcess;
use Config;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


class OurProcessController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads Our Process list view
		 * @return  View
		 * @since   2020-02-28
		 * @author  NetQuick
		 */
		public function index() 
		{
			$rangeFilter['from']       	= (null !== Request::get('dateRangeFrom')) ? Request::get('dateRangeFrom') : null;
            $rangeFilter['to']         	= (null !== Request::get('dateRangeTo')) ? Request::get('dateRangeTo') : null;
            $filterArr['rangeFilter']  	= $rangeFilter;
            $filterArr['searchFilter'] 	= (null !== Request::get('searchFilter')) ? Request::get('searchFilter') : null;
            $currentPage               	= (null !== (Request::get('page'))) ? Request::get('page') : 1;
            $paginate 					= 8;
            $ourprocessArr            	= OurProcess::getHomeList($paginate, $currentPage);
            $data                      	= array();
            $data['ourprocessArr']    	= $ourprocessArr;
            return view('ourprocess', $data);
        }
	}