<?php
namespace App\Http\Controllers;
use App\OurWorks;
use Config;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


class OurWorksController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads Our Work list view
		 * @return  View
		 * @since   2020-02-28
		 * @author  NetQuick
		 */
		public function index() 
		{			
			$rangeFilter['from']       	= (null !== Request::get('dateRangeFrom')) ? Request::get('dateRangeFrom') : null;
            $rangeFilter['to']         	= (null !== Request::get('dateRangeTo')) ? Request::get('dateRangeTo') : null;
            $filterArr['rangeFilter']  	= $rangeFilter;
            $filterArr['searchFilter'] 	= (null !== Request::get('searchFilter')) ? Request::get('searchFilter') : null;
            $currentPage               	= (null !== (Request::get('page'))) ? Request::get('page') : 1;
            $paginate 					= 3;
			$ourworksArr            	= OurWorks::getOurFrontList($paginate, $currentPage);			
            $data                      	= array();
            $data['ourworksArr']    	= $ourworksArr;
            return view('ourwork', $data);
		}
				/**
		* This method loads services detail view
		* @param   Alias of record
		* @return  View
		* @since   2020-02-07
		* @author  NetQuick
		*/ 
		public function detail($alias) 
		{
			$id      = slug::resolve_alias($alias);
			$service = OurWorks::getFrontDetail($id);
			if (!empty($service)) {
				
				$metaInfo                = array('varMetaTitle' => $service->varMetaTitle, 'varMetaKeyword' => $service->varMetaKeyword, 'varMetaDescription' => $service->varMetaDescription);
				$data                    = array();				
				$breadcrumb              = [];
				$segmentArr  = Request::segments();
				$url = '';
				foreach ($segmentArr as $key => $value) 
				{
					$url .= $value.'/';
					$breadcrumb[$key]['title'] =  ucwords(str_replace('-', ' ', $value));
					$breadcrumb[$key]['url'] =  rtrim($url,'/');
				} 

				$similarServices =  OurWorks::getSimilarRecordList($service->id);   
				$data['service']         = $service;
				$data['alias']           = $alias;
				$data['similarServices'] = $similarServices;
				$data['metaInfo']        = $metaInfo;
				$data['breadcrumb']      = $breadcrumb;							

				if(isset($service->fkIntImgId) && !empty($service->fkIntImgId)) {
					$imageArr = explode(',', $service->fkIntImgId);
					view()->share('SHARE_IMG', $imageArr[0]); 
				}            

				if (isset($service->varMetaTitle) && !empty($service->varMetaTitle)) {
					view()->share('META_TITLE', $service->varMetaTitle);
				}
				
				if (isset($service->varMetaKeyword) && !empty($service->varMetaKeyword)) {
					view()->share('META_KEYWORD', $service->varMetaKeyword);
				}
				if (isset($service->varMetaDescription) && !empty($service->varMetaDescription)) {
					view()->share('META_DESCRIPTION', substr(trim($service->varMetaDescription), 0, 500));
				}	
				// echo '<pre>'; print_r($data);die;
				return view('our-work-detail', $data);				
				}else{
					abort(404);
				}
		}
	}