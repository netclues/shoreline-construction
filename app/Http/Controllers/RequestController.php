<?php
namespace App\Http\Controllers;
use Config;
use Request;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\Rules\ValidRecaptcha;
use App\Rules\ValidateBadWord;
use App\RequestLead;
use App\ServiceCategory;
use App\Helpers\Email_sender;
use App\Helpers\MyLibrary;
use App\NewsletterLead;
use Crypt;
use File;


class RequestController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads Contactus list view
		 * @return  View
		 * @since   2020-02-07
		 * @author  NetQuick
		 */
		public function create() 
		{
            $deviceType = Config::get('Constant.DEVICE');
            
            $ServiceCategory  = ServiceCategory::getFrontList()->toArray();
            $ServiceCategory[' '] 	= '--Select Area Of Interest--';
            asort($ServiceCategory);
            

            $data = ['breadcrumb' => $this->breadcrumb,
            'deviceType' => $deviceType,
            'ServiceCategory' => $ServiceCategory];
            //echo '<pre>';print_r($ServiceCategory);die;
			return view('request-a-quote', $data);
		}


		/**
		* This method stores Contactus leads
		* @param   NA
		* @return  Redirection to Thank You page
		* @since   2020-02-07
		* @author  NetQuick
		*/
		public function store() {
            $data = Request::all();
            $messsages = array(
                'first_name.required' => 'Name field is required',
                'first_name.handle_xss' => 'Please enter valid input',
                'first_name.no_url' => 'URL is not allowed',
                'user_message.handle_xss' => 'Please enter valid input',
                'user_message.valid_input' => 'Please enter valid input',
                'user_message.no_url' => 'URL is not allowed',
                // 'area_of_interest.required' => 'area_of_interest is required',
                'area_of_interest.handle_xss' => 'Please enter valid input',
                'area_of_interest.valid_input' => 'Please enter valid input',
                'area_of_interest.no_url' => 'URL is not allowed',
                'contact_email.required' => 'Email is required',
                'g-recaptcha-response.required' => 'Captcha is required',
                'phone_number.required' => 'Phone is required',
            );

            $rules = array(
                'first_name' => ['required', 'handle_xss', 'no_url', new ValidateBadWord],
                'contact_email' => 'required|email',
                'user_message' => ['handle_xss', 'no_url', new ValidateBadWord],
                'area_of_interest' => ['handle_xss', 'no_url', new ValidateBadWord],    
            );

            if (isset($data['phone_number'])) {
                $rules['phone_number'] = 'required';
            }
            // if (isset($data['area_of_interest'])) {
            //     $rules['area_of_interest'] = 'required';
            // }

            $rules['g-recaptcha-response'] = ['required', new ValidRecaptcha];

            $validator = Validator::make($data, $rules, $messsages);

        if ($validator->passes()) {
            
            $contactus_lead = new RequestLead;
            $first_name = strip_tags($data['first_name']);
            $last_name = strip_tags($data['last_name']);
            $varName = $first_name . ' '.$last_name;    
            $contactus_lead->varName = $varName;
            $contactus_lead->varEmail = MyLibrary::getEncryptedString($data['contact_email']);

            if (isset($data['area_of_interest'])) {
                $contactus_lead->varAreaOfInterest = MyLibrary::getEncryptedString($data['area_of_interest']);
            } else {
                $contactus_lead->varAreaOfInterest = '';
            }
            if (isset($data['phone_number'])) {
                $contactus_lead->varPhoneNo = MyLibrary::getEncryptedString($data['phone_number']);
            } else {
                $contactus_lead->varPhoneNo = '';
            }
            if (isset($data['user_message'])) {
                $contactus_lead->txtUserMessage = MyLibrary::getEncryptedString(strip_tags($data['user_message']));
            } else {
                $contactus_lead->txtUserMessage = '';
            }
            // if (isset($data['service_id'])) {
            //     $contactus_lead->fkIntServiceId = $data['service_id'];
            // } else {
            //     $contactus_lead->fkIntServiceId = null;
            // }
            // echo '<pre>'; print_r($contactus_lead);die;
            $contactus_lead->varIpAddress = MyLibrary::get_client_ip();
            $contactus_lead->save();

            /*Start this code for message*/
            if (!empty($contactus_lead->id)) {
                $recordID = $contactus_lead->id;
                
                Email_sender::requestQuote($data, $contactus_lead->id);

                if (File::exists(app_path() . '/NewsletterLead.php')) {
                    if (isset($data['subscribe']) && $data['subscribe'] == "on") {

                        $emalExists = NewsletterLead::getRecords()->publish()->deleted()->where('varEmail', "=", Mylibrary::getEncryptedString($data['contact_email']))->first();
                        if (empty($emalExists)) {
                            $subscribeArr = [];
                            $subscribeArr['varEmail'] = Mylibrary::getEncryptedString($data['contact_email']);
                            $subscribeArr['varName'] = strip_tags($data['first_name']);
                            // $subscribeArr['varAreaOfInterest'] = 
                            $subscribeArr['varIpAddress'] = MyLibrary::get_client_ip();
                            $subscribeArr['created_at'] = date('Y-m-d h:i:s');
                            $subscribe = NewsletterLead::insertGetId($subscribeArr);

                            $newsLetterData = NewsletterLead::getRecords()->publish()->deleted()->checkRecordId($subscribe);
                            if ($newsLetterData->count() > 0) {
                                $newsLetterData = $newsLetterData->first()->toArray();
                                $id = Crypt::encrypt($newsLetterData['id']);
                                Email_sender::newsletter($newsLetterData, $id);
                            }

                        } else {

                            if ($emalExists->chrSubscribed == "N") {
                                $newsLetterData = $emalExists->toArray();
                                $id = Crypt::encrypt($newsLetterData['id']);
                                Email_sender::newsletter($newsLetterData, $id);
                            }
                        }
                    }
                }

                if (Request::ajax()) {
                    return json_encode(['success' => 'We have received your request. Someone from our staff will contact you shortly.']);
                } else {
                    return redirect()->route('thank-you')->with(['form_submit' => true, 'message' => 'We have received your request. Someone from our staff will contact you shortly.']);
                }

            } else {
                return redirect('/');
            }

} else {

    //return contact form with errors
    if (!empty($data['back_url'])) {
        return redirect($data['back_url'] . '#contact_form')->withErrors($validator)->withInput();
    } else {
        return Redirect::route('request-a-quote')->withErrors($validator)->withInput();
    }

}
		}
}