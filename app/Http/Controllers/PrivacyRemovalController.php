<?php
namespace App\Http\Controllers;

use App\Helpers\Email_sender;
use App\Helpers\MyLibrary;
use App\PrivacyRemovalLead;
use Illuminate\Support\Facades\Redirect;
use Request;
use Validator;
use Config;


class PrivacyRemovalController extends FrontController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('privacy-removal-form');
    }

    public function handlePost()
    {

        $data = Request::all();

        $messsages = array(
            'first_name.required' => 'Please enter first name',
            'first_name.handle_xss' => 'Please enter valid input',
            'first_name.no_url' => 'URL is not allowed',
            'last_name.handle_xss' => 'Please enter valid input',
            'last_name.no_url' => 'URL is not allowed',
            'email.required' => 'Email is required',
            'reason.handle_xss' => 'Please enter valid input',
            'reason.valid_input' => 'Please enter valid input',
            'reason.no_url' => 'URL is not allowed',
            'authorized.required' => 'Please select checkbox',
            'g-recaptcha-response.required' => 'Captcha is required',
        );

        $rules = array(
            'first_name' => 'required|handle_xss|no_url',
            'last_name' => 'handle_xss|no_url',
            'email' => 'required|email',
            'authorized' => 'required',
            'reason' => 'handle_xss|no_url|bad_words',
            'g-recaptcha-response' => 'required',
        );

        $validator = Validator::make($data, $rules, $messsages);
        if ($validator->passes()) 
        {
            $privacyRemovalLead = new PrivacyRemovalLead;
            $privacyRemovalLead->varName = trim((!empty($data['first_name'])?strip_tags($data['first_name']):null) . ' ' . (!empty($data['last_name'])?strip_tags($data['last_name']):null));
            $privacyRemovalLead->varEmail = MyLibrary::getEncryptedString($data['email']);
            if (!empty($data['reason'])) {
                $privacyRemovalLead->txtReason = MyLibrary::getEncryptedString(strip_tags($data['reason']));
            } else {
                $privacyRemovalLead->txtReason = '';
            }
            $privacyRemovalLead->chrIsAuthorized = isset($data['authorized']) && !empty($data['authorized']) ? 'Y' : 'N';
            $privacyRemovalLead->varIpAddress = MyLibrary::get_client_ip();

            $privacyRemovalLead->save();

            if (!empty($privacyRemovalLead->id)) {
                $recordID = $privacyRemovalLead->id;
                Email_sender::privacyRemovalEmailConfirmation($data, $privacyRemovalLead->id);
                return redirect()->route('thank-you')->with(['form_submit' => true, 'message' => 'We have sent confirmation link to the entered email address. Please check your inbox and confirm you are the original requester.']);
            }

        } else {
            return Redirect::back()->withErrors($validator)->withInput();
        }
    }

    public function confirmEmailAddress()
    {
        if (Request::has('token')) {
            $id = MyLibrary::getLaravelDecryptedString(Request::get('token'));

            $leadsObj = PrivacyRemovalLead::where('id', $id)->first();
            if ($leadsObj->chrIsEmailVerified == 'N') {
                $verified = PrivacyRemovalLead::where('id', $id)->update(['chrIsEmailVerified' => 'Y']);
                if ($verified) {
                    Email_sender::privacyRemovalAdminEmail($leadsObj);
                }
                return redirect()->route('thank-you')->with(['form_submit' => true, 'message' => 'Thank you for your confirmation. We have received your request to remove your stored information permanently. We will remove all your information within two weeks. If you have any concern on this please contact on this email: ' . MyLibrary::getDecryptedString(Config::get('Constant.DEFAULT_ADMIN_EMAIL'))]);
            } else {
                abort(405, 'Oops! <br/> The link you are trying to access is no longer exist.');
            }
        } else {
            abort(405, 'Oops! <br/> The link you are trying to access is no longer exist.');
        }
    }

}
