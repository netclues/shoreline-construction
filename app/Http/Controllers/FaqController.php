<?php
namespace App\Http\Controllers;
		use App\Faq;
	use Config;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


class FaqController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads faq list view
		 * @return  View
		 * @since   2020-02-07
		 * @author  NetQuick
		 */
		public function index() 
		{
						$data           = array();
$faqArr         = Faq::getRecords();
$data['faqArr'] = $faqArr;
										return view('faq', $data);
					}



		}