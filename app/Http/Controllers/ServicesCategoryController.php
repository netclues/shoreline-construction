<?php
namespace App\Http\Controllers;

use App\Http\Traits\slug;
use App\ServiceCategory;
use App\Services;
use Request;


class ServicesCategoryController extends FrontController
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
    public function __construct()
    {
        parent::__construct();
    }

  /**
   * This method loads services list view
   * @return  View
   * @since   2018-09-19
   * @author  NetQuick
   */
    public function index()
    {
        $data     = array();
        $page     = 1;
        $paginate = 6;
        if (!empty(Request::get('page'))) {
            $page = Request::get('page');
        }
        $serviceCategory         = ServiceCategory::getCategoryList($paginate, $page);
        $data['serviceCategory'] = $serviceCategory;
        return view('service-category', $data);
    }
    
  /**
   * This method loads services detail view
   * @param   Alias of record
   * @return  View
   * @since   2018-09-19
   * @author  NetQuick
   */
    public function detail($alias)
    {
        $aliasArr = explode('/', $alias);
        foreach ($aliasArr as $key => $value) {
          slug::resolve_alias($value);
        }

        $alias        = end($aliasArr);
        $id           = slug::resolve_alias($alias);
        $paginate     = 6;
        $page         = 1;
        $categoryPage = 1;

        if (!empty(Request::get('page'))) {
            $page = Request::get('page');
        }

        if (!empty(Request::get('categoryPage'))) {
            $categoryPage = Request::get('categoryPage');
        }

        $serviceCategory = ServiceCategory::getFrontDetail($id);
        if (!empty($serviceCategory)) 
        {
            $subCategoryList         = ServiceCategory::getSubCategoryList($serviceCategory->id, $paginate, $categoryPage);
            $serviceObj              = Services::getListByCategory($serviceCategory->id, $paginate, $page);
            $metaInfo                = array('varMetaTitle' => $serviceCategory->varMetaTitle, 'varMetaKeyword' => $serviceCategory->varMetaKeyword, 'varMetaDescription' => $serviceCategory->varMetaDescription);

            $data                    = array();

            $breadcrumb              = [];
            $segmentArr  = Request::segments();

            $url = '';
            foreach ($segmentArr as $key => $value) 
            {
                $url .= $value.'/';
                $breadcrumb[$key]['title'] =  ucwords(str_replace('-', ' ', $value));
                $breadcrumb[$key]['url'] =  rtrim($url,'/');
            }
            
            $data['serviceObj']      = $serviceObj;
            $data['alias']           = $alias;
            $data['metaInfo']        = $metaInfo;
            $data['breadcrumb']      = $breadcrumb;
            $data['subCategoryList'] = $subCategoryList;
            $data['serviceCategory'] = $serviceCategory;
            
            if (isset($serviceCategory->varMetaTitle) && !empty($serviceCategory->varMetaTitle)) {
                view()->share('META_TITLE', $serviceCategory->varMetaTitle);
            }

            if (isset($serviceCategory->varMetaKeyword) && !empty($serviceCategory->varMetaKeyword)) {
                view()->share('META_KEYWORD', $serviceCategory->varMetaKeyword);
            }
            
            if (isset($serviceCategory->varMetaDescription) && !empty($serviceCategory->varMetaDescription)) {
                view()->share('META_DESCRIPTION', substr(trim($serviceCategory->varMetaDescription), 0, 500));
            }
            
            return view('service-category-detail', $data);
        } else {
            abort(404);
        }
    }
}
