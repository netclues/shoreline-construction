<?php
namespace App\Http\Controllers;
use App\CmsPage;
use Request;
use App\Http\Requests;
use App\Http\Traits\slug;

class PagesController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/

	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads CMS Sitemap list view
		 * @return  View
		 * @since   2020-02-07
		 * @author  NetQuick
		 */
        public function my(){
			$data= array('name'=> 'xyz33','date'=>date('d-m-Y'));
			return view ('home')->with($data);
		}

		public function process(){
			return view ('ourprocess');
		}

		public function work(){
			return view ('ourwork');
		}

		public function design(){
			return view ('homedesign');
		}

		public function allplans(){
			return view ('viewallplans');
		}
		public function singlestorey(){
			return view ('singlestoreyplans');
		}
		public function doublestorey(){
			return view ('doublestoreyplans');
		}
		public function duplex(){
			return view ('duplexplans');
		}
		public function container(){
			return view ('containerhomes');
		}
		public function otherplans(){
			return view ('otherplans');
		}
		public function whyus(){
			return view ('whyus');
		}


		public function index() 
		{
			$pagename = Request::segment(1);
			$aliasId = slug::resolve_alias($pagename);

			$pageContent = CmsPage::getPageContentByPageAlias($aliasId);
			$CONTENT = "<h1>No Content Available</h1>";
			if(!empty($pageContent->txtDescription)){
				$CONTENT = $pageContent->txtDescription;
			}
			$data = ['CONTENT'=>$CONTENT,'breadcrumb'=>$this->breadcrumb];		
			return view('pages', $data);			
		}		
}