<?php
namespace App\Http\Controllers;
use App\BuildingCategory;
use App\BuildingHome;
use Config;
use App\Helpers\MyLibrary;
use App\Http\Traits\slug;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;


class BuildingCategoryController extends FrontController {
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct() {
		parent::__construct();
	}

		/**
		 * This method loads testimonial list view
		 * @return  View
		 * @since   2020-03-02
		 * @author  NetQuick
		 */
    public function index()
    {
        $data     = array();
        $page     = 1;
        $paginate = 8;
        if (!empty(Request::get('page'))) {
            $page = Request::get('page');
        }
        $buildingcategory         = BuildingCategory::getCategoryList($paginate, $page);
        $data['buildingcategory'] = $buildingcategory;
        return view('homedesign', $data);
    }
    
  /**
   * This method loads home Design detail view
   * @param   Alias of record
   * @return  View
   * @since   2018-09-19
   * @author  NetQuick
   */
    public function detail($alias)
    {   	
    	$ordValue = Request::get('varOrder');
    	$aliasArr = explode('/', $alias);
        foreach ($aliasArr as $key => $value) {
          slug::resolve_alias($value);
        }
        // $filterArr                 	= [];
        // $filterArr['ordFilter']    	= (null !== $ordValue) ? $ordValue : 'ASC';

        $alias        = end($aliasArr);
        $id           = slug::resolve_alias($alias);
        $paginate     = 8;
        $page         = 1;
        $categoryPage = 1;
        $ordFilter = 'ASC';

        if (!empty(Request::get('page'))) {
            $page = Request::get('page');
        }

        if (!empty(Request::get('categoryPage'))) {
            $categoryPage = Request::get('categoryPage');
        }
        if (!empty($ordFilter)) {
            $ordFilter = (null !== $ordValue) ? $ordValue : 'ASC';
        }

        $buildingcategory = BuildingCategory::getFrontDetail($id);
        
        
        if (!empty($buildingcategory)) 
        {
        	$buildingothercategory = BuildingCategory::getFrontCategories($buildingcategory->id);
        	
        	
            $subCategoryList         = BuildingCategory::getSubCategoryList($buildingcategory->id, $paginate, $categoryPage);
            $buildingcategoryObj              = BuildingHome::getListByCategory($buildingcategory->id, $paginate, $page,$ordFilter);
            $totalCount = $buildingcategoryObj->count();        	
            $metaInfo                = array('varMetaTitle' => $buildingcategory->varMetaTitle, 'varMetaKeyword' => $buildingcategory->varMetaKeyword, 'varMetaDescription' => $buildingcategory->varMetaDescription);

            $data                    = array();

            $breadcrumb              = [];
            $segmentArr  = Request::segments();

            $url = '';
            foreach ($segmentArr as $key => $value) 
            {
                $url .= $value.'/';
                $breadcrumb[$key]['title'] =  ucwords(str_replace('-', ' ', $value));
                $breadcrumb[$key]['url'] =  rtrim($url,'/');
            }
            
            $data['buildingcategoryObj']      = $buildingcategoryObj;
            $data['alias']           = $alias;
            $data['metaInfo']        = $metaInfo;
            $data['breadcrumb']      = $breadcrumb;
            // print_r($buildinghome);die;
            $data['subCategoryList'] = $subCategoryList;
            $data['buildingcategory'] = $buildingcategory;
            $data['totalCount'] = $totalCount;
            $data['buildingothercategory'] = $buildingothercategory;
            $data['ordFilter'] = $ordFilter;


            if(isset($buildingcategory->fkIntImgId) && !empty($buildingcategory->fkIntImgId)) {
		        $imageArr = explode(',', $buildingcategory->fkIntImgId);
		        view()->share('SHARE_IMG', $imageArr[0]); 
		    }    
		            
            if (isset($buildingcategory->varMetaTitle) && !empty($buildingcategory->varMetaTitle)) {
                view()->share('META_TITLE', $buildingcategory->varMetaTitle);
            }

            if (isset($buildingcategory->varMetaKeyword) && !empty($buildingcategory->varMetaKeyword)) {
                view()->share('META_KEYWORD', $buildingcategory->varMetaKeyword);
            }
            
            if (isset($buildingcategory->varMetaDescription) && !empty($buildingcategory->varMetaDescription)) {
                view()->share('META_DESCRIPTION', substr(trim($buildingcategory->varMetaDescription), 0, 500));
            }
            
            return view('building-category', $data);
        } else {
            abort(404);
        }
    }
}