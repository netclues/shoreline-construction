<?php
/**
 * The FrontController class handels Preloaded data for front side
 * configuration  process (ORM code Updates).
 * @package   Netquick powerpanel
 * @license   http://www.opensource.org/licenses/BSD-3-Clause
 * @version   1.1
 * @since     2017-08-09
 * @author    NetQuick
 */
namespace App\Http\Controllers;

use App\Alias;
use App\CmsPage;
use App\Helpers\MenuBuilder;
use App\Helpers\Page_hits;
use App\Helpers\time_zone;
use App\Http\Controllers\Controller;
use App\Http\Traits\slug;
use App\InnerBanner;
use App\Menu;
use Config;
use Cookie;
use File;
use Request;

class FrontController extends Controller
{

    use slug;
    protected $breadcrumb = [];
    protected $sitemap_content;

    public function __construct()
    {

        time_zone::time_zone();
        if (!Request::ajax()) {
            $menu_content = Menu::getFrontList();

            if (isset($menu_content[1]) && $menu_content[1][0]['menuType']['chrPublish'] == 'Y') {
                MenuBuilder::loadMenu($menu_content[1], 'headerMenu');
            }

            if (isset($menu_content[2]) && $menu_content[2][0]['menuType']['chrPublish'] == 'Y') {
                MenuBuilder::loadMenu($menu_content[2], 'footerMenu');

            }
            $this->sitemap_content = $menu_content;
        }

        $this->shareData();
    }
    public function cookiesPopupStore()
    {
        Cookie::queue('cookiesPopupStore', 'cookiesPopupStore', 525600);
        return 'CookiesSetprivacy';
    }

    public function setInnerBanner($pageObj = false)
    {
        $innerBannerArr = [];
        $innerBanner = array();

        $innerBannerArr['currentPageTitle'] = (isset($pageObj->varTitle) ? $pageObj->varTitle : Request::segment(1));

        if (isset($pageObj->id)) 
        {
            $AliasId = slug::resolve_alias_for_routes(Request::segment(1));
            $moduleID = Alias::getModuleByAliasId($AliasId);
            if (null !== Request::segment(1) && null == Request::segment(2)) {
                if (isset($pageObj->id)) {
                    $innerBanner = InnerBanner::getInnerBannerListingPage($pageObj->id, $moduleID->intFkModuleCode);
                }
            }
            
            if (null !== Request::segment(2)) 
            {
                $id = slug::resolve_alias_for_routes(Request::segment(2));
                $MODEL = '\\App\\' . Config::get('Constant.MODULE.MODEL_NAME');
                if (is_numeric($id)) {
                    $recordID = $MODEL::getRecordIdByAliasID($id);
                    if (isset($recordID->id)) {
                        $recordID = (string) $recordID->id;
                        $innerBanner = InnerBanner::getInnerBannerList($recordID, Config::get('Constant.MODULE.ID'));
                        if (empty($innerBanner)) {
                            $innerBanner = InnerBanner::getInnerBannerListingPage($pageObj->id, $moduleID->intFkModuleCode);
                        }
                    }
                }
            }

        }

        $innerBannerArr['inner_banner_data'] = $innerBanner;
        return $innerBannerArr;
    }

    public function shareData()
    {
        $shareData = [];
        $pageCms = null;
        $cmsPageId = slug::resolve_alias_for_routes(!empty(Request::segment(1)) ? Request::segment(1) : 'home');

        if (is_numeric($cmsPageId)) {
            $pageCms = CmsPage::getPageByPageId($cmsPageId);
        }

        if (!Request::ajax()) {
            if (isset($pageCms->varTitle) && strtolower($pageCms->varTitle) != 'home') {
                $shareData = $this->setInnerBanner($pageCms);
            } else {
                $shareData = $this->setInnerBanner();
            }

            if (File::exists(app_path() . '/Advertise.php') != null) {
                $advertise = \App\Advertise::getFrontRecordsByPage($cmsPageId);
                if (!empty($advertise)) {
                    foreach ($advertise as $ad) {
                        $sectionArr = unserialize($ad->txtPosition);
                        if (in_array('top', $sectionArr)) {
                            $shareData['topAd'][] = $ad;
                        }
                        if (in_array('section_100', $sectionArr)) {
                            $shareData['section_100'][] = $ad;
                        }
                        if (in_array('section_100_grid', $sectionArr)) {
                            $shareData['section_100_grid'][] = $ad;
                        }
                        if (in_array('section_107', $sectionArr)) {
                            $shareData['section_107'][] = $ad;
                        }
                    }
                }
            }

            if (File::exists(app_path() . '/Testimonial.php') != null) {
                $testimonialObj = \App\Testimonial::getLatestList();
                if (!empty($testimonialObj)) {
                    $shareData['testimonialArr'] = $testimonialObj;
                }
            }

            if (File::exists(app_path() . '/ContactInfo.php') != null) {
                $contacts = \App\ContactInfo::getContactDetails();
                $shareData['objContactInfo'] = (!empty($contacts))?$contacts:'';
            }

            if (!in_array(Request::segment(1), ['login', 'logout'])) {
                Page_hits::insertHits($pageCms);
            }
        }

        $shareData['cmsPage'] = $pageCms;
        $shareData['currentPageTitle'] = (isset($pageCms->varTitle) ? $pageCms->varTitle : Request::segment(1));
        $shareData['META_TITLE'] = (!empty($pageCms->varMetaTitle) ? $pageCms->varMetaTitle : ucfirst(Request::segment(1)));
        $shareData['META_DESCRIPTION'] = (!empty($pageCms->varMetaDescription) ? substr(trim($pageCms->varMetaDescription), 0, 200) : Config::get('Constant.DEFAULT_META_DESCRIPTION'));
        $shareData['PAGE_CONTENT'] = (!empty($pageCms->txtDescription) ? $pageCms->txtDescription : Config::get('Constant.PAGE_CONTENT'));
        $shareData['APP_URL'] = Config::get('Constant.ENV_APP_URL');
        $shareData['SHARE_IMG'] = Config::get('Constant.FRONT_LOGO_ID');
        
        view()->share($shareData);
    }
}
