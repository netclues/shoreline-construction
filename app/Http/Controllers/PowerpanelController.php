<?php
namespace App\Http\Controllers;

use App\Alias;
use App\Helpers\MyLibrary;
use App\Helpers\resize_image;
use App\Helpers\time_zone;
use App\Http\Controllers\Controller;
use App\Http\Traits\slug;
use App\TermsConditions;
use App\User;
use Artisan;
use Auth;
use File;
use Request;
use Zip;

class PowerpanelController extends Controller
{
    use slug;
    protected $breadcrumb = [];
    protected $commonVariableArr = [];
    protected $module_code;
    public $metaLength;
    public $metaDescriptionLength;

    public function __construct()
    {

        time_zone::time_zone();
        $this->commonVariableArr['userId'] = Auth::id();
        $this->commonVariableArr['ipAddress'] = MyLibrary::get_client_ip();
        $this->metaLength = 100;
        $this->metaDescriptionLength = 200;
        $this->logo();

        $this->middleware(function ($request, $next) {
            //  $userIsAdmin = false;
            $superAdmin = false;
            if (Auth::user()->hasRole('netquick_admin') || Auth::user()->hasRole('netclues_admin')) {
                $superAdmin = true;
            }
            // var_dump($superAdmin);

            if ($superAdmin === false) {
                $termsAccepted = 'N';
                $userId = Auth::id();
                $accepted = TermsConditions::getRecord($userId);
                if (!empty($accepted)) {
                    $termsAccepted = $accepted->chrAccepted;
                }
            } else {
                $termsAccepted = 'Y';
            }
            view()->share('termsAccepted', $termsAccepted);
            //  view()->share('userIsAdmin', $userIsAdmin);
            return $next($request);
        });

        view()->share('allLocale', $this->getLang());
        view()->share('metaLength', $this->metaLength);
        view()->share('metaDescriptionLength', $this->metaDescriptionLength);
    }

    public function logo()
    {
        $user_data = Auth::user();

        $sessionEmailID = '';
        if (!empty($user_data->email)) {
            $sessionEmailID = $user_data->email;
        }
        if (!empty($sessionEmailID)) {
            if (!empty($user_data->fkIntImgId)) {
                $logo_url = resize_image::resize($user_data->fkIntImgId);
                view()->share('User_logo_url', $logo_url);
            } else {
                $logo_url = '';
                view()->share('User_logo_url', $logo_url);
            }
        }
    }

    public function getLang()
    {
        $langArray = array();
        $directory = base_path('resources/lang');
        $dir = File::directories($directory);
        foreach ($dir as $directory) {
            array_push($langArray, str_replace('//', '', explode('/lang', $directory)[1]));
        }
        return $langArray;
    }

    /**
     * This method generates alias
     * @return  JSON formata data of alias
     * @since   2017-10-30
     * @author  NetQuick
     */
    public function aliasGenerate()
    {
        $alias = Request::get('alias');
        $oldAlias = Request::get('oldAlias');
        $moduleID = Request::get('moduleCode');

        $slug = slug::create_slug($alias, false);
        $alreadyExists = false;
        if (!empty($oldAlias) && $oldAlias != '') {
            if ($slug != $oldAlias) {
                $objResult = Alias::getAliasByModuleId($slug, $moduleID);
                if (!empty($objResult) && isset($objResult->intFkModuleCode)) {
                    if ($objResult->intFkModuleCode == $moduleID) {
                        $slug = slug::create_slug($alias, true);
                        $alreadyExists = true;
                    }
                }
                $response = array('alias' => $slug, 'alreadyExists' => $alreadyExists);
            } else {
                $response = array('alias' => $oldAlias, 'alreadyExists' => $alreadyExists);
            }

        } else {

            $objResult = Alias::getAliasByModuleId($slug, $moduleID);
            if (!empty($objResult) && isset($objResult->intFkModuleCode)) {
                if ($objResult->intFkModuleCode == $moduleID) {
                    $slug = slug::create_slug($alias, true);
                    $alreadyExists = true;
                }
            }
            $response = array('alias' => $slug, 'alreadyExists' => $alreadyExists);

        }
        echo json_encode($response);
    }

    /**
     * This method generates alias
     * @return  JSON formata data of alias
     * @since   2017-10-30
     * @author  NetQuick
     */
    public function updateAlias()
    {
        $newAls = Request::get('newAls');
        $oldAls = Request::get('oldAls');
        $moduleID = Request::get('moduleCode');

        $response = false;

        $result = Alias::updateAlias($oldAls, $newAls, $moduleID);
        if ($result) {
            $response = array('newAls' => $newAls, 'oldAls' => $oldAls);
        }
        echo json_encode($response);
    }

    /**
     * This method generates seo content
     * @return  Meta values
     * @since   2017-10-30
     * @author  NetQuick
     */
    public function generateSeoContent()
    {
        $fromajax = true;
        $title = (Request::get('title') == "") ? Request::get('name') : Request::get('title');
        $description = Request::get('description');
        
        $seoData = MyLibrary::generateSeocontent($title, $description, $fromajax);
        return json_encode($seoData);
    }

    public function install($file)
    {
        $zip = Zip::open(base_path('storage/' . $file . '.zip'));
        $zip->extract(base_path('storage/' . $file . '/'));

        File::copyDirectory(base_path('storage/' . $file . '/app'), base_path('app/'));
        File::copyDirectory(base_path('storage/' . $file . '/database'), base_path('database/'));
        File::copyDirectory(base_path('storage/' . $file . '/public_html'), base_path('public_html/'));
        File::copyDirectory(base_path('storage/' . $file . '/resources'), base_path('resources/'));
        File::deleteDirectory(base_path('storage/' . $file));

        ob_clean();
        ob_end_flush();
        ini_set("output_buffering", "0");
        ob_implicit_flush(true);
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $command = "D: && cd " . base_path('/') . " && composer dump-autoload";
        $proc = popen($command, 'r');
        while (!feof($proc)) {
            $this->echoEvent(fread($proc, 4096));
        }
        $this->echoEvent("finish");

        Artisan::call('cache:clear');
        Artisan::call('migrate');
        Artisan::call('db:seed', ['--class' => 'ModuleEntryTableSeeder']);
        Artisan::call('db:seed', ['--class' => 'PermissionTableSeeder']);
    }

    public function echoEvent($datatext)
    {
        echo "data: " . implode("\ndata: ", explode("\n", $datatext)) . "\n\n";
    }

    public function uploadImage()
    {

        $response = array();

        $file = Request::file('upload');
        $timestamp = date('YmdHis');
        $pathinfo = pathinfo($file->getClientOriginalName());
        $extension = $pathinfo['extension'];

        $name = self::clean($pathinfo['filename']) . '-' . $timestamp;
        $file->move(public_path() . '/assets/images/ckeditor/', $name . '.' . $extension);

        $response['url'] = url('/assets/images/ckeditor') . '/' . $name . '.' . $extension;

        return json_encode($response);

    }

    public static function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-.]/', '', $string); // Removes special chars.
    }
    
    public function reCreateAlias($title)
    {
        $slug      = slug::create_slug($title, false);
        return $slug;
    }

}
