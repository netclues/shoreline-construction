<?php
namespace App\Http\Traits;

use App\Alias;
use Config;
use \Cviebrock\EloquentSluggable\Services\SlugService;

trait slug
{
    public static function create_slug($title = false, $unique = false)
    {
        $response = null;
        if ($title != null) {
            $response = SlugService::createSlug(Alias::class, 'varAlias', $title, ['unique' => $unique]);
        }
        return $response;
    }

    public static function resolve_alias($alias)
    {

        $response = null;
        try 
        {
            $moduleID  = Config::get('Constant.MODULE.ID');
            $objResult = Alias::getAliasByModuleId($alias,$moduleID);
           
            if (!empty($objResult)) {
                $response = $objResult->id;
            } else {
                abort(404);
            }
        } catch (Exception $e) {

            abort(403, $e->getMessage() . PHP_EOL);
        }

        return $response;
    }

    public static function resolve_alias_for_routes($alias)
    {
        $response = null;
        $objResult = Alias::getAlias($alias);
        if (!empty($objResult)) {
            $response = $objResult->id;
        } else {
            $response = $alias;
        }
        return $response;
    }

}
