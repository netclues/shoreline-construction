<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Config;
class CheckClientRole {
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next) 
  {
     $client_role  = Auth::user()->hasRole('client_roles');
     if($client_role){
        return abort(403);
     }
    $response = $next($request);
   return $response; 
    
  }
}