<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;
class CheckUserStatus {
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next) 
  {
       
     $user = Auth::user();
     $userCheck= User::getRecordByIdPublicOrDelete($user->id);
      if(empty($userCheck->id)){
          Auth::logout();
          return redirect('powerpanel');
     }
   $response = $next($request);
   return $response; 
    
  }
}