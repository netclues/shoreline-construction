<?php
namespace App\Exports;
use App\RestaurantReservations;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Request;
use Config;
use App\Helpers\MyLibrary;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

class RestaurantReservationLeadExport  extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromView, ShouldAutoSize, WithCustomValueBinder
{
    public function view(): View
    {

        if (Request::get('export_type') == 'selected_records') {
            if (null !== Request::get('delete')) {
                $selectedIds = Request::get('delete');
            } else {
                $selectedIds = false;
            }
            $arrResults = RestaurantReservations::getListForExport($selectedIds);
        } else {
            $arrResults = RestaurantReservations::getListForExport();
        }

        if (count($arrResults) > 0) {
            return view('powerpanel.restaurant_reservations_lead.excel_format', ['reservationLead' => $arrResults]);
        }
    }

}
