<?php
namespace App\Exports;
use App\PrivacyRemovalLead;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Request;
use Config;
use Schema;
use DB;


class PrivacyLeadExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        if (Request::get('export_type') == 'selected_records') {
            $selectedIds = array();
            if (null !== Request::get('delete')) {
                $selectedIds = Request::get('delete');
            }
            $arrResults = PrivacyRemovalLead::getListForExport($selectedIds);

        } else {
            $filterArr['searchFilter'] = !empty(Request::get('searchValue')) ? Request::get('searchValue') : '';
            $arrResults = PrivacyRemovalLead::getListForExport(false, $filterArr);
        }        

        if (count($arrResults) > 0) {

            foreach($arrResults as $key => $value) 
            {
                $moduleList = $this->getModulesName($key, $value->varEmail);
                $linkedModule = '';
                if (!empty($moduleList)) {
                    foreach ($moduleList as $m_key => $m_value) {
                        $linkedModule .=  ' '.$m_value['moduleName'] . '('. $m_value['count'].'),';
                    }

                } else {
                    $linkedModule .= '-';
                }
            }

            return view('powerpanel.privacy_removal_leads.excel_format', ['PrivacyLead' => $arrResults,'linkedModule' => rtrim($linkedModule,',')]);
        }
    }

    public function getModulesName($key, $email)
    {

        $moduleArray = array();
        if (Schema::hasTable('contact_lead')) {
            $contactLeads = DB::table('contact_lead')->where('varEmail', $email)->count();
            if ($contactLeads > 0) {
                $moduleArray['contact_lead']['moduleName'] = 'Contact Us';
                $moduleArray['contact_lead']['moduleSlug'] = 'contact-us';
                $moduleArray['contact_lead']['count'] = $contactLeads;
            }
        }

        if (Schema::hasTable('newsletter_lead')) {
            $newsLetterLeads = DB::table('newsletter_lead')->where('varEmail', $email)->count();
            if ($newsLetterLeads > 0) {
                $moduleArray['newsletter_lead']['moduleName'] = 'Newsletter Leads';
                $moduleArray['newsletter_lead']['moduleSlug'] = 'newsletter-lead';
                $moduleArray['newsletter_lead']['count'] = $newsLetterLeads;
            }
        }

        if (Schema::hasTable('appointment_lead')) {
            $appointmentLeads = DB::table('appointment_lead')->where('varEmail', $email)->count();
            if ($appointmentLeads > 0) {
                $moduleArray['appointment_lead']['moduleName'] = 'Appointment Leads';
                $moduleArray['appointment_lead']['moduleSlug'] = 'appointment-lead';
                $moduleArray['appointment_lead']['count'] = $appointmentLeads;
            }
        }

        if (Schema::hasTable('event_lead')) {
            $eventLeads = DB::table('event_lead')->where('varEmail', $email)->count();
            if ($eventLeads > 0) {
                $moduleArray['event_lead']['moduleName'] = 'Event Leads';
                $moduleArray['event_lead']['moduleSlug'] = 'event-lead';
                $moduleArray['event_lead']['count'] = $eventLeads;
            }
        }

        if (Schema::hasTable('email_log')) {
            $emailLogs = DB::table('email_log')->where('txtTo', $email)->count();
            if ($emailLogs > 0) {
                $moduleArray['email_log']['moduleName'] = 'Email Logs';
                $moduleArray['email_log']['moduleSlug'] = 'email-log';
                $moduleArray['email_log']['count'] = $emailLogs;
            }
        }

        return $moduleArray;

    }

}
