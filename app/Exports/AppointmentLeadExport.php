<?php
namespace App\Exports;

use App\AppointmentLead;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Request;
use App\Services;
use Config;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

class AppointmentLeadExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements FromView, ShouldAutoSize, WithCustomValueBinder
{
    public function view(): View
    {
        if (Request::get('export_type') == 'selected_records') {
            $selectedIds = array();
            if (null !== Request::get('delete')) {
                $selectedIds = Request::get('delete');
            }
            $arrResults = AppointmentLead::getListForExport($selectedIds);
        } else {
            $arrResults = AppointmentLead::getListForExport();
        }

        if (count($arrResults) > 0) {

            foreach ($arrResults as $key => $value) {

                if (!empty($value->fkIntServiceId) || $value->fkIntServiceId != null) {
                    $serviceIds = array($value->fkIntServiceId);
                    $servicesNames = Services::getServicesNameByServicesId($serviceIds)->toArray();
                    if (!empty($servicesNames)) {
                        $serviceList = '';
                        foreach ($servicesNames as $selService) {
                            $serviceList .= $selService['varTitle'] . ',';
                        }
                        $serviceList = rtrim($serviceList, ',');
                        $services = $serviceList;
                    } else {
                        $services = '-';
                    }
                } else {
                    $services = '-';
                }
            }
            return view('powerpanel.appointment_lead.excel_format', ['appointmentLeads' => $arrResults, 'services' => $services]);
        }
    }

}
