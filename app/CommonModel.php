<?php
namespace App;

use App\Helpers\MyLibrary;
use Config;
use DB;
use Illuminate\Database\Eloquent\Model;
use Schema;


class CommonModel extends Model
{
		/**
		 * This method handels insert of event record
		 * @return  Object
		 * @since   2016-07-14
		 * @author  NetQuick
		 */
		public static function addRecord($data = false, $modelNameSpace = false)
		{
				$response = false;
				if ($data != false && !empty($data)) {
						if ($modelNameSpace == false) {
								$modelNameSpace = MyLibrary::getModelNameSpace();
						}
						$recordId = $modelNameSpace::insertGetId($data);
						if ($recordId > 0) {
								$response = $recordId;
						}
				}
				return $response;
		}
		
		public static function updateRecords($whereCondArr = false, $data = false, $orWhereCondArr = false, $modelNameSpace = false)
		{
				$response = false;
				if (!empty($whereCondArr) && !empty($data)) {
						if ($modelNameSpace == false) {
								$modelNameSpace = MyLibrary::getModelNameSpace();
						}
						$update = $modelNameSpace::where($whereCondArr);
						if ($orWhereCondArr != false) {
								$update = $update->orWhere($orWhereCondArr);
						}
						$update->update($data);
						$response = $update;

				}
				return $response;
		}

		public static function updateMultipleRecords($whereCondArr = false, $data = false, $orWhereCondArr = false, $modelNameSpace = false)
		{
				$response = false;
				if (!empty($whereCondArr) && !empty($data)) {
						if ($modelNameSpace == false) {
								$modelNameSpace = MyLibrary::getModelNameSpace();
						}
						$update = $modelNameSpace::whereIn('id', $whereCondArr);
						
						if ($orWhereCondArr != false) {
								$update = $update->orWhere($orWhereCondArr);
						}
						$update->update($data);
						$response = $update;
				}
				return $response;
		}

		public static function deleteMultipleRecords($whereCondArr = false, $data = false, $orWhereCondArr = false, $modelNameSpace = false)
		{
				$response = false;
				if (!empty($whereCondArr)) {
						if ($modelNameSpace == false) {
								$modelNameSpace = MyLibrary::getModelNameSpace();
						}
						$update = $modelNameSpace::whereIn('id', $whereCondArr);
						
						if ($orWhereCondArr != false) {
								$update = $update->orWhere($orWhereCondArr);
						}
						$update->delete();
						$response = $update;
				}
				return $response;
		}

		public static function getRecordByOrder($modelNameSpace = false, $order = false)
		{
			
				$response = false;
				$response = $modelNameSpace::getRecordByOrder($order);
				// print_r($modelNameSpace);die;
				return $response;
		}

		public static function updateOrder($objects)
		{
				DB::update(DB::raw("UPDATE
				nq_" . Config::get('Constant.MODULE.TABLE_NAME') . " SET `intDisplayOrder` = `intDisplayOrder` - 1
				WHERE `intDisplayOrder` >= " . $objects->intDisplayOrder . "
				AND chrDelete='N'
				AND intDisplayOrder != 0 "));
		}

		public static function updateOrderAfterEdit($currentOrder,$updatedOrder,$currentRecordId)
		{

			if($currentOrder > $updatedOrder)
			{

				DB::update(DB::raw("UPDATE
				nq_" . Config::get('Constant.MODULE.TABLE_NAME') . " SET `intDisplayOrder` = `intDisplayOrder` + 1
				WHERE `intDisplayOrder` <".$currentOrder."
				AND `intDisplayOrder` >=".$updatedOrder."
				AND `chrDelete` ='N'
				AND `intDisplayOrder` != 0
				AND `id` !=".$currentRecordId.""));	

			}else{

				DB::update(DB::raw("UPDATE
				nq_" . Config::get('Constant.MODULE.TABLE_NAME') . " SET `intDisplayOrder` = `intDisplayOrder` - 1
				WHERE `intDisplayOrder` >".$currentOrder."
				AND `intDisplayOrder` <=".$updatedOrder."
				AND `chrDelete` ='N'
				AND `intDisplayOrder` != 0
				AND `intDisplayOrder` != 1
				AND `id` !=".$currentRecordId.""));	
			}		
				

		}

		public static function updateOrderAfterAdd($addedOrder, $existingRecId)
		{		
				DB::update(DB::raw("UPDATE
				nq_" . Config::get('Constant.MODULE.TABLE_NAME') . " SET `intDisplayOrder` = `intDisplayOrder` + 1
				WHERE `intDisplayOrder` >= ".$addedOrder."
				AND `chrDelete` ='N'
				AND `intDisplayOrder` != 0
				AND `intDisplayOrder` != 1
				AND `id` != ".$existingRecId.""));
		}
   
   /* Add photo gallery code here */
		public static function updateOrderAfterAddPhotoGallery($addedOrder, $existingRecId,$albumId)
		{		
				DB::update(DB::raw("UPDATE
				nq_" . Config::get('Constant.MODULE.TABLE_NAME') . " SET `intDisplayOrder` = `intDisplayOrder` + 1
				WHERE `intDisplayOrder` >= ".$addedOrder."
				AND fkIntAlbumId=" . $albumId . "
				AND `chrDelete` ='N'
				AND `intDisplayOrder` != 0
				AND `intDisplayOrder` != 1
				AND `id` != ".$existingRecId.""));
		}

		/**
	     * This method handels reorder tour wise record
	     * @return  Object
	     * @since   2018-07-09
	     * @author  K
	     */
	    public static function updatePhotoGalleryOrder($objects,$albumId)
	    {
	    	
	    	DB::update(DB::raw("UPDATE
				nq_" . Config::get('Constant.MODULE.TABLE_NAME') . " SET `intDisplayOrder` = `intDisplayOrder` - 1
				WHERE `intDisplayOrder` >= " . $objects->intDisplayOrder . "
				AND fkIntAlbumId=" . $albumId . "
				AND chrDelete='N'
				AND intDisplayOrder != 0 "));

	    //     DB::update(DB::raw("UPDATE
					// nq_" . Config::get('Constant.MODULE.TABLE_NAME') . " SET `intDisplayOrder` = `intDisplayOrder` - 1
	    //             WHERE `intDisplayOrder` > " . $objects->intDisplayOrder . "
	    //             AND fkIntAlbumId=" . $albumId . "
					// AND chrDelete='N' 
					// AND intDisplayOrder != 0 "));
	    }
	   
	   public static function updateOrderAfterEditPhotoGallery($currentOrder,$updatedOrder,$currentRecordId,$albumId)
		 {
			if($currentOrder > $updatedOrder)
			{

				DB::update(DB::raw("UPDATE
				nq_" . Config::get('Constant.MODULE.TABLE_NAME') . " SET `intDisplayOrder` = `intDisplayOrder` + 1
				WHERE `intDisplayOrder` <".$currentOrder."
				AND fkIntAlbumId=" . $albumId . "
				AND `intDisplayOrder` >=".$updatedOrder."
				AND `chrDelete` ='N'
				AND `intDisplayOrder` != 0
				AND `id` !=".$currentRecordId.""));	

			}else{

				DB::update(DB::raw("UPDATE
				nq_" . Config::get('Constant.MODULE.TABLE_NAME') . " SET `intDisplayOrder` = `intDisplayOrder` - 1
				WHERE `intDisplayOrder` >".$currentOrder."
				AND fkIntAlbumId=" . $albumId . "
				AND `intDisplayOrder` <=".$updatedOrder."
				AND `chrDelete` ='N'
				AND `intDisplayOrder` != 0
				AND `intDisplayOrder` != 1
				AND `id` !=".$currentRecordId.""));	
			}	
		 } 
	  /* end photo gallery code here */ 

		/**
		 * This method handels retrival of record count
		 * @return  Object
		 * @since   2017-10-16
		 * @author  NetQuick
		 */
		public static function getRecordCount($filterArr = false, $returnCounter = false, $modelNameSpace = false)
		{
				$response     = false;
				$moduleFields = ['id'];
				if ($modelNameSpace == false) {
						$modelNameSpace = MyLibrary::getModelNameSpace();
				}
				$response = $modelNameSpace::getPowerPanelRecords($moduleFields)->deleted();
				if ($filterArr != false) {
						$response = $response->filter($filterArr, $returnCounter);
				}
				$response = $response->count();
				return $response;
		}

		/**
		 * This method handels retrival of record count
		 * @return  Object
		 * @since   2017-10-16
		 * @author  NetQuick
		 */
		public static function getTotalRecordCount($modelNameSpace = false)
		{
				$response     = false;
				$moduleFields = ['id'];
				
				if ($modelNameSpace == false) {
						$modelNameSpace = MyLibrary::getModelNameSpace();
				}

				$response = $modelNameSpace::getPowerPanelRecords($moduleFields)->deleted();
				$response = $response->count();
				return $response;
		}

		/**
		 * This method handels retrival of record for delete
		 * @return  Object
		 * @since   2017-10-16
		 * @author  NetQuick
		 */
		public static function getRecordsForDeleteById($id, $modelName = false, $modelNameSpace = false)
		{
				$response     = false;
				$AliasField = true;
				$moduleFields = array('id');
				if ($modelName == false) {
						$modelName = Config::get('Constant.MODULE.MODEL_NAME');
				}
				$titleField          = 'varTitle';
				$displayOrderField   = true;
				$nameFieldModules    = array('User');
				$varNameFieldModules = array('ContactLead', 'NewsletterLead', 'RequestLead' , 'RestaurantReservations','AppointmentLead');
				if (in_array($modelName, $nameFieldModules)) {
						$titleField = 'name';
				} else if (in_array($modelName, $varNameFieldModules)) {
						$titleField = 'varName';
				}

				array_push($moduleFields, $titleField);
				$avoidDisplayOrderFieldModules = array('Advertise', 'CmsPage', 'StaticBlocks', 'ContactInfo', 'ContactLead', 'NewsletterLead', 'RestaurantReservations', 'EventLead','AppointmentLead','Blogs','Events','Careers','News','Testimonial','Show');
				if (in_array($modelName, $avoidDisplayOrderFieldModules)) {
						$displayOrderField = false;
				}

				if ($displayOrderField) 
				{
					if(Schema::hasColumn(Config::get('Constant.MODULE.TABLE_NAME'), 'intDisplayOrder'))
					{
						array_push($moduleFields, 'intDisplayOrder');
					}	
				}

				if ($AliasField) 
				{
					if(Schema::hasColumn(Config::get('Constant.MODULE.TABLE_NAME'), 'intAliasId'))
					{
						array_push($moduleFields, 'intAliasId');
					}	
				}

		
				// $aliasArray = ['Blog', 'PressRelease', 'MarketReport', 'CmsPage', 'Menu',
    //         'Member', 'BlogCategory', 'Brokeragent', 'Community', 'MenuType', 'VideoAlbum',
    //         'StaticBlocks', 'PropertyTypes'];
    //     if (in_array($modelName, $aliasArray)) {
    //         $AliasField = true;
    //     }
    //     if ($AliasField) {
    //         array_push($moduleFields, 'intAliasId');
    //     }

				if ($modelNameSpace == false) {
						$modelNameSpace = MyLibrary::getModelNameSpace();
				}

				$response = $modelNameSpace::getPowerPanelRecords($moduleFields)->checkRecordId($id)->first();
				return $response;
		}
}
