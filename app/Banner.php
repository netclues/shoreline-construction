<?php
/**
 * The Banner class handels bannner queries
 * ORM implemetation.
 * @package   Netquick powerpanel
 * @license   http://www.opensource.org/licenses/BSD-3-Clause
 * @version   1.1
 * @since       2017-07-20
 */
namespace App;

use Cache;
use DB;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'banner';
    protected $fillable = [
        'varTitle', 
        'varSubTitle',
        'fkIntImgId',
        'fkIntVideoId',
        'varBannerVersion',
        'txtDescription',
        'intDisplayOrder',
        'chrPublish',
        'chrDelete',
    ];

    /**
     * This method handels retrival of front banner list
     * @return  Object
     * @since   2017-10-14
     * @author  NetQuick
     */
    public static function getHomeBannerList()
    {
        $response    = false;
        $videoFields = false;
        $response    = Cache::tags(['Banner'])->get('getHomeBannerList');
        if (empty($response)) {
            $moduleFields = ['varTitle', 'varSubTitle','chrLinkType', 'varButtonLink','varButtonName', 'fkPageId','fkIntImgId', 'fkIntVideoId', 'txtDescription','varBannerVersion'];
            $videoFields  = ['id', 'varVideoName', 'varVideoExtension', 'youtubeId', 'vimeoId'];
            $aliasFields = ['id', 'varAlias'];
            $response     = Self::getFrontRecords($moduleFields, false, $videoFields, $aliasFields)
				->displayOrderBy('ASC')				
				->deleted()
                ->publish()
                ->get();
            Cache::tags(['Banner'])->forever('getHomeBannerList', $response);
        }
        return $response;
    }

    /**
     * This method handels retrival of front banner list
     * @return  Object
     * @since   2017-10-14
     * @author  NetQuick
     */
    public static function getDefaultBannerList()
    {
        $response = false;
        $response = Cache::tags(['Banner'])->get('getDefaultBannerList');
        if (empty($response)) {
            $moduleFields = ['fkIntImgId', 'varTitle', 'varSubTitle', 'txtDescription','varBannerVersion'];
            $response     = Self::getFrontRecords($moduleFields)
                ->displayOrderBy('ASC')
                ->deleted()
                ->publish()
                ->get();
            Cache::tags(['Banner'])->forever('getDefaultBannerList', $response);
        }
        return $response;
    }

    /**
     * This method handels retrival of home banner record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function homeBannerCount()
    {
        $response     = false;
        $moduleFields = ['id'];
        $response     = Self::getPowerPanelRecords($moduleFields)
            						->deleted()
            						->count();
        return $response;
    }

    /**
     * This method handels retrival of inner banner record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function innerBannerCount()
    {
        $response     = false;
        $moduleFields = ['id'];
        $response     = Self::getPowerPanelRecords($moduleFields)
            ->deleted()
            ->count();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordList($filterArr = false)
    {
        $response     = false;
        $imageFields  = false;
        $videoFields  = false;
        $moduleFields = [
            'id',
            'chrPublish',
            'varTitle', 
            'varSubTitle',
            'txtDescription',
            'fkIntImgId',
            'fkIntVideoId',
            'intDisplayOrder',
            'varBannerVersion',
        ];

        $response   =
        $response   = Self::getPowerPanelRecords($moduleFields,$imageFields, $videoFields)

        //Self::getPowerPanelRecords($moduleFields, $pageFields,$mdlFields)

            ->deleted()
            ->filter($filterArr)
            ->get();

        return $response;
    }

    public static function getInnerRecordList($filterArr = false)
    {
        $response     = false;
        $imageFields  = false;
        $videoFields  = false;
        $moduleFields = [
           'id',
            'chrPublish',
            'fkIntImgId',
            'varTitle', 
            'varSubTitle',
            'fkIntImgId',
            'intDisplayOrder',
            'varBannerVersion'
        ];
        $response   = Self::getPowerPanelRecords($moduleFields, $imageFields, $videoFields)
            ->deleted()
            ->filter($filterArr)
            ->get();

        return $response;
    }

    public function alias()
    {
        return $this->belongsTo('App\Alias', 'fkPageId', 'id');
    }
    
    /**
     * This method handels retrival of record
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordById($id = false)
    {
        $response     = false;
        $imageFields  = false;
        $moduleFields = [
            'id',
            'chrPublish',
            'fkIntImgId',
            'varTitle', 
            'fkIntVideoId',
            'txtDescription',
			'varSubTitle',
			'varButtonName',
            'chrLinkType',
            'fkPageId',
            'varButtonLink',
            'intDisplayOrder',
            'varBannerVersion',
        ];
        $videoFields = [
            'id',
            'youtubeId',
            'varVideoName',
            'varVideoExtension',
        ];
        $pageFields = ['id'];
        $response   = Self::getPowerPanelRecords($moduleFields, $imageFields, $videoFields)
            ->deleted()
            ->checkRecordId($id)
            ->first();
        return $response;
    }

    /**
     * This method handels retrival of record by id for Log Manage
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordForLogById($id)
    {
        $response     = false;
        $moduleFields = [
            'id',
            'varTitle', 'varSubTitle',
            'varBannerVersion',
            'fkIntImgId',
            'fkIntVideoId',
            'varBannerVersion',
            'txtDescription',
            'intDisplayOrder',
            'chrPublish',
        ];

        $response = Self::getPowerPanelRecords($moduleFields)->deleted()->checkRecordId($id)->first();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    protected static $fetchedOrder    = [];
    protected static $fetchedOrderObj = null;
    public static function getRecordByOrder($order = false)
    {
        $response     = false;
        $moduleFields = [
            'id',
            'intDisplayOrder',
        ];
        if (!in_array($order, Self::$fetchedOrder)) {
            array_push(Self::$fetchedOrder, $order);
            Self::$fetchedOrderObj = Self::getPowerPanelRecords($moduleFields)
                ->deleted()
                ->orderCheck($order)
                ->first();
        }
        $response = Self::$fetchedOrderObj;
        return $response;
    }

    /**
     * This method handels retrival of record for notification
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordNotify($id = false)
    {
        $response     = false;
        $imageFields  = false;
        $moduleFields = ['varTitle'];
        $response     = Self::getPowerPanelRecords($moduleFields)
            ->deleted()
            ->checkRecordId($id)
            ->first();
        return $response;
    }

    /**
     * This method handels set/unset of default banner
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function setDefault($id = false, $flagArr = false)
    {
        $response = false;
        $response = Self::where('id', $id)->update($flagArr);
        return $response;
    }

    #Database Configurations========================================
    /**
     * This method handels retrival of front end records
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public static function getFrontRecords($moduleFields = false, $imageFields = false, $videoFields = false, $aliasFields = false)
    {
        $data     = [];
        $response = false;
        $response = self::select($moduleFields);
        if ($videoFields != false) {
            $data['video'] = function ($query) use ($videoFields) {$query->select($videoFields)->publish();};
        }

		if ($aliasFields != false) {
            $data['alias'] = function ($query) use ($aliasFields) {
                $query->select($aliasFields);
            };
		}
		
        if (count($data) > 0) {
            $response = $response->with($data);
		}
		
        return $response;
    }

    /**
     * This method handels retrival of backednd records
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public static function getPowerPanelRecords($moduleFields = false, $imageFields = false, $videoFields = false)
    {
        $data     = [];
        $response = false;
        $response = self::select($moduleFields);
        if ($imageFields != false) {
            $data['image'] = function ($query) use ($imageFields) {$query->select($imageFields);};
        }
        if ($videoFields != false) {
            $data['video'] = function ($query) use ($videoFields) {$query->select($videoFields)->publish();};
        }

        if (count($data) > 0) {
            $response = $response->with($data);
        }
        return $response;
    }

    /**
     * This method handels image relation
     * @return  Object
     * @since   2017-07-20
     */
    public function image()
    {
        $response = false;
        $response = $this->belongsTo('App\Image', 'fkIntImgId', 'id');
        return $response;
    }

    /**
     * This method handels video relation
     * @return  Object
     * @since   2017-10-04
     */
    public function video()
    {
        $response = false;
        $response = $this->belongsTo('App\Video', 'fkIntVideoId', 'id');
        return $response;
    }


    /**
     * This method handels retrival of banners records
     * @return  Object
     * @since   2016-07-20
     */
    public static function getRecords()
    {
        $response = false;
        $response = self::with(['image']);
        return $response;
    }

    /**
     * This method handels record id scope
     * @return  Object
     * @since   2016-07-24
     */
    public function scopeCheckRecordId($query, $id)
    {
        $response = false;
        $response = $query->where('id', $id);
        return $response;
    }

    /**
     * This method handels order scope
     * @return  Object
     * @since   2016-07-20
     */
    public function scopeOrderCheck($query, $order)
    {
        $response = false;
        $response = $query->where('intDisplayOrder', $order);
        return $response;
    }

    /**
     * This method handels publish scope
     * @return  Object
     * @since   2016-07-20
     */
    public function scopePublish($query)
    {
        $response = false;
        $response = $query->where(['chrPublish' => 'Y']);
        return $response;
    }

    /**
     * This method handels delete scope
     * @return  Object
     * @since   2016-07-20
     */
    public function scopeDeleted($query)
    {
        $response = false;
        $response = $query->where(['chrDelete' => 'N']);
        return $response;
    }

    /**
     * This method handels banner type scope
     * @return  Object
     * @since   2017-08-08
     */
    public function scopeBannerType($query, $type = null)
    {
        $response = false;
        $response = $query->where(['varBannerType' => $type]);
        return $response;
    }

    /**
     * This method checking banner type
     * @return  Object
     * @since   2016-07-20
     */
    public function scopeCheckHomeBannerType($query)
    {
        $response = false;
        $response = $query->where(['varBannerType' => 'home_banner']);
        return $response;
    }

    /**
     * This method checking banner type
     * @return  Object
     * @since   2016-07-14
     */
    public function scopeCheckInnerBannerType($query)
    {
        $response = false;
        $response = $query->where(['varBannerType' => 'inner_banner']);
        return $response;
    }

    /**
     * This method checking default banner
     * @return  Object
     * @since   2016-07-14
     */
    public function scopeDisplayOrderBy($query, $orderBy)
    {
        $response = false;
        $response = $query->orderBy('intDisplayOrder', $orderBy);
        return $response;
    }

    public function scopeCheckModuleId($query, $moduleId)
    {
        $response = false;
        $response = $query->where('fkModuleId', $moduleId);
        return $response;
    }

/**
 * This method handels filter scope
 * @return  Object
 * @since   2016-07-14
 */
    public function scopeFilter($query, $filterArr = false, $retunTotalRecords = false)
    {
        $response = null;
        if ($filterArr['orderByFieldName'] != null && $filterArr['orderTypeAscOrDesc'] != null) {
            $query = $query->orderBy($filterArr['orderByFieldName'], $filterArr['orderTypeAscOrDesc']);
        } else {
            $query = $query->orderBy('varTitle', 'ASC');
        }

        if (!$retunTotalRecords) {
            if (!empty($filterArr['iDisplayLength']) && $filterArr['iDisplayLength'] > 0) {
                $query = $query->skip($filterArr['iDisplayStart'])->take($filterArr['iDisplayLength']);
            }
        }

        if (!empty($filterArr['statusFilter']) && $filterArr['statusFilter'] != ' ') {
            $query = $query->where('chrPublish', $filterArr['statusFilter']);
        }

        if (!empty($filterArr['searchFilter']) && $filterArr['searchFilter'] != ' ') {
            $query = $query->where('varTitle', 'like', "%" . $filterArr['searchFilter'] . "%");
        }

        if (!empty($filterArr['bannerFilter']) && $filterArr['bannerFilter'] != ' ') {
            if ($filterArr['bannerFilter'] == 'img_banner' || $filterArr['bannerFilter'] == 'vid_banner') {
                $query = $query->orWhere('varBannerVersion', '=', $filterArr['bannerFilter']);
            }
        }

        if (!empty($filterArr['pageFilter']) && $filterArr['pageFilter'] != ' ') {
            $query = $query->where('fkModuleId', '=', $filterArr['pageFilter']);
        }

        if (!empty($query)) {
            $response = $query;
        }
        return $response;
    }

    /**
     * This method handels filter scope
     * @return  Object
     * @since   2016-07-14
     */
    public static function add_pages()
    {
        $response    = false;
        $module_code = DB::table('modules')->where('var_module_name', '=', 'cms-page')->first();
        $response    = DB::table('cms_pages')
            ->select('cms_pages.*')
            ->where('cms_pages.chr_delete', '=', 'N')
            ->where('cms_pages.chr_publish', '=', 'Y')
            ->groupBy('cms_pages.id')->get();
        return $response;
    }

}
