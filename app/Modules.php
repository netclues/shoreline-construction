<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{

    protected $table = 'module';
    protected $fillable = [
        'id',
        'varTitle',
        'varModuleName',
        'varTableName',
        'varModelName',
        'varModuleClass',
        'decVersion',
        'intDisplayOrder',
        'chrIsFront',
        'chrIsPowerpanel',
        'chrIsGenerated',
        'chrPublish',
        'chrDelete',
        'created_at',
        'updated_at',
    ];

    public static function getGeneratedModuleList()
    {
        $moduleFields = [
            'id',
            'varTitle',
            'varModuleName',
            'varTableName',
            'varModelName',
            'varModuleClass',
            'chrIsFront',
            'chrIsPowerpanel',
            'chrIsGenerated',
        ];

        $response = false;
        $response = Self::getPowerPanelRecords($moduleFields)
            ->deleted()
            ->where('chrIsGenerated', 'Y')
            ->get();
        return $response;
    }

    public static function getAllModuleList($filterArr)
    {
        $moduleFields = [
            'id',
            'varTitle',
            'varModuleName',
            'varTableName',
            'varModelName',
            'varModuleClass',
            'chrIsFront',
            'chrIsPowerpanel',
            'chrIsGenerated',
        ];

        $response = false;
        $response = Self::getPowerPanelRecords($moduleFields)
            ->deleted()
            ->powerpanelFilter($filterArr)
            ->orderBy('varTitle', 'ASC')
            ->get();
        return $response;
    }

    public static function getAllModuleData($moduleName = false)
    {
        $moduleFields = [
            'id',
            'varTitle',
            'varModuleName',
            'varTableName',
            'varModelName',
            'varModuleClass',
            'varModuleName',
        ];
        $response = false;
        $response = Self::getRecords($moduleFields)
            ->deleted()
            ->getModuleId($moduleName)
            ->first();
        return $response;
    }

    public static function getModule($moduleName = false)
    {
        $moduleFields = ['id', 'varModuleName'];
        $response = false;
        $response = Self::getRecords($moduleFields)
            ->deleted()
            ->getModuleId($moduleName)
            ->orderBy('varTitle')
            ->first();
        return $response;
    }

/** start code here **/
    public static function getModuleTableName($moduleId = false)
    {
        $moduleFields = ['id', 'varTableName', 'varModuleName'];
        $response = false;
        $response = Self::getRecords($moduleFields)
            ->deleted()
            ->checkRecordId($moduleId)
            ->first();
        return $response;
    }

    public static function getModuleDataById($moduleId = false)
    {
        $moduleFields = ['id', 'varTitle', 'varModuleName', 'varTableName', 'varModelName', 'varModuleClass'];
        $response = false;
        $response = Self::getPowerPanelRecords($moduleFields)
            ->deleted()
            ->checkRecordId($moduleId)
            ->first();
        return $response;
    }

    public static function deleteRecordById($moduleId = false)
    {
        $moduleFields = ['id'];
        $response = false;
        $response = Self::getPowerPanelRecords($moduleFields)
            ->checkRecordId($moduleId)
            ->delete();
        return $response;
    }

/** **/
    public static function getModuleList()
    {
        $moduleFields = ['id', 'varTitle', 'varModuleName'];
        $response = false;
        $response = Self::getRecords($moduleFields)
            ->publish()
            ->isFront()
            ->deleted()
            ->orderBy('varTitle', 'ASC')
            ->get();
        return $response;
    }

    public static function getModuleListForSettings($term = '')
    {
        $moduleFields = ['id', 'varTitle', 'varModuleName'];
        $response = false;
        $response = Self::getRecords($moduleFields)
            ->publish()
            ->deleted()
            ->filter($term)
            ->orderBy('varTitle')
            ->get();
        return $response;
    }

    public static function getFrontModuleListForLog()
    {
        $moduleFields = [
            'id',
            'varTitle',
            'varModelName',
            'varModuleName'];
        $response = false;
        $response = Self::getRecords($moduleFields)
            ->deleted()
            ->publish()
            ->orderBy('varTitle')
            ->get();
        return $response;
    }

    public static function getFrontModuleList()
    {
        $response = false;
        $moduleFields = [
            'id',
            'varTitle',
            'varModelName',
            'varModuleName'];

        $query = Self::getRecords($moduleFields)
            ->isFront()
            ->deleted()
            ->publish()
            ->orderBy('varTitle');

        $response = $query->get();
        return $response;
    }

    public static function getRecordAvaliModules($ignoreModuleArr = false)
    {
        $response = false;
        $moduleFields = [
            'id',
            'varTitle',
            'varModelName',
            'varModuleName'];

        $query = Self::getRecords($moduleFields)->publish()->where('chrIsFront','Y');
        if($ignoreModuleArr){
            $query = $query->whereNotIN('varModuleName', $ignoreModuleArr);
        }
        $query = $query->orderBy('varTitle');

        $response = $query->get();
        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return  Object
     * @since   2017-10-16
     * @author  NetQuick
     */
    public static function getRecordCount($filterArr = false, $returnCounter = false, $modelNameSpace = false)
    {
        $response = false;
        $moduleFields = ['id'];
        $response = Self::getPowerPanelRecords($moduleFields)->deleted();
        if ($filterArr != false) {
            $response = $response->powerpanelFilter($filterArr, $returnCounter);
        }
        $response = $response->count();
        return $response;
    }

    public static function getPrivacyRemovalModule()
    {
        $moduleFields = ['id', 'varTitle', 'varModelName', 'varModuleName', 'varTableName'];

        $response = false;
        $response = Self::getRecords($moduleFields)

            ->get();

        return $response;
    }

    /**
     * This method handels retrival of backend record list
     * @return  Object
     * @since   2017-10-24
     * @author  NetQuick
     */
    public static function getRecordList($filterArr = false)
    {
        $response = false;
        $moduleFields = ['id', 'varTitle', 'varModelName', 'varModuleName', 'varTableName'];
        $response = Self::getPowerPanelRecords($moduleFields)
            ->powerpanelFilter($filterArr)

            ->get();

        return $response;
    }

    /**
     * This method handels retrival of record count
     * @return Object
     * @since 2017-10-16
     * @author NetQuick
     */
    public static function getRecordCountforList($filterArr = false, $returnCounter = false)
    {
        $response = 0;
        $moduleFields = ['id'];
        $response = Self::getPowerPanelRecords($moduleFields);
        if ($filterArr != false) {
            $response = $response->powerpanelFilter($filterArr, $returnCounter);
        }

        $response = $response->deleted()
            ->count();
        return $response;
    }

    /**
     * This method handels retrival of blog records
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public static function getPowerPanelRecords($moduleFields = false)
    {
        $data = [];
        $response = false;
        $response = self::select($moduleFields);
        if (count($data) > 0) {
            $response = $response->with($data);
        }
        return $response;
    }

    #Database Configurations========================================
    /**
     * This method handels retrival of blog records
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public static function getRecords($moduleFields = false, $pageFields = false)
    {
        $data = [];
        $response = false;
        $response = self::select($moduleFields);
        if ($pageFields != false) {
            $data['pages'] = function ($query) use ($pageFields) {$query->select($pageFields);};
        }
        if (count($data) > 0) {
            $response = $response->with($data);
        }
        return $response;
    }
    /**
     * This method handels record id scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopeCheckRecordId($query, $id)
    {
        return $query->where('id', $id);
    }
    /**
     * This method handels publish scope
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public function scopePublish($query)
    {
        return $query->where(['chrPublish' => 'Y']);
    }
    /**
     * This method handels front scope
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public function scopeIsFront($query)
    {
        return $query->where(['chrIsFront' => 'Y']);
    }
    /**
     * This method handels publish scope
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public function scopeGetModuleId($query, $moduleName)
    {
        return $query->where('varModuleName', $moduleName);
    }

    /**
     * This method handels filter scope
     * @return  Object
     * @since   2018-01-04
     * @author  NetQuick
     */
    public function scopeFilter($query, $term = '')
    {
        $response = false;

        $query = $query->whereNotIn('varModuleName', [
            "settings",
            "home",
            "menu-type",
            "recent-updates",
            "sitemap",
            "contact-us",
            "email-log",
            "newsletter-lead",
            "log",
            "login-history",
            "faq",
            "plugins",
            "users",
            "roles",
        ]);

        if ($term != '') {
            $query = $query->where('varModuleName', 'like', "%" . $term . "%")
                ->orWhere('varTitle', 'like', "%" . $term . "%");
        }

        if (!empty($query)) {
            $response = $query;
        }
        return $response;
    }

    /**
     * This method handels filter scope
     * @return  Object
     * @since   2017-08-02
     * @author  NetQuick
     */
    public function scopePowerpanelFilter($query, $filterArr = false, $retunTotalRecords = false)
    {
        $response = null;
        if ($filterArr['orderByFieldName'] != null && $filterArr['orderTypeAscOrDesc'] != null) {
            $query = $query->orderBy($filterArr['orderByFieldName'], $filterArr['orderTypeAscOrDesc']);
        } else {
            $query = $query->orderBy('id', 'DESC');
        }
        if (isset($filterArr['searchFilter']) && !empty($filterArr['searchFilter']) && $filterArr['searchFilter'] != ' ') {
            $data = $query->where('varTitle', 'like', '%' . $filterArr['searchFilter'] . '%');
        }

        if (!$retunTotalRecords) {
            if (!empty($filterArr['iDisplayLength']) && $filterArr['iDisplayLength'] > 0) {
                $data = $query->skip($filterArr['iDisplayStart'])->take($filterArr['iDisplayLength']);
            }
        }

        if (!empty($query)) {
            $response = $query;
        }

        return $response;
    }

    /**
     * This method handels delete scope
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public function scopeDeleted($query)
    {
        return $query->where(['chrDelete' => 'N']);
    }

    public function pages()
    {
        return $this->hasOne('App\CmsPage', 'id', 'intFKModuleCode');
    }
    public function log()
    {
        return $this->hasOne('App\Log', 'id', 'fkIntModuleId');
    }
    public function emailLog()
    {
        return $this->hasOne('App\EmailLog', 'id', 'fkIntModuleId');
    }
    public function banner()
    {
        return $this->hasOne('App\Banner', 'id', 'fkModuleId');
    }
}
