<?php

namespace App;

use Cache;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Modules;

class DocumentModuleRel extends Model
{
		protected $table    = 'document_module_rel';
		protected $fillable = [
			'id',
			'varTmpId',
			'intFkDocumentId',
			'intFkModuleCode',
			'intRecordId',
			'created_at',
			'updated_at'
		];

		public static function getRecord($idArr = null){
			$response = false;
			if(!empty($idArr)){
				$response = DocumentModuleRel::select('intFkDocumentId')
				->whereIn('intFkDocumentId', $idArr)
				->get();
			}
			return $response;			
		}

		public static function addRecord($data = false){
        $response = false;
        if ($data != false && !empty($data)) {
            $recordId = DocumentModuleRel::insertGetId($data);
            if ($recordId > 0) {
                $response = $recordId;
            }
        }
        return $response;
    }

    public static function deleteRecord($whereConditions = null){
    		$response = false;
        if (!empty($whereConditions)) {
            $response = DocumentModuleRel::where($whereConditions)->delete();
        }
        return $response;	
    }
 
 /* Show the name of the used images: */
		public static function getRecordList($idArr = null){
			$response = false;
			if(!empty($idArr)){
				//SELECT * FROM `nq_image_module_rel` WHERE intFkModuleCode="15" AND intFkImgId="92";
				$collectData = DocumentModuleRel::select(['intFkDocumentId','intFkModuleCode','intRecordId'])
				->whereIn('intFkDocumentId', $idArr)
				->groupBy('intRecordId')
				->get()
				->toArray();				

				$CollectAllData = [];
				foreach ($collectData as $key => $value) {					
					$tableName = Modules::getModuleTableName($value['intFkModuleCode']);
					
					if(!empty($tableName->varTableName)) {
				    $response[] = DB::table($tableName->varTableName)->where('id', $value['intRecordId'])->get();
				  }	
				   $response[$key]['moduleName']  = $tableName->varModuleName;
				}
			}
			return $response;			
		}

		public static function getRecordListUpdated($idArr = null){
			$response = false;
			if(!empty($idArr)){
				
        //SELECT * FROM `nq_image_module_rel` WHERE intFkModuleCode="15" AND intFkImgId="92";
				$collectData = DocumentModuleRel::select(['intFkDocumentId','intFkModuleCode','intRecordId'])
				->whereIn('intFkDocumentId', $idArr)
				->get()
				->toArray();
				
				$CollectAllData = [];
				foreach ($collectData as $key => $value) {

					$tableName = Modules::getModuleTableName($value['intFkModuleCode']);
					$tableNameJSON = json_decode($tableName);
					
					if(!empty($tableNameJSON->varTableName)) {

						$getRecordImgId = DB::table($tableNameJSON->varTableName)->where('id', $value['intRecordId'])->get(); 
						$docIDExplode = $getRecordImgId['0']->fkIntDocId;
						$recordimgArr= explode(',', $docIDExplode);
						$selectedImgArr = $idArr;
						$docResult=array_diff($recordimgArr,$selectedImgArr);
						$replceDocArr = implode(',', $docResult);
						DB::table($tableNameJSON->varTableName)->where('id', $value['intRecordId'])->update(['fkIntDocId' => $replceDocArr]);		
						 DocumentModuleRel::where('intFkDocumentId', $value['intFkDocumentId'])->delete();		    
				  }	
				 }
			}
			return $response;			
		}
	/* End code here for used images	*/
   
}
