<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
/**
 * The attributes that are mass assignable.
 * @var array
 */
    protected $table = 'currency';

    public static function getCurrency()
    {
        $moduleFields = [
            'id',
            'varCountry',
            'varCurrency',
            'varCode',
            'varSymbol',
        ];
        $response = false;
        $response = Self::select($moduleFields)
                    ->orderBy('varCountry')
                    ->get();
        return $response;
    }

    public static function getCurrencyByCode($code)
    {
        $moduleFields = [
            'id',
            'varCountry',
            'varCurrency',
            'varCode',
            'varSymbol',
        ];
        $response = false;
        $response = Self::select($moduleFields)
                    ->where('varCode',$code)
                    ->first();
        return $response;
    }


}
