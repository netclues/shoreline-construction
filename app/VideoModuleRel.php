<?php

namespace App;

use Cache;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Modules;

class VideoModuleRel extends Model
{
		protected $table    = 'video_module_rel';
		protected $fillable = [
			'id',
			'varTmpId',
			'intFkVideoId',
			'intFkModuleCode',
			'intRecordId',
			'created_at',
			'updated_at'
		];
		protected static $fetchedID  = [];
		protected static $fetchedVideo = null;

		public static function getRecord($idArr = null){
			$response = false;
			if(!empty($idArr)){
				$response = VideoModuleRel::select('intFkVideoId')
				->whereIn('intFkVideoId', $idArr)
				->get();
			}
			return $response;			
		}

		public static function addRecord($data = false){
        $response = false;
        if ($data != false && !empty($data)) {
            $recordId = VideoModuleRel::insertGetId($data);
            if ($recordId > 0) {
                $response = $recordId;
            }
        }
        return $response;
    }

    public static function deleteRecord($whereConditions = null){
    		$response = false;
        if (!empty($whereConditions)) {
            $response = VideoModuleRel::where($whereConditions)->delete();
        }
        return $response;	
    }

/* Show the name of the used images: */
		public static function getRecordList($idArr = null){
			$response = false;
			if(!empty($idArr)){
				//SELECT * FROM `nq_image_module_rel` WHERE intFkModuleCode="15" AND intFkImgId="92";
				$collectData = VideoModuleRel::select(['intFkVideoId','intFkModuleCode','intRecordId'])
				->whereIn('intFkVideoId', $idArr)
				->groupBy('intRecordId')
				->get()
				->toArray();				

				$CollectAllData = [];
				foreach ($collectData as $key => $value) {					
					$tableName = Modules::getModuleTableName($value['intFkModuleCode']);
					
					if(!empty($tableName->varTableName)) {
				    $response[] = DB::table($tableName->varTableName)->where('id', $value['intRecordId'])->get();
				  }	
				   $response[$key]['moduleName']  = $tableName->varModuleName;
				}
			}
			return $response;			
		}

		public static function getRecordListUpdated($idArr = null){
			$response = false;
			if(!empty($idArr)){
				
        //SELECT * FROM `nq_image_module_rel` WHERE intFkModuleCode="15" AND intFkImgId="92";
				$collectData = VideoModuleRel::select(['intFkVideoId','intFkModuleCode','intRecordId'])
				->whereIn('intFkVideoId', $idArr)
				->get()
				->toArray();
				
				$CollectAllData = [];
				foreach ($collectData as $key => $value) {

					$tableName = Modules::getModuleTableName($value['intFkModuleCode']);
					$tableNameJSON = json_decode($tableName);
					
					if(!empty($tableNameJSON->varTableName)) {

						$getRecordImgId = DB::table($tableNameJSON->varTableName)->where('id', $value['intRecordId'])->get(); 
						$imgIDExplode = $getRecordImgId['0']->fkIntVideoId;
						$recordimgArr= explode(',', $imgIDExplode);
						$selectedImgArr = $idArr;
						$imgResult=array_diff($recordimgArr,$selectedImgArr);
						$replceImgArr = implode(',', $imgResult);
						DB::table($tableNameJSON->varTableName)->where('id', $value['intRecordId'])->update(['fkIntVideoId' => $replceImgArr]);		
						 VideoModuleRel::where('intFkVideoId', $value['intFkVideoId'])->delete();		    
				  }	
				 }
			}
			return $response;			
		}
/* End code here for used images	*/
}