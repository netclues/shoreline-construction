<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivacyRemovalLead extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'privacy_removal_leads';
    protected $fillable = [
        'id',
        'varName',
        'varEmail',
        'txtReason',
        'chrIsAuthorized',
        'chrIsEmailVerified',
        'varIpAddress',
        'created_at',
        'updated_at',
    ];

/**
 * This method handels retrival of record count
 * @return  Object
 * @since   2017-10-16
 * @author  NetQuick
 */
    public static function getRecordById($id, $moduleFields = false)
    {
        $response = false;
        $moduleFields = ['id', 'varName', 'varEmail', 'txtReason', 'chrIsAuthorized','chrIsEmailVerified','varIpAddress', 'created_at'];
        $response = Self::getPowerPanelRecords($moduleFields)->checkRecordId($id)->first();
        if ($response['varEmail'] != '') {
            $varEmail = decrypt($response['varEmail']);
        } else {
            $varEmail = $response['varEmail'];
        }
        $response['varEmail'] = $varEmail;
        return $response;
    }

    /**
     * This method handels retrival of backend record list
     * @return  Object
     * @since   2017-10-24
     * @author  NetQuick
     */
    public static function getRecordList($filterArr = false)
    {
        $response = false;
        $moduleFields = ['id', 'varName', 'varEmail', 'txtReason', 'chrIsAuthorized','chrIsEmailVerified', 'varIpAddress', 'created_at'];
        $response = Self::getPowerPanelRecords($moduleFields)
            ->filter($filterArr)
            ->get();
        return $response;
    }

    /**
     * This method handels retrival of backend record list for Export
     * @return  Object
     * @since   2017-10-24
     * @author  NetQuick
     */
    public static function getListForExport($selectedIds = false)
    {
        $response = false;
        $moduleFields = ['id', 'varName', 'varEmail', 'txtReason', 'chrIsAuthorized','chrIsEmailVerified', 'varIpAddress', 'created_at'];
        $query = Self::getPowerPanelRecords($moduleFields);
        if (!empty($selectedIds) && count($selectedIds) > 0) {
            $query->checkMultipleRecordId($selectedIds);
        }
        $response = $query->orderByCreatedAtDesc()->get();
        return $response;
    }

    public static function getRecordCount($filterArr = false, $returnCounter = false, $modelNameSpace = false)
    {
        $response = false;
        $moduleFields = ['id'];
        $response = Self::getPowerPanelRecords($moduleFields);
        if ($filterArr != false) {
            $response = $response->filter($filterArr, $returnCounter);
        }
        $response = $response->count();
        return $response;
    }

    /**
     * This method handels backend records
     * @return  Object
     * @since   2016-07-14
     * @author  NetQuick
     */
    public static function getPowerPanelRecords($moduleFields = false)
    {
        $data = [];
        $response = false;
        $response = self::select($moduleFields);
        if (count($data) > 0) {
            $response = $response->with($data);
        }
        return $response;
    }

    /**
     * This method handels record id scope
     * @return  Object
     * @since   2016-07-24
     * @author  NetQuick
     */
    public function scopeCheckRecordId($query, $id)
    {
        return $query->where('id', $id);
    }

    /**
     * This method check multiple records id
     * @return  Object
     * @since   2017-08-02
     * @author  NetQuick
     */
    public function scopeCheckMultipleRecordId($query, $Ids)
    {
        return $query->whereIn('id', $Ids);
    }

    /**
     * This method handle order by query
     * @return  Object
     * @since   2017-08-02
     * @author  NetQuick
     */
    public function scopeOrderByCreatedAtDesc($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    /**
     * This method handels filter scope
     * @return  Object
     * @since   2017-08-02
     * @author  NetQuick
     */
    public function scopeFilter($query, $filterArr = false, $retunTotalRecords = false)
    {
        $response = null;
        if ($filterArr['orderByFieldName'] != null && $filterArr['orderTypeAscOrDesc'] != null) {
            $query = $query->orderBy($filterArr['orderByFieldName'], $filterArr['orderTypeAscOrDesc']);
        } else {
            $query = $query->orderBy('id', 'DESC');
        }

        if (!$retunTotalRecords) {
            if (!empty($filterArr['iDisplayLength']) && $filterArr['iDisplayLength'] > 0) {
                $data = $query->skip($filterArr['iDisplayStart'])->take($filterArr['iDisplayLength']);
            }
        }
        if (!empty($filterArr['statusFilter']) && $filterArr['statusFilter'] != ' ') {
            $data = $query->where('chrPublish', $filterArr['statusFilter']);
        }
        if (isset($filterArr['searchFilter']) && !empty($filterArr['searchFilter'])) {
            $data = $query->where('varName', 'like', '%' . $filterArr['searchFilter'] . '%');
        }
        if (!empty($query)) {
            $response = $query;
        }
        
        return $response;
    }

}
