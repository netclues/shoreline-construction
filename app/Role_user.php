<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Role_user extends Model
{
		protected $table = 'role_user';
		protected $fillable = [
			'user_id',
			'role_id'
		];
    
    public $timestamps = false;


   public static function deleteUserRole($id=null){
			$response=false;						
			$response=Self::checkUserId($id)->delete();			
			return $response;
		}


		

		public static function getCountById($id = null)
     {
        $response = false;
        //$moduleFields = ['id'];
        //$response = Self::getPowerPanelRecords($moduleFields)
           // ->checkRoleId($id)
           // ->count();

        $response = Role_user::where('role_id',$id)->count();

        return $response;
     }

  //    function scopeCheckRoleId($query, $id) {
		// 		return $query->where('role_id', $id);
		// }

	/**
		 * This method handels role id scope
		 * @return  Object
		 * @since   2016-07-24
		 * @author  NetQuick
		 */
		function scopeCheckUserId($query, $id) {
				return $query->where('user_id', $id);
		}

  /**
	 * This method handels team relation
	 * @return  Object
	 * @since   2017-08-01
	 * @author  NetQuick
	 */
	public function roles() {
		return $this->hasMany('App\Role', 'id', 'role_id');
	}

}
