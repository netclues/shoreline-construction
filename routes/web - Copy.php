<?php 
use App\Helpers\MyLibrary;
$segmentArr = [];
$segmentArr = Request::segments();
$setConstants = MyLibrary::setConstants($segmentArr);

if(!empty(Request::segment(1)) && Request::segment(1) != "powerpanel")
{	
	$slug = Request::segment(1);	
	$arrModule = MyLibrary::setFrontRoutes($slug);
	if(isset($arrModule->modules->varModuleName))
	{
		switch($arrModule->modules->varModuleName)
		{
																		case "contact-us":
								Route::get('/' . $arrModule->alias->varAlias, ['uses' => 'ContactController@create'])->name('contact-us');
Route::post('/' . $arrModule->alias->varAlias, ['uses' => 'ContactController@store'])->name('contact-us');
						break;
																				case "faq":
								Route::get($arrModule->alias->varAlias, ['uses' => 'FaqController@index'])->name('faqList');
						break;
																				case "services":
								Route::get($arrModule->alias->varAlias, ['uses' => 'ServicesController@index'])->name('serviceList');
Route::get($arrModule->alias->varAlias . '/{alias}', ['uses' => 'ServicesController@detail'])->name('serviceDetail');
						break;
																				case "testimonial":
								Route::get('/' . $arrModule->alias->varAlias, ['uses' => 'TestimonialController@index'])->name('testimonialList');
						break;
																				case "service-category":
								Route::get($arrModule->alias->varAlias, ['uses' => 'ServicesCategoryController@index'])->name('service-category');
Route::get($arrModule->alias->varAlias . '/{alias}', ['uses' => 'ServicesCategoryController@detail'])->name('service-category-detail')->where('alias', '(.*)');
						break;
																				case "client":
								Route::get($arrModule->alias->varAlias, ['uses' => 'ClientController@index'])->name('clientList');
						break;
																				case "client-category":
								Route::get($arrModule->alias->varAlias, ['uses' => 'ClientCategoryController@index'])->name('client-category');
Route::get($arrModule->alias->varAlias . '/{alias}', ['uses' => 'ClientCategoryController@detail'])->name('client-category-detail')->where('alias', '(.*)');
						break;
															default:
				Route::get('/'.$arrModule->alias->varAlias, ['as'=> $arrModule->alias->varAlias,'uses'=> 'PagesController@index']);
			break;
		}
	}
}

//Route::get('abc', ['as' => 'abc', 'uses' => 'PagesController@my']);
Route::get('abc', 'PagesController@my');
Route::get('our-process', 'PagesController@process');
Route::get('our-work', 'PagesController@work');
Route::get('home-design', 'PagesController@design');
Route::get('all-plans', 'PagesController@allplans');
Route::get('single-storey', 'PagesController@singlestorey');
Route::get('double-storey', 'PagesController@doublestorey');
Route::get('duplex', 'PagesController@duplex');
Route::get('container', 'PagesController@container');
Route::get('otherplans', 'PagesController@otherplans');
Route::get('why-us', 'PagesController@whyus');

Route::get('/', ['uses' => 'HomeController@index']);
Route::get('thank-you', ['uses' => 'ThankyouController@index'])->name('thank-you');
Route::get('sitemap', ['uses' => 'SiteMapController@index'])->name('sitemap');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('news-letter', ['uses' => 'SubscriptionController@store'])->name('news-letter');
Route::get('news-letter/subscription/subscribe/{id}', ['uses' => 'SubscriptionController@subscribe'])->name('subscribe');
Route::get('news-letter/subscription/unsubscribe/{id}', ['uses' => 'SubscriptionController@unsubscribe'])->name('unsubscribe');
Route::post('restaurant-reservations', ['uses' => 'ReservationController@store']);

Route::get('generateSitemap', ['uses' => 'SiteMapController@generateSitemap'])->name('generateSitemap');
Route::post('/front/search', ['uses' => 'FrontController@search'])->name('search');
Route::post('/front/popupvalue', ['uses' => 'FrontController@popup'])->name('popupvalue');
Route::post('/email', ['uses' => 'EmailController@send_email'])->name('email');
Route::get('/email', ['uses' => 'EmailController@index'])->name('email');
Route::get('/fetchrss/{start}/{offset}', ['uses' => 'FetchrssController@index'])->name('fetchrss');
Route::post('/front/search', ['uses' => 'FrontController@search'])->name('search');
Route::post('/front/popupvalue', ['uses' => 'FrontController@popup'])->name('popupvalue');
Route::post('/front/cookiesPopupStore', ['as' => 'cookiesPopupStore', 'uses' => 'FrontController@cookiesPopupStore']);

Route::get('/data-removal', 'PrivacyRemovalController@index');
Route::post('/data-removal', 'PrivacyRemovalController@handlePost');
Route::get('/confirm-email-address', 'PrivacyRemovalController@confirmEmailAddress');


Route::group(['namespace' => 'Userauth'], function () {
    Route::get('/login', ['uses' => 'AuthController@showLoginForm']);
    Route::post('/login', ['uses' => 'AuthController@login']);
    Route::get('/register', ['uses' => 'AuthController@showRegistrationForm']);
    Route::post('/register', ['uses' => 'AuthController@register']);
    Route::post('/password/email', ['uses' => 'PasswordController@sendResetLinkEmail']);
    Route::get('/password/reset/{token?}', ['uses' => 'PasswordController@showResetForm']);
    Route::post('/password/reset', ['uses' => 'PasswordController@reset']);
    Route::get('/logout', ['uses' => 'AuthController@logout']);
    //Route::get('/profile', ['uses'=> 'ProfileController@index','middleware' => ['check-login']]);
});

Route::get('powerpanel/', 'Auth\LoginController@showLoginForm');
Route::get('powerpanel/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('powerpanel/login', 'Auth\LoginController@login');
Route::post('powerpanel/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('powerpanel/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('powerpanel/register', 'Auth\RegisterController@register');
Route::get('powerpanel/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('powerpanel/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('powerpanel/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('powerpanel/password/sendResetLinkAjax', ['uses' => 'Auth\ResetPasswordController@sendResetLinkAjax']);
Route::post('powerpanel/password/reset', 'Auth\ResetPasswordController@reset');

Route::post('/powerpanel/aliasGenerate', ['uses' => 'PowerpanelController@aliasGenerate'])->name('powerpanel/aliasGenerate');
Route::post('/powerpanel/updateAlias', ['uses' => 'PowerpanelController@updateAlias'])->name('powerpanel/updateAlias');
Route::get('powerpanel/install/{file}', 'PowerpanelController@install');

#Alias Module Routes#####################
Route::post('/powerpanel/generate-seo-content', ['uses' => 'PowerpanelController@generateSeoContent'])->name('powerpanel/generate-seo-content');
#Alias Module Routes#####################

Route::get('cronjob/delete-email-logs', ['uses'=> 'Powerpanel\CronController@deleteEmailLogs']);
    
  
Route::group(['namespace' => 'Powerpanel', 'middleware' => ['auth','prevent-back-history','CheckUserStatus']], function ($request) 
{     
     #Start Routes for term and conditon
     Route::post('powerpanel/add-terms-read', 'TermsConditionsController@insertRead');
     Route::post('powerpanel/add-terms-acccept', 'TermsConditionsController@insertAccept');
     Route::post('powerpanel/terms-accepted-check', 'TermsConditionsController@checkAccepted');
    #End term and contion
     
    #Dashboard Module Routes#####################
    Route::get('powerpanel', 'DashboardController@index');
    Route::get('/powerpanel/dashboard', ['uses' => 'DashboardController@index'])->name('dashboard.index');
    Route::post('/powerpanel/dashboard/ajax', ['uses' => 'DashboardController@ajaxcall'])->name('dashboard.index');
    #Dashboard Module Routes#####################

    #Menu Module Routes#####################
    Route::get('/powerpanel/menu', ['uses' => 'MenuController@index', 'middleware' => ['permission:menu-list']]);
    Route::post('/powerpanel/menu/getMenuType', ['uses' => 'MenuController@getMenuType', 'middleware' => ['permission:menu-list']]);
    Route::post('/powerpanel/menu/addMenuType', ['uses' => 'MenuController@addMenuType', 'middleware' => ['permission:menu-create']]);
    Route::post('/powerpanel/menu/saveMenu', ['uses' => 'MenuController@saveMenu', 'middleware' => ['permission:menu-create']]);
    Route::post('/powerpanel/menu/addMenuItem', ['uses' => 'MenuController@addMenuItem', 'middleware' => ['permission:menu-create']]);
    Route::post('/powerpanel/menu/addMenuItems', ['uses' => 'MenuController@addMenuItems', 'middleware' => ['permission:menu-create']]);
    Route::post('/powerpanel/menu/reload', 'MenuController@reload');
    Route::post('/powerpanel/menu/deleteMenuItem', ['uses' => 'MenuController@deleteMenuItem', 'middleware' => ['permission:menu-delete']]);
    Route::post('/powerpanel/menu/deleteMenu', ['uses' => 'MenuController@deleteMenu', 'middleware' => ['permission:menu-delete']]);
    Route::post('/powerpanel/menu/getMenuItem', ['uses' => 'MenuController@getMenuItem', 'middleware' => ['permission:menu-edit']]);
    Route::post('/powerpanel/menu/updateMenuItem', ['uses' => 'MenuController@updateMenuItem', 'middleware' => ['permission:menu-edit']]);
    Route::post('/powerpanel/menu/aliasGenerate', ['uses' => 'MenuController@aliasGenerate'])->name('powerpanel/menu/aliasGenerate');
    Route::post('/powerpanel/menu/megaMenu', ['uses' => 'MenuController@megaMenu', 'middleware' => ['permission:menu-edit']]);
    Route::post('/powerpanel/menu/getPageList', ['uses' => 'MenuController@getPageList']);
    #Menu Module Routes#####################

    #Media Manager Module Routes#####################
    Route::post('/powerpanel/media/set_image_html', ['uses' => 'MediaController@set_image_html'])->name('powerpanel/media/set_image_html');
    Route::post('/powerpanel/media/set_video_html', ['uses' => 'MediaController@set_video_html'])->name('powerpanel/media/set_video_html');
    Route::post('/powerpanel/media/upload_image', ['uses' => 'MediaController@upload_image'])->name('powerpanel/media/upload_image');
    Route::post('/powerpanel/media/upload_video', ['uses' => 'MediaController@upload_video'])->name('powerpanel/media/upload_video');
    Route::post('/powerpanel/media/user_uploaded_video', ['uses' => 'MediaController@user_uploaded_video'])->name('powerpanel/media/user_uploaded_video');
    Route::post('/powerpanel/media/get_trash_videos', ['uses' => 'MediaController@get_trash_videos'])->name('powerpanel/media/get_trash_videos');
    Route::post('/powerpanel/media/user_uploaded_image', ['uses' => 'MediaController@user_uploaded_image'])->name('powerpanel/media/user_uploaded_image');
    Route::post('/powerpanel/media/load_more_images/{user_id}', ['uses' => 'MediaController@load_more_images'])->name('powerpanel/media/load_more_images');
    Route::post('/powerpanel/media/remove_image', ['uses' => 'MediaController@remove_image'])->name('powerpanel/media/remove_image');
    Route::post('/powerpanel/media/get_recent_uploaded_images', ['uses' => 'MediaController@get_recent_uploaded_images'])->name('powerpanel/media/get_recent_uploaded_images');
    Route::post('/powerpanel/media/get_trash_images', ['uses' => 'MediaController@get_trash_images'])->name('powerpanel/media/get_trash_images');
    Route::post('/powerpanel/media/insert_image_by_url', ['uses' => 'MediaController@insert_image_by_url'])->name('powerpanel/media/insert_image_by_url');
    Route::post('/powerpanel/media/insert_video_by_url', ['uses' => 'MediaController@insert_video_by_url'])->name('powerpanel/media/insert_video_by_url');
    Route::post('/powerpanel/media/insert_vimeo_video_by_url', ['uses' => 'MediaController@insert_vimeo_video_by_url'])->name('powerpanel/media/insert_vimeo_video_by_url');
    Route::post('/powerpanel/media/remove_multiple_image', ['uses' => 'MediaController@remove_multiple_image'])->name('powerpanel/media/remove_multiple_image');
    Route::post('/powerpanel/media/remove_multiple_videos', ['uses' => 'MediaController@remove_multiple_videos'])->name('powerpanel/media/remove_multiple_videos');
    Route::post('/powerpanel/media/restore_multiple_image', ['uses' => 'MediaController@restore_multiple_image'])->name('powerpanel/media/restore_multiple_image');
    Route::post('/powerpanel/media/get_image_details', ['uses' => 'MediaController@getImageDetails'])->name('get_image_details');
    Route::post('/powerpanel/media/save_image_details', ['uses' => 'MediaController@saveImageDetails'])->name('save_image_details');
    Route::post('/powerpanel/media/crop_image', ['uses' => 'MediaController@cropImage'])->name('crop_image');
    Route::post('/powerpanel/media/save_cropped_image', ['uses' => 'MediaController@saveCroppedImage'])->name('save_cropped_image');

    Route::post('/powerpanel/media/restore-multiple-videos', ['uses' => 'MediaController@restore_multiple_videos'])->name('powerpanel/media/restore-multiple-videos');
    Route::post('/powerpanel/media/set_document_uploader', ['uses' => 'MediaController@set_document_uploader'])->name('powerpanel/media/set_document_uploader');
    Route::post('/powerpanel/media/upload_documents', ['uses' => 'MediaController@upload_documents'])->name('powerpanel/media/upload_documents');
    Route::post('/powerpanel/media/user_uploaded_docs', ['uses' => 'MediaController@user_uploaded_docs'])->name('powerpanel/media/user_uploaded_docs');
    Route::post('/powerpanel/media/remove_multiple_documents', ['uses' => 'MediaController@remove_multiple_documents'])->name('powerpanel/media/remove_multiple_documents');
    Route::post('/powerpanel/media/get_trash_documents', ['uses' => 'MediaController@get_trash_documents'])->name('powerpanel/media/get_trash_documents');

    Route::post('/powerpanel/media/check-img-inuse', ['uses' => 'MediaController@checkedUsedImg'])->name('powerpanel/media/check-img-inuse');
    Route::post('/powerpanel/media/restore-multiple-document', ['uses' => 'MediaController@restore_multiple_document'])->name('powerpanel/media/restore_multiple_document');
    Route::post('/powerpanel/media/check-document-inuse', ['uses' => 'MediaController@checkedUsedDocument'])->name('powerpanel/media/check-document-inuse');
    #Media Manager Routes#####################

    #Photo Gallery Module Routes#####################
    Route::post('/powerpanel/photo-gallery/update', ['uses' => 'PhotoGalleryController@store', 'middleware' => ['permission:photo-gallery-edit']])->name('powerpanel/photo-gallery/update');
    Route::post('/powerpanel/photo-gallery/update_status', ['uses' => 'PhotoGalleryController@update_status', 'middleware' => ['permission:photo-gallery-edit']])->name('powerpanel/photo-gallery/update_status');
    Route::post('/powerpanel/photo-gallery/destroy', ['uses' => 'PhotoGalleryController@destroy', 'middleware' => ['permission:photo-gallery-delete']])->name('powerpanel/photo-gallery/destroy');
    #Photo Gallery Module Routes#####################

    #Gallery Module Routes#####################
    Route::post('/powerpanel/gallery/update', ['uses' => 'GalleryController@store', 'middleware' => ['permission:gallery-edit']])->name('powerpanel/gallery/update');
    Route::post('/powerpanel/gallery/update_status', ['uses' => 'GalleryController@update_status', 'middleware' => ['permission:gallery-edit']])->name('powerpanel/gallery/update_status');
    Route::post('/powerpanel/gallery/destroy', ['uses' => 'GalleryController@destroy', 'middleware' => ['permission:gallery-delete']])->name('powerpanel/gallery/destroy');
    #Gallery Module Routes#####################

    #Video Gallery Module Routes#####################
    Route::post('/powerpanel/video-gallery/update', ['uses' => 'VideoGalleryController@store', 'middleware' => ['permission:video-gallery-edit']])->name('powerpanel/video-gallery/update');
    Route::post('/powerpanel/video-gallery/update_status', ['uses' => 'VideoGalleryController@update_status', 'middleware' => ['permission:video-gallery-edit']])->name('powerpanel/video-gallery/update_status');
    Route::post('/powerpanel/video-gallery/destroy', ['uses' => 'VideoGalleryController@destroy', 'middleware' => ['permission:video-gallery-delete']])->name('powerpanel/video-gallery/destroy');
    Route::post('/powerpanel/media/check-video-inuse', ['uses' => 'MediaController@checkedUsedVideo'])->name('powerpanel/media/check-video-inuse');
    #Video Gallery Module Routes#####################

    #Banner Module Routes#####################
    Route::post('/powerpanel/banners/selectRecords', ['uses' => 'BannerController@selectRecords']);
    Route::post('/powerpanel/inner-banner/selectRecords', ['uses' => 'InnerBannerController@selectRecords']);
    #End Banner Module Routes#################

    Route::post('/settings/testMail', ['uses' => 'SettingsController@testMail'])->name('settings/testMail');
    Route::post('/settings/save-module-settings', ['uses' => 'SettingsController@saveModuleSettings'])->name('settings/save-module-settings');
    Route::post('/settings/get-save-module-settings', ['uses' => 'SettingsController@getModuleSettings'])->name('settings/get-save-module-settings');
    Route::post('/settings/get-filtered-modules', ['uses' => 'SettingsController@getModulesAjax'])->name('settings/get-filtered-modules');
    Route::get('/powerpanel/roles/show/{id}', ['uses' => 'RoleController@show'])->name('powerpanel.roles.show');
    Route::patch('/powerpanel/roles/{id}', ['uses' => 'RoleController@handlePost', 'middleware' => ['permission:roles-edit']])->name('powerpanel.roles.update');

    Route::get('/powerpanel/changepassword', array('uses' => 'ProfileController@changepassword', 'middleware' => 'permission:changeprofile-change-password'))->name('powerpanel/changepassword');
    Route::post('/powerpanel/changepassword', array('uses' => 'ProfileController@handle_changepassword'))->name('powerpanel/changepassword');

    Route::post('/powerpanel/media/empty_trash_Image', ['uses' => 'MediaController@empty_trash_image'])->name('powerpanel/media/empty_trash_image');
    Route::post('/powerpanel/media/empty_trash_Video', ['uses' => 'MediaController@empty_trash_video'])->name('powerpanel/media/empty_trash_video');

    Route::post('/powerpanel/media/empty_trash_Document', ['uses' => 'MediaController@empty_trash_document'])->name('powerpanel/media/empty_trash_document');
    Route::post('/powerpanel/static-block/getChildData', ['uses' => 'StaticBlocksController@getChildData'])->name('static-block.index');
});


if ($setConstants) 
{             
    Route::group(['namespace' => 'Powerpanel', 'middleware' => ['auth', 'prevent-back-history', 'CheckUserStatus']], function ($request) {
      $MODULE_NAME = Config::get('Constant.MODULE.NAME');
      $CONTROLLER_NAME = Config::get('Constant.MODULE.CONTROLLER');
        $clientRoles = true;
        $moduleList = array("users",'roles','email-log','logs','login-history');
        if(in_array($MODULE_NAME, $moduleList)){
           $clientRoles = "CheckClientRole";
        }

        Route::get('/powerpanel/' . $MODULE_NAME, ['uses' => $CONTROLLER_NAME . '@index', 'middleware' => ['permission:' . $MODULE_NAME . '-list', $clientRoles]])->name('powerpanel.' . $MODULE_NAME . '.index');
        Route::post('/powerpanel/' . $MODULE_NAME . '/get_list', ['uses' => $CONTROLLER_NAME . '@get_list', 'middleware' => 'permission:' . $MODULE_NAME . '-list']);

        Route::get('/powerpanel/' . $MODULE_NAME . '/add', ['uses' => $CONTROLLER_NAME . '@edit', 'middleware' => ['permission:' . $MODULE_NAME . '-create', $clientRoles]])->name('powerpanel.' . $MODULE_NAME . '.add');
        Route::post('/powerpanel/' . $MODULE_NAME . '/add', ['uses' => $CONTROLLER_NAME . '@handlePost', 'middleware' => 'permission:' . $MODULE_NAME . '-create'])->name('powerpanel.' . $MODULE_NAME . '.handleAddPost');

        Route::get('/powerpanel/' . $MODULE_NAME . '/{alias}/edit', ['uses' => $CONTROLLER_NAME . '@edit', 'middleware' => ['permission:' . $MODULE_NAME . '-edit', $clientRoles]])->name('powerpanel.' . $MODULE_NAME . '.edit');
        Route::post('/powerpanel/' . $MODULE_NAME . '/{alias}/edit', ['uses' => $CONTROLLER_NAME . '@handlePost', 'middleware' => 'permission:' . $MODULE_NAME . '-edit'])->name('powerpanel.' . $MODULE_NAME . '.handleEditPost');

        Route::post('/powerpanel/' . $MODULE_NAME . '/DeleteRecord', ['uses' => $CONTROLLER_NAME . '@DeleteRecord', 'middleware' => ['permission:' . $MODULE_NAME . '-delete', $clientRoles]]);
        Route::post('/powerpanel/' . $MODULE_NAME . '/publish', ['uses' => $CONTROLLER_NAME . '@publish', 'middleware' => 'permission:' . $MODULE_NAME . '-edit']);
        Route::post('powerpanel/' . $MODULE_NAME . '/reorder', ['uses' => $CONTROLLER_NAME . '@reorder', 'middleware' => 'permission:' . $MODULE_NAME . '-list'])->name($MODULE_NAME . '.reorder');
        Route::post('powerpanel/' . $MODULE_NAME . '/destroy', ['uses' => $CONTROLLER_NAME . '@destroy', 'middleware' => 'permission:' . $MODULE_NAME . '-delete'])->name($MODULE_NAME . '.destroy');

        Route::post('/powerpanel/' . $MODULE_NAME . '/makeDefault', ['uses' => $CONTROLLER_NAME . '@makeDefault']);

        Route::post('/powerpanel/' . $MODULE_NAME . '/ajax', ['uses' => $CONTROLLER_NAME . '@ajax', 'middleware' => 'permission:' . $MODULE_NAME . '-list']);

        Route::post('/powerpanel/' . $MODULE_NAME, ['uses' => $CONTROLLER_NAME . '@handleEditPost', 'middleware' => 'permission:' . $MODULE_NAME . '-edit'])->name('powerpanel.' . $MODULE_NAME . '.handleEditPost');

        Route::post('/powerpanel/' . $MODULE_NAME . '/swaporder', $CONTROLLER_NAME . '@reorder');
        Route::post('/powerpanel/' . $MODULE_NAME . '/ajaxCatAdd', $CONTROLLER_NAME . '@addCatAjax');
        Route::get('/powerpanel/' . $MODULE_NAME . '/show/{$id}', ['uses' => $CONTROLLER_NAME . '@show', 'middleware' => 'permission:' . $MODULE_NAME . '-list'])->name('powerpanel.' . $MODULE_NAME . '.show');
        Route::post('/powerpanel/' . $MODULE_NAME . '/makeFeatured', ['uses' => $CONTROLLER_NAME . '@makeFeatured', 'middleware' => 'permission:' . $MODULE_NAME . '-edit']);

        Route::get('powerpanel/newsletter/send_email', ['uses' => 'NewsletterController@send_email'])->name('newsletters/send_email');
        Route::get('/powerpanel/settings', ['uses' => 'SettingsController@index', 'middleware' => 'permission:settings-general-setting-management'])->name('powerpanel/settings');
        Route::post('/powerpanel/settings', ['uses' => 'SettingsController@update_settings', 'middleware' => 'permission:settings-general-setting-management'])->name('powerpanel/settings');
        Route::get('/powerpanel/settings/getDBbackUp', ['uses' => 'SettingsController@getDBbackUp'])->name('powerpanel/settings/getDBbackUp');
        Route::get('/powerpanel/changeprofile', array('uses' => 'ProfileController@index', 'middleware' => 'permission:changeprofile-edit'))->name('powerpanel/changeprofile');
        Route::post('/powerpanel/changeprofile', array('uses' => 'ProfileController@changeprofile'))->name('powerpanel/changeprofile');
        Route::post('/powerpanel/appointment-lead/saveComment', 'AppointmentLeadController@saveComment');
        Route::get('/powerpanel/privacy-removal-leads/search', 'PrivacyRemovalLeadsController@searchLeadsByEmail');
        Route::post('/powerpanel/privacy-removal-leads/get-search-list', 'PrivacyRemovalLeadsController@get_search_list');
        Route::post('/powerpanel/privacy-removal-leads/get-child-data', ['uses' => 'PrivacyRemovalLeadsController@getChildData']);

    });

    Route::group(['namespace' => 'Powerpanel', 'middleware' => ['auth', 'CheckUserStatus']], function ($request) {
      $MODULE_NAME = Config::get('Constant.MODULE.NAME');
      $CONTROLLER_NAME = Config::get('Constant.MODULE.CONTROLLER');
        Route::get('/powerpanel/' . $MODULE_NAME . '/ExportRecord', ['uses' => $CONTROLLER_NAME . '@ExportRecord', 'middleware' => 'permission:' . $MODULE_NAME . '-list']);
    });
}

Route::group(['namespace' => 'Powerpanel', 'middleware' => ['auth','prevent-back-history','CheckUserStatus']], function ($request) {

    Route::get('/powerpanel/email_log', array('uses' => 'EmailLogController@index'))->name('email_log');
    Route::post('/powerpanel/email_log/get_email_log_list', array('uses' => 'EmailLogController@get_email_log_list'));
    Route::post('/powerpanel/notification', array('uses' => 'NotificationController@index'));
    Route::post('/powerpanel/notification/update_read_status', array('uses' => 'NotificationController@update_read_status'));
    Route::post('/powerpanel/notification/get_read_notification_count', array('uses' => 'NotificationController@get_read_notification_count'));
    Route::post('/powerpanel/global', array('uses' => 'GlobalSearchController@index'));
    Route::get('/powerpanel/calender', array('uses' => 'CalenderController@index'));
    Route::get('/powerpanel/calender/get_activity', array('uses' => 'CalenderController@get_activity'));
    Route::post('/powerpanel/message', array('uses' => 'MessageController@index'));
    Route::post('/powerpanel/message/update_read_status', array('uses' => 'MessageController@update_read_status'));
    Route::post('/powerpanel/message/get_read_message_count', array('uses' => 'MessageController@get_read_message_count'));

    Route::get('/powerpanel/analytics', ['uses' => 'AnalyticsController@index', 'middleware' => 'permission:analytics-list'])->name('powerpanel.analytics.index');
    Route::post('analytics/get_range_analysis', ['uses' => 'AnalyticsController@get_range_analysis', 'middleware' => 'permission:analytics-list'])->name('analytics.get_range_analysis');
    Route::get('/powerpanel/plugins', array('uses' => 'PluginController@index'));
    Route::get('/powerpanel/plugins/get_module/{module}', array('uses' => 'PluginController@get_module'));
    Route::get('/powerpanel/plugins/update_module/{module}', array('uses' => 'PluginController@update_module'));

    Route::get('/powerpanel/module-builder/create', array('uses' => 'ModuleBuilderController@create'));
    Route::get('/powerpanel/module-builder/field_template', 'ModuleBuilderController@fieldTemplate');
    Route::post('/powerpanel/module-builder/generate', 'ModuleBuilderController@generate');

});

Route::post('/powerpanel/ckeditor/upload-image', 'PowerpanelController@uploadImage');