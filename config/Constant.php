<?php 
use Jenssegers\Agent\Agent;
$agent = new Agent;				
$device ='pc';

if($agent->isMobile()){
	$device ='mobile';
}elseif($agent->is('iPad')){
	$device ='iPad';
}
return [
	'ENV_APP_KEY' => env('APP_KEY'),
	'ENV_APP_URL' => env('APP_URL'),
	'DEVICE' => $device,
	'RECOMMANDED_IMAGE_SIZES' => '600*600,400*600,600*400'
];

?>