@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif



<!-- faqs_01 S -->
<section class="page_section faqs_01">
    <div class="container">
        <div class="row">
            @if(isset($faqArr) && count($faqArr)>0)
                <div class="col-12">
                    @if (isset($PAGE_CONTENT) && !empty($PAGE_CONTENT))
                        <div class="cms mb-xs-15">
                            {!! $PAGE_CONTENT !!}
                        </div>
                    @endif
                    <div class="accordion" id="accordionExample">
                        @foreach($faqArr as $index => $faqData)
                            <div class="card">
                                <div class="card-header" id="faqs_01_{{$index}}">
                                    <h2 class="title mb-0 @if($index == 0) @else collapsed @endif" data-toggle="collapse" data-target="#faqs_01-{{$index}}" aria-expanded="true" aria-controls="faqs_01-{{$index}}">{{ htmlspecialchars_decode($faqData->varTitle) }}</h2>
                                </div>
                                <div id="faqs_01-{{$index}}" class="collapse @if($index == 0) show @endif" aria-labelledby="faqs_01_{{$index}}" data-parent="#accordionExample">
                                    <div class="card-body cms">
                                        {!! htmlspecialchars_decode($faqData->txtDescription) !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @else
                <div class="col-12">
                    <h2>Coming Soon...</h2>
                </div>
            @endif
        </div>
    </div>
</section>
<!-- faqs_01 E -->
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/faq.js') }}"></script>

@endsection

@endsection
@endif