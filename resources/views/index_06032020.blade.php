@if(!Request::ajax())	
@extends('layouts.app')
@section('content')
@endif

<!-- banner_01 S -->
<section class="banner_01" data-aos="fade-up">    
    <div class="owl-carousel owl-theme owl-dots-absolute owl-nav-absolute owl-nav-full-width">
    	@if(!empty($bannerData) && count($bannerData)>0)		
    		@foreach($bannerData as $banner)
	        <div class="item">
	        	@if(!empty($banner->fkIntVideoId))
	        		@if(!empty($banner->video->youtubeId))
	        			<div class="fill">
	        				<iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{ $banner->video->youtubeId }}?autoplay=0&cc_load_policy=1&disablekb=1&enablejsapi=1&loop=1&modestbranding=1&playsinline=1" allowfullscreen></iframe>
	        			</div>
	        		@endif
	        		@if(!empty($banner->video->vimeoId))
	        			<div class="fill">
	        				<iframe width="100%" height="100%" src="https://player.vimeo.com/video/{{ $banner->video->vimeoId }}?title=0&byline=0&portrait=0" allow="autoplay; fullscreen" allowfullscreen></iframe>
	        			</div>
	        		@endif
	        		@if(!empty($banner->video->varVideoExtension))
	        			<div class="fill">
			        		<video width="100%" height="100%" controls>
			        			<source src="{{url('assets/videos')}}/{{ $banner->video->varVideoName }}.{{ $banner->video->varVideoExtension }}" type="video/mp4">
			        			Your browser does not support the video tag.
			        		</video>
		        		</div>
	        		@endif
	        	@else
		        	<div class="fill lazybg" data-src="{!! App\Helpers\resize_image::resize($banner->fkIntImgId,1980,625) !!}"></div>
		        	<!-- Caption S -->
	                <div class="caption">
	                	<div class="nq-table">
	                		<div class="nq-center">
		                        <div class="container">
		                        	<!-- <div class="small_title">It’s proudly for us to build stylish & featured interior design</div> -->
		                        	<!-- <div class="small_title">{!! htmlspecialchars_decode($banner->varTitle) !!}</div> -->
		                        	@if(!empty($banner->varSubTitle))
		                        		<div class="small_title">{!! htmlspecialchars_decode($banner->varSubTitle) !!}</div>
		                        	@endif
		                        	{{-- @if(!empty($banner->txtDescription))
		                        		<div class="sub_title">{!! htmlspecialchars_decode($banner->txtDescription) !!}</div>
		                        	@endif --}}
		                        	{{-- <a href="#" title="Read More" class="btn btn-primary">Read More</a>  --}}
		                        </div>
	                		</div>
	                	</div>
	                </div>
	                <!-- Caption E -->
                @endif
	        </div>
	        @endforeach
	    @else
	        <div class="item">
	        	<div class="fill lazybg" data-src="{!! App\Helpers\resize_image::resize(Config::get('Constant.DEFAULT_HOME_BANNER')) !!}"></div>
	        	<!-- Caption S -->
	        	<div class="caption">
                	<div class="nq-table">
                		<div class="nq-center">
	                        <div class="container">
	                        	<div class="small_title">{{ Config::get('Constant.DEFAULT_BANNER_TITLE') }}</div>	                        	
	                        	<div class="title">{{ Config::get('Constant.DEFAULT_BANNER_SUBTITLE') }}</div>
	                        </div>
                		</div>
                	</div>
                </div>
                <!-- Caption E -->
	        </div>
	    @endif
    </div>
</section>
<!-- banner_01 E -->
<!-- section_01 S -->
@if(isset($staticDataWelcome) && count($staticDataWelcome) > 0)
<section class="building-home" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="building-home-top text-center">
					<h1 class="nqtitle">
						{!! htmlspecialchars_decode($staticDataWelcome['0']['varTitle']) !!}
					</h1>
					<p>{!! str_limit(htmlspecialchars_decode($staticDataWelcome['0']['txtDescription']),635) !!}
					</p>
					<ul class="building-home-btn">
						<li>
							<a href="{{ url('building-category') }}" title="View All Plans" class="btn">View All Plans</a>
						</li>
						<li>
							<a href="{{ url('our-process') }}" title="View Our Process" class="btn">View Our Process</a>
						</li>
					</ul>
				</div>
			</div>
			@if(!empty($buildingcategory) && count($buildingcategory)>0)	
			<div class="col-12">
				<div class="building-home-bottom d-lg-flex owl-carousel owl-theme owl-dots-absolute owl-nav-absolute">
					@foreach($buildingcategory as  $index => $category)
					<!-- loop start -->
					<div class="home-list item">
						<h2>
							<a href="{{ url('building-category',$category->alias->varAlias) }}" class="home-title" title="{{ htmlspecialchars_decode($category->varTitle) }}">{{ htmlspecialchars_decode($category->varTitle) }}</a>
						</h2>
						<div class="home-box">
							<p>{{ htmlspecialchars_decode($category->txtShortDescription) }}</p>
							<div class="thumbnail-container">
								<div class="thumbnail">
									<picture>
                                    <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}">
                                    <img src="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($category->varTitle) }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">
                                </picture>
								</div>
							</a>
							<div class="home-list-btn">
								<a href="{{ url('building-category',$category->alias->varAlias) }}" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
								<span>	View All Plans</span> <i class="icon-arrow"></i></a>
							</div>
						</div>
					</div>
					</div>
					<!-- loop end -->
					@endforeach
				</div>
			@else         
             <div class="col-12">
               <h2>No categories are available...</h2>
             </div>         
        @endif			
		</div>
	</div>
</section>
@endif
<!-- section_01 E -->
<!-- Our Service Section S -->
@if(isset($services) && count($services)>0)
@php //echo '<pre>'; print_r($services);die @endphp
<section class="our-service-section" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="nqtitle">Our Services</h2>
			</div>
			<div class="col-12">
				<div class="our-service-tabbing">
					<ul class="nav nav-tabs" role="tablist">					
					@foreach($services as $index => $doc)
						@php
							$docType = $doc['type'];
							$docList = $doc['services'];
						@endphp
						@if(isset($docType) && count($docType) > 0)																			
							<li class="nav-item">
								<a class="nav-link {{ ($index == 0) ? 'active' : '' }}" title="{{ $docType['varTitle'] }}" data-toggle="tab" href="#{{$index}}">{{ $docType['varTitle'] }}</a>
							</li>								
						@endif
					@endforeach
					</ul>					

					<!-- @if(isset($serviceCategory) && count($serviceCategory)>0)
					<ul class="nav nav-tabs" role="tablist">
						@foreach($serviceCategory as  $index => $serviceCat) 
					    <li class="nav-item">
					      <a class="nav-link @if($serviceCat->varTitle == 'Design Services') active @endif" title="{{ $serviceCat->varTitle }}" data-toggle="tab" href="#{{$serviceCat->alias->varAlias}}">{{ $serviceCat->varTitle }}</a>
					    </li>
					    @endforeach					  
					</ul>
					@endif -->
					<?php /*
					<!-- ###Sevice tab content start### -->
					<div class="tab-content">
						<div id="design-services" class="tab-pane active">
							<div class="owl-carousel owl-theme owl-nav-absolute">
								@foreach($serviceCategory as  $index => $serviceCat)

								@foreach($services as  $index => $service) 
								<?php $catId = unserialize($service->txtCategories); ?>
								<div class="item">
									<a href="{{ url('services',$service->alias->varAlias) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}">
										<div class="thumbnail-container">
											<div class="thumbnail">
												<picture>
		                                            <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($service->fkIntImgId) !!}">
		                                            <img src="{!! App\Helpers\resize_image::resize($service->fkIntImgId,360,240) !!}" alt="{{ htmlspecialchars_decode($service->varTitle) }}">
		                                        </picture>
											</div>
											<h3 class="services-title">{{ htmlspecialchars_decode($service->varTitle) }}</h3>
										</div>
									</a>
								</div>
                  		  		@endforeach	
                    			
								@endforeach							
							</div>
						</div>
						<div id="construction-services" class="fade tab-pane">
							<div class="owl-carousel owl-theme owl-nav-absolute">
								@foreach($services as  $index => $service) 
								<div class="item">
									<a href="{{ url('services',$service->alias->varAlias) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}">
										<div class="thumbnail-container">
											<div class="thumbnail">
												<picture>
		                                            <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($service->fkIntImgId) !!}">
		                                            <img src="{!! App\Helpers\resize_image::resize($service->fkIntImgId,360,240) !!}" alt="{{ htmlspecialchars_decode($service->varTitle) }}">
		                                        </picture>
											</div>
											<h3 class="services-title">{{ htmlspecialchars_decode($service->varTitle) }}</h3>
										</div>
									</a>
								</div>
								@endforeach								
							</div>
						</div>
					</div> 
					<!-- ###Sevice tab content end### -->
					*/ ?>
				</div>
		</div>
	</div>
</section>
@endif
<!-- Our Service Section E -->
<!-- Our 8 Step Building Journey S -->
@if(isset($ourprocessArr) && count($ourprocessArr)>0)
<section class="process-section">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="nqtitle" data-aos="fade-up">Our 8 Step Building Journey</h2>
			</div>
		</div>
		<div class="row">
			@foreach($ourprocessArr as  $index => $ourprocess) 
			<div class="col-lg-3 col-md-4 col-6 d-flex align-items-stretch">
				<div class="card" data-aos="fade-up" data-aos-delay="300">
					<div class="card-body">
						<h5 class="card-title"><span class="number">0{{ htmlspecialchars_decode($ourprocess->intDisplayOrder) }}</span> {{ htmlspecialchars_decode($ourprocess->varTitle) }}</h5>
							<div class="short_text">
							<p class="card-text">{!! htmlspecialchars_decode($ourprocess->txtDescription) !!}</p>
						</div>
					</div>
				</div>
			</div>
			@endforeach			
		</div>
		<div class="row">
			<div class="col-12">
				<div class="congratulation" data-aos="fade-up" data-aos-delay="2700">
					<span class="thumb-icon"></span>
					@if(isset($staticCongratulations) && count($staticDataWelcome) > 0)
					<div class="con-body">				
						<div class="cong-title">{!! htmlspecialchars_decode($staticCongratulations['0']['varTitle']) !!}</div>
						<div class="short-text">{!! str_limit(htmlspecialchars_decode($staticCongratulations['0']['txtDescription']),635) !!}</div>
					</div>
					@endif
				</div>
				<hr/>
			</div>
		</div>
	</div>
</section>
@endif
<!-- Our 8 Step Building Journey E -->
@if(isset($ourworksArr) && count($ourworksArr) > 0)
<section class="our-recent-work" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3 class="nqtitle">Our Recent Work</h3>
				<a href="{{ url('our-works') }}" title="View All Work" class="btn-secondary float-right">View All Work</a>
			</div>
		</div>
		<div class="row">			
			<div class="recent-work-desc owl-carousel owl-theme owl-nav-absolute">
				@foreach($ourworksArr as $index => $ourworks)
				<div class="item">
					<div class="col-xl-7 col-lg-6 col-md-12 col-12 pr-lg-0">
						<div class="our-recent-work-image">
							<a href="{{ url('our-works',$ourworks->alias->varAlias) }}" title="{{ htmlspecialchars_decode($ourworks->varTitle) }}" class="image_hover">
								<div class="thumbnail-container">
									<div class="thumbnail">
										<img data-src="{!! App\Helpers\resize_image::resize($ourworks->fkIntImgId,825,550) !!}" class="owl-lazy" src="assets/images/loader.svg" alt="{{ htmlspecialchars_decode($ourworks->varTitle) }}">
										<span class="mask"></span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-xl-5 col-lg-6 col-md-12 col-12">
						<div class="our-recent-work-detail">								
							<h4>
								<a href="{{ url('our-works',$ourworks->alias->varAlias) }}" class="work-name" title="{{ htmlspecialchars_decode($ourworks->varTitle) }}">
									{{ htmlspecialchars_decode($ourworks->varTitle) }}
								</a>
							</h4>
							<span>{{ htmlspecialchars_decode($ourworks->txtOwner) }}</span>
							<div class="work-desc">
								<p>{{ htmlspecialchars_decode($ourworks->txtShortDescription) }}</p>
							</div>
							<a href="{{ url('our-works',$ourworks->alias->varAlias) }}" title="Learn More" class="btn-tertiary d-flex align-items-center">
								<span>Learn More</span><i class="icon-arrow"></i>
							</a>
						</div>
					</div>
				</div>	
				@endforeach				
			</div>			
		</div>
	</div>
</section>
@endif
<!-- Testimonial S-->
@if(isset($testimonialArr) && count($testimonialArr) > 0)
<section class="testimonial-section" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h3 class="nqtitle">Testimonials</h3>
			</div>
			<div class="col-12">
				<div class="single-testimonial owl-carousel owl-theme owl-nav-absolute">
					@foreach($testimonialArr as $testimonial_info)					
					<div class="item">
						<!-- Custom video image -->
						@if(!empty($testimonial_info->fkIntVideoId))							
							 @if(!empty($testimonial_info->video->youtubeId))
							 <div class="testi-img">									
								<a data-fancybox="testimonial" href="https://www.youtube.com/watch?v={{ $testimonial_info->video->youtubeId }}">								
									<div class="thumbnail-container">
										<div class="thumbnail">
											<img src="http://img.youtube.com/vi/{{ $testimonial_info->video->youtubeId }}/hqdefault.jpg" alt="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}" title="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}">
										</div>
										<div class="overlay">
											<i class="fa fa-play-circle" title="play"></i>
										</div>
									</div>
								</a>
							</div>
							@endif								
						@else
						 <div class="testi-img">
							<a data-fancybox="testimonial" href="{!! App\Helpers\resize_image::resize($testimonial_info->fkIntImgId,391,260) !!}">
								<div class="thumbnail-container">
									<div class="thumbnail">
										<img src="{!! App\Helpers\resize_image::resize($testimonial_info->fkIntImgId,391,260) !!}" alt="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}" title="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}">
									</div>
									<div class="overlay">
										<i class="fa fa-play-circle" title="play"></i>
									</div>
								</div>
							</a>
						</div> 
							<!-- Caption E -->
						@endif
						<!-- Custom video image -->
						<div class="testi-desc">
							<h4 class="test-title">
								{{ htmlspecialchars_decode($testimonial_info->varTitle) }}
							</h4>
							<i class="fa fa-quote-left"></i>
							<div class="testi-border">
								<p>{!! htmlspecialchars_decode($testimonial_info->txtDescription) !!}</p>
									<span class="left"></span>
									<span class="center"></span>
									<span class="right"></span>								
							</div>
							<div class="testi-writer">{{ htmlspecialchars_decode($testimonial_info->varAuthor) }}</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
			<div class="col-12 text-center">
				<a href="{{ url('testimonial') }}" class="btn-secondary testimonial-btn" title="View All Testimonials">View All Testimonials</a>
			</div>
		</div>
	</div>
</section>
@endif
<!-- Testimonial E-->
<!-- section_15 E -->
@if(!Request::ajax())
@section('footer_scripts')

<script src="{{ url('assets/js/index.js') }}"></script>
@endsection
@endsection
@endif