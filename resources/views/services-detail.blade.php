@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
<section class="page_section service_detail" data-aos="fade-up">
    <div class="container">
        <div class="row">
        	@if(isset($service) && !empty($service))
            <div class="col-xl-9 col-lg-12">   
                <!-- #### multiple gallery images slider #### -->
                <div class="detail-img mt-sm-2 mb-sm-30 mb-xs-40">
                    <div class="owl-carousel owl-theme owl-dots-absolute owl-nav-absolute owl-nav-full-width">
                        @if(isset($service->fkIntImgId) && !empty($service->fkIntImgId) || count($videoObj) > 0)
                            @if(isset($service->fkIntImgId) && !empty($service->fkIntImgId))
                                @php $imgArr =  explode(',',$service->fkIntImgId) @endphp
                                @if(!empty($imgArr))
                                    @foreach($imgArr as $key => $val)
                                        <div class="item">
                                            <div class="thumbnail-container">
                                                <div class="thumbnail">
                                                    <picture>
                                                        <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($val,848,565) !!}">
                                                        <img src="{!! App\Helpers\resize_image::resize($val,848,565) !!}" alt="{{ htmlspecialchars_decode($service->varTitle) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}"/>
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>                                        
                                    @endforeach
                                @endif
                            @endif
                            @if(count($videoObj) > 0)
                                @foreach($videoObj as $v_key => $v_val)
                                    <div class="item">
                                        <div class="thumbnail-container">
                                            <div class="thumbnail">
                                                @if(!empty($v_val->youtubeId))
                                                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{ trim($v_val->youtubeId) }}?autoplay=0&cc_load_policy=1&disablekb=1&enablejsapi=1&loop=1&modestbranding=1&playsinline=1" frameborder="0" allowfullscreen></iframe>
                                                @endif
                                                @if(!empty($v_val->vimeoId))
                                                    <iframe width="100%" height="100%" src="https://player.vimeo.com/video/{{ $v_val->vimeoId }}?title=0&byline=0&portrait=0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if(!empty($v_val->varVideoExtension))
                                                    <video width="100%" height="100%" controls>
                                                        <source src="{{ $APP_URL }}/assets/videos/{{ $v_val->varVideoName.'.'.$v_val->varVideoExtension }}" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            @else
                                <div class="item">
                                    <div class="thumbnail-container">
                                        <div class="thumbnail">
                                            <picture>
                                                <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($service->fkIntImgId) !!}">
                                                <img src="{!! App\Helpers\resize_image::resize($service->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($service->varTitle) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}" />
                                            </picture>
                                        </div>
                                    </div>
                                </div>
                            @endif
                    </div>
                </div>
                <!-- ### multiple images gallery slider ### -->
                <!-- ###Service detail ck content### -->                
                <div class="cms">
                    <h2 class="nqtitle service-title">{{ htmlspecialchars_decode($service->varTitle)}}</h2>
                    @if (isset($service->txtDescription) && !empty($service->txtDescription))
                        {!! htmlspecialchars_decode($service->txtDescription) !!}    
                    @else
                        <p>{!! htmlspecialchars_decode($service->txtShortDescription) !!}</p>
                    @endif                    
                </div>
                <!-- ###Service detail ck content### -->                
                <!-- ## Related services ## -->
                @if(isset($similarServices) && $similarServices->count() > 0)                                                        
                    <div class="related">
                        <h2 class="nqtitle title mb-xs-15">Related Services</h2>
                        <div class="owl-carousel owl-theme owl-nav-absolute">
                        @foreach($similarServices as $key => $value)                                                    
                            <div class="item">
                                <article class="image_hover">
                                    <a href="{{ route('serviceDetail',$value->alias->varAlias) }}" title="{{ $value->varTitle }}">
                                        <!-- <div class="row"> -->
                                            @php $imgArr =  explode(',',$value->fkIntImgId) @endphp
                                            @if(!empty($imgArr))
                                                <!-- <div class="col-4"> -->
                                                    <div class="thumbnail-container">
                                                        <div class="thumbnail">
                                                            <picture>
                                                                <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($imgArr[0],340,551) !!}">
                                                                <img src="{!! App\Helpers\resize_image::resize($imgArr[0],340,551) !!}" alt="{{ $value->varTitle }}" title="{{ $value->varTitle }}" />
                                                            </picture>
                                                            <span class="mask"></span>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <div class="content">
                                                        <h3 class="r_title">{{ $value->varTitle }}</h3>
                                                    </div>
                                                <!-- </div> -->
                                           
                                            <!-- <div class="col">
                                                <h4 class="r_title">{{ $value->varTitle }}</h4>
                                            </div> -->
                                        <!-- </div> -->
                                    </a>
                                </article>
                            </div>
                        @endforeach
                        </div>
                    </div>
                @endif       
            </div>
            <!-- ## Related services ## -->

            <div class="col-xl-3 col-lg-12">
                <div class="service-right right-panel">
                    @if(isset($serviceCategory) && count($serviceCategory) > 0)
                        <article class="detail-category cms">
                            <div class="nqtitle form-title">Category</div>
                            <ul class="category">                    	
                        		@foreach($serviceCategory as $key => $value)                                                                   
                            		<li>
                        				<a href="{{ url('service-category') }}/{{ $value->alias->varAlias }}" title="{{ $value->varTitle }}">{{ $value->varTitle }}</a>                                          
                                        {!! Form::hidden('area_of_interest',$value->id) !!}                                                                                    
                                    </li>
                        		@endforeach     
                            </ul>
                        </article>
                    @endif                    
                    <!-- @include('layouts.sidebar') -->
                    <article class="panel-form">
                        <div class="nqtitle form-title mb-xs-15">Have Any Question?</div>
                        {!! Form::open(['method' => 'post','class'=>'request-form nqform nq-form-md', 'id'=>'request_page_form']) !!}
                            {!! Form::hidden('service_id',$service->id) !!} 
                            <div class="row align-items-start">
                                <div class="col-12 text-right">
                                    <div class="required">* Denotes Required Inputs</div>
                                </div>
                                <div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">					
                                        <label class="nq-label" for="first_name">First Name<span class="star">*</span></label>					
                                        {!! Form::text('first_name', old('first_name'), array('id'=>'first_name', 'class'=>'form-control nq-input', 'name'=>'first_name', 'maxlength'=>'60', 'ondrop'=>'return false;')) !!}
                                        @if ($errors->has('first_name'))
                                            <span class="error">{{ $errors->first('first_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">					
                                        <label class="nq-label" for="last_name">Last Name<span class="star">*</span></label>					
                                        {!! Form::text('last_name', old('last_name'), array('id'=>'last_name', 'class'=>'form-control nq-input', 'name'=>'last_name', 'maxlength'=>'60', 'ondrop'=>'return false;')) !!}
                                        @if ($errors->has('last_name'))
                                            <span class="error">{{ $errors->first('last_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label class="nq-label" for="contact_email">Email<span class="star">*</span></label>
                                        {!! Form::email('contact_email', old('contact_email'), array('id'=>'contact_email', 'class'=>'form-control nq-input', 'name'=>'contact_email', 'maxlength'=>'60', 'ondrop'=>'return false;')) !!}
                                        @if ($errors->has('contact_email'))
                                            <span class="error">{{ $errors->first('contact_email') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                    <label class="nq-label" for="phone_number">Phone<span class="star">*</span></label>
                                        {!! Form::text('phone_number', old('phone_number'), array('id'=>'phone_number', 'class'=>'form-control nq-input', 'name'=>'phone_number', 'maxlength'=>"20", 'onpaste'=>'return false;', 'ondrop'=>'return false;', 'onkeypress'=>'javascript: return KeycheckOnlyPhonenumber(event);')) !!}
                                        @if ($errors->has('phone_number'))
                                            <span class="error">{{ $errors->first('phone_number') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12  col-sm-12">
                                    <div class="form-group">
                                        <label class="nq-label" for="user_message">Comments</label>
                                        {!! Form::textarea('user_message', old('user_message'), array('class'=>'form-control nq-textarea', 'name'=>'user_message', 'rows'=>'3', 'id'=>'user_message', 'spellcheck'=>'true', 'ondrop'=>'return false;' )) !!}
                                        @if ($errors->has('user_message'))
                                            <span class="error">{{ $errors->first('user_message') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group captcha">
                                        <div id="contact_html_element" class="g-recaptcha"></div>
                                        <div class="capphitcha" data-sitekey="{{Config::get('Constant.GOOGLE_CAPCHA_KEY')}}">
                                        @if ($errors->has('g-recaptcha-response'))
                                            <label class="error help-block">{{ $errors->first('g-recaptcha-response') }}</label>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-6 col-md-6 col-sm-6 service-btn">
                                    <div class="form-group">
                                        <button type="submit" class="btn-primary" title="Send Message">Send Message</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </article>                    
                </div>
            </div>
            @else
            <div class="col-12">
            	<h2>No records found</h2>
            </div>
            @endif
        </div>
    </div>
</section>
<script type="text/javascript">
    var sitekey = '{{Config::get("Constant.GOOGLE_CAPCHA_KEY")}}';
    var onContactloadCallback = function() {
        grecaptcha.render('contact_html_element', {
            'sitekey' : sitekey
        });
    };                        
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onContactloadCallback&render=explicit" async defer></script>

@if(!Request::ajax())
@section('footer_scripts')

<script src="{{ url('assets/libraries/owl.carousel/js/owl.carousel.min.js') }}"></script>

<script src="{{ url('assets/libraries/libraries-update/owl.carousel/js/owl.carousel-update.js') }}"></script>

<script src="{{ url('https://static.addtoany.com/menu/page.js') }}"></script>

<script src="{{ url('assets/js/services-detail.js') }}"></script>

<script src="{{ url('assets/js/request-a-quote.js') }}"></script>

@endsection

@endsection
@endif