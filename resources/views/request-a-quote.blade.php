@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
<!-- contact_01 S -->
<section class="page_section request_quote" data-aos="fade-up">
	<div class="container">
		<div class="row">
			@if(!empty($PAGE_CONTENT))
				<div class="col-12">
				  	{!! $PAGE_CONTENT !!}
				</div>
			@endif
			<div class="col-12 contact-form">
				<!-- <div class="request-title cms">
					<h2>Request A Quote</h2>
				 </div> -->
				<!-- <h2 class="nqtitle">Get in Touch</h2> -->
				{!! Form::open(['method' => 'post','class'=>'request-form nqform nq-form-md', 'id'=>'request_page_form']) !!}
					<div class="row align-items-start">
                        <div class="col-md-12 text-right">
                            <div class="required">* Denotes Required Inputs</div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                            	<label class="nq-label" for="first_name">First Name<span class="star">*</span></label>
                                {!! Form::text('first_name', old('first_name'), array('id'=>'first_name', 'class'=>'form-control nq-input', 'name'=>'first_name', 'maxlength'=>'60', 'ondrop'=>'return false;')) !!}
                                @if ($errors->has('first_name'))
                                	<span class="error">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="nq-label" for="last_name">Last Name<span class="star">*</span></label>
                                {!! Form::text('last_name', old('last_name'), array('id'=>'last_name', 'class'=>'form-control nq-input', 'name'=>'last_name', 'maxlength'=>'60', 'ondrop'=>'return false;')) !!}
                                @if ($errors->has('last_name'))
                                    <span class="error">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="nq-label" for="contact_email">Email<span class="star">*</span></label>
                                {!! Form::email('contact_email', old('contact_email'), array('id'=>'contact_email', 'class'=>'form-control nq-input', 'name'=>'contact_email', 'maxlength'=>'60', 'ondrop'=>'return false;')) !!}
                                @if ($errors->has('contact_email'))
                                    <span class="error">{{ $errors->first('contact_email') }}</span>
                                @endif
                            </div>
                        </div>
                        <?php /* commented by admin 02-03-2020*/ ?>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="form-group">
                                <label class="nq-label" for="area_of_interest">Area of interest</label>
                                {!! Form::text('area_of_interest', old('area_of_interest'), array('id'=>'area_of_interest', 'class'=>'form-control nq-input', 'name'=>'area_of_interest', 'maxlength'=>'60', 'ondrop'=>'return false;')) !!}
                                @if ($errors->has('area_of_interest'))
                                    <span class="error">{{ $errors->first('area_of_interest') }}</span>
                                @endif
                            </div>
                        </div> 

                        <!-- create by author 04-03-2020 -->
                        @php //echo '<pre>';print_r($ServiceCategory);die; @endphp
                        <?php /* <div class="col-lg-6 col-md-6 col-sm-6">    
                            <div class="form-group">
                                {!! Form::select('area_of_interest',$ServiceCategory, old('area_of_interest'), array('id'=>'area_of_interest','class'=>'selectpicker ac-bootstrap-select','name'=>'area_of_interest', )); !!}
                                @if ($errors->has('area_of_interest'))
                                    <span class="error">{{ $errors->first('area_of_interest') }}</span>
                                @endif                            
                            </div>
                        </div> */ ?>
                        <!-- ended -->
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                            	<label class="nq-label" for="phone_number">Phone<span class="star">*</span></label>
                            	{!! Form::text('phone_number', old('phone_number'), array('id'=>'phone_number', 'class'=>'form-control nq-input', 'name'=>'phone_number', 'maxlength'=>"20", 'onpaste'=>'return false;', 'ondrop'=>'return false;', 'onkeypress'=>'javascript: return KeycheckOnlyPhonenumber(event);')) !!}
                            	@if ($errors->has('phone_number'))
                            		<span class="error">{{ $errors->first('phone_number') }}</span>
                            	@endif
                            </div>
                        </div>                        
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                            	<label class="nq-label" for="user_message">Comments</label>
                            	{!! Form::textarea('user_message', old('user_message'), array('class'=>'form-control nq-textarea', 'name'=>'user_message', 'rows'=>'6', 'id'=>'user_message', 'spellcheck'=>'true', 'ondrop'=>'return false;' )) !!}
                            	@if ($errors->has('user_message'))
                            		<span class="error">{{ $errors->first('user_message') }}</span>
                            	@endif
                            </div>
                        </div>
                        @if(File::exists(app_path().'/NewsletterLead.php'))
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                <div class="nq-checkbox-list">
                                    <label class="nq-checkbox pt-xs-0">
                                        <input name="subscribe" type="checkbox"> Subscribe me to your newsletter as well<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-lg-6 col-md-12 col-sm-6 text-md-center">
                            <div class="form-group captcha">
                              	<div id="contact_html_element" class="g-recaptcha"></div>
                              	<div class="capphitcha" data-sitekey="{{Config::get('Constant.GOOGLE_CAPCHA_KEY')}}">
                                @if ($errors->has('g-recaptcha-response'))
                                	<label class="error help-block">{{ $errors->first('g-recaptcha-response') }}</label>
                                @endif
                              	</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-6 text-lg-right text-md-center text-sm-right form-btn text-center">
                            <div class="form-group">
                                <button type="submit" class="btn-primary" title="Submit">Submit</button>
                            </div>
                        </div>
                    </div>
				{!! Form::close() !!}
			</div>			
		</div>
	</div>    
</section>
<script type="text/javascript">
  	var sitekey = '{{Config::get("Constant.GOOGLE_CAPCHA_KEY")}}';
  	var onContactloadCallback = function() {
    	grecaptcha.render('contact_html_element', {
      		'sitekey' : sitekey
    	});
  	};

	/* @php
	  	$current_adress = !empty($contact_info->txtAddress)?$contact_info->txtAddress:'';
	  	$pinaddress = explode("*", trim(preg_replace('/\s\s+/', '*', $current_adress)));
	  	$pinaddress = implode('<br/>', $pinaddress);
	@endphp
	var address = "{{ trim(preg_replace('/\s\s+/', ' ',  $current_adress)) }}";
	var pinaddress = "{!! $pinaddress !!}"; */
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onContactloadCallback&render=explicit" async defer></script>
<script src="{{ url('assets/libraries/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<!-- contact_01 E -->
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/request-a-quote.js') }}"></script>
<script src="{{ url('assets/libraries/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
@endsection

@endsection
@endif