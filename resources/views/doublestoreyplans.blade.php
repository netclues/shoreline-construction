@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
    <section class="page_section buildhome_list building-home" data-aos="fade-up">
        <div class="container">
            <div class="row">
               <div class="col-12 d-sm-flex align-items-center justify-content-center text-center bhomelist-title">
                    <div class="sub-title cms mr-sm-auto">
                        <h2 class="nqtitle">8 Category Of Double Story</h2>
                    </div>
                     <div class="sorting_sec">
                        <div class="sort_by">
                            <span>Sort by:</span>
                            <select class="selectpicker ac-bootstrap-select">
                                <option>A to Z</option>
                                <option>Z to A</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="bsub-list">
                     <div class="blist-image">
                         <div class="thumbnail-container">
                             <div class="thumbnail">
                                 <img src="assets/images/loader.svg" data-src="assets/images/double_story.png" class="lazy" alt="Double Story" title="Double Story">
                             </div>
                         </div>
                     </div>
                     <div class="buildhome-box text-center">
                          <h3>
                             <a href="" class="category-title" title="Double Story">Double Story</a>
                         </h3>
                         <div class="desc">
                             <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                         </div>
                          
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="bsub-list">
                     <div class="blist-image">
                         <div class="thumbnail-container">
                             <div class="thumbnail">
                                 <img src="assets/images/loader.svg" data-src="assets/images/single_story.png" class="lazy" alt="Double Story" title="Double Story">
                             </div>
                         </div>
                     </div>
                     <div class="buildhome-box text-center">
                          <h3>
                             <a href="" class="category-title" title="Double Story">Double Story</a>
                         </h3>
                         <div class="desc">
                             <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                         </div>
                          
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="bsub-list">
                     <div class="blist-image">
                         <div class="thumbnail-container">
                             <div class="thumbnail">
                                 <img src="assets/images/loader.svg" data-src="assets/images/duplex.png" class="lazy" alt="Double Story" title="Double Story">
                             </div>
                         </div>
                     </div>
                     <div class="buildhome-box text-center">
                          <h3>
                             <a href="" class="category-title" title="Double Story">Double Story</a>
                         </h3>
                         <div class="desc">
                             <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                         </div>
                          
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="bsub-list">
                     <div class="blist-image">
                         <div class="thumbnail-container">
                             <div class="thumbnail">
                                 <img src="assets/images/loader.svg" data-src="assets/images/container_homes.png" class="lazy" alt="Double Story" title="Double Story">
                             </div>
                         </div>
                     </div>
                     <div class="buildhome-box text-center">
                          <h3>
                             <a href="" class="category-title" title="Double Story">Double Story</a>
                         </h3>
                         <div class="desc">
                             <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                         </div>
                          
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="bsub-list">
                     <div class="blist-image">
                         <div class="thumbnail-container">
                             <div class="thumbnail">
                                 <img src="assets/images/loader.svg" data-src="assets/images/Others.png" class="lazy" alt="Double Story" title="Double Story">
                             </div>
                         </div>
                     </div>
                     <div class="buildhome-box text-center">
                          <h3>
                             <a href="" class="category-title" title="Double Story">Double Story</a>
                         </h3>
                         <div class="desc">
                             <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                         </div>
                          
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="bsub-list">
                     <div class="blist-image">
                         <div class="thumbnail-container">
                             <div class="thumbnail">
                                 <img src="assets/images/loader.svg" data-src="assets/images/Others.png" class="lazy" alt="Double Story" title="Double Story">
                             </div>
                         </div>
                     </div>
                     <div class="buildhome-box text-center">
                          <h3>
                             <a href="" class="category-title" title="Double Story">Double Story</a>
                         </h3>
                         <div class="desc">
                             <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                         </div>
                          
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="bsub-list">
                     <div class="blist-image">
                         <div class="thumbnail-container">
                             <div class="thumbnail">
                                 <img src="assets/images/loader.svg" data-src="assets/images/Others.png" class="lazy" alt="Double Story" title="Double Story">
                             </div>
                         </div>
                     </div>
                     <div class="buildhome-box text-center">
                          <h3>
                             <a href="" class="category-title" title="Double Story">Double Story</a>
                         </h3>
                         <div class="desc">
                             <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                         </div>
                          
                     </div>
                  </div>
               </div>
               <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="bsub-list">
                     <div class="blist-image">
                         <div class="thumbnail-container">
                             <div class="thumbnail">
                                 <img src="assets/images/loader.svg" data-src="assets/images/double_story.png" class="lazy" alt="Double Story" title="Double Story">
                             </div>
                         </div>
                     </div>
                     <div class="buildhome-box text-center">
                          <h3>
                             <a href="" class="category-title" title="Double Story">Double Story</a>
                         </h3>
                         <div class="desc">
                             <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                         </div>
                          
                     </div>
                  </div>
               </div>

                <div class="col-12">
                    <ul class="pagination" role="navigation">
                        <li class="page-item disabled" aria-disabled="true" aria-label="« Previous">
                            <span class="page-link" aria-hidden="true">‹</span>
                        </li>
                        <li class="page-item active" aria-current="page">
                            <span class="page-link">1</span>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">2</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#" rel="next" aria-label="Next »">›</a>
                        </li>
                    </ul>
                </div>

               <div class="col-12">
                  <div class="bottm-title cms">
                        <h2 class="nqtitle">Other Category</h2>
                  </div>                   
               </div>
               <div class="col-12">
                    <div class="d-lg-flex building-home-bottom owl-carousel owl-theme owl-dots-absolute owl-nav-absolute">
                        <div class="home-list item">
                            <h2>
                                <a href="" class="home-title" title="Single Story">Single Story</a>
                            </h2>
                            <div class="home-box">
                                <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                                <div class="thumbnail-container">
                                    <div class="thumbnail">
                                        <img data-src="assets/images/single_story.png" class="owl-lazy" alt="Single Story" title="Single Story">
                                    </div>
                                </div>
                                <div class="home-list-btn">
                                    <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                    <span>	View All Plans</span> <i class="icon-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                         
                        <div class="home-list item">
                            <h2>
                                <a href="" class="home-title" title="Duplex">Duplex</a>
                            </h2>
                            <div class="home-box">
                                <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                                <div class="thumbnail-container">
                                    <div class="thumbnail">
                                        <img data-src="assets/images/duplex.png" class="owl-lazy" alt="Duplex" title="Duplex">
                                    </div>
                                </div>
                                <div class="home-list-btn">
                                    <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                    <span>	View All Plans</span> <i class="icon-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="home-list item">
                            <h2>
                                <a href="" class="home-title" title="Container Homes">Container Homes</a>
                            </h2>
                            <div class="home-box">
                                <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                                <div class="thumbnail-container">
                                    <div class="thumbnail">
                                        <img data-src="assets/images/container_homes.png" class="owl-lazy" alt="Container Homes" title="Container Homes">
                                    </div>
                                </div>
                                <div class="home-list-btn">
                                    <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                    <span>	View All Plans</span> <i class="icon-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="home-list item">
                            <h2>
                                <a href="" class="home-title" title="Others">Others</a>
                            </h2>
                            <div class="home-box">
                                <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                                <div class="thumbnail-container">
                                    <div class="thumbnail">
                                        <img data-src="assets/images/Others.png" class="owl-lazy" alt="Others" title="Others">
                                    </div>
                                </div>
                                <div class="home-list-btn">
                                    <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                    <span>	View All Plans</span> <i class="icon-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </section>
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/buildhome_list.js') }}"></script>
<script src="{{ url('assets/libraries/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
@endsection

@endsection
@endif
