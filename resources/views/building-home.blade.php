@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif

<section class="page_section buildhome_list" data-aos="fade-up">
      <div class="container">
         @if(!empty($buildingcategory) && count($buildingcategory)>0)
         <div class="row">
            @foreach($buildingcategory as  $index => $category)
            @php //echo '<pre>'; print_r($buildingcategory);die; @endphp
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2>
                  
                     <a href="{{url('/building-category')}}/{{$category->alias->varAlias}}" class="home-title" title="{{ htmlspecialchars_decode($category->varTitle) }}">{{ htmlspecialchars_decode($category->varTitle) }}</a>
                  </h2>
                  <div class="home-box">
                     <p>{{ htmlspecialchars_decode($category->txtShortDescription) }}</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <!-- <img src="assets/images/single_story.png" alt="Single Story" title="Single Story"> -->
                           <picture>
                              <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}">
                              <img src="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($category->varTitle) }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">
                           </picture>
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            @endforeach
         </div>
         @if($buildingcategory->total() > $buildingcategory->perPage())
              <div class="row mt-xs-30">
                  <div class="col-12 text-center">
                      {{ $buildingcategory->links() }}
                  </div>
              </div>
           @endif
        @else
         <div class="row">
             <div class="col-12">
               <h2>No categories are available...</h2>
             </div>
         </div>
        @endif
      </div>
</section>

@if(!Request::ajax())
@section('footer_scripts')

@endsection
@endsection
@endif
