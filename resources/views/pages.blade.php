@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
<!-- terms_conditions_01 S -->
<section class="page_section terms_conditions_01" data-aos="fade-up">
    <div class="container">
        <div class="row">
            <div class="col-12">
            	<div class="cms">
                	{!! htmlspecialchars_decode($CONTENT) !!}
              	</div>
            </div>
        </div>
    </div>
</section>
<!-- terms_conditions_01 E -->
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/terms-conditions.js') }}"></script>

@endsection

@endsection
@endif