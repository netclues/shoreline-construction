@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif



<!DOCTYPE html>
<html>
  <head>
     <title>Laravel</title>
      <!-- <link href = "https://fonts.googleapis.com/css?family=Lato:100" rel = "stylesheet" 
         type = "text/css"> -->
      
      <!-- <style>
         html, body {
            height: 100%;
         }
         body {
            margin: 10;
            padding: 10;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
         }
         .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
         }
         .content {
            text-align: center;
            display: inline-block;
         }
         .title {
            font-size: 96px;
           
         }
      </style> -->
   </head>
   
   <body>
      <!-- <div class = "container">
         
         <div class = "content">
            <div class = "title">All Duplex plans  </div>    
         <div>
			
      </div> -->
      <div class="page_section buildhome_list buildhome_detail">
         <div class="container">
            <div class="row">
              @if(isset($buildinghome) && !empty($buildinghome))
               <div class="col-xl-9">  
                    <div class="sub-title cms mr-sm-auto">
                        <h2 class="nqtitle">{{ $buildinghome->varTitle }}</h2>
                    </div>                  
                    <div class="detail-top">
                        <div class="detail-slider owl-carousel owl-theme owl-dots-absolute owl-nav-absolute owl-loaded">
                            @if(isset($buildinghome->fkIntImgId) && !empty($buildinghome->fkIntImgId) || count($videoObj) > 0)
                            @if(isset($buildinghome->fkIntImgId) && !empty($buildinghome->fkIntImgId))
                                @php $imgArr =  explode(',',$buildinghome->fkIntImgId) @endphp
                                @if(!empty($imgArr))
                                    @foreach($imgArr as $key => $val)
                                        <div class="item">
                                            <div class="thumbnail-container">
                                                <div class="thumbnail">
                                                    <picture>
                                                        <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($val,848,565) !!}">
                                                        <img src="{!! App\Helpers\resize_image::resize($val,848,565) !!}" alt="{{ htmlspecialchars_decode($buildinghome->varTitle) }}" title="{{ htmlspecialchars_decode($buildinghome->varTitle) }}"/>
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumbnail-container">
                                                <div class="thumbnail">
                                                    <picture>
                                                        <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($val,848,565) !!}">
                                                        <img src="{!! App\Helpers\resize_image::resize($val,848,565) !!}" alt="{{ htmlspecialchars_decode($buildinghome->varTitle) }}" title="{{ htmlspecialchars_decode($buildinghome->varTitle) }}"/>
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumbnail-container">
                                                <div class="thumbnail">
                                                    <picture>
                                                        <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($val,848,565) !!}">
                                                        <img src="{!! App\Helpers\resize_image::resize($val,848,565) !!}" alt="{{ htmlspecialchars_decode($buildinghome->varTitle) }}" title="{{ htmlspecialchars_decode($buildinghome->varTitle) }}"/>
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            @endif
                            @if(count($videoObj) > 0)
                                @foreach($videoObj as $v_key => $v_val)
                                    <div class="item">
                                        <div class="thumbnail-container">
                                            <div class="thumbnail">
                                                @if(!empty($v_val->youtubeId))
                                                    <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{ trim($v_val->youtubeId) }}?autoplay=0&cc_load_policy=1&disablekb=1&enablejsapi=1&loop=1&modestbranding=1&playsinline=1" frameborder="0" allowfullscreen></iframe>
                                                @endif
                                                @if(!empty($v_val->vimeoId))
                                                    <iframe width="100%" height="100%" src="https://player.vimeo.com/video/{{ $v_val->vimeoId }}?title=0&byline=0&portrait=0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                                                @endif
                                                @if(!empty($v_val->varVideoExtension))
                                                    <video width="100%" height="100%" controls>
                                                        <source src="{{ $APP_URL }}/assets/videos/{{ $v_val->varVideoName.'.'.$v_val->varVideoExtension }}" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @else
                            <div class="item">
                                <div class="thumbnail-container">
                                    <div class="thumbnail">
                                        <picture>
                                            <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($buildinghome->fkIntImgId) !!}">
                                            <img src="{!! App\Helpers\resize_image::resize($buildinghome->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($buildinghome->varTitle) }}" title="{{ htmlspecialchars_decode($buildinghome->varTitle) }}" />
                                        </picture>
                                    </div>
                                </div>
                            </div>
                        @endif

                        </div>
                        @if (isset($buildinghome->txtDescription) && !empty($buildinghome->txtDescription))
                        <div class="detail-desc cms">
                          <p>{!! htmlspecialchars_decode($buildinghome->txtDescription) !!}</p>
                        </div>
                        @elseif(isset($buildinghome->txtShortDescription) && !empty($buildinghome->txtShortDescription))
                        <div class="detail-desc cms">
                          <p>{!! htmlspecialchars_decode($buildinghome->txtShortDescription) !!}</p>
                        </div>
                        @else
                        <div class="detail-desc cms">
                          <p><h2>No Content Available</h2></p>
                        </div>
                        @endif
                        
                    </div>
                      @else
                      <div class="col-9">
                        <h2>No records found</h2>
                      </div>
                      @endif

                    <?php /* Commented by admin 11-03-2020
                    <div class="bottm-title cms">
                        <h2 class="nqtitle">Other category</h2>
                    </div>                     
                     
                    <div class="row">
                        <div class="col-12">
                          @if(isset($buildingothercategory) && count($buildingothercategory) > 0)
                             <div class="d-lg-flex building-home-bottom owl-carousel owl-theme owl-dots-absolute owl-nav-absolute">
                              @foreach($buildingothercategory as  $index => $buildingOtherCat)
                                 <div class="home-list item">
                                     <h2>
                                        <a href="{{ url('/building-category/'.$buildingOtherCat->alias->varAlias)}}" class="home-title" title="{{ htmlspecialchars_decode($buildingOtherCat->varTitle) }}">{{ htmlspecialchars_decode($buildingOtherCat->varTitle) }}</a>
                                     </h2>
                                     <div class="home-box">
                                <p>{{ htmlspecialchars_decode($buildingOtherCat->txtShortDescription) }}</p>
                                <div class="thumbnail-container">
                                    <div class="thumbnail">
                                        <!-- <img data-src="assets/images/single_story.png" class="owl-lazy" alt="Single Story" title="Single Story"> -->
                                        <picture>
                                            <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($buildingOtherCat->fkIntImgId) !!}">
                                            <img src="{!! App\Helpers\resize_image::resize($buildingOtherCat->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($buildingOtherCat->varTitle) }}" title="{{ htmlspecialchars_decode($buildingOtherCat->varTitle) }}" class="lazy">
                                        </picture>
                                    </div>
                                </div>
                                <div class="home-list-btn">
                                    <a href="{{ url('/building-category/'.$buildingOtherCat->alias->varAlias)}}" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                    <span>  View All Plans</span> <i class="icon-arrow"></i></a>
                                </div>
                            </div>
                                 </div>
                                  @endforeach
                             </div>
                             @endif
                        </div>
                    </div>
                    */ ?>
                   
               </div>
               <div class="col-xl-3">
                  <div class="service-right right-panel">
                       
                       @if(isset($buildinghomeCategory) && count($buildinghomeCategory) > 0)
                           <article class="detail-category cms">
                               <div class="nqtitle form-title mb-xs-15">Category</div>
                               <ul class="category">                      
                                @php //echo '<pre>'; print_r($buildinghomeCategory);die; @endphp 
                                 @foreach($buildinghomeCategory as $key => $value)
                                    <li>
                                       <a href="{{ url('building-home-category') }}/{{ $value->alias->varAlias }}" title="{{ $value->varTitle }}">{{ $value->varTitle }}</a>
                                       {!! Form::hidden('area_of_interest',$value->varTitle) !!}
                                    </li>
                                 @endforeach
                               </ul>
                           </article>
                       @endif

                       <article class="">
                           <div class="nqtitle form-title mb-xs-15">Have Any Question?</div>                           
                           {!! Form::open(['method' => 'post','class'=>'request-form nqform nq-form-md', 'id'=>'request_page_form']) !!}
                           {!! Form::hidden('building_id',$buildinghome->id) !!}
                               <div class="row align-items-start">
                                   <div class="col-12 text-right">
                                       <div class="required">* Denotes Required Inputs</div>
                                   </div>
                                   <div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">					
                                        <label class="nq-label" for="first_name">First Name<span class="star">*</span></label>					
                                        {!! Form::text('first_name', old('first_name'), array('id'=>'first_name', 'class'=>'form-control nq-input', 'name'=>'first_name', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
                                        @if ($errors->has('first_name'))
                                            <span class="error">{{ $errors->first('first_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">					
                                        <label class="nq-label" for="last_name">Last Name<span class="star">*</span></label>					
                                        {!! Form::text('last_name', old('last_name'), array('id'=>'last_name', 'class'=>'form-control nq-input', 'name'=>'last_name', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
                                        @if ($errors->has('last_name'))
                                            <span class="error">{{ $errors->first('last_name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label class="nq-label" for="contact_email">Email<span class="star">*</span></label>
                                        {!! Form::email('contact_email', old('contact_email'), array('id'=>'contact_email', 'class'=>'form-control nq-input', 'name'=>'contact_email', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
                                        @if ($errors->has('contact_email'))
                                            <span class="error">{{ $errors->first('contact_email') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-4 col-md-4 col-sm-12">
                                    <div class="form-group">
                                    <label class="nq-label" for="phone_number">Phone<span class="star">*</span></label>
                                        {!! Form::text('phone_number', old('phone_number'), array('id'=>'phone_number', 'class'=>'form-control nq-input', 'name'=>'phone_number', 'maxlength'=>"20", 'onpaste'=>'return false;', 'ondrop'=>'return false;', 'onkeypress'=>'javascript: return KeycheckOnlyPhonenumber(event);')) !!}
                                        @if ($errors->has('phone_number'))
                                            <span class="error">{{ $errors->first('phone_number') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12  col-sm-12">
                                    <div class="form-group">
                                        <label class="nq-label" for="user_message">Comments</label>
                                        {!! Form::textarea('user_message', old('user_message'), array('class'=>'form-control nq-textarea', 'name'=>'user_message', 'rows'=>'3', 'id'=>'user_message', 'spellcheck'=>'true', 'onpaste'=>'return false;', 'ondrop'=>'return false;' )) !!}
                                        @if ($errors->has('user_message'))
                                            <span class="error">{{ $errors->first('user_message') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group captcha">
                                        <div id="contact_html_element" class="g-recaptcha"></div>
                                        <div class="capphitcha" data-sitekey="{{Config::get('Constant.GOOGLE_CAPCHA_KEY')}}">
                                        @if ($errors->has('g-recaptcha-response'))
                                            <label class="error help-block">{{ $errors->first('g-recaptcha-response') }}</label>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-6 col-md-6 col-sm-6 service-btn">
                                    <div class="form-group">
                                        <button type="submit" class="btn-primary" title="Send Message">Send Message</button>
                                    </div>
                                </div>
                               </div>
                            {!! Form::close() !!}                           
                       </article>
                   </div>
               </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="detail-gallery cms">   
                          <h2 class="nqtitle">Related Building Home Design</h2>       
                           @if(isset($similarBuildingHome) && $similarBuildingHome->count() > 0)         
                          <div class="detail-gallery-slider owl-carousel owl-theme owl-dots-absolute owl-nav-absolute owl-loaded">
                             
                            @foreach($similarBuildingHome as $key => $value)                                                    
                            <div class="item">
                                <article class="image_hover">
                                    <a href="{{ route('buildinghome-Detail',$value->alias->varAlias) }}" title="{{ $value->varTitle }}">
                                    <!-- <div class="row"> -->
                                    @php $imgArr =  explode(',',$value->fkIntImgId) @endphp
                                    @if(!empty($imgArr))
                                    <!-- <div class="col-4"> -->
                                        <div class="thumbnail-container">
                                            <div class="thumbnail">
                                                <picture>
                                                    <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($imgArr[0],340,551) !!}">
                                                    <img src="{!! App\Helpers\resize_image::resize($imgArr[0],340,551) !!}" alt="{{ $value->varTitle }}" title="{{ $value->varTitle }}" />
                                                </picture>
                                                <span class="mask"></span>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="content">
                                            <h3 class="r_title">{{ $value->varTitle }}</h3>
                                        </div>                                                
                                    </a>
                                </article>
                            </div>
                        @endforeach

                          </div>
                          @endif
                      </div>
                </div>
            </div>
         </div>
      </div>
   </body>

</html>
<script type="text/javascript">
    var sitekey = '{{Config::get("Constant.GOOGLE_CAPCHA_KEY")}}';
    var onContactloadCallback = function() {
        grecaptcha.render('contact_html_element', {
            'sitekey' : sitekey
        });
    };                        
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onContactloadCallback&render=explicit" async defer></script>

@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/libraries/fancybox/js/jquery.fancybox.min') }}"></script>
<script src="{{ url('assets/js/buildhome_list.js') }}"></script>
<script src="{{ url('assets/js/buildhome_detail.js') }}"></script>
<script src="{{ url('assets/js/request-a-quote.js') }}"></script>
@endsection
@endsection
@endif
