@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
<section class="page_section ourwork_detail" data-aos="fade-up">
    <div class="container">
        <div class="row">
        	@if(isset($service) && !empty($service))				
            <div class="col-xl-12 col-lg-12">                
                <div class="detail-img">
                    @if(isset($service->fkIntImgId) && !empty($service->fkIntImgId))
                        @if(isset($service->fkIntImgId) && !empty($service->fkIntImgId))
                            @php $imgArr =  explode(',',$service->fkIntImgId) @endphp
                            @if(!empty($imgArr))
                                @foreach($imgArr as $key => $val)
                                    <div class="thumbnail-container">
                                        <div class="thumbnail">
                                            <picture>
                                                <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($val,700,466) !!}">
                                                <img src="{!! App\Helpers\resize_image::resize($val,700,466) !!}" alt="{{ htmlspecialchars_decode($service->varTitle) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}"/>
                                            </picture>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @endif                            
                    @else
                        <div class="item">
                            <div class="thumbnail-container">
                                <div class="thumbnail">
                                    <picture>
                                        <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($service->fkIntImgId) !!}">
                                        <img src="{!! App\Helpers\resize_image::resize($service->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($service->varTitle) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}" />
                                    </picture>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                 <?php /* @if (isset($service->txtDescription) && !empty($service->txtDescription)) */ ?>                 
                    <div class="cms detail_bg">
                        @if (isset($service->varTitle) && !empty($service->varTitle))
                        <h2 class="nqtitle">{{ htmlspecialchars_decode($service->varTitle)}}</h2>
                        @endif

                        @if (isset($service->txtOwner) && !empty($service->txtOwner))
                    	<span class="place_title">{{ htmlspecialchars_decode($service->txtOwner)}}</span>
                        @endif

                        @if (isset($service->txtDescription) && !empty($service->txtDescription))
                            {!! htmlspecialchars_decode($service->txtDescription) !!}  
                        @else
                            <p>{!! htmlspecialchars_decode($service->txtShortDescription) !!}</p>
                        @endif                     
                    </div>
                <?php /* @endif */ ?>
            </div>            
            @else
            <div class="col-12">
            	<h2>No records found</h2>
            </div>
            @endif
        </div>
    </div>
</section>
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/libraries/owl.carousel/js/owl.carousel.min.js') }}"></script>

<script src="{{ url('assets/libraries/libraries-update/owl.carousel/js/owl.carousel-update.js') }}"></script>

<script src="{{ url('https://static.addtoany.com/menu/page.js') }}"></script>

@endsection

@endsection
@endif