<td style="vertical-align: middle">
    <input type="text" style="width: 100%" required class="form-control txtFieldTitle"/>
</td>
<td style="vertical-align: middle">
    <input type="text" style="width: 100%" required class="form-control txtFieldName"/>
</td>
<td style="vertical-align: middle">
    <select class="form-control txtdbType" style="width: 100%">
        <option value="increments">Increments</option>
        <option value="integer">Integer</option>
        <option value="unsignedInteger">Unsigned Integer</option>
        <option value="string">String</option>
        <option value="text">Text</option>
        <option value="char">Char</option>
        <option value="date">Date</option>
        <option value="dateTime">Date Time</option>
        <option value="timestamp">Timestamp</option>
    </select>
</td>
<td style="vertical-align: middle">
    <select class="form-control txtValidation" style="width: 100%">
        <option value="">None</option>
        <option value="required">Required</option>
        <option value="min:5">Min:5</option>
        <option value="email">Email</option>
        <option value="numeric">Numeric</option>
    </select>
</td>
<td style="vertical-align: middle">
    <select class="form-control drdHtmlType" style="width: 100%">
        <option value="text">Text</option>
        <option value="email">Email</option>
        <option value="number">Number</option>
        <option value="date">Date</option>
        <option value="password">Password</option>
        <option value="select">Select</option>
        <option value="radio">Radio</option>
        <option value="checkbox">Checkbox</option>
        <option value="textarea">TextArea</option>
        <option value="timestamp">Timestamp</option>
    </select>
</td>
<td style="vertical-align: middle;display: none;">
    <div class="checkbox" style="text-align: center">
        <label style="padding-left: 0px">
            <input type="checkbox" style="margin-left: 0px!important;" class="flat-red chkPrimary"/>
        </label>
    </div>
</td>
<td style="vertical-align: middle">
    <div class="checkbox" style="text-align: center">
        <label style="padding-left: 0px">
            <input type="checkbox" style="margin-left: 0px!important;" class="flat-red chkSearchable" checked/>
        </label>
    </div>
</td>
<td style="vertical-align: middle">
    <div class="checkbox" style="text-align: center">
        <label style="padding-left: 0px">
            <input type="checkbox" style="margin-left: 0px!important;" class="flat-red chkInForm" checked/>
        </label>
    </div>
</td>
<td style="text-align: center;vertical-align: middle">
    <i onclick="removeItem(this)" class="remove fa fa-trash-o"
    style="cursor: pointer;font-size: 20px;color: red"></i>
</td>