@extends('powerpanel.layouts.app')
@section('title') Module Builder - PowerPanel @endsection
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css">
@stop
@section('content')
@include('powerpanel.partials.breadcrumbs')
<div class="row">
  <div class="col-sm-12">
    @if(Session::has('message'))
    <div class="alert alert-success">
      <button class="close" data-close="alert"></button>
      {{ Session::get('message') }}
    </div>
    @endif
    <div class="alert module_url" style="background-color: #DEF0D8; display: none;"> </div>
    <div class="portlet light bordered">
      <div class="portlet-body">
        <div class="tabbable tabbable-tabdrop">
          <div class="tab-content settings">
            <div class="tab-pane active form_pattern" id="general">
              <div class="row">
                <div class="col-md-12">
                  <form id="form">
                    <input type="hidden" name="_token" id="token" value="{!! csrf_token() !!}"/>
                    <div class="form-group col-md-4">
                      <label for="txtModelName">Module Title<span class="required">*</span></label>
                      <input type="text" class="form-control" required id="txtModuleTitle" placeholder="Enter Module Title">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="txtModelName">Model Name<span class="required">*</span></label>
                      <input type="text" class="form-control" required id="txtModelName" placeholder="Enter Model name">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="txtCustomTblName">Custom Table Name</label>
                      <input type="text" class="form-control" id="txtCustomTblName" required placeholder="Enter table name">
                    </div>
                    <div class="form-group col-md-8">
                      <label for="txtModelName">Options</label>
                      <div class="form-inline form-group" style="border-color: transparent">
                        <div class="checkbox chk-align">
                          <label>
                            <input type="checkbox" class="flat-red" id="chkDisplayOrder">
                            <span class="chk-label-margin">Display Order</span>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-md-12" style="margin-top: 7px">
                      <div class="form-control" style="border-color: transparent;padding-left: 0px">
                        <label style="font-size: 18px">Fields</label>
                      </div>
                    </div>
                    <div class="table-responsive col-md-12">
                      <table class="table table-striped table-bordered" id="table" style="width: 100%;">
                        <thead class="no-border">
                          <tr>
                            <th width="30%">Title to display</th>
                            <th width="30%">Field Name</th>
                            <th width="15%">DB Type</th>
                            <th width="15%">Validations</th>
                            <th width="15%">Html Type</th>
                            <th style="display: none;" width="5%">Primary</th>
                            <th width="5%">Searchable</th>
                            <th width="5%">In Form</th>
                            <th width="5%"></th>
                          </tr>
                        </thead>
                        <tbody id="container" class="no-border-x no-border-y ui-sortable">
                        </tbody>
                      </table>
                    </div>
                    <div class="form-inline col-md-12" style="padding-top: 10px">
                      <div class="form-group chk-align" style="border-color: transparent;">
                        <button type="button" class="btn btn-success btn-flat btn-green" id="btnAdd"> Add Field
                        </button>
                      </div>
                      <div class="form-group chk-align" style="border-color: transparent; display: none">
                        <button type="button" class="btn btn-success btn-flat btn-green" id="btnPrimary"> Add Primary
                        </button>
                      </div>
                      <div class="form-group chk-align" style="border-color: transparent; display: none">
                        <button type="button" class="btn btn-success btn-flat btn-green" id="btnTimeStamps"> Add
                        Timestamps
                        </button>
                      </div>
                    </div>
                    <div class="form-inline col-md-12" style="padding:15px 15px;text-align: right">
                      <div class="form-group" style="border-color: transparent;padding-left: 10px">
                        <button type="submit" class="btn btn-flat btn-primary btn-blue" id="btnGenerate">Generate
                        </button>
                      </div>
                      <!-- <div class="form-group" style="border-color: transparent;padding-left: 10px">
                        <button type="button" class="btn btn-default btn-flat" id="btnReset" data-toggle="modal"
                        data-target="#confirm-delete"> Reset
                        </button>
                      </div> -->
                    </div>
                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
                      aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Confirm Reset</h4>
                          </div>
                          <div class="modal-body">
                            <p style="font-size: 16px">This will reset all of your fields. Do you want to
                            proceed?</p>
                            <p class="debug-url"></p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">No
                            </button>
                            <a id="btnModelReset" class="btn btn-flat btn-danger btn-ok" data-dismiss="modal">Yes</a>
                          </div>
                        </div>
                      </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script src="{{ url('resources/pages/scripts/module_builder.js') }}"></script>
@endsection