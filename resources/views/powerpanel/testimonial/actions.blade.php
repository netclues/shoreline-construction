@section('css')
<link href="{{ url('resources/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ url('resources/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection
@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection
@section('content')
@php $settings = json_decode(Config::get("Constant.MODULE.SETTINGS")); @endphp
@include('powerpanel.partials.breadcrumbs')
<div class="col-md-12 settings">
	@if(Session::has('message'))
	<div class="row">
		<div class="alert alert-success">
			<button class="close" data-close="alert"></button>
			{{ Session::get('message') }}
		</div>
	</div>
	@endif
	<div class="row">
		<div class="portlet light bordered">
			<div class="portlet-body">
				<div class="tabbable tabbable-tabdrop">
					<div class="tab-content row form_pattern">
						<div class="col-md-12">
							<div class="portlet-body">
								{!! Form::open(['method' => 'post','enctype' => 'multipart/form-data','id'=>'frmTestimonial']) !!}
								<div class="form-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group {{ $errors->has('testimonialby') ? 'has-error' : '' }} form-md-line-input">
												{!! Form::text('testimonialby',isset($testimonials->varTitle) ? $testimonials->varTitle:old('testimonialby') ,array('class' => 'form-control input-sm seoField maxlength-handler','maxlength'=>'150','id' => 'testimonialby','autocomplete'=>'off')) !!}
												<label class="form_title" for="testimonialby">{{ trans('template.testimonialModule.testimonialBy') }} <span aria-required="true" class="required"> * </span></label>
												<span style="color: red;">
													{{ $errors->first('testimonialby') }}
												</span>
											</div>
										</div>
									</div>
									
									<!-- created by admin 02-03-2020 -->
									@if ((isset($testimonials->varBannerVersion) && $testimonials->varBannerVersion == 'img_banner') ||
									old('bannerversion')=='img_banner' || (!isset($testimonials->varBannerVersion) && old('bannerversion') ==
									null))
										@php $checked_yes = 'checked' @endphp
									@else
										@php $checked_yes = '' @endphp	
									@endif
									@if ((isset($testimonials->varBannerVersion) && $testimonials->varBannerVersion == 'vid_banner') ||
									old('bannerversion')=='vid_banner')
										@php $ichecked_vid_yes = 'checked' @endphp	
									@else
										@php $ichecked_vid_yes = '' @endphp
									@endif
									
									<div class="form-group bannerversion {{ $errors->has('bannerversion') ? ' has-error' : '' }}">
									<label class="form_title" for="bannerversion">{!! trans('template.testimonialModule.version') !!} <span
										aria-required="true" class="required"> </span></label>
									<div class="md-radio-inline">
										<div class="md-radio">
										<input type="radio" {{ $checked_yes }} value="img_banner" id="img_banner" name="bannerversion"
											class="md-radiobtn versionradio">
										<label for="img_banner">
											<span class="inc"></span>
											<span class="check"></span>
											<span class="box"></span> {!! trans('template.testimonialModule.imageBanner') !!}
										</label>
										</div>
										<div class="md-radio">
										<input type="radio" {{ $ichecked_vid_yes }} value="vid_banner" id="vid_banner"
											name="bannerversion" class="md-radiobtn versionradio">
										<label for="vid_banner">
											<span class="inc"></span>
											<span class="check"></span>
											<span class="box"></span> {!! trans('template.testimonialModule.videoBanner') !!}
										</label>
										</div>
									</div>
									<span class="help-block">
										<strong>{{ $errors->first('bannerversion') }}</strong>
									</span>
									</div>

									@include('powerpanel.partials.imageControl',['type' => 'single','label' =>
									trans('template.testimonialModule.selectBanner').' <span aria-required="true" class="required"> * </span>' ,'data'=> isset($testimonials)?$testimonials:null , 'id' =>
									'banner_image', 'name' => 'img_id_image_upload', 'settings' => $settings,'width' => '392', 'height'
									=> '261'])

									@include('powerpanel.partials.videoControl',['type' => 'single' ,'label' =>
									trans('template.testimonialModule.selectVideo').' <span aria-required="true" class="required"> * </span>','data'=> isset($testimonials)?$testimonials:null, 'id' =>
									'banner_video','videoData'=> isset($videoDataForSingle)?$videoDataForSingle:null, 'name' => 'video_id'])
									<!-- created by admin 02-03-2020 -->

									<!-- #Commented by author 02-03-2020# -->	
									<?php /*								
									<div class="row">
										<div class="col-md-12">
											<div class="form-group form-md-line-input">
												<div class="input-group date-picker" data-date-format="{{Config::get('Constant.DEFAULT_DATE_FORMAT')}}">
													<span class="input-group-btn date_default">
														<span class="btn date-set">
														<i class="fa fa-calendar"></i>
														</span>
													</span>
													<input type="text" class="form-control datepicker" id="testimonialdate" name="testimonialdate" value="{{isset($testimonials->dtStartDateTime)?date(Config::get('Constant.DEFAULT_DATE_FORMAT'),strtotime(str_replace('/','-',$testimonials->dtStartDateTime))):date(Config::get('Constant.DEFAULT_DATE_FORMAT'))}}" readonly>
													
													<label class="form_title" for="testimonial">{{ trans('template.testimonialModule.testimonialDate') }}</label>
												</div>
											</div>
										</div>
									</div>
									*/ ?>

									<!-- commented by admin 02-03-2020 -->
									<!-- @include('powerpanel.partials.imageControl',['type' => 'single','label' => trans('template.photoAlbum.selectPhoto') ,'data'=> isset($testimonials)?$testimonials:null , 'id' => 'testimonial_image', 'name' => 'img_id', 'settings' => $settings, 'width' => '500', 'height' => '500']) -->

									<div class="row">
										<div class="col-md-12">
											<div class="form-group @if($errors->first('testimonial')) has-error @endif">
												<label class="form_title" for="testimonial">{{ trans('template.common.description') }} <span aria-required="true" class="required"> * </span></label>
												<span class="help-block">
													<strong>{{ $errors->first('testimonial') }}</strong>
												</span>
												{!! Form::textarea('testimonial',isset($testimonials->txtDescription)?$testimonials->txtDescription:old('testimonial'),array('class' => 'form-control','id'=>'txtDescription','palceholder'=>trans('template.testimonialModule.testimonial'),'style'=>'max-height:80px;')) !!}
											</div>
										</div>
									</div>	
									<div class="row">
										<div class="col-md-12">
											<div class="form-group {{ $errors->has('author_name') ? 'has-error' : '' }} form-md-line-input">
												{!! Form::text('author_name',isset($testimonials->varAuthor) ? $testimonials->varAuthor:old('author_name') ,array('class' => 'form-control input-sm seoField maxlength-handler','maxlength'=>'150','id' => 'author_name','autocomplete'=>'off')) !!}
												<label class="form_title" for="author_name">{{ trans('template.testimonialModule.author') }} <span aria-required="true" class="required"> * </span></label>
												<span style="color: red;">
													{{ $errors->first('author_name') }}
												</span>
											</div>
										</div>
									</div>								
									<h3>{{ trans('template.common.displayinformation') }}</h3>
									<!-- created by author -->									
									<div class="row">	
										<div class="col-md-6">
										@php
												$display_order_attributes = array('class' => 'form-control','maxlength'=>10,'placeholder'=>trans('template.common.displayorder'),'autocomplete'=>'off');
											
											@endphp
											<div class="form-group @if($errors->first('display_order')) has-error @endif form-md-line-input">
												{!! Form::text('display_order', isset($testimonials->intDisplayOrder)?$testimonials->intDisplayOrder:$total, $display_order_attributes) !!}
												<label class="form_title" class="site_name">{{ trans('template.common.displayorder') }} <span aria-required="true" class="required"> * </span></label> 
												<span class="help-block">
												<strong>{{ $errors->first('display_order') }}</strong>
												</span>
											</div>
										</div>
										<div class="col-md-6">
											@include('powerpanel.partials.displayInfo',['display' => isset($testimonials->chrPublish)?$testimonials->chrPublish:null])
										</div>
									</div>
									<!-- ended 17032020-->																
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-12">
											<button type="submit" name="saveandedit" class="btn btn-green-drake" value="saveandedit">{!! trans('template.common.saveandedit') !!}</button>
											<button type="submit" name="saveandexit" class="btn btn-green-drake" value="saveandexit">{!! trans('template.common.saveandexit') !!}</button>
											<a class="btn btn-outline red" href="{{ url('powerpanel/testimonial') }}">{{ trans('template.common.cancel') }}</a>
										</div>
									</div>
								</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
@include('powerpanel.partials.ckeditor')
<script src="{{ url('resources/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/pages/scripts/testimonial_validations.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	window.site_url =  '{!! url("/") !!}';
	var moduleAlias = 'testimonials';
	$(document).ready(function(){
		$('.datepicker').datepicker({
			autoclose: true,
			endDate:new Date(),
			format:DEFAULT_DT_FMT_FOR_DATEPICKER
		});
	});
	$('.date-set,.datepicker').on('click',function(){
		$('.datepicker').datepicker('show');
	});
	$('.maxlength-handler').maxlength({
				limitReachedClass: "label label-danger",
				alwaysShow: true,
				threshold: 5,
				twoCharLinebreak: false
	});
</script>
<script src="{{ url('resources/pages/scripts/custom.js') }}" type="text/javascript"></script>
@endsection