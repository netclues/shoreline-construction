<!DOCTYPE html>
<html>    
    <head>        
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="robots" content="noindex,nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0" />
    <meta name="theme-color" content="" />
    <!-- Meta Description S -->
        <title>Oops! 404 The requested page not found</title>
        <meta name="title" content="Oops! 404 The requested page not found">
        <meta name="description" content="Oops! 404 The requested page not found">
    <!-- Meta Description E -->

    <!-- Monitoring S -->
        @if(!empty($site_monitor))
	        <meta name="Monitoring" content="{{ $site_monitor->varTitle }}">
	    @endif
    <!-- Monitoring E -->
     <!-- CSRF-Token Canonical Link S -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
	    <link rel="canonical" href="{{ url()->current() }}" />	    
    <!-- CSRF-Token Canonical Link E -->

    <!-- Style Sheet File S -->
	    <link rel="stylesheet" href="{!! url('assets/css/main.css') !!}" />
    <!-- Style Sheet File E -->

    <!-- Google Font Link S -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Muli:400,500,600|Work+Sans:400,500,600,700&display=swap" rel="stylesheet"> -->

        <link href="https://fonts.googleapis.com/css?family=Muli:400,500,600&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600&display=swap" rel="stylesheet">

    <!-- Google Font Link E -->

    <!-- Favicon Icon S -->
	    <link rel="shortcut icon" href="{!! url('assets/images/favicon.ico') !!}" type="image/x-icon" />
	    <link rel="apple-touch-icon" sizes="144x144" href="{!! url('assets/images/apple-touch-icon-144.png') !!}" />
	    <link rel="apple-touch-icon" sizes="114x114" href="{!! url('assets/images/apple-touch-icon-114.png') !!}" />
	    <link rel="apple-touch-icon" sizes="72x72" href="{!! url('assets/images/apple-touch-icon-72.png') !!}" />
	    <link rel="apple-touch-icon" sizes="57x57" href="{!! url('assets/images/apple-touch-icon-57.png') !!}" />
    <!-- Favicon Icon E -->

    <!-- Javascript File S -->
        <!-- <script type="text/javascript" src="http://localhost/shoreline-construction/public_html/assets/libraries/jquery/jquery-3.3.1.min.js"></script> -->
    	<script type="text/javascript" src="{!! url('assets/js/jquery-3.4.1.min.js') !!}"></script>
    <!-- Javascript File E -->
    </head>
    <body>
        <div class="container">
            <div class="content">
            <section class="section notfound_01">
                <div class="nq_container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="notfound_box">
                            <div class="msg_404">
                                <span class="four">4</span>
                                <span class="four">0</span>
                                <span class="four">4</span>
                            </div>
                            <div class="oops_msg">
                                <!-- <span class="oops">Oops!</span>  -->The Page you requested was not found!         
                            </div>
                            <div class="backhome">
                                <a href="{{url('/')}}" title="Back To Home" class="btn-primary"><i class="fa fa-home"></i> Back To Home</a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </section>
            </div>
        </div>
    </body>
</html>
