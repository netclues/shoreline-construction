@section('css')
<link href="{{ url('resources/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<link href="{{ url('resources/global/plugins/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL PLUGINS -->
@endsection
@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection
@section('content')
@include('powerpanel.partials.breadcrumbs')
@php $settings = json_decode(Config::get("Constant.MODULE.SETTINGS")); @endphp
@if (count($errors) > 0)
<ul>
  @foreach ($errors->all() as $error)
  <li>{{ $error }}</li>
  @endforeach
</ul>
@endif
<div class="row">
  <div class="col-sm-12">
    @if(Session::has('message'))
    <div class="alert alert-success">
      <button class="close" data-close="alert"></button>
      {{ Session::get('message') }}
    </div>
    @endif
    <div class="portlet light bordered">
      <div class="portlet-body">
        <div class="tabbable tabbable-tabdrop">
          <div class="tab-content">
            <div class="row">
              <div class="col-md-12">
                <div class="tab-pane active" id="general">
                  <div class="portlet-body form_pattern">
                    {!! Form::open(['method' => 'post','id'=>'frmBuildingHome']) !!}
                    <div class="form-body">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group @if($errors->first('title')) has-error @endif form-md-line-input">
                            {!! Form::text('title', isset($buildinghome->varTitle)?$buildinghome->varTitle:old('title'), array('maxlength' => 150, 'class' => 'form-control hasAlias seoField maxlength-handler','autocomplete'=>'off','data-url' => 'powerpanel/buildinghome')) !!}
                            <label class="form_title" for="site_name">{{ trans('template.common.title') }} <span aria-required="true" class="required"> * </span></label>
                            <span class="help-block">
                              {{ $errors->first('title') }}
                            </span>
                          </div>
                        </div>
                      </div>
                      <!-- code for alias -->
                      {!! Form::hidden(null, null, array('class' => 'hasAlias','data-url' => 'powerpanel/buildinghome')) !!}
                      {!! Form::hidden('alias', isset($buildinghome->alias->varAlias)?$buildinghome->alias->varAlias:old('alias'), array('class' => 'aliasField')) !!}
                      {!! Form::hidden('oldAlias', isset($buildinghome->alias->varAlias)?$buildinghome->alias->varAlias:old('alias')) !!}
                      <div class="form-group alias-group {{!isset($buildinghome)?'hide':''}}">
                        <label class="form_title" for="{{ trans('template.url') }}">{{ trans('template.common.url') }} :</label>
                        <a href="javascript:void;" class="alias">{!! url("/") !!}</a>
                        <a href="javascript:void(0);" class="editAlias" title="Edit">
                          <i class="fa fa-edit"></i>
                          <a class="without_bg_icon openLink" title="Open Link" target="_blank" href="{{url('buildinghome/'.(isset($buildinghome->alias->varAlias) && isset($buildinghome)?$buildinghome->alias->varAlias:''))}}">
                            <i class="fa fa-external-link" aria-hidden="true"></i>
                          </a>
                        </a>
                      </div>
                      <span class="help-block">
                        {{ $errors->first('alias') }}
                      </span>
                      <!-- code for alias -->
                     @permission('building-category-list') 
                      <div class="row">
                        <div class="col-md-12">
                        <div class="form-group @if($errors->first('category_id')) has-error @endif form-md-line-input">
                            <label class="form_category_id" for="category_id">{{ trans('template.common.selectcategory') }}<span aria-required="true" class="required"> * </span></label>
                            <div id="categoryDropdown">
                              @php echo $categoryHeirarchyMain; @endphp
                            </div>
                            <span class="help-block">
                              {{ $errors->first('category_id') }}
                            </span>
                          </div>
                          </div>
                        </div>
                       </div>
                      @endpermission                      
                      
                      
                      @include('powerpanel.partials.imageControl',['type' => 'multiple','label' => trans('template.common.selectimage') ,'data'=> isset($buildinghome)?$buildinghome:null , 'id' => 'buildinghome_image', 'name' => 'img_id', 'settings' => $settings, 'width' => '252', 'height' => '224'])

                      @include('powerpanel.partials.videoControl',['type' => 'multiple' ,'label' => trans('template.buildinghomeModule.selectVideo') ,'data'=> isset($buildinghome)?$buildinghome:null,'videoData' => isset($videoData)?$videoData:null , 'id' => 'buildinghome_video', 'name' => 'video_id'])

                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group @if($errors->first('short_description')) has-error @endif form-md-line-input">
                              {!! Form::textarea('short_description', isset($buildinghome->txtShortDescription)?$buildinghome->txtShortDescription:old('short_description'), array('maxlength' => isset($settings->short_desc_length)?$settings->short_desc_length:400,'class' => 'form-control seoField maxlength-handler','id'=>'varShortDescription','rows'=>'3')) !!}
                              <label class="form_title">{{ trans('template.common.shortdescription') }}<span aria-required="true" class="required"> * </span></label>
                              <span class="help-block">{{ $errors->first('short_description') }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group @if($errors->first('description')) has-error @endif">
                              <label class="form_title">{{ trans('template.common.description') }}</label>
                              {!! Form::textarea('description', isset($buildinghome->txtDescription)?$buildinghome->txtDescription:old('description'), array('placeholder' => trans('template.common.description'),'class' => 'form-control','id'=>'txtDescription')) !!}
                              <span class="help-block">{{ $errors->first('description') }}</span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-8">
                            <div class="form-group">
                              @if ( (isset($buildinghome->chrFeaturedService) && $buildinghome->chrFeaturedService == 'N') || Request::old('featuredBuildingHome')=='N' || (!isset($buildinghome->chrFeaturedService) && Request::old('featuredBuildingHome')==null))
                              @php  $featured_checked_no = 'checked'  @endphp
                              @else
                              @php  $featured_checked_no = ''  @endphp
                              @endif
                              @if (isset($buildinghome->chrFeaturedService) && $buildinghome->chrFeaturedService == 'Y' || (Request::old('featuredService') == 'Y'))
                              @php  $featured_checked_yes = 'checked'  @endphp
                              @else
                              @php  $featured_checked_yes = ''  @endphp
                              @endif
                        <label class="control-label form_title">
                         {{ trans('template.buildinghomeModule.isFeaturedBuildingHome') }}?</label>
                              <div class="md-radio-inline">
                                <div class="md-radio">
                                  <input class="md-radiobtn" type="radio" value="Y" name="featuredService" id="featuredServiceY" {{ $featured_checked_yes }}>
                                  <label for="featuredServiceY"> <span></span> <span class="check"></span> <span class="box"></span> {{ trans('template.common.yes') }} </label>
                                </div>
                                <div class="md-radio">
                                  <input class="md-radiobtn" type="radio" value="N" name="featuredService" id="featuredServiceN" {{ $featured_checked_no }}/>
                                  <label for="featuredServiceN"> <span></span> <span class="check"></span> <span class="box"></span> {{ trans('template.common.no') }} </label>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              <span><strong>{{ trans('template.common.note') }}: 
                              {{ trans('template.buildinghomeModule.featuredBuildingHomeNote') }}*</strong></span>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="nopadding">
                              @include('powerpanel.partials.seoInfo',['form'=>'frmBuildingHome','inf'=>isset($metaInfo)?$metaInfo:false])
                            </div>
                          </div>
                        </div>
                        <h3 class="form-section">{{ trans('template.common.displayinformation') }}</h3>
                        <div class="row">
                          <div class="col-md-6">
                            @php
                            $display_order_attributes = array('class' => 'form-control','maxlength'=>10,'placeholder'=>trans('template.common.displayorder'),'autocomplete'=>'off');
                            @endphp
                            <div class="form-group @if($errors->first('display_order')) has-error @endif form-md-line-input">
                              {!! Form::text('display_order',  isset($buildinghome->intDisplayOrder)?$buildinghome->intDisplayOrder:$total, $display_order_attributes) !!}
                              <label class="form_title" for="site_name">{{ trans('template.common.displayorder') }}<span aria-required="true" class="required"> * </span></label>
                              <span class="help-block">
                                {{ $errors->first('display_order') }}
                              </span>
                            </div>
                          </div>
                          <div class="col-md-6">
                            @include('powerpanel.partials.displayInfo',['display' => isset($buildinghome->chrPublish)?$buildinghome->chrPublish:null ])
                          </div>
                        </div>
                      </div>
                      <div class="form-actions">
                        <div class="row">
                          <div class="col-md-12">
                            <button type="submit" name="saveandedit" class="btn btn-green-drake" value="saveandedit">{!! trans('template.common.saveandedit') !!}</button>
                            <button type="submit" name="saveandexit" class="btn btn-green-drake" value="saveandexit">{!! trans('template.common.saveandexit') !!}</button>
                            <a class="btn btn-outline red" href="{{ url('powerpanel/buildinghome') }}">{{ trans('template.common.cancel') }}</a>
                          </div>
                        </div>
                      </div>
                      {!! Form::close() !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('powerpanel.partials.addCat',['module' => 'building-category','categoryHeirarchy' => $categoryHeirarchy])
  @endsection
  @section('scripts')
  <script type="text/javascript">
  window.site_url =  '{!! url("/") !!}';
  var seoFormId = 'frmBuildingHome';
  var user_action = "{{ isset($buildinghome)?'edit':'add' }}";
  var moduleAlias = 'buildinghome';
  var categoryAllowed = false;
  @permission('buildinghome-category-list')
  categoryAllowed = true;
  @endpermission
  </script>
  <script src="{{ url('resources/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
  @include('powerpanel.partials.ckeditor')
  <script src="{{ url('resources/pages/scripts/custom.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/global/plugins/custom-alias/alias-generator.js') }}" type="text/javascript"></script>
  <!-- BEGIN CORE PLUGINS -->
  
  <script src="{{ url('resources/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="{{ url('resources/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/global/plugins/seo-generator/seo-info-generator.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/pages/scripts/buildinghome_validations.js') }}" type="text/javascript"></script>
  
  <script src="{{ url('resources/global/plugins/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.js')}}" type="text/javascript"></script>
  <script type="text/javascript">
  $(function() 
  {
   $('.icp-auto').iconpicker({
      hideOnSelect: true
   });
  });
  </script>

  <!-- END PAGE LEVEL SCRIPTS -->
  @endsection