<div class="tab-pane setting {{ (($tab_value=='social_media_share')?'active':'')  }}" id="social_media_share">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet-form">
					{!! Form::open(['method' => 'post','id' => 'frmSocialShare']) !!}
					{!! Form::hidden('tab', 'social_media_share', ['id' => 'social_media_share']) !!}
					<div class="form-body">
						<label><i class="fa fa-check"></i> {{ trans('template.setting.facebookShare') }}</label>
						<div class="form-group {{ $errors->has('fb_id') ? ' has-error' : '' }} form-md-line-input">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-facebook"></i>
								</span>
								
								{!! Form::text('fb_id' , Config::get('Constant.SOCIAL_SHARE_FB_ID'), array('class' => 'form-control', 'id' => 'fb_id', 'autocomplete'=>"off", 'onkeypress' => "return isNumberKey(event)")) !!}
								<label class="form_title" for="fb_id">{{ trans('template.setting.facebookPageID') }} <span aria-required="true" class="required"> * </span></label>
								<span class="help-block">
									{{ $errors->first('fb_id') }}
								</span>
							</div>
						</div>
						<div class="form-group {{ $errors->has('fb_api') ? ' has-error' : '' }} form-md-line-input">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-facebook"></i>
								</span>
								
								{!! Form::text('fb_api' , Config::get('Constant.SOCIAL_SHARE_FB_API_KEY'), array('class' => 'form-control', 'id' => 'fb_api', 'autocomplete'=>"off")) !!}
								<label class="form_title" for="fb_api">{{ trans('template.setting.facebookApiKey') }} <span aria-required="true" class="required"> * </span></label>
								<span class="help-block">
									{{ $errors->first('fb_api') }}
								</span>
							</div>
						</div>
						<div class="form-group {{ $errors->has('fb_secret_key') ? ' has-error' : '' }} form-md-line-input">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-facebook"></i>
								</span>
								{!! Form::text('fb_secret_key' , Config::get('Constant.SOCIAL_SHARE_FB_SECRET_KEY'), array('class' => 'form-control', 'id' => 'fb_secret_key', 'autocomplete'=>"off")) !!}
								<label class="form_title" for="fb_secret_key">{{ trans('template.setting.facebookSecretKey') }} <span aria-required="true" class="required"> * </span></label>
								<span class="help-block">
									{{ $errors->first('fb_secret_key') }}
								</span>
							</div>
						</div>
						<div class="form-group {{ $errors->has('fb_access_token') ? ' has-error' : '' }} form-md-line-input">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-facebook"></i>
								</span>
								
								{!! Form::text('fb_access_token' , Config::get('Constant.SOCIAL_SHARE_FB_ACCESS_TOKEN'), array('class' => 'form-control', 'id' => 'fb_access_token', 'autocomplete'=>"off")) !!}
								<label class="form_title" for="fb_access_token">{{ trans('template.setting.facebookAccessToken') }} <span aria-required="true" class="required"> * </span></label>
								<span class="help-block">
									{{ $errors->first('fb_access_token') }}
								</span>
							</div>
						</div>
						<div class="clearfix"></div>
						<label><i class="fa fa-check"></i> {{ trans('template.setting.twitterShare') }}</label>
						<div class="form-group {{ $errors->has('twitter_api') ? ' has-error' : '' }} form-md-line-input">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-twitter"></i>
								</span>
								
								{!! Form::text('twitter_api' , Config::get('Constant.SOCIAL_SHARE_TWITTER_API_KEY'), array('class' => 'form-control', 'id' => 'twitter_api', 'autocomplete'=>"off")) !!}
								<label class="form_title" for="twitter_api">{{ trans('template.setting.twitterApiKey') }} <span aria-required="true" class="required"> * </span></label>
								<span class="help-block">
									{{ $errors->first('twitter_api') }}
								</span>
							</div>
						</div>
						<div class="form-group {{ $errors->has('twitter_secret_key') ? ' has-error' : '' }} form-md-line-input">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-twitter"></i>
								</span>
								
								{!! Form::text('twitter_secret_key' , Config::get('Constant.SOCIAL_SHARE_TWITTER_SECRET_KEY'), array('class' => 'form-control', 'id' => 'twitter_secret_key', 'autocomplete'=>"off")) !!}
								<label class="form_title" for="twitter_secret_key">{{ trans('template.setting.twitterSecretKey') }} <span aria-required="true" class="required"> * </span></label>
								<span class="help-block">
									{{ $errors->first('twitter_secret_key') }}
								</span>
							</div>
						</div>
						<div class="form-group {{ $errors->has('twitter_access_token') ? ' has-error' : '' }} form-md-line-input">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-twitter"></i>
								</span>
								
								{!! Form::text('twitter_access_token' , Config::get('Constant.SOCIAL_SHARE_TWITTER_ACCESS_TOKEN'), array('class' => 'form-control', 'id' => 'twitter_access_token', 'autocomplete'=>"off")) !!}
								<label class="form_title" for="twitter_access_token">{{ trans('template.setting.twitterAccessToken') }} <span aria-required="true" class="required"> * </span></label>
								<span class="help-block">
									{{ $errors->first('twitter_access_token') }}
								</span>
							</div>
						</div>
						<div class="form-group {{ $errors->has('twitter_access_token_key') ? 'has-error' : '' }} form-md-line-input">
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-twitter"></i>
								</span>
								{!! Form::text('twitter_access_token_key' , Config::get('Constant.SOCIAL_SHARE_TWITTER_ACCESS_SECRET_KEY'), array('class' => 'form-control', 'id' => 'twitter_access_token_key', 'autocomplete'=>"off")) !!}
								<label class="form_title" for="twitter_access_token_key">{{ trans('template.setting.twitterAccessTokenSceretKey') }} <span aria-required="true" class="required"> * </span>
							</label>
							<span class="help-block">
								{{ $errors->first('twitter_access_token_key') }}
							</span>
						</div>
					</div>
					<div class="clearfix"></div>
					<label><i class="fa fa-check"></i> {{ trans('template.setting.linkedinShare') }}</label>
					<div class="form-group {{ $errors->has('linkedin_api') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-linkedin"></i>
							</span>
							{!! Form::text('linkedin_api' , Config::get('Constant.SOCIAL_SHARE_LINKEDIN_API_KEY'), array('class' => 'form-control', 'id' => 'linkedin_api', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="linkedin_api">{{ trans('template.setting.linkedinApiKey') }} <span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('linkedin_api') }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('linkedin_secret_key') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-linkedin"></i>
							</span>
							
							{!! Form::text('linkedin_secret_key' , Config::get('Constant.SOCIAL_SHARE_LINKEDIN_SECRET_KEY'), array('class' => 'form-control', 'id' => 'linkedin_secret_key', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="linkedin_secret_key">{{ trans('template.setting.linkedinSecretKey') }} <span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('linkedin_secret_key') }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('linkedin_access_token') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-linkedin"></i>
							</span>
							{!! Form::text('linkedin_access_token' , Config::get('Constant.SOCIAL_SHARE_LINKEDIN_ACCESS_TOKEN'), array('class' => 'form-control', 'id' => 'linkedin_access_token', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="linkedin_access_token">{{ trans('template.setting.linkedinAccessToken') }} <span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('linkedin_access_token') }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('linkedin_access_token_key') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-linkedin"></i>
							</span>
							{!! Form::text('linkedin_access_token_key' , Config::get('Constant.SOCIAL_SHARE_LINKEDIN_ACCESS_SECRET_KEY'), array('class' => 'form-control', 'id' => 'linkedin_access_token_key', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="linkedin_access_token_key">{{ trans('template.setting.linkedinAccessTokenSceretKey') }} <span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('linkedin_access_token_key') }}
							</span>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-green-drake submit">{!! trans('template.common.saveandedit') !!}</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>