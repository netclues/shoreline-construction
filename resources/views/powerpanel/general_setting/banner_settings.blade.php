<div class="tab-pane setting {{ (($tab_value=='banner_settings')?'active':'') }}" id="banner">
<div class="row">
	<div class="col-md-12">
		<div class="portlet-form">
			{!! Form::open(['method' => 'post','id'=>'frmSettings']) !!}
			{!! Form::hidden('tab', 'banner_settings', ['id' => 'banner']) !!}
			<div class="form-body">
				<div class="form-group {{ $errors->has('DEFAULT_BANNER_TITLE') ? ' has-error' : '' }} form-md-line-input">
					{!! Form::text('DEFAULT_BANNER_TITLE', Config::get('Constant.DEFAULT_BANNER_TITLE') , array('maxlength' => '150', 'class' => 'form-control maxlength-handler', 'id' => 'DEFAULT_BANNER_TITLE' , 'placeholder' => trans('template.setting.DEFAULT_BANNER_TITLE'),'autocomplete'=>'off')) !!}
					<label class="form_title" for="DEFAULT_BANNER_TITLE">{{ trans('template.setting.DEFAULT_BANNER_TITLE') }}</label>
					<span class="help-block">
						{{ $errors->first('DEFAULT_BANNER_TITLE') }}
					</span>
				</div>
				<div class="form-group {{ $errors->has('DEFAULT_BANNER_SUBTITLE') ? ' has-error' : '' }} form-md-line-input">
					{!! Form::text('DEFAULT_BANNER_SUBTITLE', Config::get('Constant.DEFAULT_BANNER_SUBTITLE') , array('maxlength' => '150', 'class' => 'form-control maxlength-handler', 'id' => 'DEFAULT_BANNER_SUBTITLE' , 'placeholder' => trans('template.setting.DEFAULT_BANNER_SUBTITLE'),'autocomplete'=>'off')) !!}
					<label class="form_title" for="DEFAULT_BANNER_SUBTITLE">{{ trans('template.setting.DEFAULT_BANNER_SUBTITLE') }}</label>
					<span class="help-block">
						{{ $errors->first('DEFAULT_BANNER_SUBTITLE') }}
					</span>
				</div>
				<div class="form-group {{ $errors->has('default_home_banner') ? ' has-error' : '' }}">
					<div class="image_thumb">
						<label for="home_banner_image" class="form_title">{{ trans('template.setting.DEFAULT_HOME_BANNER') }}</label>
						<div class="clearfix"></div>
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-preview thumbnail default_home_banner_img" data-trigger="fileinput" style="width:100%; height:120px;position: relative;">
								@if (!empty(Config::get('Constant.DEFAULT_HOME_BANNER')))
								<img src="{{ App\Helpers\resize_image::resize(Config::get('Constant.DEFAULT_HOME_BANNER')) }}"/>
								@else
								<img src="{{ url('resources/images/upload_file.gif') }}"/>
								@endif
							</div>
							<div class="input-group">
								<a class="media_manager" onclick="MediaManager.open('default_home_banner');"><span class="fileinput-new"></span></a>
							</div>
							<div class="overflow_layer default_home_banner">
								<a onclick="MediaManager.open('default_home_banner');" class="media_manager remove_img"><i class="fa fa-pencil"></i></a>
								<a href="javascript:;" class="fileinput-exists remove_img removeimg" data-id="default_home_banner" data-dismiss="fileinput"><i class="fa fa-trash-o"></i></a>
							</div>
							{!! Form::hidden('default_home_banner',!empty(Config::get('Constant.DEFAULT_HOME_BANNER'))?Config::get('Constant.DEFAULT_HOME_BANNER'):Request::old('default_home_banner') , array('class' => 'form-control', 'id' => 'default_home_banner')) !!}
						</div>
						<div class="clearfix"></div>
						<span>{{ trans('template.common.imageSize',['height'=>'400','width'=>'1920']) }}</span>
						<span class="help-block">
							{{ $errors->first('default_home_banner') }}
						</span>
					</div>
				</div>
				<div class="form-group {{ $errors->has('default_inner_banner') ? ' has-error' : '' }}">
					<div class="image_thumb">
						<label for="DEFAULT_INNER_BANNER" class="form_title">{{ trans('template.setting.DEFAULT_INNER_BANNER') }}</label>
						<div class="clearfix"></div>
						<div class="fileinput fileinput-new inner" data-provides="fileinput">
							<div class="fileinput-preview thumbnail default_inner_banner_img" data-trigger="fileinput" style="width:100%; height:120px;position: relative;">
								@if (!empty(Config::get('Constant.DEFAULT_INNER_BANNER')))
								<img src="{{ App\Helpers\resize_image::resize(Config::get('Constant.DEFAULT_INNER_BANNER')) }}"/>
								@else
								<img src="{{ url('resources/images/upload_file.gif') }}"/>
								@endif
							</div>
							<div class="input-group">
								<a class="media_manager" onclick="MediaManager.open('default_inner_banner');"><span class="fileinput-new inner"></span></a>
							</div>
							<div class="overflow_layer default_inner_banner">
								<a onclick="MediaManager.open('default_inner_banner');" class="media_manager remove_img"><i class="fa fa-pencil"></i></a>
								<a href="javascript:;" class="fileinput-exists remove_img removeimg" data-id="default_inner_banner" data-dismiss="fileinput"><i class="fa fa-trash-o"></i></a>
							</div>
							{!! Form::hidden('default_inner_banner',!empty(Config::get('Constant.DEFAULT_INNER_BANNER'))?Config::get('Constant.DEFAULT_INNER_BANNER'):Request::old('default_inner_banner') , array('class' => 'form-control', 'id' => 'default_inner_banner')) !!}
						</div>
						<div class="clearfix"></div>
						<span>{{ trans('template.common.imageSize',['height'=>'400','width'=>'1920']) }}</span>
						<span class="help-block">
							{{ $errors->first('default_inner_banner') }}
						</span>
					</div>
				</div>
				<button type="submit" class="btn btn-green-drake">{!! trans('template.common.saveandedit') !!}</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
</div>
