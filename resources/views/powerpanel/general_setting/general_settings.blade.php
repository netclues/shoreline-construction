<div class="tab-pane {{ (($tab_value=='general_settings' || empty($tab_value))?'active':'') }}" id="general">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-form">
				{!! Form::open(['method' => 'post','id'=>'frmSettings']) !!}
				{!! Form::hidden('tab', 'general_settings', ['id' => 'general']) !!}
				<div class="form-body">
					<div class="form-group {{ $errors->has('site_name') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::text('site_name', Config::get('Constant.SITE_NAME') , array('maxlength' => '150', 'class' => 'form-control maxlength-handler', 'id' => 'site_name' , 'placeholder' => trans('template.setting.siteName'),'autocomplete'=>'off')) !!}
						<label class="form_title" for="site_name">{{ trans('template.setting.siteName') }} <span aria-required="true" class="required"> * </span></label>
						<span class="help-block">
							{{ $errors->first('site_name') }}
						</span>
					</div>
					<div class="form-group {{ $errors->has('front_logo_id') ? ' has-error' : '' }}">
						<div class="image_thumb">
							<label for="front_logo" class="form_title">{{ trans('template.setting.frontLogo') }} <span aria-required="true" class="required"> * </span></label>
							<div class="clearfix"></div>
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-preview thumbnail front_logo_img" data-trigger="fileinput" style="width:100%; height:120px;position: relative;">
									@if (!empty(Config::get('Constant.FRONT_LOGO_ID')))
									<img src="{{ App\Helpers\resize_image::resize(Config::get('Constant.FRONT_LOGO_ID')) }}"/>
									@else
									<img src="{{ url('resources/images/upload_file.gif') }}"/>
									@endif
								</div>
								<div class="input-group">
									<a class="media_manager" onclick="MediaManager.open('front_logo');"><span class="fileinput-new"></span></a>
								</div>
								{!! Form::hidden('front_logo_id',!empty(Config::get('Constant.FRONT_LOGO_ID'))?Config::get('Constant.FRONT_LOGO_ID'):Request::old('image_upload') , array('class' => 'form-control', 'id' => 'front_logo')) !!}
							</div>
							<div class="clearfix"></div>
							<span>{{ trans('template.common.imageSize',['height'=>'300','width'=>'600']) }}</span>
							<span class="help-block">
								{{ $errors->first('front_logo_id') }}
							</span>
						</div>
					</div>
					<button type="submit" class="btn btn-green-drake">{!! trans('template.common.saveandedit') !!}</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>

