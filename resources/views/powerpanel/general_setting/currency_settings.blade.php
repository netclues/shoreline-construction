<div class="tab-pane {{ (($tab_value=='currency_settings')?'active':'') }}" id="currency_tab">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-form">
				{!! Form::open(['method' => 'post','id' => 'frmCurrency']) !!}
				{!! Form::hidden('tab', 'currency_settings', ['id' => 'currency_tab']) !!}
				<div class="form-body">
					<div class="form-group">
						<label class="form_title" for="timezone">Currency</label>
						<select class="form-control bs-select select2" name="currency" id="currency" style="width: 100%">
							@foreach($currencyList as $key => $cur)
							@if($cur->varCode == Config::get('Constant.CURRENCY'))
							@php $selected = 'selected' @endphp
							@else
							@php $selected = '' @endphp
							@endif
							<option {{ $selected }} value="{{ $cur->varCode }}">{{ $cur->varCountry }} ( {{ $cur->varSymbol }} )</option>
							@endforeach
						</select>
					</div>
					<button type="submit" class="btn btn-green-drake">{!! trans('template.common.saveandedit') !!}</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>