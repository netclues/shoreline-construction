@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection
@section('content')
@include('powerpanel.partials.breadcrumbs')
<div class="row">
	<div class="col-md-12">
		@if(Session::has('message'))
		<div class="alert alert-success">
			<button class="close" data-close="alert"></button>
			{{ Session::get('message') }}
		</div>
		@endif
		<div class="portlet light bordered">
			<div class="portlet-body form_pattern">
				<div class="tabbable tabbable-tabdrop">
					<div class="notify"></div>
					<ul class="nav nav-pills tab_section">

						@permission('settings-general-setting-management')
						<li class="{{ (($tab_value=='general_settings' || empty($tab_value))?'active':'') }}">
							<a href="#general" data-toggle="tab">{{ trans('template.setting.general') }}</a>
						</li>
						@endpermission

						@permission('settings-email-setting')
						<li class="{{ (($tab_value=='email_settings')?'active':'') }}">
							<a href="#email_settings" data-toggle="tab">Email Settings</a>
						</li>
						@endpermission

						<li class="{{ (($tab_value=='banner_settings')?'active':'') }}">
							<a href="#banner" data-toggle="tab">
							{{ trans('template.setting.bannersettings') }} </a>
						</li>

						@permission('settings-smtp-mail-setting')
						<li class="{{ (($tab_value=='smtp_settings')?'active':'') }}">
							<a href="#smtp-mail" data-toggle="tab">
							{{ trans('template.setting.SMTPMail') }} </a>
						</li>
						@endpermission

						@permission('settings-seo-setting')
						<li class="{{ (($tab_value=='seo_settings')?'active':'') }}">
							<a href="#seo" data-toggle="tab">{{ trans('template.setting.seo') }}</a>
						</li>
						@endpermission

						<!-- <li class="{{ (($tab_value=='currency_settings')?'active':'') }}">
							<a href="#currency_tab" data-toggle="tab">Currency Setting</a>
						</li> -->

						
						@permission('settings-social-setting')
						<li class="{{ (($tab_value=='social_settings')?'active':'') }}">
							<a href="#social" data-toggle="tab">{{ trans('template.setting.social') }}</a>
						</li>
						@endpermission
						
						@permission('settings-social-media-share-setting')
						<!-- <li class="{{ (($tab_value=='social_media_share')?'active':'') }}">
							<a href="#social_media_share" data-toggle="tab">{{ trans('template.setting.socialMediaShare') }}</a>
						</li> -->
						@endpermission

						@permission('settings-other-setting')
						<li class="{{ (($tab_value=='other_settings')?'active':'') }}">
							<a href="#other_settings" data-toggle="tab">{{ trans('template.setting.otherSettings') }}</a>
						</li>
						@endpermission

						@permission('settings-maintenance-setting')
						<li class="{{ (( $tab_value=='maintenance')?'active':'') }}">
							<a href="#maintenance" data-toggle="tab">{{ trans('template.setting.maintenance') }}</a>
						</li>
						@endpermission

						<!-- @permission('settings-module-setting')
						<li class="modulewisesettings {{ (( $tab_value=='module')?'active':'') }}">
							<a href="#modulesettings"  data-toggle="tab">{{ trans('template.setting.modulesettings') }}</a>
						</li>
						@endpermission -->
						
					</ul>
					<div class="tab-content settings">
					
						@permission('settings-general-setting-management')
							@include('powerpanel.general_setting.general_settings')
						@endpermission

						@include('powerpanel.general_setting.email_settings')

						@include('powerpanel.general_setting.banner_settings')

						@permission('settings-smtp-mail-setting')
							@include('powerpanel.general_setting.smtp_settings')
						@endpermission

						@permission('settings-seo-setting')
							@include('powerpanel.general_setting.seo_settings')
						@endpermission
						
						@permission('settings-social-setting')
							@include('powerpanel.general_setting.social_settings')
						@endpermission

						@permission('settings-other-setting')
							@include('powerpanel.general_setting.other_settings')
						@endpermission

						@permission('settings-maintenance-setting')
							@include('powerpanel.general_setting.maintainance_settings')
						@endpermission

				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	window.site_url =  '{!! url("/") !!}';
</script>
@include('powerpanel.partials.ckeditor')
<script src="{{ url('resources/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/pages/scripts/setting.js') }}" type="text/javascript"></script>
@endsection