<div class="tab-pane setting {{ (($tab_value=='social_settings')?'active':'') }}" id="social">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-form">
				{!! Form::open(['method' => 'post','id' => 'frmSocial']) !!}
				{!! Form::hidden('tab', 'social_settings', ['id' => 'social']) !!}
				<div class="form-body">
					<div class="form-group {{ $errors->has('fb_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-facebook"></i>
							</span>
							
							{!! Form::text('fb_link' , Config::get('Constant.SOCIAL_FB_LINK'), array('class' => 'form-control', 'id' => 'fb_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="fb_link">{{ trans('template.setting.facebookLink') }}</label>
							<span class="help-block">
								{{ $errors->first('fb_link') }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('instagram_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-instagram"></i>
							</span>
							{!! Form::text('instagram_link' , Config::get('Constant.SOCIAL_INSTAGRAM_LINK'), array('class' => 'form-control', 'id' => 'instagram_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="instagram_link">{{ trans('template.setting.instagramLink') }}</label>
							<span class="help-block">
								{{ $errors->first('instagram_link') }}
							</span>
						</div>
					</div>
					<?php /*
					<div class="form-group {{ $errors->has('twitter_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-twitter"></i>
							</span>
							
							{!! Form::text('twitter_link' , Config::get('Constant.SOCIAL_TWITTER_LINK'), array('class' => 'form-control', 'id' => 'twitter_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="twitter_link">{{ trans('template.setting.twitterLink') }}</label>
							<span class="help-block">
								{{ $errors->first('twitter_link') }}
							</span>
						</div>
					</div> 
					<div class="form-group {{ $errors->has('youtube_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-youtube-play"></i>
							</span>
							
							{!! Form::text('youtube_link' , Config::get('Constant.SOCIAL_YOUTUBE_LINK'), array('class' => 'form-control', 'id' => 'youtube_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="youtube_link">{{ trans('template.setting.youtubeLink') }}</label>
							<span class="help-block">
								{{ $errors->first('youtube_link') }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('linkedin_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-linkedin"></i>
							</span>
							{!! Form::text('linkedin_link' , Config::get('Constant.SOCIAL_LINKEDIN_LINK'), array('class' => 'form-control', 'id' => 'linkedin_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="linkedin_link">{{ trans('template.setting.linkedinLink') }}</label>
							<span class="help-block">
								{{ $errors->first('linkedin_link') }}
							</span>
						</div>
					</div> 
					
					<div class="form-group {{ $errors->has('tumblr_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-tumblr"></i>
							</span>
							{!! Form::text('tumblr_link' , Config::get('Constant.SOCIAL_TUMBLR_LINK'), array('class' => 'form-control', 'id' => 'tumblr_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="tumblr_link">{{ trans('template.setting.tumblrLink') }}</label>
							<span class="help-block">
								{{ $errors->first('tumblr_link') }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('pinterest_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-pinterest-p"></i>
							</span>
							{!! Form::text('pinterest_link' , Config::get('Constant.SOCIAL_PINTEREST_LINK'), array('class' => 'form-control', 'id' => 'pinterest_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="pinterest_link">{{ trans('template.setting.pinterestLink') }}</label>
							<span class="help-block">
								{{ $errors->first('pinterest_link') }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('flickr_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-flickr"></i>
							</span>
							{!! Form::text('flickr_link' , Config::get('Constant.SOCIAL_FLICKR_LINK'), array('class' => 'form-control', 'id' => 'flickr_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="flickr_link">{{ trans('template.setting.flickrLink') }}</label>
							<span class="help-block">
								{{ $errors->first('flickr_link') }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('dribbble_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-dribbble"></i>
							</span>
							{!! Form::text('dribbble_link' , Config::get('Constant.SOCIAL_DRIBBBLE_LINK'), array('class' => 'form-control', 'id' => 'dribbble_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="dribbble_link">{{ trans('template.setting.dribbbleLink') }}</label>
							<span class="help-block">
								{{ $errors->first('dribbble_link') }}
							</span>
						</div>
					</div>
					<div class="form-group {{ $errors->has('rss_feed_link') ? ' has-error' : '' }} form-md-line-input">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-rss"></i>
							</span>
							{!! Form::text('rss_feed_link' , Config::get('Constant.SOCIAL_RSS_FEED_LINK'), array('class' => 'form-control', 'id' => 'rss_feed_link', 'autocomplete'=>"off")) !!}
							<label class="form_title" for="rss_feed_link">{{ trans('template.setting.rssfeeLink') }}</label>
							<span class="help-block">
								{{ $errors->first('rss_feed_link') }}
							</span>
						</div>
					</div>
					*/ ?>
					<button type="submit" class="btn btn-green-drake submit">{!! trans('template.common.saveandedit') !!}</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>