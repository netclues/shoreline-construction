<div class="tab-pane {{ (($tab_value=='email_settings')?'active':'') }}" id="email_settings">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-form">
				{!! Form::open(['method' => 'post','id'=>'frmEmailSettings']) !!}
				{!! Form::hidden('tab', 'email_settings', ['id' => 'email_settings']) !!}
				<div class="form-body">
					<div class="personal_email"> 
						<div class="form-group form-md-line-input">
							{!! Form::text('default_admin_email', \App\Helpers\MyLibrary::getDecryptedString(Config::get('Constant.DEFAULT_ADMIN_EMAIL')) , array('maxlength' => '150','class' => 'form-control', 'id' => 'default_admin_email' , 'autocomplete'=>'off')) !!}
							<label class="form_title" for="default_admin_email">{{ trans('template.setting.defaultAdminEmail') }}<span class="required">*</span></label>
						</div>
						<div class="note_text"><strong>Note:</strong> For sending email on multiple email add comma separated email address (Ex: abc@xyz.com, xyz@abc.com)</div>
					</div>	
					<div class="personal_email"> 
						<div class="form-group form-md-line-input">
							{!! Form::text('default_contact_email', \App\Helpers\MyLibrary::getDecryptedString(Config::get('Constant.DEFAULT_CONTACT_EMAIL')) , array('maxlength' => '150','class' => 'form-control', 'id' => 'default_contact_email' , 'autocomplete'=>'off')) !!}
							<label class="form_title" for="default_contact_email">{{ trans('template.setting.defaultContactEmail') }}<span class="required">*</span></label>
						</div>
						<div class="note_text"><strong>Note:</strong> For sending email on multiple email add comma separated email address (Ex: abc@xyz.com, xyz@abc.com)</div>
					</div>
					<div class="personal_email"> 
						<div class="form-group form-md-line-input">
							{!! Form::text('default_request_email', \App\Helpers\MyLibrary::getDecryptedString(Config::get('Constant.DEFAULT_REQUEST_EMAIL')) , array('maxlength' => '150','class' => 'form-control', 'id' => 'default_request_email' , 'autocomplete'=>'off')) !!}
							<label class="form_title" for="default_request_email">{{ trans('template.setting.defaultRequestEmail') }}<span class="required">*</span></label>
						</div>
						<div class="note_text"><strong>Note:</strong> For sending email on multiple email add comma separated email address (Ex: abc@xyz.com, xyz@abc.com)</div>
					</div>
					<div class="personal_email">
						<div class="form-group form-md-line-input">
							{!! Form::text('default_cc_email',\App\Helpers\MyLibrary::getDecryptedString(Config::get('Constant.DEFAULT_CC_EMAIL')) ,array('maxlength' => '150','class' => 'form-control', 'id' => 'default_cc_email' , 'autocomplete'=>'off')) !!}
							<label class="form_title" for="default_cc_email">Default CC EMAIL ADDRESS</label>
						</div>
						<div class="note_text"><strong>Note:</strong> For sending email on multiple email add comma separated email address (Ex: abc@xyz.com, xyz@abc.com)</div>
					</div>	

					<div class="personal_email">
						<div class="form-group form-md-line-input">
							{!! Form::text('default_bcc_email',\App\Helpers\MyLibrary::getDecryptedString(Config::get('Constant.DEFAULT_BCC_EMAIL')) ,array('maxlength' => '150','class' => 'form-control', 'id' => 'default_bcc_email' , 'autocomplete'=>'off')) !!}
							<label class="form_title" for="default_bcc_email">Default BCC EMAIL ADDRESS</label>
						</div>
						<div class="note_text"><strong>Note:</strong> For sending email on multiple email add comma separated email address (Ex: abc@xyz.com, xyz@abc.com)</div>
					</div>	

					<div class="form-group form-md-line-input">
						{!! Form::text('default_replyto_email', \App\Helpers\MyLibrary::getDecryptedString(Config::get('Constant.DEFAULT_REPLYTO_EMAIL')) , array('maxlength' => '150','class' => 'form-control', 'id' => 'default_replyto_email' , 'autocomplete'=>'off')) !!}
						<label class="form_title" for="default_replyto_email">{{ trans('template.setting.replyToEmail') }}</label>
					</div>
					<button type="submit" class="btn btn-green-drake">{!! trans('template.common.saveandedit') !!}</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
