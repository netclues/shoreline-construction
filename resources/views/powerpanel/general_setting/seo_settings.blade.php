<div class="tab-pane {{ (($tab_value=='seo_settings')?'active':'') }}" id="seo">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-form">
				{!! Form::open(['method' => 'post','id' => 'frmSeo','enctype'=>'multipart/form-data']) !!}
				{!! Form::hidden('tab', 'seo_settings', ['id' => 'seo']) !!}
				<div class="form-body">
					<div class="form-group {{ $errors->has('meta_title') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::text('meta_title' , Config::get('Constant.DEFAULT_META_TITLE'), array('maxlength' => '150','class' => 'form-control maxlength-handler', 'id' => 'meta_title', 'autocomplete'=>"off")) !!}
						<label class="form_title" for="meta_title">{{ trans('template.common.metatitle') }} <span aria-required="true" class="required"> * </span></label>
						<span class="help-block">
							{{ $errors->first('meta_title') }}
						</span>
					</div>
					<div class="form-group {{ $errors->has('meta_description') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::textarea('meta_description' , Config::get('Constant.DEFAULT_META_DESCRIPTION'), array('class' => 'form-control', 'id' => 'meta_description', 'rows' => '4')) !!}
						<label class="form_title" for="form_control_1">{{ trans('template.common.metadescription') }} <span aria-required="true" class="required"> * </span></label>
						<span class="help-block">
							{{ $errors->first('meta_description') }}
						</span>
					</div>
					<div class="form-group {{ $errors->has('google_analytic_code') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::textarea('google_analytic_code' , Config::get('Constant.GOOGLE_ANALYTIC_CODE'), array('class' => 'form-control', 'id' => 'google_analytic_code','rows' => '4')) !!}
						<label class="form_title" for="google_analytic_code">{{ trans('template.setting.googleAnalytic') }} </label>
						<span class="help-block">
							{{ $errors->first('google_analytic_code') }}
						</span>
					</div>
					<div class="form-group {{ $errors->has('google_tag_manager_for_body') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::textarea('google_tag_manager_for_body' , Config::get('Constant.GOOGLE_TAG_MANAGER_FOR_BODY'), array('class' => 'form-control', 'id' => 'google_tag_manager_for_body', 'rows' => '4')) !!}
						<label class="form_title" for="google_tag_manager_for_body">{{ trans('template.setting.googleTagManager') }}</label>
						<span class="help-block">
							{{ $errors->first('google_tag_manager_for_body') }}
						</span>
					</div>
					<div class="form-group {{ $errors->has('robotfile_content') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::textarea('robotfile_content' , $robotFileContent, array('class' => 'form-control', 'id' => 'robotfile_content', 'autocomplete'=>"off")) !!}
						<label class="form_title" for="ROBOT_FILR">Robot TXT File Content</label>
						<span class="help-block">
							{{ $errors->first('robotfile_content') }}
						</span>
					</div>
					<div class="form-group form-md-line-input">
						{!! Form::file('xml_file' , array('class' => 'form-control', 'id' => 'bingfile','accept'=>"text/xml")) !!}
						@php
						$BingfileName = Config::get('Constant.BING_FILE_PATH');
						@endphp
						<label class="form_title" for="BingFile">Upload Bing File</label>
						<div class="clearfix"></div>
						<span>Recommended File type (.xml)</span>
						<div class="clearfix"></div>
						@if($BingfileName != "" || $BingfileName != null)
						<span>File Name:{{ $BingfileName }}</span>
						@endif
						<span class="help-block">
							{{ $errors->first('xml_file') }}
						</span>
					</div>
					<div class="form-group form-md-line-input hidden">
						<label class="form_title" for="generate_sitemap">Sitemap:&nbsp;</label>
						<a target="_blank" href="{{url('generateSitemap')}}" class="btn default"><i class="fa fa-sitemap" aria-hidden="true"></i> Click to generate sitemap</a>
					</div>
					<button type="submit" class="btn btn-green-drake">{!! trans('template.common.saveandedit') !!}</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>