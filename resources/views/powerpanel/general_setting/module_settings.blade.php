@permission('settings-module-setting')
<div class="tab-pane setting {{ $tab_value=='module')?'active':'') }}" id="modulesettings">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-form">
				<div class="col-md-6">
					{!! Form::text('search' , null, array('id' => 'moduleSearch', 'class' => 'form-control', 'placeholder'=>'Module Search', 'autocomplete'=>"off")) !!}
				</div>
				<div class="col-md-2">
					<a href="javascript:;" class="btn btn-green-drake search-module-settings submit"><i class="fa fa-search"></i></a>
					<a href="javascript:;" class="btn btn-green-drake modulewisesettings submit"><i class="fa fa-refresh"></i></a>
				</div><br/><br/><br/>
				<div class="clearfix"></div>
				<div id='moduleDiv'></div>
			</div>
		</div>
	</div>
</div>
@endpermission