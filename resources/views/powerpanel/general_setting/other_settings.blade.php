<div class="tab-pane setting {{ (($tab_value=='other_settings')?'active':'') }}" id="other_settings">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-form">
				{!! Form::open(['method' => 'post','id' => 'other_settings']) !!}
				{!! Form::hidden('tab', 'other_settings', ['id' => 'other_settings']) !!}
				<div class="form-body">
					{{-- @php
					<div class="form-group">
						<label class="form_title" for="default_page_size">{{ trans('template.setting.defaultPagesize') }}</label>
						<select class="form-control select2" name="default_page_size" id="default_page_size" style="width:100%">
							@php $ten_selected = '' @endphp
							@php $twenty_selected = '' @endphp
							@php $fifty_selected = '' @endphp
							@php $hundred_selected = '' @endphp
							@php $all_selected = '' @endphp
							@if (Config::get('Constant.DEFAULT_PAGE_SIZE')  == '10')
							@php $ten_selected = 'selected' @endphp
							@elseif (Config::get('Constant.DEFAULT_PAGE_SIZE') == '20')
							@php $twenty_selected = 'selected' @endphp
							@elseif (Config::get('Constant.DEFAULT_PAGE_SIZE') == '50')
							@php $fifty_selected = 'selected' @endphp
							@elseif (Config::get('Constant.DEFAULT_PAGE_SIZE') == '100')
							@php $hundred_selected = 'selected' @endphp
							@elseif (Config::get('Constant.DEFAULT_PAGE_SIZE') == 'All')
							@php $all_selected = 'selected' @endphp
							@else
							@php $ten_selected = '' @endphp
							@php $twenty_selected = '' @endphp
							@php $fifty_selected = '' @endphp
							@php $hundred_selected = '' @endphp
							@php $all_selected = '' @endphp
							@endif
							<option {{ $ten_selected }} value="10">10</option>
							<option {{ $twenty_selected }} value="20">20</option>
							<option {{ $fifty_selected }} value="50">50</option>
							<option {{ $hundred_selected }} value="100">100</option>
							<option {{ $all_selected }} value="All">{{ trans('template.common.all') }}</option>
						</select>
					</div> --}}
					@if(!empty($timezone))
						<div class="form-group">
							<label class="form_title" for="timezone">{{ trans('template.setting.timezone') }}</label>
							<select class="form-control bs-select select2" name="timezone" id="timezone" style="width: 100%">
								@foreach ($timezone as $allzones)
								@if(!empty(Config::get('Constant.DEFAULT_TIME_ZONE')))
								@if($allzones->zone_name == Config::get('Constant.DEFAULT_TIME_ZONE'))
								@php  $selected = 'selected'  @endphp
								@else
								@php  $selected = ''  @endphp
								@endif
								@elseif($allzones->zone_name == 'America/Cayman')
								@php  $selected = 'selected'  @endphp
								@else
								@php  $selected = ''  @endphp
								@endif
								<option {{$selected}} value="{{$allzones->zone_name}}">{{$allzones->zone_name}}</option>
								@endforeach
							</select>
						</div>
					@endif
					<div class="clearfix"></div>
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select select2" name="default_date_format" id="default_date_format" style="width:100%">
							<option value="d/m/Y" <?php if (Config::get('Constant.DEFAULT_DATE_FORMAT') == "d/m/Y") {echo 'selected="selected"';}?> >d/m/Y (Eg: {{ Carbon\Carbon::today()->format('d/m/Y') }})  </option>
							<option value="m/d/Y" <?php if (Config::get('Constant.DEFAULT_DATE_FORMAT') == "m/d/Y") {echo 'selected="selected"';}?> >m/d/Y (Eg: {{ Carbon\Carbon::today()->format('m/d/Y') }})  </option>
							<option value="Y/m/d" <?php if (Config::get('Constant.DEFAULT_DATE_FORMAT') == "Y/m/d") {echo 'selected="selected"';}?> >Y/m/d (Eg: {{ Carbon\Carbon::today()->format('Y/m/d') }})  </option>
							<option value="Y/d/m" <?php if (Config::get('Constant.DEFAULT_DATE_FORMAT') == "Y/d/m") {echo 'selected="selected"';}?> >Y/d/m (Eg: {{ Carbon\Carbon::today()->format('Y/d/m') }})  </option>
							<option value="M/d/Y" <?php if (Config::get('Constant.DEFAULT_DATE_FORMAT') == "M/d/Y") {echo 'selected="selected"';}?> >M/d/Y (Eg: {{ Carbon\Carbon::today()->format('M/d/Y') }})  </option>
							<option value="M d Y" <?php if (Config::get('Constant.DEFAULT_DATE_FORMAT') == "M d Y") {echo 'selected="selected"';}?> >M d Y (Eg: {{ Carbon\Carbon::today()->format('M d Y') }})  </option>
						</select>
						<label class="form_title" for="default_date_format">{{ trans('template.common.defaultDateFormat') }} (d/m/Y)</label>
					</div>
					<div class="form-group form-md-line-input">
						<select class="form-control bs-select select2" name="time_format" id="time_format" style="width:100%">
							<option value="h:i A" <?php if (Config::get('Constant.DEFAULT_TIME_FORMAT') == "h:i A") {echo 'selected="selected"';}?> >12 {{ trans('template.common.hours') }}</option>
							<option value="H:i" <?php if (Config::get('Constant.DEFAULT_TIME_FORMAT') == "H:i") {echo 'selected="selected"';}?> >24 {{ trans('template.common.hours') }}</option>
						</select>
						<label class="form_title" for="time_format">{{ trans('template.common.defaultTimeFormat') }}</label>
					</div>
					<div class="form-group {{ $errors->has('google_map_key') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::text('google_map_key' , Config::get('Constant.GOOGLE_MAP_KEY'), array('class' => 'form-control', 'id' => 'google_map_key', 'autocomplete'=>"off")) !!}
						<label class="form_title" for="google_map_key">{{ trans('template.setting.googleMapKey') }} <span aria-required="true" class="required"> * </span></label>
						<span class="help-block">
							{{ $errors->first('google_map_key') }}
						</span>
					</div>
					<div class="form-group {{ $errors->has('google_capcha_key') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::text('google_capcha_key' ,!empty(Config::get('Constant.GOOGLE_CAPCHA_KEY'))?Config::get('Constant.GOOGLE_CAPCHA_KEY'):'', array('class' => 'form-control', 'id' => 'google_capcha_key', 'autocomplete'=>"off")) !!}
						<label class="form_title" for="google_map_key">{{ trans('template.setting.googleCapchaKey') }}  <span aria-required="true" class="required"> * </span></label>
						<span class="help-block">
							{{ $errors->first('google_capcha_key') }}
						</span>
					</div>
					<div class="form-group {{ $errors->has('google_capcha_secret') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::text('google_capcha_secret' ,!empty(Config::get('Constant.GOOGLE_CAPTCHA_SECRET'))?Config::get('Constant.GOOGLE_CAPTCHA_SECRET'):'', array('class' => 'form-control', 'id' => 'google_capcha_secret', 'autocomplete'=>"off")) !!}
						<label class="form_title" for="google_map_key">{{ trans('template.setting.googleCapchaSecret') }}  <span aria-required="true" class="required"> * </span></label>
						<span class="help-block">
							{{ $errors->first('google_capcha_secret') }}
						</span>
					</div>
					<div class="form-group">
						<label for="banner_type" class="form_title">{{ trans('template.setting.filterBadWords') }}:</label>
						<div class="md-radio-inline">
							<div class="md-radio">
								@if ((!empty(Config::get('Constant.BAD_WORDS')) && Config::get('Constant.BAD_WORDS') == 'Y') || (null == Request::old('bad_words') || Request::old('bad_words') == 'Y'))
								@php  $checked_yes = 'checked'  @endphp
								@else
								@php  $checked_yes = ''  @endphp
								@endif
								<input type="radio" {{ $checked_yes }} value="Y" id="badWordsYes" name="bad_words" class="md-radiobtn">
								<label for="badWordsYes">
									<span class="inc"></span>
									<span class="check"></span>
									<span class="box"></span> {{ trans('template.common.yes') }}
								</label>
							</div>
							<div class="md-radio">
								@if (Config::get('Constant.BAD_WORDS') == 'N' || (!empty(Config::get('Constant.BAD_WORDS')) && Config::get('Constant.BAD_WORDS') == 'N'))
								@php  $checked_yes = 'checked'  @endphp
								@else
								@php  $checked_yes = ''  @endphp
								@endif
								<input type="radio" {{ $checked_yes }} value="N" id="badWordsNo" name="bad_words" class="md-radiobtn">
								<label for="badWordsNo">
									<span class="inc"></span>
									<span class="check"></span>
									<span class="box"></span> {{ trans('template.common.no') }}
								</label>
							</div>
						</div>
					</div>
					<div class="form-group {{ $errors->has('php_ini_content') ? ' has-error' : '' }} form-md-line-input">
						{!! Form::textarea('php_ini_content' , $phpIniContent, array('class' => 'form-control', 'id' => 'php_ini_content', 'autocomplete'=>"off")) !!}
						<label class="form_title" for="PHP_INI_CONTENT">{{ trans('template.setting.phpIniSettings') }}</label>
						<span class="help-block">
							{{ $errors->first('php_ini_content') }}
						</span>
					</div>
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label class="form_title" for="AVAILABLE_SOCIAL_LINKS_FOR_TEAM_MEMEBER">Available Social Links For Team Member</label>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 text-right">
							<a href="javascript:void(0);" class="addMoreSocial add_more" title="Add More"><i class="fa fa-plus"></i> Add Social Link</a>
						</div>
						<div class="clearfix"></div>
						<div class="multi_social_links">
							@php
							$socialcnt=0;
							$selectedSocialLinks=unserialize(Config::get('Constant.AVAILABLE_SOCIAL_LINKS_FOR_TEAM_MEMBER'));
							@endphp
							@if(is_array($selectedSocialLinks) && count($selectedSocialLinks)>0 &&!empty($selectedSocialLinks))
							@foreach($selectedSocialLinks as $socialLinks)
							
							<div class="single_social_link">
								<div class="col-md-4">
									<div class="form-group {{ $errors->has('AVAILABLE_SOCIAL_LINKS_FOR_TEAM_MEMEBER') ? ' has-error' : '' }} form-md-line-input">
										{!! Form::text('available_social_links_for_team['.($socialcnt).'][title]' , $socialLinks['title'], array('class' => 'form-control', 'id' => 'available_social_links_for_team'.($socialcnt).'_1', 'autocomplete'=>"off")) !!}
										<label class="form_title" for="AVAILABLE_SOCIAL_LINKS_FOR_TEAM_MEMEBER">Title <span aria-required="true" class="required"> * </span></label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group {{ $errors->has('AVAILABLE_SOCIAL_LINKS_FOR_TEAM_MEMEBER') ? ' has-error' : '' }} form-md-line-input">
										{!! Form::text('available_social_links_for_team['.($socialcnt).'][placeholder]' , $socialLinks['placeholder'], array('class' => 'form-control', 'id' => 'available_social_links_for_team'.($socialcnt).'_2', 'autocomplete'=>"off")) !!}
										<label class="form_title" for="AVAILABLE_SOCIAL_LINKS_FOR_TEAM_MEMEBER">Place Holder</label>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group {{ $errors->has('AVAILABLE_SOCIAL_LINKS_FOR_TEAM_MEMEBER') ? ' has-error' : '' }} form-md-line-input">
										{!! Form::text('available_social_links_for_team['.($socialcnt).'][class]', $socialLinks['class'], array('class' => 'form-control', 'id' => 'available_social_links_for_team'.($socialcnt).'_3', 'autocomplete'=>"off")) !!}
										<label class="form_title" for="AVAILABLE_SOCIAL_LINKS_FOR_TEAM_MEMEBER">Class</label>
										<a href="javascript:void(0);" class="removeSocial add_more" title="Remove"><i class="fa fa-times"></i> Remove</a>
									</div>
								</div>
							</div>
							@php $socialcnt++; @endphp
							@endforeach
							@endif
						</div>
					</div>
					<button type="submit" class="btn btn-green-drake submit">{!! trans('template.common.saveandedit') !!}</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>