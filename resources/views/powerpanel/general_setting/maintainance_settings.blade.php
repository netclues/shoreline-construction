<div class="tab-pane setting {{ (( $tab_value=='maintenance')?'active':'') }}" id="maintenance">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-form">
				{!! Form::open(['method' => 'post','id' => 'frmMaintenance']) !!}
				{!! Form::hidden('tab', 'maintenance', ['id' => 'maintenance']) !!}
				<div class="form-body">
					<div class="form-group hidden">
						<label><i class="fa fa-refresh"></i> {{ trans('template.setting.resetCounter') }}</label>
						<a href="{{url('powerpanel/settings/getDBbackUp')}}"><i class="fa fa-hdd-o" aria-hidden="true"></i> Database Backup</a>
					</div>
					<div class="form-group">
						<div class="checkbox-list-validation">
							<label class="checkbox-inline">
								{!!	Form::checkbox('reset[]', 'moblihits') !!}
								{{ trans('template.setting.resetMobileHits') }}
							</label>
							<label class="checkbox-inline">
								{!!	Form::checkbox('reset[]', 'emaillog') !!}
								{{ trans('template.setting.resetEmailLogs') }}
							</label>
							<label class="checkbox-inline">
								{!!	Form::checkbox('reset[]', 'webhits') !!}
								{{ trans('template.setting.resetWebHits') }}
							</label>
							
							@permission('contact-us-list')
								<label class="checkbox-inline">
									{!!	Form::checkbox('reset[]', 'contactleads') !!}
									{{ trans('template.setting.resetContactLeads') }}
								</label>
							@endpermission

							@permission('request-a-quote-list')
								<label class="checkbox-inline">
									{!!	Form::checkbox('reset[]', 'requestleads') !!}
									{{ trans('template.setting.resetRequestLeads') }}
								</label>
							@endpermission

							@permission('newsletter-lead-list')
							<label class="checkbox-inline">
								{!!	Form::checkbox('reset[]', 'newsletterleads') !!}
								{{ trans('template.setting.resetNewsletterLeads') }}
							</label>
							@endpermission

							<label class="checkbox-inline">
								{!!	Form::checkbox('reset[]', 'flushAllCache') !!}
								Flush All Cache
							</label>
						</div>
						<span class="help-block">
							{{ $errors->first('reset') }}
						</span>
					</div>
					<button type="submit" class="btn btn-green-drake submit">{{ trans('template.common.reset') }}</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>