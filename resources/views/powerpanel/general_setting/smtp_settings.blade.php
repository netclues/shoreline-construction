<div class="tab-pane {{ (($tab_value=='smtp_settings' )?'active':'') }}" id="smtp-mail">
	<div class="row">
		<div class="col-md-12">
			<div class="portlet-form">
				{!! Form::open(['method' => 'post','id'=>'smtpForm']) !!}
				<input type="password" style="width: 0;height: 0; visibility: hidden;position:absolute;left:0;top:0;"/>
				{!! Form::hidden('tab', 'smtp_settings', ['id' => 'smtp']) !!}
				<div class="form-body">
					<div class="form-actions form-group" style="padding:0">
						<div class="pull-left">
							<button type="button" id="testSMTP" class="btn btn-green-drake">{{ trans('template.setting.testSMTPSettings') }}</button>
						</div>
					</div>
					<div class="form-group {{ $errors->has('mailer') ? ' has-error' : '' }} form-md-line-input">
						<select class="form-control bs-select select2" name="mailer" id="mailer" style="width: 100%;">
							<option {{ ((Config::get('Constant.MAILER') == 'sendmail')?'selected':'') }} value="mail_api">{{ trans('template.setting.mailAPI') }}</option>
							<option {{ ((Config::get('Constant.MAILER') == 'smtp')?'selected':'') }} value="smtp">{{ trans('template.setting.smtp') }}</option>
							<option {{ ((Config::get('Constant.MAILER') == 'log')?'selected':'') }} value="log">Log (Turn off mail and this will set emails in laravel logs)</option>
						</select>
						<label class="form_title" for="mailer">{{ trans('template.setting.mailer') }} <span aria-required="true" class="required"> * </span></label>
					</div>

					<div id="mail_api_option">
						<div class="form-group {{ $errors->has('mail_api_url') ? ' has-error' : '' }} form-md-line-input">
							{!! Form::text('mail_api_url', Config::get('Constant.MAIL_API_URL') , array('class' => 'form-control' , 'autocomplete'=>'off')) !!}
							<label class="form_title">{{ trans('template.setting.mailApiUrl') }}<span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('mail_api_url') }}
							</span>
						</div>

						<div class="form-group {{ $errors->has('mail_api_username') ? ' has-error' : '' }} form-md-line-input">
							{!! Form::text('mail_api_username', Config::get('Constant.MAIL_API_USERNAME') , array('class' => 'form-control' , 'autocomplete'=>'off')) !!}
							<label class="form_title">{{ trans('template.setting.mailApiUser') }}<span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('mail_api_username') }}
							</span>
						</div>

						<div class="form-group {{ $errors->has('mail_api_pwd') ? ' has-error' : '' }} form-md-line-input">
							<input type="password" class="form-control" name="mail_api_pwd" id="mail_api_pwd" value="{{Config::get('Constant.MAIL_API_PASSWORD') }}" autocomplete="off">
							<label class="form_title" >{{ trans('template.setting.mailApiPassword') }}<span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('mail_api_pwd') }}
							</span>
						</div>


						<div class="form-group {{ $errors->has('mail_api_token') ? ' has-error' : '' }} form-md-line-input">
							{!! Form::text('mail_api_token', Config::get('Constant.MAIL_API_TOKEN') , array('class' => 'form-control' , 'autocomplete'=>'off')) !!}
							<label class="form_title" >{{ trans('template.setting.mailApiToken') }}<span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('mail_api_token') }}
							</span>
						</div>


						<div class="form-group {{ $errors->has('mail_api_site_id') ? ' has-error' : '' }} form-md-line-input">
							{!! Form::text('mail_api_site_id', Config::get('Constant.MAIL_API_SITE_ID') , array('class' => 'form-control' , 'autocomplete'=>'off')) !!}
							<label class="form_title">{{ trans('template.setting.mailApiSiteId') }}<span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('mail_api_site_id') }}
							</span>
						</div>

					</div>

					<div id="smtp_option" style="display:none;">
						<div class="form-group {{ $errors->has('smtp_server') ? ' has-error' : '' }} form-md-line-input">
							{!! Form::text('smtp_server', Config::get('Constant.SMTP_SERVER') , array('maxlength' => '150','class' => 'form-control maxlength-handler', 'id' => 'smtp_server' , 'autocomplete'=>'off')) !!}
							<label class="form_title" for="smtp_server">{{ trans('template.setting.smtpServer') }}<span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('smtp_server') }}
							</span>
						</div>
						<div class="form-group {{ $errors->has('smtp_username') ? ' has-error' : '' }} form-md-line-input">
							{!! Form::text('smtp_username', Config::get('Constant.SMTP_USERNAME') , array('maxlength' => '150','class' => 'form-control maxlength-handler', 'id' => 'smtp_username' , 'autocomplete'=>'off')) !!}
							<label class="form_title" for="smtp_username">{{ trans('template.setting.smtpUsername') }} <span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('smtp_username') }}
							</span>
						</div>
						<div class="form-group {{ $errors->has('smtp_password') ? ' has-error' : '' }} form-md-line-input">
							<input type="password" maxlength="150" class="form-control maxlength-handler" name="smtp_password" id="smtp_password" value="{{Config::get('Constant.SMTP_PASSWORD') }}" autocomplete="off">
							<label class="form_title" for="smtp_password">{{ trans('template.setting.smtpPassword') }} <span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('smtp_password') }}
							</span>
						</div>
						<div class="form-group {{ $errors->has('smtp_encryption') ? ' has-error' : '' }}">
							<label class="form_title" for="smtp_encryption">{{ trans('template.setting.smtpEncryption') }}<span aria-required="true" class="required"> * </span></label>
							<select class="bs-select select2" name="smtp_encryption" id="smtp_encryption">
								<option {{ (Config::get('Constant.SMTP_ENCRYPTION') == 'null'?'selected':'') }} value="null">{{ trans('template.setting.none') }}</option>
								<option {{ (Config::get('Constant.SMTP_ENCRYPTION') == 'tls'?'selected':'') }} value="tls">{{ trans('template.setting.tls') }}</option>
								<option {{ (Config::get('Constant.SMTP_ENCRYPTION') == 'ssl'?'selected':'') }} value="ssl">{{ trans('template.setting.ssl') }}</option>
							</select>
						</div>
						<div class="form-group form-md-radios">
							<label class="form_title control-label" for="form_control_1">{{ trans('template.setting.smtpAuthentication') }} <span aria-required="true" class="required"> * </span></label>
							<div class="md-radio-inline">
								<div class="md-radio">
									<input type="radio" id="yes" name="smtp_authenticattion" value="Y" class="md-radiobtn" <?php if (Config::get('Constant.SMTP_AUTHENTICATION') == "Y") {echo 'checked="checked"';}?> >
									<label for="yes">
										<span></span>
										<span class="check"></span>
										<span class="box"></span>{{ trans('template.common.yes') }}
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" id="no" name="smtp_authenticattion" value="N" class="md-radiobtn" <?php if (Config::get('Constant.SMTP_AUTHENTICATION') == "N") {echo 'checked="checked"';}?> >
									<label for="no">
										<span></span>
										<span class="check"></span>
										<span class="box"></span> {{ trans('template.common.no') }}
									</label>
								</div>
							</div>
						</div>
						<div class="form-group {{ $errors->has('smtp_port') ? ' has-error':'' }} form-md-line-input">
							{!! Form::text('smtp_port',Config::get('Constant.SMTP_PORT'), array('maxlength' => '150', 'class' => 'form-control maxlength-handler', 'id' => 'smtp_port_no')) !!}
							<label class="form_title" for="smtp_port">{{ trans('template.setting.smtpPort') }} <span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('smtp_port') }}
							</span>
						</div>
						<div class="form-group {{ $errors->has('smtp_sender_name') ? ' has-error' : '' }} form-md-line-input">
							{!! Form::text('smtp_sender_name',Config::get('Constant.SMTP_SENDER_NAME'), array('maxlength' => '150', 'class' => 'form-control maxlength-handler', 'id' => 'smtp_sender_name','autocomplete'=>'off')) !!}
							<label class="form_title" for="smtp_sender_name">{{ trans('template.setting.senderName') }} <span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('smtp_sender_name') }}
							</span>
						</div>
						<div class="form-group {{ $errors->has('smtp_sender_id') ? ' has-error' : '' }} form-md-line-input">
							{!! Form::text('smtp_sender_id',\App\Helpers\MyLibrary::getDecryptedString(Config::get('Constant.SMTP_SENDER_EMAIL')), array('maxlength' => '150', 'class' => 'form-control', 'id' => 'smtp_sender_id','autocomplete'=>'off')) !!}
							<label class="form_title" for="smtp_sender_id">{{ trans('template.setting.senderEmail') }} <span aria-required="true" class="required"> * </span></label>
							<span class="help-block">
								{{ $errors->first('smtp_sender_id') }}
							</span>
						</div>
					</div>
					<button type="submit" class="btn btn-green-drake">{!! trans('template.common.saveandedit') !!}</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>