@section('css')
<link href="{{ url('resources/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet"
  type="text/css" />
<link href="{{ url('resources/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
  type="text/css" />
@endsection
@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection
@section('content')
@include('powerpanel.partials.breadcrumbs')
<div class="col-md-12">
  @if(Session::has('message'))
  <div class="alert alert-success">
    <button class="close" data-close="alert"></button>
    {{ Session::get('message') }}
  </div>
  @endif
  <div class="row">
    <div class="portlet light bdisplay_ordered">
      <div class="portlet-body form_pattern">
        <div class="tabbable tabbable-tabdrop">
          <div class="tab-content">
            {!! Form::open(['method' => 'post','id'=>'changeProfile']) !!}
            <div class="form-body">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} form-md-line-input">
                {!! Form::text('name',$user_data->name,array('class' => 'form-control input-sm', 'maxlength'=>'150','id'
                => 'name','autocomplete'=>'off','placeholder'=>trans('template.common.name'))) !!}
                <label class="form_title" for="name">{{ trans('template.common.name') }} <span aria-required="true"
                    class="required"> * </span></label>
                <span class="help-block">
                  {{ $errors->first('name') }}
                </span>
              </div>
              <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} form-md-line-input">
                {!! Form::text('email',$user_data->email,array('class' => 'form-control input-sm',
                'maxlength'=>'100','id' => 'email','autocomplete'=>'off','placeholder'=>trans('template.common.email')))
                !!}
                <label class="form_title" for="email">{{ trans('template.common.email') }} <span aria-required="true"
                    class="required"> * </span></label>
                <span class="help-block">
                  {{ $errors->first('email') }}
                </span>
              </div>
              <div class="personal_email">
                <div class="form-group {{ $errors->has('personalId') ? 'has-error' : '' }} form-md-line-input">
                  {!! Form::text('personalId',$user_data->personalId,array('class' => 'form-control input-sm',
                  'maxlength'=>'100','id' => 'personalId','autocomplete'=>'off','placeholder'=>'Personal Email')) !!}
                  <label class="form_title" for="email">Personal Email <span aria-required="true" class="required"> *
                    </span></label>
                  <span class="help-block">
                    {{ $errors->first('personalId') }}
                  </span>
                </div>
                <div class="note_text"><strong>Note:</strong> Forgot password email will land on mentioned email id</div>
              </div>
            </div>
            
           <!--  @include('powerpanel.partials.imageControl',['type' => 'single','label' => trans('template.myProfile.profilephoto') ,'data'=> isset($user_data)?$user_data:null , 'id' => 'user_photo', 'name' => 'img_id_user_photo', 'settings' => isset($settings)?$settings:'', 'height' => '100', 'width' => '100'])  -->
          
          <div class="form-actions">
            <div class="row">
              <div class="col-md-12">
                <button type="submit" name="save-settings" class="btn btn-green-drake"
                  value="Update Profile">{{ trans('template.myProfile.updateprofile') }}</button>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="clearfix"></div>
@endsection
@section('scripts')
<script src="{{ url('resources/pages/scripts/change_profile.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/select2/js/components-select2.min.js') }}" type="text/javascript">
</script>
@endsection