@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection

@section('css')
<link href="{{ url('resources/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}"
  rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"
  rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/highslide/highslide.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@include('powerpanel.partials.breadcrumbs')
<!-- BEGIN PAGE BASE CONTENT -->
{!! csrf_field() !!}
<div class="row">
  <div class="col-md-12">
    <!-- Begin: life time stats -->
    @if(Session::has('message'))
    <div class="alert alert-success">
      <button class="close" data-close="alert"></button>
      {{ Session::get('message') }}
    </div>
    @endif
    <div class="portlet light portlet-fit portlet-datatable bordered">
      @if($iTotalRecords > 0)
      <div class="portlet-title select_box">
        <span class="title">{{ trans('template.common.filterby') }}:</span>
        <select id="statusfilter" data-sort data-order class="bs-select select2">
          <option value=" ">{{ trans('template.common.selectstatus') }}</option>
          <option value="Y">{{ trans('template.common.publish') }}</option>
          <option value="N">{{ trans('template.common.unpublish') }}</option>
        </select>

        <select class="form-control bs-select select2 categoryFilter" id="category_id" name="category_id">
          @if(count($ClientCategory)>0)
          <option value=" ">--{{ trans('template.common.selectcategory') }}--</option>
          @foreach($ClientCategory as $category)
          <option value="{{ $category->id }}"
            {{ ($category->id == Request::old('category_id') || app('request')->input('category')==$category->id)? 'selected' : '' }}>
            {{ $category->varTitle }}</option>
          @endforeach
          @else
          <option value=" " disabled="disabled">{{ trans('template.common.pleaseaddcategory') }}</option>
          @endif
        </select>

        <span class="btn btn-icon-only green-new" type="button" id="refresh" title="Reset">
          <i class="fa fa-refresh" aria-hidden="true"></i>
        </span>
        @permission('client-create')
        <div class="pull-right">
          <a class="btn btn-green-drake" href="{{ url('powerpanel/client/add') }}">
            {{ trans('template.clientModule.addClientMember') }}</a>
        </div>
        @endpermission
				<div class="col-md-2 pull-right">
          <input type="search" placeholder="Search By Name" class="form-control" id="searchfilter">
        </div>
      </div>

      <div class="portlet-body">
        <div class="table-container">
          <table class="new_table_desing table table-striped table-bordered table-hover table-checkable"
            id="datatable_ajax">
            <thead>
              <tr role="row" class="heading">
                <th width="2%"><input type="checkbox" class="group-checkable"></th>
                <th width="20%">{{ trans('template.common.name') }}</th>
                <th width="5%" align="left">{{ trans('template.common.shortdescription') }}</th>
                <th width="10%">{{ trans('template.common.image') }}</th>
                <th width="10%" align="center">{{ trans('template.common.category') }}</th>
                <th width="10%" align="center">{{ trans('template.common.order') }}</th>
                <th width="15%" align="center">{{ trans('template.common.publish') }}</th>
                <th width="15%" align="right">{{ trans('template.common.actions') }}</th>
              </tr>
            </thead>
            <tbody> </tbody>
          </table>
          @permission('client-delete')
          <a href="javascript:;"
            class="btn-sm btn btn-outline red right_bottom_btn deleteMass">{{ trans('template.common.delete') }}
          </a>
          @endpermission
        </div>
      </div>
      @else
      @include('powerpanel.partials.addrecordsection',['type'=>Config::get('Constant.MODULE.TITLE'), 'adUrl' =>
      url('powerpanel/client/add')])
      @endif
    </div>
  </div>
</div>
  @php  
  $tableState = true;
    $seg = url()->previous(); 
    $segArr = explode('/', $seg);
    if(!in_array('client', $segArr)){
     $tableState = false;
    }
  @endphp
@include('powerpanel.partials.deletePopup')
@include('powerpanel.partials.onepushmodal')
@endsection
@section('scripts')
<script type="text/javascript">
window.site_url = '{!! url("/") !!}';
var DELETE_URL = '{!! url("/powerpanel/client/DeleteRecord") !!}';
var onePushShare = '{!! url("/powerpanel/share") !!}';
var onePushGetRec = '{!! url("/powerpanel/share/getrec") !!}';
var tableState = '{{ $tableState }}';
</script>
<script src="{{ url('resources/global/plugins/jquery-cookie-master/src/jquery.cookie.js') }}" type="text/javascript">
</script>
<script src="{{ url('resources/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/datatables/dataTables.editor.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
  type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
  type="text/javascript"></script>
<script src="{{ url('resources/pages/scripts/client-datatables-ajax.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/pages/scripts/custom.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/highslide/highslide-with-html.js') }}" type="text/javascript"></script>
@endsection