@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@stop
@section('css')
<link href="{{ url('resources/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}"
  rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
  type="text/css" />
<link href="{{ url('resources/global/plugins/highslide/highslide.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@include('powerpanel.partials.breadcrumbs')
<div class="row">
  <div class="col-md-12">
    @if(Session::has('message'))
    <div class="alert alert-success">
      <button class="close" data-close="alert"></button>
      {{ Session::get('message') }}
    </div>
    @endif
    <div class="portlet light portlet-fit portlet-datatable bordered">
      <div class="notify"></div>
      <div class="portlet-title select_box">
        <div class="col-md-12 nopadding">

          <div class="banner_tabs">
            <ul class="nav nav-pills tab_section list-group banner-tab">
              <li class="list-group item-action">
                <a title="{{ trans('Go to Home Banners') }}" href="{{ url('powerpanel/banners') }}"
                  class="nav-link nav-toggle">
                  {{ trans('Home Banner') }}
                </a>
              </li>
              <li class="list-group item-action  active">
                <a title="{{ trans('Go to Inner Banners') }}" href="{{ url('powerpanel/inner-banner') }}"
                  class="nav-link nav-toggle">
                  {{ trans('Inner Banner') }}</a>
              </li>
            </ul>
          </div>

          <div class="banner_tabs filter_sec">
            <span class="title">{!! trans('template.common.filterby') !!}:</span>
            <select id="statusfilter" data-sort data-order class="bs-select select2">
              <option value=" ">{!! trans('template.common.selectstatus') !!}</option>
              <option value="Y">{!! trans('template.common.publish') !!}</option>
              <option value="N">{!! trans('template.common.unpublish') !!}</option>
            </select>            
          </div>         

          @if(!empty($cms_pages))
          <div class="banner_tabs filter_sec">
            <select id="pageFilter" data-sort placeholder="Select Module" data-order class="bs-select select2">
              <option value=" ">{!! trans('Select Module') !!}</option>
              <option value="1">Home</option>
              @foreach ($moduleObj as $module)
              <option value="{{ $module->id }}">{{ $module->varTitle }}</option>
              @endforeach
            </select>            
          </div>
          @endif
          <div class="banner_tabs filter_sec">
          <span class="btn btn-icon-only green-new" type="button" id="refresh" title="Reset">
                <i class="fa fa-refresh" aria-hidden="true"></i>
            </span>  
          </div>        

          @permission('banners-create')
          <div class="pull-right">
            <a class="btn btn-green-drake" href="{{ url('powerpanel/inner-banner/add') }}">{!! trans('Add Inner
              Banner')
              !!}</a>
          </div>
          @endpermission

          <div class="col-md-2 pull-right">
            <input type="search" placeholder="Search By Title" class="form-control" id="searchfilter">
          </div>
        </div>
        <div class="portlet-body">
          <div class="table-container">
            <table class="new_table_desing table table-striped table-bordered table-hover table-checkable"
              id="banners_datatable_ajax">
              <thead>
                <tr role="row" class="heading">
                  <th width="5%" align="center"><input type="checkbox" class="group-checkable"></th>
                  <th width="15%" align="text-left">{!! trans('template.common.title') !!}</th>
                  <th width="15%" align="text-center">SUB TITLE</th>
                  <th width="15%" align="text-center">{!! trans('template.common.image') !!}</th>
                  <th width="15%" align="text-center">{!! trans('template.InnerBannerModule.module') !!}</th>
                  <th width="15%" align="text-center">{!! trans('template.InnerBannerModule.page') !!}</th>
                  <th width="12%" align="text-center">{!! trans('template.common.publish') !!}</th>
                  <th width="12%" align="text-right">{!! trans('template.common.actions') !!}</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
            @permission('banners-delete')
            <a href="javascript:;" class="btn-sm btn red btn-outline right_bottom_btn deleteMass">
              {!! trans('template.common.delete') !!}
            </a>
            @endpermission
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('powerpanel.partials.deletePopup')
  <div class="new_modal new_share_popup modal fade bs-modal-md" id="confirm_share" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-vertical">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          </div>
          <div class="modal-body delMsg text-center">
            <form role="form" id='frmshareoption'>
              <div class="form-body">
                <div class="form-group">
                  <input name="varTitle" class="form-control spinner"
                    placeholder="{!! trans('template.InnerBannerModule.processSomething') !!}" type="text">
                </div>
                <div class="form-group">
                  <textarea name="txtDescription" class="form-control"
                    placeholder="{!! trans('template.common.shortdescription') !!}" rows="3"></textarea>
                </div>
                <div class="form-group">
                  <div class="checkbox-list">
                    <label class="checkbox-inline">
                      <input name="socialmedia[]" type="checkbox" value="facebook">
                      <i class="fa fa-facebook"></i>&nbsp; {!! trans('template.InnerBannerModule.facebook') !!}
                    </label>
                    <label class="checkbox-inline">
                      <input name="socialmedia[]" type="checkbox" value="twitter">
                      <i class="fa fa-twitter"></i>&nbsp; {!! trans('template.InnerBannerModule.twitter') !!}
                    </label>
                    <label class="checkbox-inline">
                      <input name="socialmedia[]" type="checkbox" value="linkedin">
                      <i class="fa fa-linkedin"></i>&nbsp; {!! trans('template.InnerBannerModule.linkedin') !!}
                    </label>
                  </div>
                </div>
                <button type="submit" class="btn btn-green-drake">{!! trans('template.common.submit') !!}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
    @php  
  $tableState = true;
    $seg = url()->previous(); 
    $segArr = explode('/', $seg);
    if(!in_array('inner-banner', $segArr)){
     $tableState = false;
    }
  @endphp
  @endsection
  @section('scripts')
  <script type="text/javascript">
  window.site_url = '{!! url("/") !!}';
  var DELETE_URL = '{!! url("/powerpanel/inner-banner/DeleteRecord") !!}';
  var onePushShare = '{!! url("/powerpanel/inner-banner/share") !!}';
  var tableState = '{{ $tableState }}';
  </script>
  <script src="{{ url('resources/global/plugins/jquery-cookie-master/src/jquery.cookie.js') }}" type="text/javascript">
  </script>
  <script src="{{ url('resources/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
    type="text/javascript"></script>
  <script src="{{ url('resources/pages/scripts/table-inner-banner-ajax.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/pages/scripts/custom.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/pages/scripts/banners-index-validations.js') }}" type="text/javascript"></script>
  <script src="{{ url('resources/global/plugins/highslide/highslide-with-html.js') }}" type="text/javascript"></script>
  <script type="text/javascript">
  $(document).on('click', '.share', function(e) {
    e.preventDefault();
    $('.new_share_popup').modal('show');
    $('#confirm_share').modal({
        backdrop: 'static',
        keyboard: false
      })
      .one('click', '#share', function() {
        deleteItem(url, alias);
      });
  });
  </script>
  @endsection
