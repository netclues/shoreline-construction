@section('css')
<link href="{{ url('resources/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet"
  type="text/css" />
@endsection
@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection
@section('content')
@php $settings = json_decode(Config::get("Constant.MODULE.SETTINGS")); @endphp
@include('powerpanel.partials.breadcrumbs')
<div class="col-md-12 settings">
  @if(Session::has('message'))
  <div class="alert alert-success">
    <button class="close" data-close="alert"></button>
    {{ Session::get('message') }}
  </div>
  @endif
  <div class="row">
    <div class="portlet light bordered">
      <div class="portlet-body">
        <div class="tabbable tabbable-tabdrop">
          <div class="tab-content">
            <div class="row">
              <div class="col-md-12">
                <div class="portlet-body form_pattern">
                  {!! Form::open(['method' => 'post','enctype' => 'multipart/form-data','id'=>'frmInnerBanner']) !!}
                  <div class="form-body">
                    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }} form-md-line-input">
                      {!! Form::text('title', isset($banners->varTitle)?$banners->varTitle:old('title'),
                      array('maxlength' => 150,'class' => 'form-control input-sm maxlength-handler', 'data-url' =>
                      'powerpanel/banners','id' => 'title','placeholder' =>
                      trans('template.common.title'),'autocomplete'=>'off')) !!}
                      <label class="form_title" for="title">{!! trans('template.common.title') !!} <span
                          aria-required="true" class="required"> * </span></label>
                      <span style="color:#e73d4a">
                        {{ $errors->first('title') }}
                      </span>
                    </div>
                    <div class="form-group {{ $errors->has('sub_title') ? ' has-error' : '' }} form-md-line-input">
                      {!! Form::text('sub_title', isset($banners->varSubTitle)?$banners->varSubTitle:old('sub_title'),
                      array('maxlength' => 150,'class' => 'form-control input-sm maxlength-handler', 'data-url' =>
                      'powerpanel/banners','id' => 'sub_title','placeholder' =>
                      trans('template.common.sub_title'),'autocomplete'=>'off')) !!}
                      <label class="form_title" for="sub_title">{!! trans('template.common.sub_title') !!}</label>
                      <span style="color:#e73d4a">
                        {{ $errors->first('sub_title') }}
                      </span>
                    </div>
                    <div class="form-group" id="pages">
                      <label class="form_title" for="pages">{!! trans('template.common.selectmodule') !!} <span
                          aria-required="true" class="required"> * </span></label>
                      <select class="form-control bs-select select2" name="modules" id="modules">
                        <option value="">-{!! trans('template.common.selectmodule') !!}-</option>
                        @if(count($modules) > 0)
                        @foreach ($modules as $pagedata)
                        @php
                        $permissionName = $pagedata->varModuleName.'-list';
                        $avoidModules = array('faq','contact-us','testimonial', 'appointment-lead','contact-us', 'gallery', 'privacy-removal-leads', 'restaurant-reservations','client');
                        @endphp
                        @permission($permissionName)
                        @if (ucfirst($pagedata->varTitle)!='Home' && !in_array($pagedata->varModuleName,$avoidModules))
                        <option data-model="{{ $pagedata->varModelName }}" data-module="{{ $pagedata->varModuleName }}"
                          value="{{ $pagedata->id }}"
                          {{ (isset($banners->fkModuleId) && $pagedata->id == $banners->fkModuleId) || $pagedata->id == old('modules')? 'selected' : '' }}>
                          {{ $pagedata->varTitle }}</option>
                        @endif
                        @endpermission
                        @endforeach
                        @endif
                      </select>
                      <span style="color:#e73d4a">
                        {{ $errors->first('modules') }}
                      </span>
                    </div>
                    <div style="display:none;" class="form-group" id="records">
                      <label class="form_title" for="pages">{!! trans('template.InnerBannerModule.selectPage') !!}<span
                          aria-required="true" class="required"> * </span></label>
                      <select class="form-control bs-select select2" name="foritem" id="foritem" style="width:100%">
                        <option value="">--{!! trans('template.InnerBannerModule.selectPage') !!}--</option>
                      </select>
                      <span style="color:#e73d4a">
                        {{ $errors->first('foritem') }}
                      </span>
                    </div>

                    @include('powerpanel.partials.imageControl',['type' => 'multiple','label' => trans('template.bannerModule.selectBanner').' <span aria-required="true" class="required"> * </span>' ,'data'=> isset($banners)?$banners:null , 'id' => 'banner_image', 'name' => 'img_id', 'settings' => $settings, 'width' => '1920', 'height' => '400'])

                    <h3 class="form-section">{!! trans('template.common.displayinformation') !!}</h3>
                    <div class="row">
                      <div class="col-md-6" style="display: none;">
                        @php
                          $inner_display_order_attributes = array('class' => 'form-control','autocomplete'=>'off');
                        @endphp
                        <div
                          class="form-group @if($errors->first('display_order')) has-error @endif form-md-line-input">
                          {!! Form::text('display_order',isset($banners->intDisplayOrder)?$banners->intDisplayOrder:$total_banner, $inner_display_order_attributes) !!}
                          <label class="form_title" for="display_order">{!! trans('Display Order') !!} <span
                              aria-required="true" class="required"> * </span></label>
                          <span class="help-block">
                            <strong>{{ $errors->first('display_order') }}</strong>
                          </span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        @include('powerpanel.partials.displayInfo',['display' => isset($banners->chrPublish)?$banners->chrPublish:''])
                      </div>
                    </div>
                  </div>
                  <div class="form-actions">
                    <div class="row">
                      <div class="col-md-12">
                        <button type="submit" name="saveandedit" class="btn btn-green-drake" value="saveandedit">{!!
                          trans('template.common.saveandedit') !!}</button>
                        <button type="submit" name="saveandexit" class="btn btn-green-drake" value="saveandexit">{!!
                          trans('template.common.saveandexit') !!}</button>
                        <a class="btn red btn-outline" href="{{ url('powerpanel/inner-banner') }}">{!!
                          trans('template.common.cancel')!!}</a>
                      </div>
                    </div>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<script src="{{ url('resources/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript">
</script>
@include('powerpanel.partials.ckeditor')
<script type="text/javascript">
window.site_url = '{!! url("/") !!}';
var selectedRecord = '{{ isset($banners->fkIntPageId)?$banners->fkIntPageId:'' }}';
var user_action = "{{ isset($banners)?'edit':'add' }}";
</script>
<script src="{{ url('resources/pages/scripts/custom.js') }}" type="text/javascript"></script>>
<script src="{{ url('resources/pages/scripts/inner-banner-index-validations.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"
  type="text/javascript"></script>
<script type="text/javascript">
$('.maxlength-handler').maxlength({
  limitReachedClass: "label label-danger",
  alwaysShow: true,
  threshold: 5,
  twoCharLinebreak: false
});

$('#modules').select2({
  placeholder: "Select Module",
  width: '100%'
}).on("change", function(e) {
  $("#modules").closest('.has-error').removeClass('has-error');
  $("#modules-error").remove();
  $('#records').show();
});
$('#foritem').select2({
  placeholder: "Select Module",
  width: '100%'
}).on("change", function(e) {
  $("#foritem").closest('.has-error').removeClass('has-error');
  $("#foritem-error").remove();
});
</script>
@endsection
