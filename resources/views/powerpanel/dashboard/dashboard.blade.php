@extends('powerpanel.layouts.app')

@section('title')

{{Config::get('Constant.SITE_NAME')}} - PowerPanel

@endsection

@section('content')

<!-- BEGIN DASHBOARD STATS 1-->

<div class="row">

	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

		<div class="dashboard-stat creative_widgets new_dashboard bg-gradient-x-primary">

			<div class="visual">

				<i class="fa fa-globe"></i>

			</div>

			<div class="details">

				<div class="desc" title="Web Hits">

					<i class="widget-thumb-icon icon-screen-desktop"></i> Web Hits

					@if(isset($hits['web']) && !empty($hits['web']))

					<a class="right_btn" title="Click here to view more" href="{{url('powerpanel/pages')}}">

					<i class="glyphicon glyphicon-chevron-right"></i></a>

					@endif

				</div>

			</div>

			<div class="clearfix"></div>

			<div class="contant_detail">

				<div class="list_hits">

					<ul>

						<li><div class="top_count"><span>{!! (!empty($hits['web'])?$hits['web']:0) !!}</span>Total</div></li>

						<li><div class="top_count"><span>{!! (!empty($currentMonth['web'])?$currentMonth['web']:0) !!}</span>{{date('M Y')}}</div></li>

						<li><div class="top_count"><span>{!! (!empty($currentYear['web'])?$currentYear['web']:0) !!}</span>Year {{date('Y')}}</div></li>

					</ul>

				</div>

			</div>

		</div>

	</div>

	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

		<div class="creative_widgets dashboard-stat new_dashboard bg-gradient-x-danger">

			<div class="visual">

				<i class="fa fa-globe"></i>

			</div>

			<div class="details">

				<div class="desc" title="Mobile Hits">

					<i class="widget-thumb-icon icon-screen-smartphone"></i>Mobile Hits

					@if(isset($hits['mobile']) && !empty($hits['mobile']))

					<a class="right_btn" title="Click here to view more" href="{{url('powerpanel/pages')}}">

					<i class="glyphicon glyphicon-chevron-right"></i></a>

					@endif

				</div>

			</div>

			<div class="clearfix"></div>

			<div class="contant_detail">

				<div class="list_hits">

					<ul>

						<li><div class="top_count"><span>{!! (!empty($hits['mobile'])?$hits['mobile']:0) !!}</span>Total</div></li>

						<li><div class="top_count"><span>{!! (!empty($currentMonth['mobile'])?$currentMonth['mobile']:0) !!}</span>{{date('M Y')}}</div></li>

						<li><div class="top_count"><span>{!! (!empty($currentYear['mobile'])?$currentYear['mobile']:0) !!}</span>Year {{date('Y')}}</div></li>

					</ul>

				</div>

			</div>

		</div>

	</div>

	@permission('contact-us-list')

	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

		<div class="creative_widgets dashboard-stat new_dashboard bg-gradient-x-warning">

			<div class="visual">

				<i class="fa fa-globe"></i>

			</div>

			<div class="details">

				<div class="desc" title="Contact Leads">

					<i class="widget-thumb-icon icon-users"></i> Contact Leads

					@if(isset($leads) && $leads->count() > 0 )

					<a class="right_btn" title="Click here to view more" href="{{url('powerpanel/contact-us')}}">

					<i class="glyphicon glyphicon-chevron-right"></i></a>

					@endif

				</div>

			</div>

			<div class="clearfix"></div>

			<div class="contant_detail">

				<div class="list_hits">

					<ul>

						<li><div class="top_count"><span>{!! $contactLeadCount !!}</span>Total</div></li>

						<li><div class="top_count"><span>{{$currentMonthContactCount}}</span>{{date('M Y')}}</div></li>

						<li><div class="top_count"><span>{{$currentYearContactCount}}</span>Year {{date('Y')}}</div></li>

					</ul>

				</div>

			</div>

		</div>

	</div>

	@endpermission

	@permission('request-a-quote-list')

	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

		<div class="creative_widgets dashboard-stat new_dashboard bg-gradient-x-warning">

			<div class="visual">

				<i class="fa fa-globe"></i>

			</div>

			<div class="details">

				<div class="desc" title="Request a Quote Leads">

					<i class="widget-thumb-icon icon-users"></i> Request a Quote Leads

					@if(isset($requestleads) && $requestleads->count() > 0 )

					<a class="right_btn" title="Click here to view more" href="{{url('powerpanel/request-a-quote')}}">

					<i class="glyphicon glyphicon-chevron-right"></i></a>

					@endif

				</div>

			</div>

			<div class="clearfix"></div>

			<div class="contant_detail">

				<div class="list_hits">

					<ul>

						<li><div class="top_count"><span>{!! $requestLeadCount !!}</span>Total</div></li>

						<li><div class="top_count"><span>{{$currentMonthRequestCount}}</span>{{date('M Y')}}</div></li>

						<li><div class="top_count"><span>{{$currentYearRequestCount}}</span>Year {{date('Y')}}</div></li>

					</ul>

				</div>

			</div>

		</div>

	</div>

	@endpermission

	@permission('newsletter-lead-list')

	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

		<div class="creative_widgets dashboard-stat new_dashboard bg-gradient-x-success">

			<div class="visual">

				<i class="fa fa-globe"></i>

			</div>

			<div class="details">

				<div class="desc" title="Newsletter Leads">

					<i class="widget-thumb-icon icon-envelope "></i> Newsletter Leads

					@if(isset($newsleads) && !empty($newsleads))

					<a class="right_btn" title="Click here to view more" href="{{url('powerpanel/newsletter-lead')}}">

					<i class="glyphicon glyphicon-chevron-right"></i></a>

					@endif

				</div>

			</div>

			<div class="clearfix"></div>

			<div class="contant_detail">

				<div class="list_hits">

					<ul>

						<li><div class="top_count"><span>{!! $subscriberLeadCount !!}</span>Total</div></li>

						<li><div class="top_count"><span>{{$currentMonthNewsLetterCount}}</span>{{date('M Y')}}</div></li>

						<li><div class="top_count"><span>{{$currentYearNewsLetterCount}}</span>Year {{date('Y')}}</div></li>

					</ul>

				</div>

			</div>

		</div>

	</div>

	@endpermission

</div>

<div class="clearfix"></div>

<!-- END DASHBOARD STATS 1-->

<div class="row">

	@permission('appointment-lead-list')

	<div class="col-md-6 col-sm-6">

		<!-- BEGIN PORTLET-->

		<div class="portlet light">

			<div class="portlet-title dash-title">

				<div class="caption">

					<i class="icon-share font-green_drark hide"></i>

					<span class="caption-subject font-green_drark bold uppercase"

					title="Book An Appointment">Book An Appointments</span>

				</div>

			</div>

			<div class="portlet-body dash-table">

				<div class="table-scrollable">

					<table class="new_table_desing table table-condensed table-hover">

						<thead>

							<tr>

								<!-- <th width="30%" align="left" title="Name"> Name </th> -->

								<th width="30%" align="left" title="{{ trans('template.common.name') }}"> Name</th>

								<th width="30%" align="center" title="{{ trans('template.common.emailid') }}">{{ trans('template.common.emailid') }}</th>

								<th width="10%" align="center" title="{{ trans('template.common.details') }}"> {{ trans('template.common.details') }} </th>

								<th width="40%" align="right" title="{{ trans('template.powerPanelDashboard.receivedDateTime') }}"> {{ trans('template.powerPanelDashboard.receivedDateTime') }}</th>

							</tr>

						</thead>

						<tbody>

						 @if($bookAnAppointmentleads->count() == 0)

						 <tr>

							<td align="center" colspan="5">{{ trans('template.powerPanelDashboard.noBookingLead') }} <a target="_blank" href="https://www.netclues.com/social-media-marketing"> {{ trans('template.powerPanelDashboard.here') }} </a>

							{{ trans('template.powerPanelDashboard.findbookigLead') }}

						</td>

					</tr>						

						@else

						@foreach ($bookAnAppointmentleads as $key => $bookAnAppointmentlead)

						@if($key<=4)

						<tr>

							<td align="left">{{ $bookAnAppointmentlead->varName }}</td>

							<td align="left">{{ App\Helpers\MyLibrary::getDecryptedString($bookAnAppointmentlead->varEmail) }} </td>

							<td align="left" class='numeric text-center'>

							<a data-toggle='modal' class="bookanappointmentLead" id="{!! $bookAnAppointmentlead->id !!}" href='#DetailbookanappointmentLead{!! $bookAnAppointmentlead->id !!}' title="{{ trans('template.powerPanelDashboard.clickDetails') }}">

								<span class='icon-magnifier-add' aria-hidden='true'></span>

							</a>

						</td>

							<td align="right">{{ date(''.Config::get('Constant.DEFAULT_DATE_FORMAT').' '.Config::get('Constant.DEFAULT_TIME_FORMAT').'',strtotime($bookAnAppointmentlead->created_at)) }}</td>

						</tr>

						@endif

						@endforeach

						@endif

					</tbody>

				</table>

			</div>

		</div>

		@if(isset($bookAnAppointmentleads) && !empty($bookAnAppointmentleads) && count($bookAnAppointmentleads) > 0)

		<div class="pull-right">

			<a class="btn btn-green-drake" href="{{ url('powerpanel/appointment-lead') }}" title="{{ trans('template.powerPanelDashboard.seeAllRecords') }}">{{ trans('template.powerPanelDashboard.seeAllRecords') }}</a>

		</div>

		@endif

	</div>

	<!-- END PORTLET-->

</div>

@endpermission

@permission('newsletter-lead-list')

<div class="col-md-6 col-sm-6">

	<!-- BEGIN PORTLET-->

	<div class="portlet light">

		<div class="portlet-title dash-title">

			<div class="caption">

				<i class="icon-share font-green_drark hide"></i>

				<span class="caption-subject font-green_drark bold uppercase"

				title="{{ trans('template.sidebar.newsletterleads') }}">{{ trans('template.sidebar.newsletterleads') }}</span>

			</div>

		</div>

		<div class="portlet-body dash-table">

			<div class="table-scrollable">

				<table class="new_table_desing table table-condensed table-hover">

					<thead>

						<tr>

							<!-- <th width="30%" align="left" title="Name"> Name </th> -->

							<th width="40%" align="left" title="{{ trans('template.powerPanelDashboard.user') }}"> {{ trans('template.powerPanelDashboard.user') }} </th>

							<th width="20%" align="center" title="{{ trans('template.powerPanelDashboard.subscribed') }}"> {{ trans('template.powerPanelDashboard.subscribed') }} </th>

							<th width="40%" align="right" title="{{ trans('template.powerPanelDashboard.receivedDateTime') }}"> {{ trans('template.powerPanelDashboard.receivedDateTime') }}</th>

						</tr>

					</thead>

					<tbody>

					

					  @if($newsleads->count() == 0)

						<tr>

							<td align="center" colspan="5">{{ trans('template.powerPanelDashboard.noNewsLetter') }} <a target="_blank" href="https://www.netclues.com/social-media-marketing"> {{ trans('template.powerPanelDashboard.here') }} </a>

							{{ trans('template.powerPanelDashboard.newsLetterLead') }}

						</td>

					</tr>

					@else

					@foreach ($newsleads as $key => $newsleads)

					@if($key<=4)

					<tr>

						<!-- <td align="left">{{ $newsleads->name }}</td> -->

						<td align="left">{{ App\Helpers\MyLibrary::getDecryptedString($newsleads->varEmail) }} </td>

						<td align="center">{{ $newsleads->chrSubscribed }}</td>

						<td align="right">{{ date(''.Config::get('Constant.DEFAULT_DATE_FORMAT').' '.Config::get('Constant.DEFAULT_TIME_FORMAT').'',strtotime($newsleads->created_at)) }}</td>

					</tr>

					@endif

					@endforeach

					@endif

				</tbody>

			</table>

		</div>

	</div>

	@if(isset($newsleads)  && $newsleads->count() > 0)

	<div class="pull-right">

		<a class="btn btn-green-drake" href="{{ url('powerpanel/newsletter-lead') }}" title="{{ trans('template.powerPanelDashboard.seeAllRecords') }}">{{ trans('template.powerPanelDashboard.seeAllRecords') }}</a>

	</div>

	@endif

</div>

<!-- END PORTLET-->

</div>

<div class="clearfix"></div>

@endpermission

@permission('contact-us-list')

<div class="col-md-6 col-sm-6">

 <div class="portlet light">

	<div class="portlet-title dash-title">

		<div class="caption">

			<i class="icon-share font-green_drark hide"></i>

			<span class="caption-subject font-green_drark bold uppercase" title="Contact Leads">

			{{ trans('template.sidebar.contactuslead') }}</span>

		</div>

	</div>

	<div class="portlet-body dash-table">

		<div class="table-scrollable">

			<table class="table table-condensed table-hover">

				<thead>

					<tr>

						<th width="30%" align="left" title="{{ trans('template.common.name') }}"> {{ trans('template.common.name') }} </th>

						<th width="30%" align="left" title="{{ trans('template.common.emailid') }}"> {{ trans('template.common.emailid') }} </th>

						<th width="10%" align="center" title="{{ trans('template.common.details') }}"> {{ trans('template.common.details') }} </th>

						<th width="40%" align="right" title="{{ trans('template.powerPanelDashboard.receivedDateTime') }}"> {{ trans('template.powerPanelDashboard.receivedDateTime') }}</th>

					</tr>

				</thead>

				<tbody>

				

					@if($leads->count() == 0)

					<tr>

						<td align="center" colspan="4">{{ trans('template.powerPanelDashboard.noContactLead') }} <a target="_blank" href="https://www.netclues.com/social-media-marketing"> {{ trans('template.powerPanelDashboard.here') }}</a> {{ trans('template.powerPanelDashboard.findContactLead') }} </td>

					</tr>

					@else

					@foreach ($leads as $key=>$lead)

					@if($key<=4)

					<tr>

						<td>{!! $lead->varName !!}</td>

						<td align="left">{!! App\Helpers\MyLibrary::getDecryptedString($lead->varEmail); !!}</td>

						<td align="left" class='numeric text-center'>

							<a data-toggle='modal' class="contactUsLead" id="{!! $lead->id !!}" href='#DetailsLeads{!! $lead->id !!}' title="{{ trans('template.powerPanelDashboard.clickDetails') }}">

								<span class='icon-magnifier-add' aria-hidden='true'></span>

							</a>

						</td>

						<td align="right">{{ date(''.Config::get('Constant.DEFAULT_DATE_FORMAT').'  '.Config::get('Constant.DEFAULT_TIME_FORMAT').'', strtotime($lead->created_at)) }}</td>

					</tr>

					@endif

					@endforeach

					@endif

				</tbody>

			</table>

		</div>

	</div>

	@if(isset($leads) && $leads->count() > 0 )

	<div class="pull-right">

		<a class="btn btn-green-drake" href="{{ url('powerpanel/contact-us') }}" title="{{ trans('template.powerPanelDashboard.seeAllRecords') }}">{{ trans('template.powerPanelDashboard.seeAllRecords') }}</a>

	</div>

	@endif

 </div>

</div>

@endpermission

<!-- request a quote 04032020 -->
@permission('request-a-quote-list')

<div class="col-md-6 col-sm-6">

 <div class="portlet light">

	<div class="portlet-title dash-title">

		<div class="caption">

			<i class="icon-share font-green_drark hide"></i>

			<span class="caption-subject font-green_drark bold uppercase" title="Request a quote leads">

			{{ trans('template.sidebar.requestlead') }}</span>

		</div>

	</div>

	<div class="portlet-body dash-table">

		<div class="table-scrollable">

			<table class="table table-condensed table-hover">

				<thead>

					<tr>

						<th width="30%" align="left" title="{{ trans('template.common.name') }}"> {{ trans('template.common.name') }} </th>

						<th width="30%" align="left" title="{{ trans('template.common.emailid') }}"> {{ trans('template.common.emailid') }} </th>

						<th width="10%" align="center" title="{{ trans('template.common.details') }}"> {{ trans('template.common.details') }} </th>

						<th width="40%" align="right" title="{{ trans('template.powerPanelDashboard.receivedDateTime') }}"> {{ trans('template.powerPanelDashboard.receivedDateTime') }}</th>

					</tr>

				</thead>

				<tbody>

				

					@if($requestleads->count() == 0)

					<tr>

						<td align="center" colspan="4">{{ trans('template.powerPanelDashboard.noContactLead') }} <a target="_blank" href="https://www.netclues.com/social-media-marketing"> {{ trans('template.powerPanelDashboard.here') }}</a> {{ trans('template.powerPanelDashboard.findContactLead') }} </td>

					</tr>

					@else

					@foreach ($requestleads as $key=>$lead)

					@if($key<=4)

					<tr>

						<td>{!! $lead->varName !!}</td>

						<td align="left">{!! App\Helpers\MyLibrary::getDecryptedString($lead->varEmail); !!}</td>

						<td align="left" class='numeric text-center'>

							<a data-toggle='modal' class="requestLead" id="{!! $lead->id !!}" href='#DetailsLeads{!! $lead->id !!}' title="{{ trans('template.powerPanelDashboard.clickDetails') }}">

								<span class='icon-magnifier-add' aria-hidden='true'></span>

							</a>

						</td>

						<td align="right">{{ date(''.Config::get('Constant.DEFAULT_DATE_FORMAT').'  '.Config::get('Constant.DEFAULT_TIME_FORMAT').'', strtotime($lead->created_at)) }}</td>

					</tr>

					@endif

					@endforeach

					@endif

				</tbody>

			</table>

		</div>

	</div>

	@if(isset($requestleads) && $requestleads->count() > 0 )

	<div class="pull-right">

		<a class="btn btn-green-drake" href="{{ url('powerpanel/request-a-quote') }}" title="{{ trans('template.powerPanelDashboard.seeAllRecords') }}">{{ trans('template.powerPanelDashboard.seeAllRecords') }}</a>

	</div>

	@endif

 </div>

</div>

@endpermission
<!-- request a quote end -->

<!-- start reservation code -->

@permission('restaurant-reservations-list')

<div class="col-md-6 col-sm-6">

 <div class="portlet light">

	<div class="portlet-title dash-title">

		<div class="caption">

			<i class="icon-share font-green_drark hide"></i>

			<span class="caption-subject font-green_drark bold uppercase" title="Reservation Leads">

			Reservation Leads</span>

		</div>

	</div>

	<div class="portlet-body dash-table">

		<div class="table-scrollable">

			<table class="table table-condensed table-hover">

				<thead>

					<tr>

						<th width="30%" align="left" title="{{ trans('template.common.name') }}"> {{ trans('template.common.name') }} </th>

						<th width="30%" align="left" title="{{ trans('template.common.emailid') }}"> {{ trans('template.common.emailid') }} </th>

						<th width="10%" align="center" title="{{ trans('template.common.details') }}"> {{ trans('template.common.details') }} </th>

						<th width="40%" align="right" title="{{ trans('template.powerPanelDashboard.receivedDateTime') }}"> {{ trans('template.powerPanelDashboard.receivedDateTime') }}</th>

					</tr>

				</thead>

				<tbody>

				

					@if($reservationleads->count() == 0)

					<tr>

						<td align="center" colspan="4">{{ trans('template.powerPanelDashboard.noReservationLead') }} <a target="_blank" href="https://www.netclues.com/social-media-marketing"> {{ trans('template.powerPanelDashboard.here') }}</a> {{ trans('template.powerPanelDashboard.findreservationLead') }} </td>

					</tr>

					@else

					@foreach ($reservationleads as $key=>$reservation)

					@if($key<=4)

					<tr>

						<td>{!! $reservation->varName !!}</td>

						<td align="left">{!! App\Helpers\MyLibrary::getDecryptedString($reservation->varEmail); !!}</td>

						<td align="left" class='numeric text-center'>

							<a data-toggle='modal' class="reservationLead" id="{!! $reservation->id !!}" href='#DetailsReservationLeads{!! $reservation->id !!}' title="{{ trans('template.powerPanelDashboard.clickDetails') }}">

								<span class='icon-magnifier-add' aria-hidden='true'></span>

							</a>

						</td>

						<td align="right">{{ date(''.Config::get('Constant.DEFAULT_DATE_FORMAT').'  '.Config::get('Constant.DEFAULT_TIME_FORMAT').'', strtotime($reservation->created_at)) }}</td>

					</tr>

					@endif

					@endforeach

					@endif

				</tbody>

			</table>

		</div>

	</div>

	@if(isset($reservationleads) && $reservationleads->count() > 0 )
	<div class="pull-right">
		<a class="btn btn-green-drake" href="{{ url('powerpanel/restaurant-reservations') }}" title="{{ trans('template.powerPanelDashboard.seeAllRecords') }}">{{ trans('template.powerPanelDashboard.seeAllRecords') }}</a>

	</div>

	@endif

 </div>

</div>

@endpermission

<!-- end reservation code -->

</div>

<!-- END CONTENT BODY -->

<div class="new_modal modal fade detailsCmsPage" tabindex="-1" aria-hidden="true">

</div>

<div class="new_modal modal fade detailsContactUsLead" tabindex="-1" aria-hidden="true">

</div>

<div class="new_modal modal fade detailsRequestLead" tabindex="-1" aria-hidden="true">

</div>

<div class="new_modal modal fade detailsReservationLeads" tabindex="-1" aria-hidden="true">

</div>

<div class="new_modal modal fade BlogDetails" tabindex="-1" aria-hidden="true">

</div>

<div class="new_modal modal fade DetailbookanappointmentLead" tabindex="-1" aria-hidden="true"></div>







@endsection

@section('scripts')

<script type="text/javascript">window.site_url =  '{!! url("/") !!}'; </script>

<script src="{{ url('resources/pages/scripts/dashboard-ajax.js') }}" type="text/javascript"></script>

<script type="text/javascript">

	@if(Session::has('alert-success'))

		toastr.options = {

			"closeButton": true,

			"debug": false,

			"positionClass": "toast-top-right",

			"onclick": null,

			"showDuration": "1000",

			"hideDuration": "1000",

			"timeOut": "5000",

			"extendedTimeOut": "1000",

			"showEasing": "swing",

			"hideEasing": "linear",

			"showMethod": "fadeIn",

			"hideMethod": "fadeOut"

		}

		toastr.success("{{Session::get('alert-success')}} Welcome to {{Config::get('Constant.SITE_NAME')}}.");

	@endif

	@if(Session::has('alert-success'))

		$("#topMsg").show().delay(5000).fadeOut();

		$("#topMsg").fadeOut("slow", function() {

			$('.page-header').css('top','0');

			$('.page-container').css('top','0');

		});

	@endif

	$(document).on('click','#close_icn', function(e){

		$("#topMsg").hide();

		$('.page-header').css('top','0');

		$('.page-container').css('top','0');

	});

</script>

@endsection