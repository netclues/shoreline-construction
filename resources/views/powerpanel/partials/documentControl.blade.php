@if($type == "multiple")
<div class="row">
  <div class="col-md-12">
    <div class="image_thumb">
      <div class="form-group">
        <label class="form_title">{{ $label }}</label>
        <div class="clearfix"></div>
        <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width:100%;height:120px;position: relative;">
            <img class="img_opacity" src="{{ url('resources\images\upload_file.gif') }}" />
          </div>
          <div class="input-group">
            <a class="document_manager document-multiple-selection" data-multiple="true" onclick="MediaManager.openDocumentManager('{{ $id }}');"><span class="fileinput-new"></span></a>
            <input class="form-control" type="hidden" id="{{ $id }}" name="{{ $name }}" value="{{ isset($data->fkIntDocId)?$data->fkIntDocId:old($name) }}" />
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      @if(!empty($documentData) && count($documentData) > 0)
      <div id="{{ $id }}_documents">
        <div class="multi_image_list" id="multi_document_list">
          <ul>
            @foreach($documentData as $key => $value)
            <li id="doc_{{ $value->id }}">
              <span>
                @if($value->varDocumentExtension == "pdf")
                <img title="{{ $value->txtDocumentName }}" alt="{{ $value->txtDocumentName }}"
                  src="{{ url('assets/images/documents_logo/pdf.png') }}">
                @elseif($value->varDocumentExtension == "xls")
                <img title="{{ $value->txtDocumentName }}" alt="{{ $value->txtDocumentName }}"
                  src="{{ url('assets/images/documents_logo/xls.png') }}">
                @elseif($value->varDocumentExtension == "docx" || $value->varDocumentExtension == "doc")
                <img title="{{ $value->txtDocumentName }}" alt="{{ $value->txtDocumentName }}"
                  src="{{ url('assets/images/documents_logo/doc.png') }}">
                @elseif($value->varDocumentExtension == "ppt")
                <img title="{{ $value->txtDocumentName }}" alt="{{ $value->txtDocumentName }}"
                  src="{{ url('assets/images/documents_logo/ppt.png') }}">
                @elseif($value->varDocumentExtension == "txt")
                <img title="{{ $value->txtDocumentName }}" alt="{{ $value->txtDocumentName }}"
                  src="{{ url('assets/images/documents_logo/txt.png') }}">
                @else
                <img title="{{ $value->txtDocumentName }}" alt="{{ $value->txtDocumentName }}"
                  src="{{ url('assets/images/documents_logo/document_icon.png') }}">
                @endif
                <a href="javascript:;" onclick="MediaManager.removeDocumentFromGallery('{{ $value->id }}');"
                  class="delect_image" data-dismiss="fileinput"><i class="fa fa-times"></i></a>
              </span>
            </li>
            @endforeach
          </ul>
        </div>
      </div>
      @else
      <div id="{{ $id }}_documents"></div>
      @endif
      <span>(Recommended documents *.txt, *.pdf, *.doc, *.docx, *.ppt, *.xls formats are supported. Document should be
        maximum size of 15 MB.)</span>
    </div>
  </div>
</div>
@elseif($type == "single")
<div class="row">
  <div class="col-md-12">
    <div class="image_thumb document_thumb">
      <div class="form-group">
        <label class="form_title">{{ $label }}</label>
        <div class="clearfix"></div>
        <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-preview thumbnail document-fileinput {{ $id }}_documents" data-trigger="fileinput" style="width:100%;height:120px;position: relative;">
          @if(isset($data->document))
              @if($data->document->varDocumentExtension == "pdf")
              <img title="{{ $data->document->txtDocumentName }}" alt="{{ $data->document->txtDocumentName }}" src="{{ url('assets/images/documents_logo/pdf.png') }}">
              @elseif($data->document->varDocumentExtension == "xls")
              <img title="{{ $data->document->txtDocumentName }}" alt="{{ $data->document->txtDocumentName }}" src="{{ url('assets/images/documents_logo/xls.png') }}">
              @elseif($data->document->varDocumentExtension == "docx" || $data->document->varDocumentExtension == "doc")
              <img title="{{ $data->document->txtDocumentName }}" alt="{{ $data->document->txtDocumentName }}" src="{{ url('assets/images/documents_logo/doc.png') }}">
              @elseif($data->document->varDocumentExtension == "ppt")
              <img title="{{ $data->document->txtDocumentName }}" alt="{{ $data->document->txtDocumentName }}" src="{{ url('assets/images/documents_logo/ppt.png') }}">
              @elseif($data->document->varDocumentExtension == "txt")
              <img title="{{ $data->document->txtDocumentName }}" alt="{{ $data->document->txtDocumentName }}" src="{{ url('assets/images/documents_logo/txt.png') }}">
              @else
              <img title="{{ $data->document->txtDocumentName }}" alt="{{ $data->document->txtDocumentName }}" src="{{ url('assets/images/documents_logo/document_icon.png') }}">
              @endif
          @else
            <img class="img_opacity" src="{{ url('resources\images\upload_file.gif') }}" />
          @endif
          </div>
          <div class="input-group">
          <a class="document_manager document-multiple-selection" data-multiple="false" onclick="MediaManager.openDocumentManager('{{ $id }}');"><span class="fileinput-new"></span></a>
          <input class="form-control" type="hidden" id="{{ $id }}" name="{{ $name }}" value="{{ isset($data->document->fkIntDocId)?$data->document->fkIntDocId:old($name) }}" />

          </div>
          <div class="overflow_layer" style="display:none">
            <a onclick="MediaManager.openDocumentManager('{{ $id }}');" class="document_manager remove_img"><i class="fa fa-pencil"></i></a>
            <a href="javascript:;" class="remove_img removeDocument" data-dismiss="fileinput"> <i class="fa fa-trash-o"></i> </a>
          </div>
          @if(!empty($data->document->txtDocumentName))
            <input disabled="disabled" id="document_name" class="form-control" type="text" value="{{ $data->document->txtDocumentName }}.{{ $data->document->varDocumentExtension }}" />
          @else
            <input disabled="disabled" id="document_name" class="form-control" type="text" value="" style="display:none" />
          @endif
        </div>
      </div>
      <div class="clearfix"></div>
      <span>(Recommended documents *.txt, *.pdf, *.doc, *.docx, *.ppt, *.xls formats are supported. Document should be maximum size of 15 MB.)</span>
    </div>
  </div>
</div>
@endif
