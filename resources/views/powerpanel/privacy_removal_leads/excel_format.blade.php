<!doctype html>
<html>
  <head>
    <title>{{ Config::get('Constant.SITE_NAME') }} Privacy Removal Leads</title>
  </head>
  <body>
      @if(isset($PrivacyLead) && !empty($PrivacyLead))
          <div class="row">
           <div class="col-12">
              <table class="search-result allData" id="" border="1">
                 <thead>
                  <tr>
                        <th style="font-weight: bold;text-align:center" colspan="6">{{ Config::get('Constant.SITE_NAME') }} {{ trans("template.privacyRemovalLeadModule.privacyRemovalLeads") }}</th>
                   </tr>
                    <tr>
                       <th style="font-weight: bold;">{{ trans('template.common.name') }}</th>
                       <th style="font-weight: bold;">{{ trans('template.common.email') }}</th>
                       <th style="font-weight: bold;">{{ trans('template.privacyRemovalLeadModule.reason') }}</th>
                       <th style="font-weight: bold;">Record Locations</th>
                       <th style="font-weight: bold;">Confirmation</th>
                       <th style="font-weight: bold;">{{ trans('template.privacyRemovalLeadModule.receivedDateTime') }}</th>
                    </tr>
                 </thead>
                 <tbody>
                  @foreach($PrivacyLead as $row)
                    <tr>
                       <td>{{ !empty($row->varName)?$row->varName:'-' }}</td>
                       <td>{{ \App\Helpers\MyLibrary::getDecryptedString($row->varEmail) }}</td>
                       <td>{{ (!empty($row->txtReason)?\App\Helpers\MyLibrary::getDecryptedString($row->txtReason):'-') }}</td>
                       <td>{{ $linkedModule }}</td>
                       <td>{{ ($row->chrIsEmailVerified == 'Y' ? 'Yes' : 'No') }}</td>
                       <td>{{ date(''.Config::get('Constant.DEFAULT_DATE_FORMAT').' '.Config::get('Constant.DEFAULT_TIME_FORMAT').'',strtotime($row->created_at)) }}</td>
                    </tr>
                  @endforeach
                 </tbody>
              </table>
           </div>
        </div>
      @endif
  </html>
