<!doctype html>
<html>
  <head>
    <title>{{ Config::get('Constant.SITE_NAME') }} Request A Quote Leads</title>
  </head>
  <body>
      @if(isset($RequestLead) && !empty($RequestLead))
          <div class="row">
           <div class="col-12">
              <table class="search-result allData" id="" border="1">
                 <thead>
                  <tr>
                        <th style="font-weight: bold;text-align:center" colspan="6">{{ Config::get('Constant.SITE_NAME') }} {{ trans("template.requestleadModule.requestQuoteLeads") }}</th>
                   </tr>
                    <tr>
                       <th style="font-weight: bold;">{{ trans('template.common.name') }}</th>
                       <th style="font-weight: bold;">{{ trans('template.common.email') }}</th>
                       <th style="font-weight: bold;">{{ trans('template.requestleadModule.phone') }}</th>
                       <th style="font-weight: bold;">Area Of Interest</th>
                       <th style="font-weight: bold;">{{ trans('template.requestleadModule.message') }}</th>
                       <th style="font-weight: bold;">{{ trans('template.requestleadModule.receivedDateTime') }}</th>
                    </tr>
                 </thead>
                 <tbody>
                  @foreach($RequestLead as $row)
					@php
						//$aof_id = $row->varAreaOfInterest;
						//$aof_json_category = \App\ServiceCategory::getRequestQuoteList($aof_id);						
					@endphp
                    <tr>
                       <td>{{ $row->varName }}</td>
                       <td>{{ \App\Helpers\MyLibrary::getDecryptedString($row->varEmail) }}</td>
                       <td>{{ (!empty($row->varPhoneNo)?\App\Helpers\MyLibrary::getDecryptedString($row->varPhoneNo):'-') }}</td>
                       <td>{{ (!empty($row->varAreaOfInterest)?\App\Helpers\MyLibrary::getDecryptedString($row->varAreaOfInterest):'-') }}</td>					   
					        <?php /*<td>{{ $aof_json_category->varTitle }}</td> */ ?>
					   <td>{{ (!empty($row->txtUserMessage)?\App\Helpers\MyLibrary::getDecryptedString(strip_tags($row->txtUserMessage)):'-') }}</td>
                       <td>{{ date(''.Config::get('Constant.DEFAULT_DATE_FORMAT').' '.Config::get('Constant.DEFAULT_TIME_FORMAT').'',strtotime($row->created_at)) }}</td>
                    </tr>
                  @endforeach
                 </tbody>
              </table>
           </div>
        </div>
      @endif
  </html>
