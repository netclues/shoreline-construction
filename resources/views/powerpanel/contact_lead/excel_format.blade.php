<!doctype html>
<html>
  <head>
    <title>{{ Config::get('Constant.SITE_NAME') }} Contact Leads</title>
  </head>
  <body>
      @if(isset($ContactLead) && !empty($ContactLead))
          <div class="row">
           <div class="col-12">
              <table class="search-result allData" id="" border="1">
                 <thead>
                  <tr>
                        <th style="font-weight: bold;text-align:center" colspan="6">{{ Config::get('Constant.SITE_NAME') }} {{ trans("template.contactleadModule.contactUsLeads") }}</th>
                   </tr>
                    <tr>
                       <th style="font-weight: bold;">{{ trans('template.common.name') }}</th>
                       <th style="font-weight: bold;">{{ trans('template.common.email') }}</th>
                       <th style="font-weight: bold;">{{ trans('template.contactleadModule.phone') }}</th>
                       <!-- <th style="font-weight: bold;">Area Of Interest</th> -->
                       <th style="font-weight: bold;">{{ trans('template.contactleadModule.message') }}</th>
                       <th style="font-weight: bold;">{{ trans('template.contactleadModule.receivedDateTime') }}</th>
                    </tr>
                 </thead>
                 <tbody>
                  @foreach($ContactLead as $row)
                    <tr>
                       <td>{{ $row->varName }}</td>
                       <td>{{ \App\Helpers\MyLibrary::getDecryptedString($row->varEmail) }}</td>
                       <td>{{ (!empty($row->varPhoneNo)?\App\Helpers\MyLibrary::getDecryptedString($row->varPhoneNo):'-') }}</td>
                       <!-- <td>{{ (!empty($row->txtUserMessage)?\App\Helpers\MyLibrary::getDecryptedString(strip_tags($row->varAreaOfInterest)):'-') }}</td> -->
                       <td>{{ (!empty($row->txtUserMessage)?\App\Helpers\MyLibrary::getDecryptedString(strip_tags($row->txtUserMessage)):'-') }}</td>
                       <td>{{ date(''.Config::get('Constant.DEFAULT_DATE_FORMAT').' '.Config::get('Constant.DEFAULT_TIME_FORMAT').'',strtotime($row->created_at)) }}</td>
                    </tr>
                  @endforeach
                 </tbody>
              </table>
           </div>
        </div>
      @endif
  </html>
