@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection
@section('css')
<link href="{{ url('resources/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/highslide/highslide.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
@include('powerpanel.partials.breadcrumbs')
<div class="row">
  <div class="col-md-12">
    @if(Session::has('message'))
    <div class="alert alert-success">
      <button class="close" data-close="alert"></button>
      {{ Session::get('message') }}
    </div>
    @endif
    <div class="portlet light portlet-fit portlet-datatable">
      @if($total > 0)
      <div class="portlet-title select_box">

        <span class="title">{{ trans('template.common.filterby') }}:</span>
        <select id="statusfilter" data-sort data-order class="bs-select select2">
          <option value=" ">{{ trans('template.common.selectstatus') }}</option>
          <option value="Y">{{ trans('template.common.publish') }}</option>
          <option value="N">{{ trans('template.common.unpublish') }}</option>
        </select>
        <span class="btn btn-icon-only green-new" type="button" id="refresh" title="Reset">
          <i class="fa fa-refresh" aria-hidden="true"></i>
        </span>

        @permission('contact-info-create')
        <div class="pull-right">
          <a class="btn btn-green-drake" href="{{ url('powerpanel/contact-info/add') }}">
            {{ trans('template.contactModule.addnewcontact') }}</a>
        </div>
        @endpermission

        <div class="col-md-2 pull-right">
          <input type="search" placeholder="Search By Name" class="form-control" id="searchfilter">
        </div>

      </div>
      <div class="portlet-body">
        <div class="table-container">
          <table class="new_table_desing table table-striped table-bordered table-hover table-checkable"
            id="contacts_datatable_ajax">
            <thead>
              <tr role="row" class="heading">
                @if($client_role)
                 <th width="2%" align="center"> </th>                 
                @else
                 <th width="2%" align="center"><input type="checkbox" class="group-checkable"></th> 
                @endif
                <th width="20%" align="left">{{ trans('template.common.name') }}</th>
                <th width="20%" align="left">{{ trans('template.common.email') }}</th>
                <th width="13%" align="center">{{ trans('template.common.address') }}</th>
                <!-- <th width="13%" align="center">{{ trans('Primary') }}</th> -->
                <!-- <th width="10%" align="center">{{ trans('template.common.publish') }}</th> -->
                <th width="10%" align="right">{{ trans('template.common.actions') }}</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
          @permission('contact-info-delete')
          <a href="javascript:;"
            class="btn-sm btn red btn-outline right_bottom_btn deleteMass">{{ trans('template.common.delete') }}</a>
          @endpermission
        </div>
      </div>
      @else
      @include('powerpanel.partials.addrecordsection',['type'=>Config::get('Constant.MODULE.TITLE'), 'adUrl' => url('powerpanel/contact-info/add')]) @endif
    </div>
  </div>
</div>
  @php  
    $tableState = true;
    $seg = url()->previous(); 
    $segArr = explode('/', $seg);
    if(!in_array('contact-info', $segArr)){
     $tableState = false;
    }
  @endphp
@include('powerpanel.partials.deletePopup')
@endsection
@section('scripts')
<script type="text/javascript">
window.site_url = '{!! url("/") !!}';
var DELETE_URL = '{!! url("/powerpanel/contact-info/DeleteRecord") !!}';
var tableState = '{{ $tableState }}';
</script>
<script src="{{ url('resources/global/plugins/jquery-cookie-master/src/jquery.cookie.js') }}" type="text/javascript">
</script>
<script src="{{ url('resources/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
  type="text/javascript"></script>
<script src="{{ url('resources/pages/scripts/table-contacts-ajax.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/pages/scripts/custom.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/highslide/highslide-with-html.js') }}" type="text/javascript"></script>
@endsection