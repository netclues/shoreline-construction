@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection
@section('content')
@php $settings = json_decode(Config::get("Constant.MODULE.SETTINGS")); @endphp
@include('powerpanel.partials.breadcrumbs')

<style type="text/css">
	.removePhone, .removeEmail, .removePhone:hover, .removeEmail:hover{ color: #e73d4a; }
</style>

<div class="col-md-12 settings">
	@if(Session::has('message'))
	<div class="row">
	<div class="alert alert-success">
		<button class="close" data-close="alert"></button>
		{{ Session::get('message') }}
	</div>
	</div>
	@endif
	<div class="row">
		<div class="portlet light bordered">
			<div class="portlet-body">
				<div class="tabbable tabbable-tabdrop">
					<div class="tab-content">
						<div class="form_pattern">
							{!! Form::open(['method' => 'post','id'=>'frmContactUS']) !!}
							<div class="form-body">
								<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }} form-md-line-input">
									{!! Form::text('name',isset($contactInfo->varTitle)?$contactInfo->varTitle:old('name'), array('class' => 'form-control input-sm maxlength-handler', 'placeholder'=>'Name', 'maxlength'=>'150','id' => 'name','autocomplete'=>'off')) !!}
									<label class="form_title" for="name">{{ trans('template.common.name') }} <span aria-required="true" class="required"> * </span></label>
									<span class="help-block">
										{{ $errors->first('name') }}
									</span>
								</div>
								
								<div class="multi-email">
									@php $emcnt=0;  @endphp 
									@if(isset($contactInfo->varEmail) && !empty($contactInfo->varEmail))
										@php $selectedEmail=unserialize($contactInfo->varEmail); @endphp
										@foreach($selectedEmail as $email)									
											<div class="form-group emailField {{ $errors->has('email') ? 'has-error' : '' }} form-md-line-input">
												{!! Form::email('email['.($emcnt).']',$email, array('class' => 'form-control input-sm maxlength-handler emailInput','maxlength'=>'100','placeholder'=>'Email','id' => 'email'.($emcnt),'autocomplete'=>'off')) !!}
												<label class="form_title" for="email">{{ trans('template.common.email') }} </label>
												<?php /*
												@if($emcnt==0)
												<a href="javascript:void(0);" class="addMoreEmail add_more" title="Add More"><i class="fa fa-plus"></i> Add More</a>
												@else
												<a href="javascript:void(0);" class="removeEmail add_more" title="Remove"><i class="fa fa-times"></i> Remove</a>
												@endif
												*/ ?>
												<span class="help-block">
													{{ $errors->first('email['.($emcnt).']') }}
												</span>
											</div>
										@php 
											if( $emcnt < count($selectedEmail)-1){ 
												$emcnt++; 
											} 
										@endphp
										@endforeach
									@else
										<div class="form-group emailField {{ $errors->has('email[0]') ? 'has-error' : '' }} form-md-line-input">
											{!! Form::email('email[0]',Request::old('email[0]'), array('class' => 'form-control input-sm emailInput', 'placeholder'=>'Email', 'maxlength'=>'100','autocomplete'=>'off')) !!}
											<label class="form_title" for="email">{{ trans('template.common.email') }} </label>
											<?php /*
											@if($emcnt == 0)
											<a href="javascript:void(0);" class="addMoreEmail add_more" title="Add More"><i class="fa fa-plus"></i> Add More</a>
											@else
											<a href="javascript:void(0);" class="removeEmail add_more" title="Remove"><i class="fa fa-times"></i> Remove</a>
											@endif
											*/ ?>
											<span class="help-block">
												{{ $errors->first('email[0]') }}
											</span>
										</div>
									@endif
								</div>
											
								<div class="multi-phone">
									@php $phcnt=0; @endphp
									@if(isset($contactInfo->varPhoneNo) && !empty($contactInfo->varPhoneNo)) 
											@php $selectedPhone = unserialize($contactInfo->varPhoneNo); @endphp
											@foreach($selectedPhone as $key => $phone)
												<div class="form-group phoneField {{ $errors->has('phone_no[0]') ? 'has-error' : '' }} form-md-line-input">
													{!! Form::text('phone_no['.($phcnt).']',$phone, array('class' => 'form-control input-sm phoneInput','id' => 'phone_no'.($phcnt),'placeholder' => 'Phone No','autocomplete'=>'off','maxlength'=>"20", 'onkeypress'=>"javascript: return KeycheckOnlyPhonenumber(event);",'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
													<label class="form_title" for="phone_no">{{ trans('template.common.phoneno') }} </label>
													<?php /*
													@if($phcnt==0)
													<a href="javascript:void(0);" class="addMorePhone add_more" title="Add More"><i class="fa fa-plus"></i> Add More</a>
													@else
													<a href="javascript:void(0);" class="removePhone add_more" title="Remove"><i class="fa fa-times"></i> Remove</a>
													@endif
													*/ ?>
													<span class="help-block">
														{{ $errors->first('phone_no') }}
													</span>
												</div>
												@php if( $phcnt < count($selectedPhone)-1){ 
																$phcnt++; 
															}  
												@endphp
											@endforeach
									@else
										<div class="form-group phoneField {{ $errors->has('phone_no') ? 'has-error' : '' }} form-md-line-input">
											{!! Form::text('phone_no[0]',Request::old('phone_no[0]'), array('class' => 'form-control input-sm phoneInput','id' => 'phone_no'.($phcnt),'placeholder' => 'Phone No','autocomplete'=>'off','maxlength'=>"20", 'onkeypress'=>"javascript: return KeycheckOnlyPhonenumber(event);",'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
											<label class="form_title" for="phone_no">{{ trans('template.common.phoneno') }} </label>
											<?php /*
											@if($phcnt == 0)
												<a href="javascript:void(0);" class="addMorePhone add_more" title="Add More"><i class="fa fa-plus"></i> Add More</a>
											@else
												<a href="javascript:void(0);" class="removePhone add_more" title="Remove"><i class="fa fa-times"></i> Remove</a>
											@endif
											*/ ?>
											<span class="help-block">
												{{ $errors->first('phone_no') }}
											</span>
										</div>
									@endif
								</div>
							
								<div class="row">
									<div class="col-md-12">
										<input type="hidden" value="{{ isset($contactInfo->varLatitude)?$contactInfo->varLatitude:old('lattitude') }}" name="lattitude" id="latbox"/>
										<input type="hidden" value="{{ isset($contactInfo->varLongitude)?$contactInfo->varLongitude:old('longitude') }}" name="longitude" id="lonbox"/>
										
										<div id="map" style="margin-left: 0px; margin-top: 15px; margin-bottom: 10px; width:100%;height:220px;"></div>
										<p style="float:right;"><strong>{{ trans('template.common.note') }}:</strong> Use Map Pin to get the Address automatically.</p>
									</div>
									<div class="col-md-12">
										<div class="form-group form-md-line-input @if($errors->first('address')) has-error @endif">
											{!! Form::textarea('address',isset($contactInfo->txtAddress)?trim($contactInfo->txtAddress):old('address'), array('class' => 'form-control maxlength-handler','maxlength'=>'400','id'=>'address','placeholder'=>'Address','style'=>'max-height:80px;')) !!}
											<label class="form_title" for="address">{{ trans('template.common.address') }} <span aria-required="true" class="required"> * </span></label>
											<span class="help-block">
												{{ $errors->first('address') }}
											</span>
										</div>
									</div>
								</div>
								
							</div>
							<div class="row" style="display: none;">
								<div class="col-md-12">
										<div class="form-group @if($errors->first('order')) has-error @endif form-md-line-input">
											{!! Form::text('order', isset($contactInfo->intDisplayOrder)?$contactInfo->intDisplayOrder:$total, array('maxlength'=>10,'placeholder' => trans('template.common.order'),'class' => 'form-control','autocomplete'=>'off')) !!}
											<label class="form_title" class="site_name">{{ trans('template.common.displayorder') }} <span aria-required="true" class="required"> * </span></label>
											<span style="color: red;">
												<strong>{{ $errors->first('order') }}</strong>
											</span>
										</div>
								</div>
							</div>

							@include('powerpanel.partials.imageControl',['type' => 'single','label' => trans('template.common.selectimage') ,'data'=> isset($contactInfo)?$contactInfo:null , 'id' => 'contactInfo_image', 'name' => 'img_id', 'settings' => $settings, 'width' => '500', 'height' => '500'])
							
							<h3 class="form-section">{{ trans('template.common.displayinformation') }}</h3>
							<div class="row">
									<?php /*
									<div class="col-md-6">
											<div class="form-group {{ $errors->has('primary') ? ' has-error' : '' }} ">
												<label  class="form_title" for="primary">{{ trans('template.common.primary') }} <span aria-required="true" class="required"> * </span></label>
												<div class="md-radio-inline">
													<div class="md-radio">
														@if ((isset($contactInfo->chrIsPrimary) && $contactInfo->chrIsPrimary == 'Y') || (null == Request::old('primary') || Request::old('primary') == 'Y'))
														@php  $checked_yes = 'checked'  @endphp
														@else
														@php  $checked_yes = ''  @endphp
														@endif
														<input type="radio" {{ $checked_yes }} value="Y" id="radio6" name="primary" class="md-radiobtn">
														<label for="radio6">
															<span class="inc"></span>
															<span class="check"></span>
															<span class="box"></span> Yes
														</label>
													</div>
													<div class="md-radio">
														@if ((isset($contactInfo->chrIsPrimary) && $contactInfo->chrIsPrimary == 'N') || old('primary')=='N')
														@php  $checked_no = 'checked'  @endphp
														@else
														@php  $checked_no = ''  @endphp
														@endif
														<input type="radio" {{ $checked_no }} value="N" id="radio7" name="primary" class="md-radiobtn">
														<label for="radio7">
															<span class="inc"></span>
															<span class="check"></span>
															<span class="box"></span> No
														</label>
													</div>
												</div>
												<span class="help-block">
													{{ $errors->first('primary') }}
												</span>
											</div>
										</div>
										*/ ?>
									<div class="col-md-6">
											@include('powerpanel.partials.displayInfo',['display'=> isset($contactInfo->chrPublish)?$contactInfo->chrPublish:null])
									</div>
							</div>
							
							<div class="form-actions">
								<div class="row">
									<div class="col-md-12">
										<button type="submit" name="saveandedit" class="btn btn-green-drake" value="saveandedit">{!! trans('template.common.saveandedit') !!}</button>
										<button type="submit" name="saveandexit" class="btn btn-green-drake" value="saveandexit">{!! trans('template.common.saveandexit') !!}</button>
										<a class="btn btn-outline red" href="{{ url('powerpanel/contact-info') }}">{{ trans('template.common.cancel') }}</a>
									</div>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
@endsection
@section('scripts')
<script src="{{ url('resources/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBqof4FJWnGe2eCCG8HGXMajO1TiaxkVf4"></script>

<script type="text/javascript">
var latval = "{{ isset($contactInfo->varLatitude)?$contactInfo->varLatitude:'' }}";
var longval = "{{ isset($contactInfo->varLongitude)?$contactInfo->varLongitude:'' }}";
var address = "{{ isset($contactInfo->txtAddress)?trim($contactInfo->txtAddress):'' }}";
if((latval=='' && longval=='') && address!=''){
	var geocoder = new google.maps.Geocoder();	
	geocoder.geocode({
			'address': address
	}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
					latval = results[0].geometry.location.lat();
					longval = results[0].geometry.location.lng();
			}
		});
}
if (latval == '' && longval == '' || address == '') {
		latval = '19.321187240779548';
		longval = '-81.2274169921875';
		var lat = parseFloat(latval);
		var lng = parseFloat(longval);
		var latlng = new google.maps.LatLng(lat, lng);
		var geocoder = geocoder = new google.maps.Geocoder();
		geocoder.geocode({
				'latLng': latlng
		}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
						if (results[1]) {
								$('#address').on('blur',);
						}
				}
		});
}
var markers = [];
var defaultposition;
var mapOptions = {
		zoom: 11,
		streetViewControl: false,
		center: new google.maps.LatLng(latval, longval),
		mapTypeId: google.maps.MapTypeId.ROADMAP
};
var map = new google.maps.Map(document.getElementById('map'), mapOptions);
defaultposition = new google.maps.LatLng(latval, longval);
addMarker(defaultposition);
google.maps.event.addListener(map, 'click', function(event) {
		var lat = parseFloat(event.latLng.lat());
		var lng = parseFloat(event.latLng.lng());
		var latlng = new google.maps.LatLng(lat, lng);
		var geocoder = geocoder = new google.maps.Geocoder();
		geocoder.geocode({
				'latLng': latlng
		}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
						if (results[0]) {
								document.getElementById("address").value = results[0].formatted_address;
								$('#address').on('blur',);
								$('#address-error').hide();
								$('#address-error').parent().parent().remove('.has-error');
						}
				}
		});
		clearMarkers();
		addMarker(event.latLng);
		document.getElementById("latbox").value = event.latLng.lat();
		document.getElementById("lonbox").value = event.latLng.lng();
		$('#latbox').on('blur',);
		$('#lonbox').on('blur',);
});

function addMarker(location) {
		var marker = new google.maps.Marker({
				animation: google.maps.Animation.DROP,
				position: location,
				draggable: true,
				map: map
		});
		markers.push(marker);
}

function clearMarkers() {
		setAllMap(null);
}

function setAllMap(map) {
		for (var i = 0; i < markers.length; i++) {
				markers[i].setMap(map);
		}
}
</script>
<script src="{{ url('resources/pages/scripts/contacts_validations.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/pages/scripts/numbervalidation.js') }}" type="text/javascript"></script>
<script type="text/javascript">window.site_url =  '{!! url("/") !!}';	</script>
<script src="{{ url('resources/pages/scripts/custom.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
	$('.maxlength-handler').maxlength({
				limitReachedClass: "label label-danger",
				alwaysShow: true,
				threshold: 5,
				twoCharLinebreak: false
	});
</script>
@endsection