<!doctype html>
<html>
  <head>
    <title>{{ Config::get('Constant.SITE_NAME') }} Log Details</title>
  </head>
  <body>
      @if(isset($logsLeads) && !empty($logsLeads))
          <div class="row">
           <div class="col-12">
              <table class="search-result allData" id="" border="1">
                 <thead>
                  <tr>
                        <th style="font-weight: bold;text-align:center" colspan="6">{{ Config::get('Constant.SITE_NAME') }} Log Details</th>
                   </tr>
                    <tr>
                       <th style="font-weight: bold;">{{ trans('template.common.name') }}</th>
                       <th style="font-weight: bold;">{{ trans('template.common.email') }}</th>
                       <th style="font-weight: bold;">Action</th>
                       <th style="font-weight: bold;">ModuleName</th>
                       <th style="font-weight: bold;">IP Address</th>
                       <th style="font-weight: bold;">RECEIVED DATE/TIME</th>
                    </tr>
                 </thead>
                 <tbody>
                  @foreach($logsLeads as $row)
                    <tr>
                       <td>{{ $row->user->name }}</td>
                       <td>{{ \App\Helpers\MyLibrary::getDecryptedString($row->user->email) }}</td>
                       <td>{{ $row->varAction }}</td>
                       <td>{{ $row->module->varTitle }}</td>
                       <td>{{ $row->varIpAddress }}</td>
                       <td>{{ date('' . Config::get('Constant.DEFAULT_DATE_FORMAT') . ' ' . Config::get('Constant.DEFAULT_TIME_FORMAT') . '', strtotime($row->created_at)) }}</td>
                    </tr>
                  @endforeach
                 </tbody>
              </table>
           </div>
        </div>
      @endif
  </html>
