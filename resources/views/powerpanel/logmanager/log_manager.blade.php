@extends('powerpanel.layouts.app')
@section('title')
{{Config::get('Constant.SITE_NAME')}} - PowerPanel
@endsection

@section('css')
<link href="{{ url('resources/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('resources/global/plugins/highslide/highslide.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
@include('powerpanel.partials.breadcrumbs')
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
	 <div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet light portlet-fit portlet-datatable bordered">
				@if($total > 0)
     <div class="portlet-title select_box service_select_box">
				<span class="title">{{ trans('template.common.filterby') }}:</span>
		   @if(count($modules)>0)
					<select id="moduleFilter" data-sort placeholder="Select Module" data-order class="bs-select select2">
						<option value=" "> Select Module </option>
						@foreach ($modules as $module)
								<option value="{{ $module->id }}">{{ $module->varTitle }}</option>
						@endforeach
					</select>
				@endif

				<select id="actionFilter" data-sort placeholder="Select Action" data-order class="bs-select select2">
					<option value=" "> Select Action </option>
					<option value="add">Add</option>
					<option value="edit">Edit</option>
					<option value="delete">Delete</option>	
					<option value="publish">Publish</option>	
					<option value="unpublish">Unpublish</option>	
				</select>
				<span class="btn btn-icon-only green-new" type="button" id="refresh" title="Reset">
					<i class="fa fa-refresh" aria-hidden="true"></i>
				</span>

				<div class="col-md-2 pull-right">
          <input type="search" placeholder="Search By User" class="form-control" id="searchfilter">
        </div>

				 <div class="clearfix"></div>
				<div class="portlet-body">
						<div class="table-container">
							 <table class="new_table_desing table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
									<thead>
										 <tr role="row" class="heading">
												<th width="2%" align="center">
													 <input type="checkbox" class="group-checkable">
												</th>
												<th width="20%" align="left">{{ trans('template.common.user') }}</th>
												<th width="10%" align="left">{{ trans('template.common.module') }}</th>
												<th width="10%" align="center">{{ trans('template.logManagerModule.oldValue') }}</th>
												<th width="10%" align="center">{{ trans('template.logManagerModule.newValue') }}</th>
												<th width="10%" align="center">{{ trans('template.common.title') }}</th>
												<th width="10%" align="center">{{ trans('template.common.action') }}</th>
												<th width="10%" align="center">{{ trans('template.common.ipAddress') }}</th>
												<th width="20%" align="center">{{ trans('template.common.dateandtime') }}</th>
										 </tr>
									</thead>
									<tbody> </tbody>
							 </table>
							 @permission('log-delete')
							 <a href="javascript:;" class="btn-sm btn btn-outline red right_bottom_btn deleteMass">{{ trans("template.common.delete") }} 
							 </a>         
							 @endpermission
							 <a href="#selectedRecords" class="btn-sm btn btn-green-drake right_bottom_btn ExportRecord" data-toggle="modal">Export</a>     
						</div>
				  </div>
				 </div>
				@else
					@include('powerpanel.partials.addrecordsection')
				@endif
			</div>
	 </div>
</div>
<div class="new_modal modal fade bs-modal-md" id="confirm" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-vertical">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					Alert!
				</div>
				<div class="modal-body text-center delMsg"></div>
				<div class="modal-footer">
					<button type="button" id="delete" class="btn red btn-outline">{{ trans("template.common.delete") }}</button>
					<button type="button" class="btn btn-green-drake" data-dismiss="modal">{{ trans("template.common.close") }}</button>       
				</div>
			</div>
		</div>
	</div>
</div>
<div class="new_modal modal fade" id="noRecords" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-vertical">	
			<div class="modal-content">
					<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							{{ trans('template.common.alert') }} 
					</div>
					<div class="modal-body text-center">{{ trans('template.contactleadModule.noExport') }} </div>
					<div class="modal-footer">
							<button type="button" class="btn btn-green-drake" data-dismiss="modal">{{ trans('template.common.ok') }}</button>
					</div>
			</div>
		</div>
	</div>
</div>
<div class="new_modal modal fade" id="selectedRecords" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-vertical">	
			<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						{{ trans('template.common.alert') }}
					</div>
					<div class="modal-body text-center">{{ trans('template.contactleadModule.recordsExport') }}</div>
					<div class="modal-footer">
						<div align="center">
							<div class="md-radio-inline">
								<div class="md-radio">
									<input type="radio" value="selected_records" id="selected_records" name="export_type" class="md-radiobtn">
									<label for="selected_records">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span> {{ trans('template.contactleadModule.selectedRecords') }}
									</label>
								</div>
								<div class="md-radio">
									<input type="radio" value="all_records" id="all_records" name="export_type" class="md-radiobtn"  checked="checked">
									<label for="all_records">
										<span class="inc"></span>
										<span class="check"></span>
										<span class="box"></span>{{ trans('template.contactleadModule.allRecords') }} 
									</label>
								</div>
							</div>
						</div>
						<button type="button" class="btn btn-green-drake" id="ExportRecord" data-dismiss="modal">{{ trans('template.common.ok') }} </button>
					</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="new_modal modal fade" id="noSelectedRecords" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-vertical">	
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						{{ trans('template.common.alert') }} 
				</div>
				<div class="modal-body text-center">{{ trans('template.contactleadModule.leastRecord') }} </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-green-drake" data-dismiss="modal">{{ trans('template.common.ok') }} </button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('scripts')
<script type="text/javascript">window.site_url =  '{!! url("/") !!}';</script>
<script src="{{ url('resources/global/plugins/jquery-cookie-master/src/jquery.cookie.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/select2/js/components-select2.min.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/pages/scripts/log-datatables-ajax.js') }}" type="text/javascript"></script>
<!-- <script src="{{ url('resources/pages/scripts/custom.js') }}" type="text/javascript"></script> -->
<script src="{{ url('resources/pages/scripts/logs-functions.js') }}" type="text/javascript"></script>
<script src="{{ url('resources/global/plugins/highslide/highslide-with-html.js') }}" type="text/javascript"></script>
@endsection