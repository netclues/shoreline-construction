@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif



<!-- client_01 S -->
<section class="page_section client_01">
    <div class="container">
        @if(!empty($client) && count($client) > 0)
            @if (isset($PAGE_CONTENT) && !empty($PAGE_CONTENT) || $client->total() > $client->perPage())
                <div class="row">
                    @if (isset($PAGE_CONTENT) && !empty($PAGE_CONTENT))
                        <div class="col-12">
                            <div class="cms mb-xs-30">
                                {!! $PAGE_CONTENT !!}
                            </div>
                        </div>
                    @endif
                    @if($client->total() > $client->perPage())
                        <div class="col-12 mb-xs-30">
                            {{ $client->links() }}
                        </div>
                    @endif
                </div>
            @endif
            
            @if(isset($client) && count($client))
                <div class="row">
                    @foreach($client as  $index => $clientContent)
                        <div class="col-6 col-md-4 gap">
                            <div class="thumbnail-container nq-thumbnail">
                                <div class="thumbnail">
                                    <picture>
                                        <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($clientContent->fkIntImgId,359,143) !!}" srcset="{!! url('assets/images/loader.gif') !!}">
                                        <img src="{!! App\Helpers\resize_image::resize($clientContent->fkIntImgId,359,143) !!}" src="{!! url('assets/images/loader.gif') !!}" alt="{{ $clientContent->varTitle }}" title="{{ $clientContent->varTitle }}">
                                    </picture>
                                </div>
                                <div class="nq-caption">
                                    @if (!empty($clientContent->txtLink))
                                        <a href="{{ $clientContent->txtLink }}" title="{{ $clientContent->varTitle }}" class="btn btn-primary" rel="nofollow" target="_blank"><i class="fa fa-link"></i></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            @if($client->total() > $client->perPage())
                <div class="row mt-xs-30">
                    <div class="col-12">
                        {{ $client->links() }}
                    </div>
                </div>
            @endif
        @else
            <div class="row">
                <div class="col-12">
                    <h2>Coming soon...</h2>
                </div>
            </div>
        @endif
    </div>
</section>
<!-- client_01 E -->
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/client.js') }}"></script>

@endsection

@endsection
@endif