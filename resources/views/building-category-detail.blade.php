@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
      <div class="page_section buildhome_list buildhome_detail" data-aos="fade-up">
         <div class="container">
            @if(isset($buildingcategory) && !empty($buildingcategory))
            <div class="row">
               <div class="col-xl-9">                    
                    <div class="detail-top">
                        <div class="detail-slider owl-carousel owl-theme owl-dots-absolute owl-nav-absolute owl-loaded">
                            @if(isset($buildingcategory->fkIntImgId) && !empty($buildingcategory->fkIntImgId) || count($videoObj) > 0)
                            @if(isset($buildingcategory->fkIntImgId) && !empty($buildingcategory->fkIntImgId))
                                @php $imgArr =  explode(',',$buildingcategory->fkIntImgId) @endphp
                                @if(!empty($imgArr))
                                    @foreach($imgArr as $key => $val)
                                        <div class="item">
                                            <div class="thumbnail-container">
                                                <div class="thumbnail">
                                                    <picture>
                                                        <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($val,848,565) !!}">
                                                        <img src="{!! App\Helpers\resize_image::resize($val,848,565) !!}" alt="{{ htmlspecialchars_decode($buildingcategory->varTitle) }}" title="{{ htmlspecialchars_decode($buildingcategory->varTitle) }}"/>
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumbnail-container">
                                                <div class="thumbnail">
                                                    <picture>
                                                        <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($val,848,565) !!}">
                                                        <img src="{!! App\Helpers\resize_image::resize($val,848,565) !!}" alt="{{ htmlspecialchars_decode($buildingcategory->varTitle) }}" title="{{ htmlspecialchars_decode($buildingcategory->varTitle) }}"/>
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="thumbnail-container">
                                                <div class="thumbnail">
                                                    <picture>
                                                        <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($val,848,565) !!}">
                                                        <img src="{!! App\Helpers\resize_image::resize($val,848,565) !!}" alt="{{ htmlspecialchars_decode($buildingcategory->varTitle) }}" title="{{ htmlspecialchars_decode($buildingcategory->varTitle) }}"/>
                                                    </picture>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            @endif                            
                        @else
                            <div class="item">
                                <div class="thumbnail-container">
                                    <div class="thumbnail">
                                        <picture>
                                            <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($buildingcategory->fkIntImgId) !!}">
                                            <img src="{!! App\Helpers\resize_image::resize($buildingcategory->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($buildingcategory->varTitle) }}" title="{{ htmlspecialchars_decode($buildingcategory->varTitle) }}" />
                                        </picture>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <!--     <div class="item">
                                <div class="thumbnail-container">
                                   <div class="thumbnail">
                                       <img src="assets/images/loader.svg" data-src="assets/images/duplex.png" class="owl-lazy" alt="Double Story" title="Double Story">
                                   </div>
                               </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail-container">
                                   <div class="thumbnail">
                                       <img src="assets/images/loader.svg" data-src="assets/images/Others.png" class="owl-lazy" alt="Double Story" title="Double Story">
                                   </div>
                               </div>
                            </div> -->
                        </div>
                        <div class="detail-desc cms">
                          {!! $buildingcategory->txtDescription !!}
                        </div>
                        @endif
                        <div class="detail-gallery cms">
                          <h2 class="nqtitle">Gallery</h2>
                          <div class="detail-gallery-slider owl-carousel owl-theme owl-dots-absolute owl-nav-absolute owl-loaded">
                              <div class="item">
                                <div class="image_hover">
                                  <div class="thumbnail-container">
                                      <div class="thumbnail">
                                        <a href="assets/images/gallery1.jpg" data-fancybox="gallery" title="Duplex">
                                          <img src="assets/images/loader.svg" data-src="assets/images/gallery1.jpg" class="owl-lazy" alt="Duplex">
                                          <span class="mask">
                                            <i class="fa fa-search"></i>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- <div class="item">
                                <div class="image_hover">
                                  <div class="thumbnail-container">
                                      <div class="thumbnail">
                                        <a href="assets/images/gallery2.jpg" data-fancybox="gallery" title="Duplex">
                                          <img src="assets/images/loader.svg" data-src="assets/images/gallery2.jpg" class="owl-lazy" alt="Duplex">
                                          <span class="mask">
                                            <i class="fa fa-search"></i>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                <div class="image_hover">
                                  <div class="thumbnail-container">
                                      <div class="thumbnail">
                                        <a href="assets/images/gallery3.jpg" data-fancybox="gallery" title="Duplex">
                                          <img src="assets/images/loader.svg" data-src="assets/images/gallery3.jpg" class="owl-lazy" alt="Duplex">
                                          <span class="mask">
                                            <i class="fa fa-search"></i>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                <div class="image_hover">
                                  <div class="thumbnail-container">
                                      <div class="thumbnail">
                                        <a href="assets/images/gallery1.jpg" data-fancybox="gallery" title="Duplex">
                                          <img src="assets/images/loader.svg" data-src="assets/images/gallery1.jpg" class="owl-lazy" alt="Duplex">
                                          <span class="mask">
                                            <i class="fa fa-search"></i>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                <div class="image_hover">
                                  <div class="thumbnail-container">
                                      <div class="thumbnail">
                                        <a href="assets/images/gallery2.jpg" data-fancybox="gallery" title="Duplex">
                                          <img src="assets/images/loader.svg" data-src="assets/images/gallery2.jpg" class="owl-lazy" alt="Duplex">
                                          <span class="mask">
                                            <i class="fa fa-search"></i>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                <div class="image_hover">
                                  <div class="thumbnail-container">
                                      <div class="thumbnail">
                                        <a href="assets/images/gallery3.jpg" data-fancybox="gallery" title="Duplex">
                                          <img src="assets/images/loader.svg" data-src="assets/images/gallery3.jpg" class="owl-lazy" alt="Duplex">
                                          <span class="mask">
                                            <i class="fa fa-search"></i>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                <div class="image_hover">
                                  <div class="thumbnail-container">
                                      <div class="thumbnail">
                                        <a href="assets/images/gallery1.jpg" data-fancybox="gallery" title="Duplex">
                                          <img src="assets/images/loader.svg" data-src="assets/images/gallery1.jpg" class="owl-lazy" alt="Duplex">
                                          <span class="mask">
                                            <i class="fa fa-search"></i>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                <div class="image_hover">
                                  <div class="thumbnail-container">
                                      <div class="thumbnail">
                                        <a href="assets/images/gallery2.jpg" data-fancybox="gallery" title="Duplex">
                                          <img src="assets/images/loader.svg" data-src="assets/images/gallery2.jpg" class="owl-lazy" alt="Duplex">
                                          <span class="mask">
                                            <i class="fa fa-search"></i>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                <div class="image_hover">
                                  <div class="thumbnail-container">
                                      <div class="thumbnail">
                                        <a href="assets/images/gallery3.jpg" data-fancybox="gallery" title="Duplex">
                                          <img src="assets/images/loader.svg" data-src="assets/images/gallery3.jpg" class="owl-lazy" alt="Duplex">
                                          <span class="mask">
                                            <i class="fa fa-search"></i>
                                          </span>
                                        </a>
                                      </div>
                                    </div>
                                 </div>
                              </div> -->
                          </div>
                      </div>
                    </div>
                    <div class="bottm-title cms">
                        <h2 class="nqtitle">Other category</h2>
                    </div>                     
                     
                    <div class="row">
                        <div class="col-12">
                             <div class="d-lg-flex building-home-bottom owl-carousel owl-theme owl-dots-absolute owl-nav-absolute">
                                 <div class="home-list item">
                                     <h2>
                                         <a href="" class="home-title" title="Single Story">Single Story</a>
                                     </h2>
                                     <div class="home-box">
                                         <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                                         <div class="thumbnail-container">
                                             <div class="thumbnail">
                                                 <img data-src="assets/images/single_story.png" class="owl-lazy" alt="Single Story" title="Single Story">
                                             </div>
                                         </div>
                                         <div class="home-list-btn">
                                             <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                             <span>   View All Plans</span> <i class="icon-arrow"></i></a>
                                         </div>
                                     </div>
                                 </div>
                                  
                                 <div class="home-list item">
                                     <h2>
                                         <a href="" class="home-title" title="Duplex">Duplex</a>
                                     </h2>
                                     <div class="home-box">
                                         <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                                         <div class="thumbnail-container">
                                             <div class="thumbnail">
                                                 <img data-src="assets/images/duplex.png" class="owl-lazy" alt="Duplex" title="Duplex">
                                             </div>
                                         </div>
                                         <div class="home-list-btn">
                                             <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                             <span>   View All Plans</span> <i class="icon-arrow"></i></a>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="home-list item">
                                     <h2>
                                         <a href="" class="home-title" title="Container Homes">Container Homes</a>
                                     </h2>
                                     <div class="home-box">
                                         <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                                         <div class="thumbnail-container">
                                             <div class="thumbnail">
                                                 <img data-src="assets/images/container_homes.png" class="owl-lazy" alt="Container Homes" title="Container Homes">
                                             </div>
                                         </div>
                                         <div class="home-list-btn">
                                             <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                             <span>   View All Plans</span> <i class="icon-arrow"></i></a>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="home-list item">
                                     <h2>
                                         <a href="" class="home-title" title="Others">Others</a>
                                     </h2>
                                     <div class="home-box">
                                         <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                                         <div class="thumbnail-container">
                                             <div class="thumbnail">
                                                 <img data-src="assets/images/Others.png" class="owl-lazy" alt="Others" title="Others">
                                             </div>
                                         </div>
                                         <div class="home-list-btn">
                                             <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                             <span>   View All Plans</span> <i class="icon-arrow"></i></a>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="home-list item">
                                     <h2>
                                         <a href="" class="home-title" title="Duplex">Duplex</a>
                                     </h2>
                                     <div class="home-box">
                                         <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                                         <div class="thumbnail-container">
                                             <div class="thumbnail">
                                                 <img data-src="assets/images/duplex.png" class="owl-lazy" alt="Duplex" title="Duplex">
                                             </div>
                                         </div>
                                         <div class="home-list-btn">
                                             <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                             <span>   View All Plans</span> <i class="icon-arrow"></i></a>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                        </div>
                    </div>
               </div>
               <div class="col-xl-3">
                  <div class="service-right right-panel">
                       
                       @if(isset($serviceCategory) && count($serviceCategory) > 0)
                           <article class="detail-category cms">
                               <div class="nqtitle form-title mb-xs-15">Category</div>
                               <ul class="category">                       
                                 @foreach($serviceCategory as $key => $value)
                                    <li>
                                       <a href="{{ url('service-category') }}/{{ $value->alias->varAlias }}" title="{{ $value->varTitle }}">{{ $value->varTitle }}</a>
                                    </li>
                                 @endforeach
                               </ul>
                           </article>
                       @endif

                       <article class="">
                           <div class="nqtitle form-title mb-xs-15">Have Any Question?</div>
                           <form class="nqform nq-form-md">
                               <div class="row align-items-start">
                                   <div class="col-12 text-right">
                                       <div class="required">* Denotes Required Inputs</div>
                                   </div>
                                   <div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
                                       <div class="form-group">
                                           <label class="nq-label" for="name">Name<span class="star">*</span></label>
                                           <input type="text" class="form-control nq-input" id="name" name="name" maxlength="60" onpaste="return false;" ondrop="return false;">
                                       </div>
                                   </div>
                                   <div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
                                       <div class="form-group">
                                           <label class="nq-label" for="email">Email<span class="star">*</span></label>
                                           <input type="email" class="form-control nq-input" id="email" name="email" maxlength="60" onpaste="return false;" ondrop="return false;">
                                       </div>
                                   </div>
                                   <div class="col-xl-12 col-lg-4 col-md-4 col-sm-12">
                                       <div class="form-group">
                                           <label class="nq-label" for="phone">Phone</label>
                                           <input type="text" class="form-control nq-input" id="phone" name="phone" maxlength="60" onpaste="return false;" ondrop="return false;">
                                       </div>
                                   </div>
                                   <div class="col-xl-12 col-lg-12 col-md-12  col-sm-12">
                                       <div class="form-group">
                                           <label class="nq-label" for="comments">Comments</label>
                                           <textarea class="form-control nq-textarea" id="comments" name="comments" rows="3" onpaste="return false;" ondrop="return false;" spellcheck="true"></textarea>
                                       </div>
                                   </div>
                                   <div class="col-xl-12 col-lg-6 col-md-6 col-sm-6">
                                       <div class="form-group captcha">
                                           <img src="{{ url('assets/images/captcha-img.gif') }}">
                                       </div>
                                   </div>
                                   <div class="col-xl-12 col-lg-6 col-md-6  col-sm-6 form-btn">
                                       <div class="form-group">
                                           <button type="submit" class="btn-primary" title="Send Message">Send Message</button>
                                       </div>
                                   </div>
                               </div>
                           </form>
                       </article>
                   </div>
               </div>
            </div>
         </div>
      </div>
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/libraries/fancybox/js/jquery.fancybox.min') }}"></script>
<script src="{{ url('assets/js/buildhome_list.js') }}"></script>
<script src="{{ url('assets/js/buildhome_detail.js') }}"></script>
@endsection

@endsection
@endif
