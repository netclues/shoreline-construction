@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
<div class="process_listing" data-aos="fade-up">
   <div class="container">
      <!-- <div class="row">
         <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-flex align-items-stretch">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title"><span class="number">01</span> Budget</h5>
                     <div class="short_text">
                     <p class="card-text">It’s simple, meet with your bank and get your loan pre-approved for a budget that is best for you.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-flex align-items-stretch">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title"><span class="number">02</span> Home Selection</h5>
                     <div class="short_text">
                     <p class="card-text">Choose a design to suit your land, lifestyle and budget.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-flex align-items-stretch">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title"><span class="number">03</span> Design Acceptance</h5>
                     <div class="short_text">
                     <p class="card-text">Once you are ready to move forward, simply sign and pay the acceptance deposit.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-flex align-items-stretch">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title"><span class="number">04</span> Plan Preparations</h5>
                     <div class="short_text">
                     <p class="card-text">Make arrangements for a site visit. Shoreline will commence soil test. Your house plans are prepared.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-flex align-items-stretch">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title"><span class="number">05</span> Approvals</h5>
                     <div class="short_text">
                     <p class="card-text">We endeavor to create plans that will meet all guidelines, however all plans are subject to certifier approval.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-flex align-items-stretch">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title"><span class="number">06</span> Building Construction</h5>
                     <div class="short_text">
                     <p class="card-text">Building materials are ordered and construction of your new home commences.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-flex align-items-stretch">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title"><span class="number">07</span> Design Acceptance</h5>
                     <div class="short_text">
                     <p class="card-text">During the course of construction you will have the option to choose your finishes based on your selected budget.</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-4 col-sm-6 col-12 d-flex align-items-stretch">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title"><span class="number">08</span> Certificate of Occupancy</h5>
                     <div class="short_text">
                     <p class="card-text">The C/O is given and you can begin to move into your new home.</p>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <div class="row">
         <div class="col-12">
         @if(isset($ourprocessArr) && count($ourprocessArr) > 0)
            <div class="process-detail">                  
            @foreach($ourprocessArr as $ourprocess_info)
               <div class="process-step">
                  <div class="step-number">step<span>0{{ htmlspecialchars_decode($ourprocess_info->intDisplayOrder) }}</span></div>
                  <h5 class="card-title">
                  {{ htmlspecialchars_decode($ourprocess_info->varTitle) }}
                  </h5>
                     <div class="short_text">
                        <p>{!! htmlspecialchars_decode($ourprocess_info->txtDescription) !!}</p>
                  </div>
               </div>
            @endforeach                  
               <!-- <div class="process-step">
                  <div class="step-number">step<span>02</span></div>
                  <h5 class="card-title">
                        Home Selection
                  </h5>
                     <div class="short_text">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                  </div>
               </div>
               <div class="process-step">
                  <div class="step-number">step<span>03</span></div>
                  <h5 class="card-title">
                        Design Acceptance
                  </h5>
                     <div class="short_text">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                  </div>
               </div>
               <div class="process-step">
                  <div class="step-number">step<span>04</span></div>
                  <h5 class="card-title">
                     Plan Preparations
                  </h5>
                     <div class="short_text">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                  </div>
               </div>
               <div class="process-step">
                  <div class="step-number">step<span>05</span></div>
                  <h5 class="card-title">
                        Approvals
                  </h5>
                     <div class="short_text">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                  </div>
               </div>
               <div class="process-step">
                  <div class="step-number">step<span>06</span></div>
                  <h5 class="card-title">
                        Building Construction
                  </h5>
                     <div class="short_text">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                  </div>
               </div>
               <div class="process-step">
                  <div class="step-number">step<span>07</span></div>
                  <h5 class="card-title">
                        Design Acceptance
                  </h5>
                     <div class="short_text">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                  </div>
               </div> -->
               <!-- <div class="process-step">
                  <div class="step-number">step<span>08</span></div>
                  <h5 class="card-title">
                        Certificate of Occupancy
                  </h5>
                     <div class="short_text">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                  </div>
               </div>                      -->                     
            </div>
            @else
            <div class="process-step">                                                
                  <div class="short_text">
                        <h2>No Record Available</h2>
                  </div>
               </div>
            @endif
         </div>
      </div>
   </div>
</div>
@if(!Request::ajax())
@section('footer_scripts')
@endsection
@endsection
@endif
