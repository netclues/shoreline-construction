@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif



<!-- service_category_detail_01 S -->
<section class="page_section service_category_detail_01">
    @if(isset($serviceCategory) && !empty($serviceCategory))
    <div class="scd_content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="nqtitle mb-xs-15">{{ $serviceCategory->varTitle }}</h2>
                    <div class="cms mb-xs-15">
                    	<blockquote>
                    		<p>{{ $serviceCategory->txtShortDescription }}</p>
                    	</blockquote>
                        {!! $serviceCategory->txtDescription !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if(!empty($subCategoryList) && count($subCategoryList)>0)
    <div class="scd_category">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <hr>
                    <h2 class="nqtitle-ip mb-xs-15">Sub Categories</h2>
                    @if($subCategoryList->total() > $subCategoryList->perPage())
                        <div class="mb-xs-30">
                    	   {{ $subCategoryList->links() }}
                        </div>
                    @endif
                </div>
            </div>

            <div class="row">
            	@foreach($subCategoryList as  $index => $category)
            	@php
            		$segmentArr = Request::segments();
            		$segment = '';
            	@endphp
            	@foreach($segmentArr as $key => $value)
            		@php $segment .= $value.'/'; @endphp
            	@endforeach
                <div class="col-sm-6 col-md-4">
                    <article>
                        <div class="thumbnail-container" style="padding-bottom: 66.66%; background-color: rgba(0, 0, 0, 0.08);">
                            <div class="thumbnail">
                            	<picture>
                                    <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}">
                                    <img src="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($category->varTitle) }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">
                                </picture>
                            </div>
                        </div>
                        <h3 class="title"><a href="{{ url('/') }}/{{ rtrim($segment,'/') }}/{{ $category->alias->varAlias }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">{{ htmlspecialchars_decode($category->varTitle) }}</a></h3>
                        <div class="caption">
                            {{ htmlspecialchars_decode($category->txtShortDescription) }}
                        </div>
                        <a href="{{ url('/') }}/{{ rtrim($segment,'/') }}/{{ $category->alias->varAlias }}" title="Read More" class="btn btn-primary btn-block mt-xs-15">Read More</a>
                    </article>
                </div>
                @endforeach
            </div>
            
            @if($subCategoryList->total() > $subCategoryList->perPage())
            <div class="row mt-xs-30">
                <div class="col-12">
                    {{ $subCategoryList->links() }}
                </div>
            </div>
            @endif
        </div>
    </div>
    @endif

    @if(!empty($serviceObj) && count($serviceObj)>0)
    <div class="scd_service">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <hr>
                    <h2 class="nqtitle-ip mb-xs-15">Services</h2>
                    @if($serviceObj->total() > $serviceObj->perPage())
                        <div class="mb-xs-30">
                    	   {{ $serviceObj->links() }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
            	@foreach($serviceObj as  $index => $service)
                <div class="col-sm-6 col-md-4">
                    <article>
                        <div class="thumbnail-container" style="padding-bottom: 66.66%; background-color: rgba(0, 0, 0, 0.08);">
                            <div class="thumbnail">
                                <picture>
                                    <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($service->fkIntImgId) !!}">
                                    <img src="{!! App\Helpers\resize_image::resize($service->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($service->varTitle) }}" title="{{ htmlspecialchars_decode($service->varTitle) }}">
                                </picture>
                            </div>
                        </div>
                        <h3 class="title"><a href="{{ url('services') }}/{{ $service->alias->varAlias }}" title="{{ htmlspecialchars_decode($service->varTitle) }}">{{ htmlspecialchars_decode($service->varTitle) }}</a></h3>
                        <div class="caption">
                            {!! htmlspecialchars_decode($service->txtShortDescription) !!}
                        </div>
                        <a href="{{ url('services') }}/{{ $service->alias->varAlias }}" title="Read More" class="btn btn-primary btn-block mt-xs-15">Read More</a>
                    </article>
                </div>
                @endforeach
            </div>

            @if($serviceObj->total() > $serviceObj->perPage())
            <div class="row mt-xs-30">
                <div class="col-12">
                    {{ $serviceObj->links() }}
                </div>
            </div>
            @endif
        </div>
    </div>
    @endif
</section>
<!-- service_category_detail_01 E -->
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/service-category-detail.js') }}"></script>

@endsection

@endsection
@endif