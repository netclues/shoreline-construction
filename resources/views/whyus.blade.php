@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
      <section class="page_section request_quote" data-aos="fade-up">
         <div class="container">
            <div class="row">
               <div class="col-12">
                     <div class="request-title cms">
                        <h2>Request A Quote</h2>
                     </div>
                     <form method="POST" action="" accept-charset="UTF-8" class="request-form nqform nq-form-md" id="contact_page_form" novalidate="novalidate"><input name="_token" type="hidden" value="4B1mJF91MGTNSzTKPpjfiBDXnMHXmiRQjFJYxVUZ">
                     <div class="row align-items-start">
                              <div class="col-md-12 text-right">
                                  <div class="required" aria-required="true">* Denotes Required Inputs</div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                  <div class="form-group">
                                    <label class="nq-label" for="first_name">First Name<span class="star">*</span></label>
                                      <input id="first_name" class="form-control nq-input" name="first_name" maxlength="60" onpaste="return false;" ondrop="return false;" type="text">
                                                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                  <div class="form-group">
                                      <label class="nq-label" for="first_name">Last Name<span class="star">*</span></label>
                                      <input id="first_name" class="form-control nq-input" name="first_name" maxlength="60" onpaste="return false;" ondrop="return false;" type="text">
                                                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                  <div class="form-group">
                                      <label class="nq-label" for="contact_email">Email<span class="star">*</span></label>
                                      <input id="contact_email" class="form-control nq-input" name="contact_email" maxlength="60" onpaste="return false;" ondrop="return false;" type="email">
                                                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6">
                                  <div class="form-group">
                                      <label class="nq-label" for="contact_email">Area of interest<span class="star">*</span></label>
                                      <input id="contact_email" class="form-control nq-input" name="contact_email" maxlength="60" onpaste="return false;" ondrop="return false;" type="email">
                                                                  </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                  <div class="form-group">
                                    <label class="nq-label" for="phone_number">Phone<span class="star">*</span></label>
                                    <input id="phone_number" class="form-control nq-input" name="phone_number" maxlength="20" onpaste="return false;" ondrop="return false;" onkeypress="javascript: return KeycheckOnlyPhonenumber(event);" type="text">
                                                                </div>
                              </div>                        
                              <div class="col-lg-12 col-md-12 col-sm-12">
                                  <div class="form-group">
                                    <label class="nq-label" for="user_message">Comments</label>
                                    <textarea class="form-control nq-textarea" name="user_message" rows="6" id="user_message" spellcheck="true" onpaste="return false;" ondrop="return false;" cols="50"></textarea>
                                                                </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 text-md-center">
                                  <div class="form-group captcha">
                                       <div id="contact_html_element" class="g-recaptcha"><div style="width: 304px; height: 78px;"><div><iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LcKXMgUAAAAAKzb_F08TaQC8SOMIUZ51s21Eflr&amp;co=aHR0cDovL2xvY2FsaG9zdDo4MA..&amp;hl=en&amp;v=n1ZaVsRK4TYyiKxYab0h8MUD&amp;size=normal&amp;cb=n6yei1jjuvld" width="304" height="78" role="presentation" name="a-7m6ur4nz2nll" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe></div><textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea></div></div>
                                       <div class="capphitcha" data-sitekey="6LcKXMgUAAAAAKzb_F08TaQC8SOMIUZ51s21Eflr">
                                                                     </div>
                                  </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 text-lg-right text-sm-right form-btn text-center">
                                  <div class="form-group">
                                      <button type="submit" class="btn-primary" title="Submit">Submit</button>
                                  </div>
                              </div>
                          </div>
                     </form>
               </div>
            </div>
         </div>
      </section>
@if(!Request::ajax())
@section('footer_scripts')


@endsection

@endsection
@endif
