<article class="panel-form">
	<div class="nqtitle form-title mb-xs-15">Have Any Question?</div>
	{!! Form::open(['method' => 'post','class'=>'request-form nqform nq-form-md', 'id'=>'request_page_form']) !!}
		<div class="row align-items-start">
			<div class="col-12 text-right">
				<div class="required">* Denotes Required Inputs</div>
			</div>
			<div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
				<div class="form-group">					
					<label class="nq-label" for="first_name">First Name<span class="star">*</span></label>					
					{!! Form::text('first_name', old('first_name'), array('id'=>'first_name', 'class'=>'form-control nq-input', 'name'=>'first_name', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
					@if ($errors->has('first_name'))
						<span class="error">{{ $errors->first('first_name') }}</span>
					@endif
				</div>
			</div>
			<div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
				<div class="form-group">					
					<label class="nq-label" for="last_name">Last Name<span class="star">*</span></label>					
					{!! Form::text('last_name', old('last_name'), array('id'=>'last_name', 'class'=>'form-control nq-input', 'name'=>'last_name', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
					@if ($errors->has('last_name'))
						<span class="error">{{ $errors->first('last_name') }}</span>
					@endif
				</div>
			</div>
			
			<div class="col-xl-12 col-lg-4 col-md-4 col-sm-6">
				<div class="form-group">
					<label class="nq-label" for="contact_email">Email<span class="star">*</span></label>
					{!! Form::email('contact_email', old('contact_email'), array('id'=>'contact_email', 'class'=>'form-control nq-input', 'name'=>'contact_email', 'maxlength'=>'60', 'onpaste'=>'return false;', 'ondrop'=>'return false;')) !!}
					@if ($errors->has('contact_email'))
						<span class="error">{{ $errors->first('contact_email') }}</span>
					@endif
				</div>
			</div>
			<div class="col-xl-12 col-lg-4 col-md-4 col-sm-12">
				<div class="form-group">
				<label class="nq-label" for="phone_number">Phone<span class="star">*</span></label>
					{!! Form::text('phone_number', old('phone_number'), array('id'=>'phone_number', 'class'=>'form-control nq-input', 'name'=>'phone_number', 'maxlength'=>"20", 'onpaste'=>'return false;', 'ondrop'=>'return false;', 'onkeypress'=>'javascript: return KeycheckOnlyPhonenumber(event);')) !!}
					@if ($errors->has('phone_number'))
						<span class="error">{{ $errors->first('phone_number') }}</span>
					@endif
				</div>
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12  col-sm-12">
				<div class="form-group">
					<label class="nq-label" for="user_message">Comments</label>
					{!! Form::textarea('user_message', old('user_message'), array('class'=>'form-control nq-textarea', 'name'=>'user_message', 'rows'=>'3', 'id'=>'user_message', 'spellcheck'=>'true', 'onpaste'=>'return false;', 'ondrop'=>'return false;' )) !!}
					@if ($errors->has('user_message'))
						<span class="error">{{ $errors->first('user_message') }}</span>
					@endif
				</div>
			</div>
			<div class="col-xl-12 col-lg-6 col-md-6 col-sm-6">
				<div class="form-group captcha">
					<div id="contact_html_element" class="g-recaptcha"></div>
					<div class="capphitcha" data-sitekey="{{Config::get('Constant.GOOGLE_CAPCHA_KEY')}}">
					@if ($errors->has('g-recaptcha-response'))
						<label class="error help-block">{{ $errors->first('g-recaptcha-response') }}</label>
					@endif
					</div>
				</div>
			</div>
			<div class="col-xl-12 col-lg-6 col-md-6 col-sm-6 service-btn">
				<div class="form-group">
					<button type="submit" class="btn-primary" title="Send Message">Send Message</button>
				</div>
			</div>
		</div>
	{!! Form::close() !!}
</article>
<script type="text/javascript">
  	var sitekey = '{{Config::get("Constant.GOOGLE_CAPCHA_KEY")}}';
  	var onContactloadCallback = function() {
    	grecaptcha.render('contact_html_element', {
      		'sitekey' : sitekey
    	});
  	};

	/* @php
	  	$current_adress = !empty($contact_info->txtAddress)?$contact_info->txtAddress:'';
	  	$pinaddress = explode("*", trim(preg_replace('/\s\s+/', '*', $current_adress)));
	  	$pinaddress = implode('<br/>', $pinaddress);
	@endphp
	var address = "{{ trim(preg_replace('/\s\s+/', ' ',  $current_adress)) }}";
	var pinaddress = "{!! $pinaddress !!}"; */
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onContactloadCallback&render=explicit" async defer></script>
<script src="{{ url('assets/js/request-a-quote.js') }}"></script>