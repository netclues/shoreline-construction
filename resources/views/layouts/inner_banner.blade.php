<!-- inner_banner_01 S -->
<section class="inner_banner_01" data-aos="fade-up">
    <!-- <div id="inner-banner" class="carousel slide" data-ride="carousel" data-interval="4500" data-pause="hover" data-wrap="true">
        Wrapper for slides
        <div class="carousel-inner">
            @if(isset($inner_banner_data) && !empty($inner_banner_data))
                @if(!empty($inner_banner_data->fkIntImgId))
                    @php
                        $imageArr  = array();
                        $imageArr = explode(',',$inner_banner_data->fkIntImgId)
                    @endphp
                    @foreach($imageArr as $key => $value)
                        <div class="carousel-item @if($key == 0) active @endif">
                            <div class="fill" style="background-image: url('{!! App\Helpers\resize_image::resize($value) !!}');"></div>
                        </div>
                    @endforeach
                @endif
            @else
                <div class="carousel-item active">
                    <div class="fill" style="background-image: url('{!! App\Helpers\resize_image::resize(Config::get('Constant.DEFAULT_INNER_BANNER')) !!}');"></div>
                </div>
            @endif
        </div>
        Indicators S
        @if(isset($imageArr) && !empty($imageArr))
        <ol class="carousel-indicators indicators">
            @foreach($imageArr as  $key => $value)
                <li data-target="#inner-banner" data-slide-to="{{$key}}" class="@if($key == 0) active @endif"></li>
            @endforeach
        </ol>
        @endif
    </div> -->
    <div class="caption">
        <div class="nq-table">
            <div class="nq-center">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            @php 
                                $pageid = $cmsPage['id'];
                                //echo '<pre>'; print_r($cmsPage['id']);die;
                            @endphp 
                            
                            <h1 class="title">{{ (!empty($inner_banner_data->varTitle)?$inner_banner_data->varTitle:$currentPageTitle) }}</h1>
                            @if(!empty($inner_banner_data->varSubTitle))
                                <div class="sub-title">{{ $inner_banner_data->varSubTitle }}</div>
                            @endif

                            @if(isset($breadcrumb) && count($breadcrumb)>0)
                                <ul class="nq-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                                    <li><a href="{{url('/')}}" title="Home">Home</a></li>
                                    @php $i = 1; @endphp
                                    @foreach($breadcrumb as $key => $value)
                                        @if(count($breadcrumb) == $i)
                                            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
                                                <a href="{{ url('/') }}/{{ $value['url'] }}" title="{{ $value['title'] }}" itemtype="https://schema.org/Thing" itemprop="item">{{ $value['title'] }}</a>
                                                <meta itemprop="name" content="{{ $value['title'] }}"/>
                                                <meta itemprop="position" content="@php echo $i; @endphp" />
                                            </li>
                                        @else
                                            <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                                <?php /* @if($pageid =='44')
                                                    <a href="{{ url('/') }}/building-category" title="{{ $value['title'] }}" itemtype="https://schema.org/Thing" itemprop="item">{{ $value['title'] }}</a>
                                                @else */ ?>
                                                    <a href="{{ url('/') }}/{{ $value['url'] }}" title="{{ $value['title'] }}" itemtype="https://schema.org/Thing" itemprop="item">{{ $value['title'] }}</a>
                                                <?php /* @endif */ ?>
                                                <meta itemprop="name" content="{{ $value['title'] }}" />
                                                <meta itemprop="position" content="@php echo $i; @endphp" />
                                            </li>
                                        @endif
                                        @php $i++; @endphp
                                    @endforeach
                                </ul>
                            @else
                                <ul class="nq-breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                                      <li><a title="Home" href="{{url('/')}}" class="link">Home</a></li>
                                      <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem" class="active">
                                          <a href="{{ url()->current() }}" title="{{ (!empty($inner_banner_data->varTitle)?$inner_banner_data->varTitle:$currentPageTitle) }}" itemtype="https://schema.org/Thing" itemprop="item">{{ (!empty($inner_banner_data->varTitle)?$inner_banner_data->varTitle:$currentPageTitle) }}</a>
                                          <meta itemprop="name" content="{{ (!empty($inner_banner_data->varTitle)?$inner_banner_data->varTitle:$currentPageTitle) }}"/>
                                          <meta itemprop="position" content="1" />
                                      </li>
                                </ul>
                            @endif

                            @if(isset($breadcrumb) && count($breadcrumb) > 0)
                                <?php /* @if($pageid =='44')
                                    <div class="back-to-list"><a href="{{ url('/') }}/building-category" title="Back to Listing" class="btn-primary"><i class="fa fa-caret-left"></i>Back to Listing</a></div>
                                @else */ ?>
                                    <div class="back-to-list"><a href="{{url($breadcrumb[0]['url'])}}" title="Back to Listing" class="btn-primary"><i class="fa fa-caret-left"></i>Back to Listing</a></div>
                                <?php /* @endif */ ?>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- inner_banner_01 E -->
