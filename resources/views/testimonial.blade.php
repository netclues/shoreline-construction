@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
<!-- testimonials_01 S -->
<section class="page_section testimonials_01" data-aos="fade-up">
    <div class="container">	
    	@if(!empty($testimonialArr) && count($testimonialArr)>0)
    		@if (isset($PAGE_CONTENT) && !empty($PAGE_CONTENT) || $testimonialArr->total() > $testimonialArr->perPage())
		        <div class="row">
		        	@if (isset($PAGE_CONTENT) && !empty($PAGE_CONTENT))
			        	<div class="col-12 mb-xs-30">
			        		{!! $PAGE_CONTENT !!}
			        	</div>
		        	@endif
		        	@if($testimonialArr->total() > $testimonialArr->perPage())
			            <div class="col-12 mb-xs-30">
			                {{ $testimonialArr->links() }}
			            </div>
		            @endif
		        </div>
	        @endif
	        <div class="row">
				@php $index = 0; @endphp
	            @foreach($testimonialArr as $testimonial_info)
				@php $index++; @endphp
	            <div class="col-12">	            	
	            	<article>
						@if(!empty($testimonial_info->fkIntVideoId))							
							 @if(!empty($testimonial_info->video->youtubeId))
							 <div class="testi-img">									
								<a data-fancybox="testimonial {{$index}}" href="https://www.youtube.com/watch?v={{ $testimonial_info->video->youtubeId }}">								
									<div class="thumbnail-container">
										<div class="thumbnail">
											<img src="http://img.youtube.com/vi/{{ $testimonial_info->video->youtubeId }}/mqdefault.jpg" alt="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}" width="391" height="260" title="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}">
										</div>
										<div class="overlay">
											@if(!empty($testimonial_info->fkIntVideoId))
												<i class="fa fa-play-circle" title="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}"></i>
											@else
												<i class="fa fa-plus-circle" title="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}"></i>
											@endif
										</div>
									</div>
								</a>
							</div>
							@endif								
						@else
						 <div class="testi-img">
							<a data-fancybox="testimonial {{$index}}" href="{!! App\Helpers\resize_image::resize($testimonial_info->fkIntImgId,392,261) !!}">
								<div class="thumbnail-container">
									<div class="thumbnail">
										<img src="{!! App\Helpers\resize_image::resize($testimonial_info->fkIntImgId,392,261) !!}" alt="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}" title="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}">
									</div>
									<div class="overlay">
										@if(!empty($testimonial_info->fkIntVideoId))
											<i class="fa fa-play-circle" title="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}"></i>
										@else
											<i class="fa fa-plus-circle" title="{{ htmlspecialchars_decode($testimonial_info->varTitle) }}"></i>
										@endif
									</div>
								</div>
							</a>
						</div> 							
						@endif	            		
						<div class="testi-desc">
							<h4 class="test-title">
								{{ htmlspecialchars_decode($testimonial_info->varTitle) }}								
							</h4>
							<i class="fa fa-quote-left"></i>
							<div class="testi-border">
								 <!-- {!! htmlspecialchars_decode($testimonial_info->txtDescription) !!} -->
								 <div class="t_b_content">
									<p>{!! htmlspecialchars_decode($testimonial_info->txtDescription) !!}</p>
									<span class="left"></span>
									<span class="center"></span>
									<span class="right"></span>
								 </div>
							</div>
							<div class="testi-writer">{{ htmlspecialchars_decode($testimonial_info->varAuthor) }}</div>
						</div>
	            	</article>
	            </div>
	            @endforeach
	        </div>
	        @if($testimonialArr->total() > $testimonialArr->perPage())
		        <div class="row mt-xs-30">
		            <div class="col-12">
		                {{ $testimonialArr->links() }}
		            </div>
		        </div>
	        @endif
        @else
	        <div class="row">
	        	<div class="col-12">
	        		<h2>Coming Soon...</h2>
	        	</div>
	        </div>
        @endif
    </div>
</section>
<!-- testimonials_01 E -->
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/testimonial.js') }}"></script>
@endsection
@endsection
@endif