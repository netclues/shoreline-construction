@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif
    <section class="page_section buildhome_list building-home" data-aos="fade-up">
        <div class="container">            
            <div class="row">            
              @if(isset($buildingcategoryObj) && count($buildingcategoryObj) > 0)
              <div class="col-12 d-sm-flex align-items-center justify-content-center text-center bhomelist-title">                    
                <div class="sub-title cms mr-sm-auto">
					@if($totalCount > 1)
                    <h2 class="nqtitle">{{ $totalCount }} Plans Of {{ $metaInfo['varMetaTitle'] }}</h2>
					@else
					<h2 class="nqtitle">{{ $totalCount }} Plan Of {{ $metaInfo['varMetaTitle'] }}</h2>
					@endif
                </div>                
                    <div class="sorting_sec">
                    <div class="sort_by">
                        <span>Sort by:</span>
                        <select class="selectpicker ac-bootstrap-select">
                            <option value="ASC" @if($ordFilter == "ASC") selected="select" @else '' @endif>A to Z</option>
                            <option value="DESC" @if($ordFilter == "DESC") selected="select" @else '' @endif>Z to A</option>
                            </select>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
              @foreach($buildingcategoryObj as $index => $buildingcat)
               <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12">
                  <div class="bsub-list">
                     <div class="blist-image">
                        <a href="{{ url('/buildinghome/'.$buildingcat->alias->varAlias)}}" title="{{ $buildingcat->varTitle }}">
                            <div class="thumbnail-container">
                                 <div class="thumbnail">
                                     <!-- <img src="assets/images/loader.svg" data-src="assets/images/double_story.png" class="lazy" alt="Double Story" title="Double Story"> -->
                                     <picture>
                                        <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($buildingcat->fkIntImgId) !!}">
                                        <img src="{!! App\Helpers\resize_image::resize($buildingcat->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($buildingcat->varTitle) }}" title="{{ htmlspecialchars_decode($buildingcat->varTitle) }}" class="lazy">
                                    </picture>
                                 </div>
                            </div>
                     </div>
                     <div class="buildhome-box text-center">
                          <h3>
                             <a href="{{ url('/buildinghome/'.$buildingcat->alias->varAlias)}}" class="category-title" title="{{ $buildingcat->varTitle }}">{{ $buildingcat->varTitle }}</a>
                         </h3>
                         <div class="desc">
                             <p>{{ htmlspecialchars_decode($buildingcat->txtShortDescription) }}</p>
                         </div>
                          
                     </div>
                  </div>
               </div>
               @endforeach
               @if($buildingcategoryObj->total() > $buildingcategoryObj->perPage())
                <div class="row mt-xs-30">
                    <div class="col-12 text-center">
                        {{ $buildingcategoryObj->links() }}
                    </div>
                </div>
            @endif
                @else
            <div class="col-12">
                <h2>No records found</h2>
            </div>
            @endif
               <div class="col-12">
                  <div class="bottm-title cms">
                        <h2 class="nqtitle">Other Category</h2>
                  </div>                   
               </div>
               <div class="col-12">
                @if(isset($buildingothercategory) && count($buildingothercategory) > 0)
                
                    <div class="d-lg-flex building-home-bottom owl-carousel owl-theme owl-dots-absolute owl-nav-absolute">
                        @foreach($buildingothercategory as  $index => $buildingOtherCat)
                        <div class="home-list item">
                            <h2>
                                <a href="{{ route('building-category-detail',$buildingOtherCat->alias->varAlias) }}" class="home-title" title="{{ htmlspecialchars_decode($buildingOtherCat->varTitle) }}">{{ htmlspecialchars_decode($buildingOtherCat->varTitle) }}</a>
                            </h2>
                            <div class="home-box">
                                <p>{{ htmlspecialchars_decode($buildingOtherCat->txtShortDescription) }}</p>
                                <a href="{{ route('building-category-detail',$buildingOtherCat->alias->varAlias) }}" title="{{ htmlspecialchars_decode($buildingOtherCat->varTitle) }}">
                                    <div class="thumbnail-container">
                                        <div class="thumbnail">
                                            <!-- <img data-src="assets/images/single_story.png" class="owl-lazy" alt="Single Story" title="Single Story"> -->
                                            <picture>
                                                <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($buildingOtherCat->fkIntImgId) !!}">
                                                <img src="{!! App\Helpers\resize_image::resize($buildingOtherCat->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($buildingOtherCat->varTitle) }}" title="{{ htmlspecialchars_decode($buildingOtherCat->varTitle) }}" class="lazy">
                                            </picture>
                                        </div>
                                    </div>
                                </a>
                                <div class="home-list-btn">
                                    <a href="{{ route('building-category-detail',$buildingOtherCat->alias->varAlias) }}" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                                    <span>	View All Plans</span> <i class="icon-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                         @endforeach
                       
                        @else
                        <div class="col-12">
                            <h2>No records found</h2>
                        </div>
                        
                    </div>
                    @endif
              </div>
            </div>
            
        </div>
    </section>
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/buildhome_list.js') }}"></script>
<script src="{{ url('assets/libraries/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
@endsection

@endsection
@endif
