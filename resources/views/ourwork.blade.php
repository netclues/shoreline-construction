@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif

@if(!empty($ourworksArr) && count($ourworksArr)>0)
<section class="page_section ourworks_list" data-aos="fade-up">
   <div class="container">
      @php 
         $count = 1;         
      @endphp
      @foreach($ourworksArr as $ourwork_info)
      <div class="row">
         <div class="recent-work-desc">
            @php 
               $count++;
               if($count % 2 == 0){ @endphp
               <div class="col-xl-7 col-lg-6 col-md-6 col-12 pl-lg-0 order-sm-2">
               @php } else{ @endphp 
               <div class="col-xl-7 col-lg-6 col-md-6 col-12 pr-lg-0">
               @php } @endphp   
               <div class="our-recent-work-image">
                  <a href="{{ route('our-works-Detail',$ourwork_info->alias->varAlias) }}" title="{{ htmlspecialchars_decode($ourwork_info->varTitle) }}" class="image_hover">
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img data-src="{!! App\Helpers\resize_image::resize($ourwork_info->fkIntImgId,825,550) !!}" class="lazy" src="{!! App\Helpers\resize_image::resize($ourwork_info->fkIntImgId,825,550) !!}" alt="{{ htmlspecialchars_decode($ourwork_info->varTitle) }}">                           
                           <span class="mask"></span>
                        </div>
                     </div>
                  </a>
               </div>
            </div>
            <div class="col-xl-5 col-lg-6 col-md-6 col-12">
               <div class="our-recent-work-detail">                        
                  <h4>
                     <a href="{{ route('our-works-Detail',$ourwork_info->alias->varAlias) }}" class="work-name" title="{{ htmlspecialchars_decode($ourwork_info->varTitle) }}">
                        {{ htmlspecialchars_decode($ourwork_info->varTitle) }}
                     </a>
                  </h4>
                  <span>{{ htmlspecialchars_decode($ourwork_info->txtOwner) }}</span>
                  <div class="work-desc">
                     <p>{{ htmlspecialchars_decode($ourwork_info->txtShortDescription) }}</p>                     
                  </div>
                  <a href="{{ route('our-works-Detail',$ourwork_info->alias->varAlias) }}" title="Learn More" class="btn-tertiary d-flex align-items-center">                  
                     <span>Learn More</span><i class="icon-arrow"></i>
                  </a>
               </div>
            </div>
         </div>       
      </div>
      @endforeach      
      @if($ourworksArr->total() > $ourworksArr->perPage())
            <div class="row mt-xs-30">
                <div class="col-12">
                    {{ $ourworksArr->links() }}
                </div>
            </div>
      @endif
      <!-- <div class="row">
         <div class="col-12">
              <ul class="pagination" role="navigation">
                  <li class="page-item disabled" aria-disabled="true" aria-label="« Previous">
                      <span class="page-link" aria-hidden="true">‹</span>
                  </li>
                  <li class="page-item active" aria-current="page">
                      <span class="page-link">1</span>
                  </li>
                  <li class="page-item">
                      <a class="page-link" href="#">2</a>
                  </li>
                  <li class="page-item">
                      <a class="page-link" href="#" rel="next" aria-label="Next »">›</a>
                  </li>
              </ul>
          </div>
      </div> -->
   </div>
</section>
@else
<section class="page_section ourworks_list" data-aos="fade-up">
    <div class="container">
    	<div class="row">
    		<div class="col-12">
    			{!! $PAGE_CONTENT !!}
    		</div>
    	</div>
    </div>
</section>
@endif
@if(!Request::ajax())
@section('footer_scripts')

@endsection

@endsection
@endif
