@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif



<!-- client_category_detail_01 S -->
    <section class="page_section client_category_detail_01">
        @if(isset($clientCategory) && !empty($clientCategory))
	        <div class="ccd_content">
	            <div class="container">
	                <div class="row">
	                    <div class="col-12">
	                        <h2 class="nqtitle mb-xs-15">{{ $clientCategory->varTitle }}</h2>
	                        <div class="cms mb-xs-15">
	                            <blockquote>
	                                <p>{{ $clientCategory->txtShortDescription }}</p>
	                            </blockquote>
	                            {!! $clientCategory->txtDescription !!}
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    @endif

        @if(!empty($subCategoryList) && count($subCategoryList)>0)
	        <div class="ccd_category">
	            <div class="container">
	                <div class="row">
	                    <div class="col-12">
	                        <hr>
	                        <h2 class="nqtitle-ip mb-xs-15">Sub Categories</h2>
	                        @if($subCategoryList->total() > $subCategoryList->perPage())
		                        <div class="mb-xs-30">
		                            {{ $subCategoryList->links() }}
		                        </div>
	                        @endif
	                    </div>
	                </div>
	                <div class="row">
	                	@foreach($subCategoryList as  $index => $category)
	                		@php
	                			$segmentArr = Request::segments();
	                			$segment = '';
	                		@endphp
	                		@foreach($segmentArr as $key => $value)
	                			@php $segment .= $value.'/'; @endphp
	                		@endforeach
		                    <div class="col-sm-6 col-md-4">
		                        <article>
		                            <div class="thumbnail-container" style="padding-bottom: 66.66%; background-color: rgba(0, 0, 0, 0.08);">
		                                <div class="thumbnail">
		                                	<picture>
		                                	    <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}">
		                                	    <img src="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($category->varTitle) }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">
		                                	</picture>
		                                </div>
		                            </div>
		                            <h3 class="title"><a href="{{ url('/') }}/{{ rtrim($segment,'/') }}/{{ $category->alias->varAlias }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">{{ htmlspecialchars_decode($category->varTitle) }}</a></h3>
		                            <div class="caption">{{ htmlspecialchars_decode($category->txtShortDescription) }}</div>
		                            <a href="{{ url('/') }}/{{ rtrim($segment,'/') }}/{{ $category->alias->varAlias }}" title="Read More" class="btn btn-primary btn-block mt-xs-15">Read More</a>
		                        </article>
		                    </div>
	                    @endforeach
	                </div>

	                @if($subCategoryList->total() > $subCategoryList->perPage())
		                <div class="row mt-xs-30">
		                    <div class="col-12">
		                        {{ $subCategoryList->links() }}
		                    </div>
		                </div>
	                @endif
	            </div>
	        </div>
	   	@endif

        @if(isset($clientObj) && count($clientObj)>0)
		    <div class="ccd_service">
		        <div class="container">
		            <div class="row">
		                <div class="col-12">
		                    <hr>
		                    <h2 class="nqtitle-ip mb-xs-15">Clients</h2>
		                    <div class="mb-xs-30">
		                        {{ $clientObj->links() }}
		                    </div>
		                </div>
		            </div>
		            <div class="row">
		            	@foreach($clientObj as  $index => $clientObjData)
			                <div class="col-sm-6 col-md-4">
			                    <article>
			                        <div class="thumbnail-container" style="padding-bottom: 66.66%; background-color: rgba(0, 0, 0, 0.08);">
			                            <div class="thumbnail">
			                                <picture>
		                                	    <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($clientObjData->fkIntImgId) !!}">
		                                	    <img src="{!! App\Helpers\resize_image::resize($clientObjData->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($clientObjData->varTitle) }}" title="{{ htmlspecialchars_decode($clientObjData->varTitle) }}">
		                                	</picture>
			                            </div>
			                        </div>
			                        <h3 class="title">
				                        @if(!empty($clientObjData->txtLink))
				                         	<a href="{{ $clientObjData->txtLink }}" title="{{ htmlspecialchars_decode($clientObjData->varTitle) }}" target="_blank" rel="nofollow">{{ htmlspecialchars_decode($clientObjData->varTitle) }}</a>
				                        @else 
				                         	{{ htmlspecialchars_decode($clientObjData->varTitle) }}
				                        @endif
			                        </h3>
			                        <div class="caption">{!! htmlspecialchars_decode($clientObjData->txtDescription) !!}</div>
			                        @if(!empty($clientObjData->txtLink))
			                        	<a href="{{ $clientObjData->txtLink }}" title="Read More" target="_blank" rel="nofollow" class="btn btn-primary btn-block mt-xs-15">Read More</a>
			                        @endif 
			                    </article>
			                </div>
		                @endforeach
		            </div>
		            <div class="row mt-xs-30">
		                <div class="col-12">
		                    {{ $clientObj->links() }}
		                </div>
		            </div>
		        </div>
		    </div>
        @endif
    </section>
<!-- client_category_detail_01 E -->
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/client-category-detail.js') }}"></script>

@endsection

@endsection
@endif