@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif

@if(!empty($services) && count($services)>0)
<section class="page_section service_listing" data-aos="fade-up">
    <div class="container">
        <?php /*@if (isset($PAGE_CONTENT) && !empty($PAGE_CONTENT) || $services->total() > $services->perPage())
            <!-- <div class="row">
                @if(!empty($PAGE_CONTENT))
                    <div class="col-12 mb-xs-30">
                        {!! $PAGE_CONTENT !!}
                    </div>
                @endif
                @if($services->total() > $services->perPage())
                    <div class="col-12 mb-xs-30">
                        {{ $services->links() }}
                    </div>
                @endif
            </div> -->
        @endif */ ?>
        @php            
            $count = 0;
        @endphp
        @if(isset($services) && count($services) > 0)
        <div class="row">
            <div class="col-12">
                <div class="tab_overflow">
                    <ul class="nav nav-tabs mb-lg-40" role="tablist">
                    @foreach($services as $index => $doc)
						@php
							$docType = $doc['type'];
							$docList = $doc['services'];
						@endphp
						@if(isset($docType) && count($docType) > 0)
                        <li class="nav-item">
                          <a class="nav-link {{ ($index == 0) ? 'active' : '' }}" title="{{ $docType['varTitle'] }}" data-toggle="tab" href="#{{ $docType['alias']['varAlias'] }}">{{ $docType['varTitle'] }}</a>
                        </li>
                        @endif
					@endforeach                       
                    </ul>
                </div>
            </div>
        </div>
        @endif

        <div class="tab-content">
        @foreach($services as $index => $doc)
            @php
                $docType = $doc['type'];
                $docList = $doc['services']['data'];
            @endphp
            <div id="{{ $docType['alias']['varAlias'] }}" class="tab-pane {{ ($index == 0) ? 'active' : '' }}">
                <div class="row center">
                @if(!empty($docList) && count($docList) > 0)
                    @foreach($docList as  $index => $service_data)
                    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                            <div class="item">
                                <article class="image_hover">
                                    <div class="thumbnail-container">
                                        <a href="{{ url('services',$service_data['alias']['varAlias']) }}" title="{{ htmlspecialchars_decode($service_data['varTitle']) }}" class="thumbnail">
                                            <picture>
                                                <source type="image/webp" srcset="{!! App\Helpers\LoadWebpImage::resize($service_data['fkIntImgId']) !!}">
                                                <img src="{!! App\Helpers\resize_image::resize($service_data['fkIntImgId'],360,240) !!}" alt="{{ htmlspecialchars_decode($service_data['varTitle']) }}">
                                            </picture>
                                            <span class="mask"></span>
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h3 class="name"><a href="{{ url('services',$service_data['alias']['varAlias']) }}" title="{{ htmlspecialchars_decode($service_data['varTitle']) }}">{{ htmlspecialchars_decode($service_data['varTitle'],50) }}</a></h3>
                                    </div>
                                </article>
                            </div> 
                    </div>
                    @endforeach 
                @else
                    <div class="col-lg-3 col-md-4 col-sm-6 mb-4">
                        <div class="item">
                            <article class="image_hover">                                
                                <div class="content">
                                    <h3 class="name">No Service Found</h3>
                                </div>
                            </article>
                        </div> 
                    </div>
                @endif
                </div>
            </div>
        @endforeach
        </div>          
        </div> 
    </div>
</section>
@else
<section class="page_section service_01">
    <div class="container">
    	<div class="row">
    		<div class="col-12">
    			{!! $PAGE_CONTENT !!}
    		</div>
    	</div>
    </div>
</section>
@endif

@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/services.js') }}"></script>
@endsection

@endsection
@endif