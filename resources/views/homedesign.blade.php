@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif

<section class="page_section buildhome_list" data-aos="fade-up">
      <div class="container">
         @if(!empty($buildingcategory) && count($buildingcategory)>0)
         <div class="row">
            @foreach($buildingcategory as  $index => $category)
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2>
                     <a href="{{ route('building-category-detail',$category->alias->varAlias) }}" class="home-title" title="{{ htmlspecialchars_decode($category->varTitle) }}">{{ htmlspecialchars_decode($category->varTitle) }}</a>
                  </h2>
                  <div class="home-box">
                     <p>{{ htmlspecialchars_decode($category->txtShortDescription) }}</p>
                     <a href="{{ route('building-category-detail',$category->alias->varAlias) }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">
                        <div class="thumbnail-container">
                           <div class="thumbnail">
                              <!-- <img src="assets/images/single_story.png" alt="Single Story" title="Single Story"> -->
                              <picture>
                                    <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}">
                                    <img src="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($category->varTitle) }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">
                                </picture>
                           </div>
                        </div>
                     </a>
                     <div class="home-list-btn">
                        <a href="{{ route('building-category-detail',$category->alias->varAlias) }}" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            @endforeach



            <!-- <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2>
                     <a href="" class="home-title" title="Double Story">Double Story</a>
                  </h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/double_story.png" alt="Double Story" title="Double Story">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2>
                     <a href="" class="home-title" title="Duplex">Duplex</a>
                  </h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/duplex.png" alt="Duplex" title="Duplex">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2>
                     <a href="" class="home-title" title="Container Homes">Container Homes</a>
                  </h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/container_homes.png" alt="Container Homes" title="Container Homes">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2>
                     <a href="" class="home-title" title="Others">Others</a>
                  </h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/Others.png" alt="Others" title="Others">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2>
                     <a href="" class="home-title" title="Single Story">Single Story</a>
                  </h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/single_story.png" alt="Single Story" title="Single Story">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2>
                     <a href="" class="home-title" title="Double Story">Double Story</a>
                  </h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/double_story.png" alt="Double Story" title="Double Story">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2>
                     <a href="" class="home-title" title="Duplex">Duplex</a>
                  </h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/duplex.png" alt="Duplex" title="Duplex">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div> -->


            <!-- <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2 class="home-title">Container Homes</h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/container_homes.png" alt="Container Homes" title="Container Homes">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2 class="home-title">Others</h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/Others.png" alt="Others" title="Others">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2 class="home-title">Double Story</h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/double_story.png" alt="Double Story" title="Double Story">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-md-4 col-sm-6">
               <div class="home-list">
                  <h2 class="home-title">Duplex</h2>
                  <div class="home-box">
                     <p>Garage Plans, Apartments Storage Sheds, and More...</p>
                     <div class="thumbnail-container">
                        <div class="thumbnail">
                           <img src="assets/images/duplex.png" alt="Duplex" title="Duplex">
                        </div>
                     </div>
                     <div class="home-list-btn">
                        <a href="" title="View All Plans" class="btn-tertiary d-flex justify-content-center align-items-center">
                        <span>View All Plans</span> <i class="icon-arrow"></i></a>
                     </div>
                  </div>
               </div>
            </div> -->
         </div>

         <!-- <div class="row">
            <div class="col-12">
                    <ul class="pagination" role="navigation">
                        <li class="page-item disabled" aria-disabled="true" aria-label="« Previous">
                            <span class="page-link" aria-hidden="true">‹</span>
                        </li>
                        <li class="page-item active" aria-current="page">
                            <span class="page-link">1</span>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">2</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#" rel="next" aria-label="Next »">›</a>
                        </li>
                    </ul>
                </div>
         </div> -->
         @if($buildingcategory->total() > $buildingcategory->perPage())
              <div class="row mt-xs-30">
                  <div class="col-12 text-center">
                      {{ $buildingcategory->links() }}
                  </div>
              </div>
           @endif
        @else
         <div class="row">
             <div class="col-12">
               <h2>No categories are available...</h2>
             </div>
         </div>
        @endif
      </div>
</section>

@if(!Request::ajax())
@section('footer_scripts')

@endsection
@endsection
@endif
