@extends('layouts.app')
@section('content')
<section class="section notfound_01">
  <div class="nq_container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="notfound_box">
              <div class="msg_404">
                <span class="four">4</span>
                <span class="four">0</span>
                <span class="four">4</span>
              </div>
              <div class="oops_msg">
                <!-- <span class="oops">Oops!</span>  -->The Page you requested was not found!         
              </div>
              <div class="backhome">
                <a href="{{url('/')}}" title="Back To Home" class="btn-primary"><i class="fa fa-home"></i> Back To Home</a>
              </div>
            </div>
        </div>
    </div>
  </div>
</section>
@endsection
@section('footer_scripts')
<script src="{{ url('assets/js/common.js') }}"></script>
@endsection

    