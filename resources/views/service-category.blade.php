@if(!Request::ajax())
@extends('layouts.app')
@section('content')
@include('layouts.inner_banner')
@endif



<!-- service_category_01 S -->
<section class="page_section service_category_01">
    <div class="container">
    	@if(!empty($serviceCategory) && count($serviceCategory)>0)
    		@if (isset($PAGE_CONTENT) && !empty($PAGE_CONTENT) || $serviceCategory->total() > $serviceCategory->perPage())
		        <div class="row">
		            <div class="col-12">
		            	@if (isset($PAGE_CONTENT) && !empty($PAGE_CONTENT))
			                <div class="cms mb-xs-15">
			                    {!! $PAGE_CONTENT !!}
			                </div>
		                @endif
		                @if($serviceCategory->total() > $serviceCategory->perPage())
		                	<div class="mb-xs-30 text-center">		                		
		                		{{ $serviceCategory->links() }}
		                	</div>
		                @endif
		            </div>
		        </div>
	        @endif

	        <div class="row">
	        	@foreach($serviceCategory as  $index => $category)
	            <div class="col-sm-6 col-md-4">
	                <article>
	                    <div class="thumbnail-container" style="padding-bottom: 66.66%; background-color: rgba(0,0,0,0.1);">
	                        <div class="thumbnail">
	                            <picture>
                                    <source type="image/webp" srcset="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}">
                                    <img src="{!! App\Helpers\resize_image::resize($category->fkIntImgId) !!}" alt="{{ htmlspecialchars_decode($category->varTitle) }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">
                                </picture>
	                        </div>
	                    </div>
	                    <h3 class="title"><a href="{{ route('service-category-detail',$category->alias->varAlias) }}" title="{{ htmlspecialchars_decode($category->varTitle) }}">{{ htmlspecialchars_decode($category->varTitle) }}</a></h3>
	                    <div class="caption">
	                        {{ htmlspecialchars_decode($category->txtShortDescription) }}
	                    </div>
	                    <a href="{{ route('service-category-detail',$category->alias->varAlias) }}" title="Read More" class="btn btn-primary btn-block mt-xs-15">Read More</a>
	                </article>
	            </div>
	            @endforeach
	        </div>

	        @if($serviceCategory->total() > $serviceCategory->perPage())
		        <div class="row mt-xs-30">
		            <div class="col-12 text-center">
		                {{ $serviceCategory->links() }}
		            </div>
		        </div>
	        @endif
        @else
        	<div class="row">
        	    <div class="col-12">
        	    	<h2>No categories are available...</h2>
        	    </div>
        	</div>
        @endif
    </div>
</section>
<!-- service_category_01 E -->
@if(!Request::ajax())
@section('footer_scripts')
<script src="{{ url('assets/js/service-category.js') }}"></script>

@endsection

@endsection
@endif